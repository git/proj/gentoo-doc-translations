msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(title):6
msgid "DistCC Cross-compiling Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(mail:link):9
msgid "agaffney@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(mail):9
msgid "Andrew Gaffney"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(author:title):11
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(mail:link):12
msgid "nightmorph@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(mail):12
msgid "Joshua Saddler"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(abstract):15
msgid "This guide shows you how to set up distcc for cross-compiling across different processor architectures."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(version):24
msgid "1.3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(date):25
msgid "2006-02-17"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(title):28
msgid "Cross-compiling with distcc"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(title):30
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):33
msgid "<c>distcc</c> is a tool that lets you share the burden of software compiling across several networked computers. As long as the networked boxes are all using the same toolchain built for the same processor architecture, no special <c>distcc</c> setup is required. But what do you do if you need to compile for a different architecture using differing computers? This guide will show you how to configure <c>distcc</c> to compile for different architectures."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(title):45
msgid "Emerge the needed utilities"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):48
msgid "First, you will need to emerge <c>crossdev</c> on all the machines that will be involved in the compiling process. <c>crossdev</c> is a tool that makes building cross-architecture toolchains easy. It was originally written by <mail link=\"kumba@gentoo.org\">Joshua Kinard</mail> and was re-written from the ground up by <mail link=\"vapier@gentoo.org\">Mike Frysinger</mail>. Its usage is straightforward: <c>crossdev -t sparc</c> will build a full cross-toolchain targetting the Sparc architecture. This includes binutils, gcc, glibc, and linux-headers. If you need more help, try running <c>crossdev --help</c>. Obviously, you will need to emerge the proper cross-toolchain on all the helper boxes."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):61
msgid "Next, you will need to emerge <c>distcc</c> on all the machines that will be involved in the process. This includes the box that will run emerge and the boxes with the cross-compilers. Please see the <uri link=\"/doc/en/distcc.xml\">Gentoo Distcc Documentation</uri> for more information on setting up and using <c>distcc</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(title):72
msgid "Arch-specific notes"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):75
msgid "If you are cross-compiling between different subarchitectures for Intel x86 (e.g. i586 and i686), you must still build a full cross-toolchain for the desired CHOST, or else the compilation will fail. This is because i586 and i686 are actually different CHOSTs, despite the fact that they are both considered \"x86.\" Please keep this in mind when you build your cross-toolchains. For example, if the target box is i586, this means that you must build i586 cross-toolchains on your i686 helper boxes."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(title):88
msgid "Configuring distcc to cross-compile correctly"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):91
msgid "In the default distcc setup, cross-compiling will <e>not</e> work properly. The problem is that many builds just call <c>gcc</c> instead of the full compiler name (e.g. <c>sparc-unknown-linux-gnu-gcc</c>). When this compile gets distributed to a distcc helper box, the native compiler gets called instead of your shiny new cross-compiler."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):99
msgid "Fortunately, there is a workaround for this little problem. All it takes is a wrapper script and a few symlinks on the box that will be running <c>emerge</c>. I'll use my Sparc box as an example. Wherever you see <c>sparc-unknown-linux-gnu</c> below, you will want to insert your own CHOST (<c>x86_64-pc-linux-gnu</c> for an AMD64 box, for example). When you first emerge distcc, the <path>/usr/lib/distcc/bin</path> directory looks like this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(note):108
msgid "The following instructions are to be performed only on the box running the emerge. Do not perform these steps on the helper boxes."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre:caption):113
msgid "Available compilers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre):113
#, no-wrap
msgid "\n# <i>cd /usr/lib/distcc/bin</i>\n# <i>ls -l</i>\ntotal 0\nlrwxrwxrwx  1 root root 15 Dec 23 20:13 c++ -&gt; /usr/bin/distcc\nlrwxrwxrwx  1 root root 15 Dec 23 20:13 cc -&gt; /usr/bin/distcc\nlrwxrwxrwx  1 root root 15 Dec 23 20:13 g++ -&gt; /usr/bin/distcc\nlrwxrwxrwx  1 root root 15 Dec 23 20:13 gcc -&gt; /usr/bin/distcc\nlrwxrwxrwx  1 root root 15 Dec 23 20:13 sparc-unknown-linux-gnu-c++ -&gt; /usr/bin/distcc\nlrwxrwxrwx  1 root root 15 Dec 23 20:13 sparc-unknown-linux-gnu-g++ -&gt; /usr/bin/distcc\nlrwxrwxrwx  1 root root 15 Dec 23 20:13 sparc-unknown-linux-gnu-gcc -&gt; /usr/bin/distcc\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):126
msgid "Here is what you want to do:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre:caption):130
msgid "Modifying distcc"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre):130
#, no-wrap
msgid "\n# <i>rm c++ g++ gcc cc</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):134
msgid "Next, we'll create the new script on this box. Fire up your favorite editor and create a file with the following text in it, then save it as <path>sparc-unknown-linux-gnu-wrapper</path>. Remember to change the CHOST (in this case, <c>sparc-unknown-linux-gnu</c>) to the actual CHOST of the box that will be running the emerge."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre:caption):142
msgid "The new wrapper script"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre):142
#, no-wrap
msgid "\n#!/bin/bash\nexec /usr/lib/distcc/bin/sparc-unknown-linux-gnu-g${0:$[-2]} \"$@\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):147
msgid "Next, we'll make the script executable and create the proper symlinks:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre:caption):151
msgid "Creating the symlinks"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre):151
#, no-wrap
msgid "\n# <i>chmod a+x sparc-unknown-linux-gnu-wrapper</i>\n# <i>ln -s sparc-unknown-linux-gnu-wrapper cc</i>\n# <i>ln -s sparc-unknown-linux-gnu-wrapper gcc</i>\n# <i>ln -s sparc-unknown-linux-gnu-wrapper g++</i>\n# <i>ln -s sparc-unknown-linux-gnu-wrapper c++</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):159
msgid "When you're done, <path>/usr/lib/distcc/bin</path> will look like this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre:caption):163
msgid "A proper set of compilers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(pre):163
#, no-wrap
msgid "\n# <i>ls -l</i>\ntotal 4\nlrwxrwxrwx  1 root root 25 Jan 18 14:20 c++ -&gt; sparc-unknown-linux-gnu-wrapper\nlrwxrwxrwx  1 root root 25 Jan 18 14:20 cc -&gt; sparc-unknown-linux-gnu-wrapper\nlrwxrwxrwx  1 root root 25 Jan 18 14:20 g++ -&gt; sparc-unknown-linux-gnu-wrapper\nlrwxrwxrwx  1 root root 25 Jan 18 14:20 gcc -&gt; sparc-unknown-linux-gnu-wrapper\nlrwxrwxrwx  1 root root 15 Nov 21 10:42 sparc-unknown-linux-gnu-c++ -&gt; /usr/bin/distcc\nlrwxrwxrwx  1 root root 15 Nov 21 10:42 sparc-unknown-linux-gnu-g++ -&gt; /usr/bin/distcc\nlrwxrwxrwx  1 root root 15 Jul 27 10:52 sparc-unknown-linux-gnu-gcc -&gt; /usr/bin/distcc\n-rwxr-xr-x  1 root root 70 Jan 18 14:20 sparc-unknown-linux-gnu-wrapper\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):176
msgid "Congratulations; you now have a (hopefully) working cross-distcc setup."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(title):183
msgid "How this works"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(p):186
msgid "When <c>distcc</c> is called, it checks to see what it was called as (e.g. <c>i686-pc-linux-gnu-gcc</c>, <c>sparc-unknown-linux-gnu-g++</c>, etc.) When distcc then distributes the compile to a helper box, it passes along the name it was called as. The distcc daemon on the other helper box then looks for a binary with that same name. If it sees just <c>gcc</c>, it will look for <c>gcc</c>, which is likely to be the native compiler on the helper box, if it is not the same architecture as the box running <c>emerge</c>. When the <e>full</e> name of the compiler is sent (e.g. <c>sparc-unknown-linux-gnu-gcc</c>), there is no confusion."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//cross-compiling-distcc.xml(None):0
msgid "translator-credits"
msgstr ""

