msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-18 15:18+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):6
msgid "Hardware 3D Acceleration Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(mail:link):9
msgid "dberkholz"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(author:title):11 ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(author:title):14
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(mail:link):12
msgid "peesh"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(mail:link):15
msgid "nightmorph"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(abstract):18
msgid "This document is a guide to getting 3D acceleration working using the DRM with Xorg in Gentoo Linux."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(version):27
msgid "5"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(date):28
msgid "2011-09-11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):31
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):33
msgid "What is hardware 3D acceleration and why do I want it?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):36
msgid "With hardware 3D acceleration, three-dimensional rendering uses the graphics processor on your video card instead of taking up valuable CPU resources drawing 3D images. It's also referred to as \"hardware acceleration\" instead of \"software acceleration\" because without this 3D acceleration your CPU is forced to draw everything itself using the Mesa software rendering libraries, which takes up quite a bit of processing power. While Xorg typically supports 2D hardware acceleration, it often lacks hardware 3D acceleration. Three-dimensional hardware acceleration is valuable in situations requiring rendering of 3D objects such as games, 3D CAD and modeling."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):51
msgid "How do I get hardware 3D acceleration?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):54
msgid "In many cases, both binary and open-source drivers exist. Open source drivers are preferable since we're using Linux and open source is one of its underlying principles. Sometimes, binary drivers are the only option, especially if your graphics card is so new that open source drivers have not yet been written to support its features. Binary drivers include <c>x11-drivers/nvidia-drivers</c> for nVidia cards and <c>x11-drivers/ati-drivers</c> for AMD/ATI cards."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):66
msgid "What is DRI?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):69
msgid "The <uri link=\"http://dri.freedesktop.org/wiki/\">Direct Rendering Infrastructure</uri>, also known as the DRI, is a framework for allowing direct access to graphics hardware in a safe and efficient manner. It includes changes to the X server, to several client libraries and to the kernel. The first major use for the DRI is to create fast OpenGL implementations."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):80
msgid "What is the DRM and how does it relate to regular Xorg?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):83
msgid "The DRM (Direct Rendering Manager) is an <e>enhancement</e> to Xorg that adds 3D acceleration for cards by adding the kernel module necessary for direct rendering."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):92
msgid "Purpose"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):95
msgid "This guide is for people who can't get direct rendering working with just Xorg. The DRM works for the following drivers:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):101
msgid "3dfx"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):102
msgid "intel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):103
msgid "matrox"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):104
msgid "nouveau"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):105
msgid "rage128"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):106
msgid "radeon"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):107
msgid "mach64"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):108
msgid "sis300"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(li):109
msgid "via"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):112
msgid "See the <uri link=\"http://dri.freedesktop.org/\">DRI homepage</uri> for more info and documentation."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):122
msgid "Install Xorg and configure your kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):124
msgid "Install Xorg"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):127
msgid "Please read our <uri link=\"/doc/en/xorg-config.xml\">Xorg Configuration Guide</uri> to get Xorg up and running."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):135
msgid "Configure your kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):138
msgid "Probe for your chipset and enable just that one."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):142
msgid "Checking your AGP chipset"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):142
#, no-wrap
msgid "\n# <i>emerge pciutils; lspci | grep AGP</i>\n# <i>00:01.0 PCI bridge: Intel Corp. 440BX/ZX/DX - 82443BX/ZX/DX AGP bridge (rev 03)</i>\n<comment>(Your output may not match the above due to different hardware.)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):148
msgid "If your chipset is not supported by the kernel you might have some succes by passing <c>agp=try_unsupported</c> as a kernel parameter. This will use Intel's generic routines for AGP support. To add this parameter, edit your bootloader configuration file!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):155
msgid "Most, if not all, kernels should have these options. This was configured using a standard <c>gentoo-sources</c> kernel."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):160
msgid "Configuring the kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):160
#, no-wrap
msgid "\n# <i>ls -l /usr/src/linux </i>\nlrwxrwxrwx 1 root root 22 2007-02-14 20:12 /usr/src/linux -&gt; linux-2.6.18-gentoo-r4\n<comment>(Make sure /usr/src/linux links to your current kernel.)</comment>\n# <i>cd /usr/src/linux</i>\n# <i>make menuconfig</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):168
msgid "make menuconfig options"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):168
#, no-wrap
msgid "\nProcessor type and features ---&gt;\n&lt;*&gt; MTRR (Memory Type Range Register) support\nDevice drivers ---&gt;\n   Graphics support ---&gt;\n   &lt;M&gt; /dev/agpgart (AGP Support) ---&gt;\n      <comment>(The agpgart option is not present on 64-bit kernels; just choose your chipset support.)</comment>\n      &lt;M&gt; Intel 440LX/BX/GX, I8xx and E7x05 support\n      <comment>(Enable your chipset instead of the above.)</comment>\n   &lt;M&gt; Direct Rendering Manager (XFree86 4.1.0 and higher DRI support) ---&gt;\n      &lt;M&gt; <comment>(Select your graphics card from the list)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):184
msgid "Compile and install your kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):187
msgid "Compiling and installing kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):187
#, no-wrap
msgid "\n# <i>make &amp;&amp; make modules_install</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):191
msgid "Don't forget to set up <path>grub.conf</path> or <path>lilo.conf</path> and run <c>/sbin/lilo</c> if you use LILO."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):199
msgid "Add your user to the video group"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):202
msgid "Next, add your user(s) to the video group, as explained in the <uri link=\"/doc/en/handbook\">handbook</uri>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):207
msgid "Adding a user to the video group"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):207
#, no-wrap
msgid "\n# <i>gpasswd -a username video</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):216
msgid "Configure direct rendering"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):218
msgid "Configure Xorg"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):221
msgid "Hopefully just adding your user to the <c>video</c> group is sufficient to enable direct rendering. However, you may also need to create a file in <path>/etc/X11/xorg.conf.d/</path>. You can name it anything you like; just make sure it ends in <path>.conf</path>. Open up your favorite text editor and create a file with this inside it:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):229
msgid "/etc/X11/xorg.conf.d/10-dri.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):229
#, no-wrap
msgid "\nSection \"Device\"\n  Driver \"radeon\"\n  <comment>(Replace radeon with the name of your driver.)</comment>\nEndSection\nSection \"dri\"\n  Mode 0666\nEndSection\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):242
msgid "Changes to /etc/conf.d/modules"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):245
msgid "You will need to add the module name that your card uses to <path>/etc/conf.d/modules</path> to ensure that the module is loaded automatically when the system starts up."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):251
msgid "Editing /etc/conf.d/modules"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):251
#, no-wrap
msgid "\n<comment>(Change module name as required.)</comment>\nmodules=\"<i>intel-agp</i>\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(note):256
msgid "If you compiled <c>agpgart</c> as a module, you will also need to add it to <path>/etc/conf.d/modules</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):266
msgid "Test 3D acceleration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):268
msgid "Reboot to the new kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):271
msgid "Reboot your computer to your new kernel and login as a normal user. It's time to see if you have direct rendering and how good it is. <c>glxinfo</c> and <c>glxgears</c> are part of the <c>mesa-progs</c> package, so make sure it is installed before you attempt to run these commands."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):278
msgid "Testing rendering"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):278
#, no-wrap
msgid "\n$ <i>startx</i>\n<comment>(No need to load modules for your driver or agpgart, even if you compiled them as a module.)</comment>\n<comment>(They will be loaded automatically.)</comment>\n$ <i>glxinfo | grep rendering</i>\ndirect rendering: Yes\n<comment>(If it says \"No\", you don't have 3D acceleration.)</comment>\n$ <i>glxgears</i>\n<comment>(Test your frames per second (FPS) at the default size. The number should be\nsignificantly higher than before configuring DRM. Do this while the CPU is as idle as\npossible.)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(note):291
msgid "FPS may be limited by your screen's refresh rate, so keep this in mind if <c>glxgears</c> reports only about 70-100 FPS. <c>games-fps/ut2004-demo</c> or other 3D games are better benchmarking tools, as they give you real-world performance results."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):301
msgid "Get the most out of direct rendering"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):304
msgid "If you want to set more features, for performance or other reasons, check out the <uri link=\"http://dri.freedesktop.org/wiki/FeatureMatrix\">feature matrix</uri> on the DRI web site or the <uri link=\"http://dri.sourceforge.net/doc/dri_driver_features.phtml\">features listing</uri> on Sourceforge."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):317
msgid "Troubleshooting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):319
msgid "It doesn't work. I don't have rendering, and I can't tell why."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):322
msgid "Try <c>modprobe radeon</c> before you start the X server (replace <c>radeon</c> with the name of your driver). Also, try building agpgart into the kernel instead of as a module."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):331
msgid "When I startx, I get this error: \"[drm] failed to load kernel module agpgart\""
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):334
msgid "That's because you compiled agpgart into the kernel instead of as a module. Ignore it unless you're having problems."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):342
msgid "I have a Radeon, and I want TV-Out."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):345
msgid "The drivers originally developed by the <uri link=\"http://gatos.sf.net\">GATOS</uri> project have been merged into Xorg's codebase. You don't need anything special for TV-Out; <c>x11-drivers/xf86-video-ati</c> will work just fine."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):355
msgid "It doesn't work. My card is so incredibly new and cool that it isn't supported at all."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):361
msgid "Try out the binary drivers. For AMD cards, use <c>ati-drivers</c>; a listing is at <uri>http://support.amd.com/us/gpudownload/Pages/index.aspx</uri>. If those don't support it, use fbdev. It's slow, but it works."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):370
msgid "I have a PCI card and it doesn't work. Help!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):373
msgid "Create a config file in <path>/etc/X11/xorg.conf.d/</path>; name it anything you want as long as it ends in <path>.conf</path>. Add the following to it:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre:caption):378
msgid "/etc/X11/xorg.conf.x/10-pcimode.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(pre):378
#, no-wrap
msgid "\nSection \"Device\"\n  Option \"ForcePCIMode\" \"True\"\nEndSection\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):389
msgid "References"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(uri):394
msgid "http://forums.gentoo.org/viewtopic.php?t=46681"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(uri):395
msgid "http://forums.gentoo.org/viewtopic.php?t=29264"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(uri):396
msgid "http://dri.freedesktop.org/"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(uri):397
msgid "http://www.retinalburn.net/linux/dri_status.html"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(title):403
msgid "Feedback"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(p):406
msgid "With suggestions, questions, etc., e-mail <mail link=\"dberkholz\"/>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//dri-howto.xml(None):0
msgid "translator-credits"
msgstr ""

