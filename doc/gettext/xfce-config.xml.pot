msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-10-28 22:38+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):6
msgid "The Xfce Configuration Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(mail:link):9
msgid "nightmorph"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(abstract):12
msgid "This guide provides an extensive introduction to Xfce, a fast, lightweight, full-featured desktop environment."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(version):21
msgid "6"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(date):22
msgid "2011-09-18"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):25
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):27
msgid "The Xfce desktop environment"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):30
msgid "<uri link=\"http://www.xfce.org\">Xfce</uri> is a fast, lightweight desktop environment for Unix-like operating systems. It is designed for productivity, and is quite configurable while still adhering to the <uri link=\"http://www.freedesktop.org\">Freedesktop</uri> specifications."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):37
msgid "Unlike heavier desktop environments, such as <uri link=\"http://www.gnome.org\">Gnome</uri> and <uri link=\"http://www.kde.org\">KDE</uri>, Xfce uses far fewer system resources. Additionally, it offers greater modularity and fewer dependencies; it takes up less space on your hard disk and takes less time to install."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):45
msgid "This guide will not only show you how to install and configure a minimal Xfce environment, but will also explore options to create a full-featured desktop in keeping with the Xfce philosophy: light, fast, and modular."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):51
msgid "The last part of this guide lists a few commands to run after upgrading to Xfce 4.8, so be sure to follow them if you are upgrading from an older version."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):61 ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):82
msgid "Installing Xfce"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):63
msgid "The basics"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):66
msgid "First, make sure you've configured Xorg as shown in the <uri link=\"/doc/en/xorg-config.xml\">X Server Configuration Howto</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):71
msgid "Next, double-check your USE flags in <path>/etc/make.conf</path>; you'll probably at least want <c>USE=\"-gnome -kde -minimal -qt4 dbus jpeg lock session startup-notification thunar udev X\"</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):77
msgid "Now that you've set your <c>USE</c> variables in <path>/etc/make.conf</path>, it's time to install Xfce."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):82
#, no-wrap
msgid "\n# <i>emerge -avt xfce4-meta</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):86
msgid "Next, add your regular user(s) to the <c>cdrom</c>, <c>cdrw</c>, and <c>usb</c> groups, so that they can mount and use devices such as cameras, optical drives, and USB sticks."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):92
msgid "Adding users to the hardware groups"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):92
#, no-wrap
msgid "\n<comment>(Replace username with your actual user)</comment>\n# <i>for x in cdrom cdrw usb ; do gpasswd -a username $x ; done</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):97
msgid "Next, update your environment variables:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):101
msgid "Updating environment variables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):101
#, no-wrap
msgid "\n# <i>env-update &amp;&amp; source /etc/profile</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):105
msgid "You'll also need a graphical terminal so that you can continue working with your new desktop environment. <c>x11-terms/terminal</c> is a good choice, as it's made specifically for Xfce. Install Terminal as shown:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):111
msgid "Installing Terminal"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):111
#, no-wrap
msgid "\n# <i>emerge x11-terms/terminal</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):120
msgid "Configuring Xfce"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):122 ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):147
msgid "Starting Xfce"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):125
msgid "Now that Xfce is now installed, we'll configure it to be the default desktop environment when we issue the <c>startx</c> command. Exit your root shell and log on as a regular user."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):131
msgid "Setting Xfce as the default desktop environment"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):131
#, no-wrap
msgid "\n$ <i>echo \"exec startxfce4\" &gt; ~/.xinitrc</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(note):135
msgid "If you have ConsoleKit installed, your <path>~/.xinitrc</path> should instead contain <c>exec ck-launch-session startxfce4</c>. Otherwise, some of your applications may stop working. You'll also need to add consolekit to the default runlevel by running the following command as root: <c>rc-update add consolekit default</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):143
msgid "Now start your graphical environment by typing <c>startx</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):147
#, no-wrap
msgid "\n$ <i>startx</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):151
msgid "Congratulations, and welcome to your new Xfce desktop environment. Go ahead, explore it a bit. Then continue reading to learn how you can configure Xfce to suit your needs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):160
msgid "Program access"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):163
msgid "You might notice right-clicking on the desktop shows you the menu of all your applications. It's useful, but your desktop can easily be completely obscured by open windows, making it hard to to launch a new program. So, one of the first things you may wish to do is give yourself a handy application menu on your panel. Right click on this panel, and choose \"Add New Item\". Scroll through the list of choices and select \"Xfce Menu\". You can choose where you want it to be displayed on your panel. When clicked, it displays the application/preferences menu, providing a nicely categorized list of your installed programs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):177
msgid "Sessions &amp; startup"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):180
msgid "If you've installed (or plan to install) popular Gnome or KDE applications such as <c>k3b</c>, <c>nautilus</c>, <c>kmail</c>, <c>evolution</c>, etc. then you should make sure that Xfce launches the appropriate services for these at startup. Navigate to Menu --&gt; Settings --&gt; Sessions &amp; Startup. On the \"Advanced\" tab, select the appropriate checkbox. This might slightly increase Xfce startup times, but it decreases load times for KDE and Gnome applications."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):189
msgid "Xfce has the ability to save your session settings and running programs from the \"General\" tab in the Sessions &amp; Startup menu. They can be automatically saved when you logout, or Xfce can ask you each time. This feature is particularly useful for undoing configuration mistakes. Accidentally killed a panel? Just select \"No\" when prompted to save your current session, and the next time you start Xfce, your old desktop is restored. Want to automatically launch your open webbrowser, terminal, and email client the next time you login? Just save your session before logging out."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):200
msgid "You've now got a basic working environment installed and configured. But if you're interested in doing more, then continue reading!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):210
msgid "Additional Applications"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):212
msgid "Panel plugins"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):215
msgid "In this chapter, we'll discuss some useful plugins and applications for everyday use within Xfce."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):220
msgid "There are many plugins for the panel available in Portage; see for yourself with <c>emerge --search xfce</c>. Though for the most part their names are self-explanatory, a few deserve extra attention, as they are quite helpful. To use them, simply <c>emerge</c> them. They'll be added to the list of available items in the \"Add New Items\" menu shown when you right-click on the panel."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):229
msgid "<c>xfce4-battery-plugin</c> is perfect for laptop users. It displays battery percentage, time remaining, power source (AC or battery), fan status, warnings, and can even be configured to execute commands at certain power levels. This feature can be used to put the laptop into hibernate mode when the battery is almost exhausted."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):236
msgid "<c>xfce4-verve-plugin</c> is a small command line embedded into the panel. It's quicker than opening up another terminal when you want to run a command."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):241
msgid "<c>xfce4-mount-plugin</c> gives you a handy method of mounting devices listed in <path>/etc/fstab</path> just by clicking your mouse"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):245
msgid "<c>xfce4-sensors-plugin</c> lets you monitor your hardware sensors, such as CPU temperature, fan RPM, hard drive temp, motherboard voltage, and more"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):251
msgid "If you can't find what you're looking for in the plugins specifically made for Xfce, try searching through the list of Gnome panel applets! That's right, by first emerging <c>xfce4-xfapplet-plugin</c>, you can install and run any applet made for Gnome."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):261
msgid "Useful programs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):264
msgid "We should now <c>emerge</c> some useful applications and utilities: <c>xfce4-mixer</c>, <c>xfce4-taskmanager</c>, <c>xfwm4-themes</c>, <c>orage</c>, <c>leafpad</c>, <c>xfce4-power-manager</c>, <c>x11-terms/terminal</c>, and <c>thunar</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):271
msgid "<c>xfce4-mixer</c> is a volume control for your sound card. It can also be run as a panel applet, giving you fast access to playback volume. <c>xfce4-taskmanager</c> displays a list of all running programs, and the CPU and memory consumption each one takes up. By right-clicking an item, you can kill a misbehaving application, pause and restart it, or even alter its runtime priority, which lets you fine-tune how much of a demand it puts on your system's resources."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):281
msgid "<c>xfwm4-themes</c> adds several window manager themes. You may want to add a more full-coverage icon theme such as <c>tango-icon-theme</c> just to round out your desktop."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):287
msgid "<c>orage</c> is a simple, handy calendar. <c>leafpad</c> is a barebones text editor that starts up extremely quickly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):292
msgid "<c>xfce4-power-manager</c> is an application to monitor and manage power usage. This is especially important for laptops! The power manager allows you to adjust screen brightness, choose maximum performance or battery-saving modes, and setup hibernate, suspend, and shutdown actions when the lid is shut or buttons are pressed. You can set <uri link=\"http://goodies.xfce.org/projects/applications/xfce4-power-manager\">xfce4-power-manager</uri> to warn you when your battery reaches certain levels, or even turn off your machine. The application comes with a couple of helpful panel plugins to display battery/charging status, and a brightness control."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):304
msgid "<c>x11-terms/terminal</c> is an X11 terminal emulator, far more configurable and useful than the barebones <c>xterm</c>. <c>terminal</c> supports Unicode text, color schemes, pseudo-transparency and hardware-accelerated transparency via Xfce's built-in compositor, all out-of-the-box. Just make sure that the default action on the terminal launcher of your panel runs <path>/usr/bin/Terminal</path> instead of <path>xterm</path>. Right-click the launcher and choose \"Properties\" to change the command."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):314
msgid "<c>thunar</c> is Xfce's default graphical file manager. It's fast yet quite powerful, can support several plugins for even more functionality; just install them with <c>emerge</c>. Let's take a look:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):321
msgid "<c>thunar-archive-plugin</c> lets you create and extract archive files using the right-click menu. It provides a handy <uri link=\"http://www.foo-projects.org/~benny/projects/thunar-archive-plugin\">front-end</uri> for graphical archiving applications such as <c>xarchiver</c>, <c>squeeze</c>, and <c>file-roller</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):328
msgid "<c>tumbler</c> lets you preview certain types of files from within Thunar, such as images and fonts."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):332
msgid "<c>thunar-volman</c> automatically <uri link=\"http://foo-projects.org/~benny/projects/thunar-volman/\">manages</uri> removable media and drives."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):339
msgid "Next, let's see about adding some useful but lightweight desktop applications, in keeping with Xfce's philosophy."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):344
msgid "Though <c>leafpad</c> is nice enough as a basic text editor, if you need a full-featured word processor but don't want the bloat of OpenOffice, try emerging <c>abiword</c>. <uri link=\"http://www.abisource.com\">AbiWord</uri> is lighter, faster, and is completely interoperable with industry-standard document types."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):352
msgid "Need a nice email client/newsreader that isn't as demanding as <c>thunderbird</c> or <c>evolution</c>? Try emerging <c>claws-mail</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):357
msgid "For your internet chat needs, <c>irssi</c> is an excellent, tiny, incredibly configurable IRC client that runs in your terminal. If you prefer a compact all-in-one client that handles nearly all chat protocols, you may want to <c>emerge pidgin</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):364
msgid "If you need movie and music players, look no further than <c>mplayer</c> and <uri link=\"/proj/en/desktop/sound/decibel.xml\">decibel-audio-player</uri>. They can play most every media format available quite nicely."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):370
msgid "Finally, you'll need a webbrowser. Nearly all graphical webbrowsers require more resources than most of your other desktop applications. Still, <c>firefox</c> and <c>midori</c> are always good choices. Alternatively, you may find <c>opera</c> to be quite fast. However, <c>opera</c> is not available on as many processor architectures as <c>firefox</c>, and it has more dependencies unless you override them with a few USE flags."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):379
msgid "Adding a webbrowser"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):379
#, no-wrap
msgid "\n<comment>(Installing Mozilla Firefox)</comment>\n# <i>emerge firefox</i>\n<comment>(Installing Midori)</comment>\n# <i>emerge midori</i>\n<comment>(Installing Opera)</comment>\n# <i>echo \"www-client/opera gtk -kde\" &gt;&gt; /etc/portage/package.use</i>\n# <i>emerge opera</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):389
msgid "Now that we've explored some good suggestions for rounding out your desktop applications, let's see what else we can do to enhance your Xfce experience."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):397
msgid "Graphical login"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):400
msgid "Remember when we added <c>startxfce4</c> to our <path>~/.xinitrc</path>? All you have to do to get into your desktop is type <c>startx</c> after logging in. This is fine if you prefer a completely text-based boot and login, but let's use a display manager that will automatically start Xfce after booting (so that you can login graphically)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):408
msgid "First, let's make sure Xfce loads at boot:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):412
msgid "Adding xdm to the default runlevel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):412
#, no-wrap
msgid "\n# <i>rc-update add xdm default</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):416
msgid "We aren't quite finished yet. We have to pick a display manager and set the appropriate variable. Though there are a few choices available in Portage, for this guide, we'll stick with <uri link=\"http://slim.berlios.de\">SLiM</uri>, the Simple Login Manager."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):423
msgid "<c>slim</c> is speedy and lightweight, with minimal dependencies. Perfect for Xfce!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):428
msgid "Installing SLiM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):428
#, no-wrap
msgid "\n# <i>emerge -avt slim</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(note):432
msgid "The <c>branding</c> USE flag will pull in the <c>slim-themes</c> package, which will give you an assortment of login themes, including a Gentoo Linux theme."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):437
msgid "Then edit the DISPLAYMANAGER variable in <path>/etc/conf.d/xdm</path>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):441
msgid "Editing /etc/conf.d/xdm"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):441
#, no-wrap
msgid "\nDISPLAYMANAGER=\"slim\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):445
msgid "SLiM can automatically start your Xfce session if you add <c>XSESSION=\"Xfce4\"</c> to <path>/etc/env.d/90xsession</path>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):450
msgid "Setting XSESSION"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):450
#, no-wrap
msgid "\n# <i>echo XSESSION=\\\"Xfce4\\\" &gt; /etc/env.d/90xsession</i>\n# <i>env-update &amp;&amp; source /etc/profile</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):458
msgid "Beautifying your desktop"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):461
msgid "A little customization of your desktop's appearance can go a long way. Xfce has all the options you'd expect from a modern desktop environment, font antialiasing settings, color schemes, dozens of window decorations, themes, and more. If these aren't enough, it's easy to install third-party themes, icon sets, mouse cursor themes, and wallpapers."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):469
msgid "A selection of nice Gentoo wallpapers in a variety of resolutions are hosted on the <uri link=\"/main/en/graphics.xml\">Gentoo website</uri>. If you're looking for icon sets and complete Xfce themes, <uri link=\"http://www.xfce-look.org/\">Xfce-Look</uri> has a huge collection. The important thing to remember about any third-party eyecandy you download is that it will usually first need to be unpacked and then installed to the proper directory. Icon sets go in <path>/usr/share/icons/</path>, and themes go to <path>/usr/share/themes/</path>; use these directories when you want all users to be able to access themes and icon sets. Individual users can install themes and icon sets to <path>~/.themes/</path> and <path>~/.icons/</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):482
msgid "If you installed SLiM as your display manager, there are lots of themes in the <c>slim-themes</c> package available in Portage. Also, be sure to check the SLiM <uri link=\"http://slim.berlios.de/themes01.php\">themes page</uri> for more themes. Creating your own SLiM theme is fairly easy; just read the <uri link=\"http://slim.berlios.de/themes_howto.php\">Themes HowTo</uri>. Gentoo also ships a <c>slim-themes</c> package that you can <c>emerge</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):491
msgid "Finally, Xfce has its own built-in compositor to manage window transparency. This option can be found in Menu --&gt; Settings --&gt; Window Manager. For best performance, you will need to be running a graphics card with drivers that support hardware-accelerated rendering. Make sure you emerged <c>xfwm4</c> with the <c>xcomposite</c> USE flag. Next, you will need to enable compositing in <path>/etc/X11/xorg.conf</path> by adding the following section:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):500
msgid "Enabling composite in xorg.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):500
#, no-wrap
msgid "\nSection \"Extensions\"\n    Option  \"Composite\"  \"Enable\"\nEndSection\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):506
msgid "This is the bare minimum configuration required for Xfce and Xorg-X11. However, setting up hardware-accelerated rendering depends on your individual graphics card, and is beyond the scope of this guide. Please see the other guides in the <uri link=\"/doc/en/index.xml?catid=desktop\">Desktop Documentation Resources</uri> list to learn about configuring hardware-accelerated rendering for your graphics card."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):515
msgid "Once you've finished setting up a beautiful Xfce desktop, the next thing to do is take a picture of it to share with other folks! Just install <c>xfce4-screenshooter</c> and post your pictures somewhere for all to admire."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):526
msgid "Summary"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):530
msgid "Congratulations on making it this far! You've installed and configured a speedy desktop environment with a solid suite of applications for your computing needs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):539
msgid "Upgrading Xfce"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):542
msgid "If you're upgrading Xfce from an old version to 4.8 or newer, then you will need to remove your old cached sessions. For each of your users, run the following commands to remove your old incompatible cached sessions:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre:caption):548
msgid "Deleting old sessions from the cache"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(pre):548
#, no-wrap
msgid "\n$ <i>rm -r ~/.cache/sessions</i>\n$ <i>rm -r ~/.config/xfce*</i>\n$ <i>rm -r ~/.config/Thunar</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(title):557
msgid "Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(p):560
msgid "Need additional help on configuring and using Xfce? Need more lightweight application suggestions? Try checking out:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(uri:link):566
msgid "http://forums.gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(uri):566
msgid "The Gentoo forums"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):567
msgid "#xfce on irc.freenode.net"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(li):568
msgid "The installed help files and other documentation provided by Xfce: <path>/usr/share/xfce4/doc/C/index.html</path>. Just point your browser at it and start reading. There are even a lot of \"hidden\" configuration options detailed in the help files."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(uri:link):574
msgid "http://www.xfce.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(uri):574
msgid "Xfce's home page"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//xfce-config.xml(None):0
msgid "translator-credits"
msgstr ""

