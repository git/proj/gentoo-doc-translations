msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):6
msgid "Gentoo Linux Kernel Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):9
msgid "swift@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):9
msgid "Sven Vermeulen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):11
msgid "Contributor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):12
msgid "lostlogic@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):12
msgid "Brandon Low"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):14 ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):17 ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):20 ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):23 ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):26 ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):29 ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(author:title):32
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):15
msgid "dsd@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):15
msgid "Daniel Drake"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):18
msgid "carl@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):18
msgid "Carl Anderson"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):21
msgid "peesh@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):21
msgid "Jorge Paulo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):24
msgid "bennyc@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):24
msgid "Benny Chuang"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):27
msgid "greg_g@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):27
msgid "Gregorio Guidi"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):30
msgid "fox2mike@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):30
msgid "Shyam Mani"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail:link):33
msgid "nightmorph@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(mail):33
msgid "Joshua Saddler"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(abstract):36
msgid "This document gives you an overview on all kernel sources that Gentoo provides through Portage."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(version):45
msgid "3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(date):46
msgid "2010-09-05"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):49
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):53
msgid "As with everything else in Gentoo Linux, the philosophy of the Gentoo Kernel team is to give you, the user, as much freedom of choice as possible. If you take a look at the output of <c>emerge -s sources</c> you see a large variety of kernels to choose from. In this document, I will attempt to give you a brief rundown of the goals of each of the patch sets, which we at Gentoo design, and also explain the other kernel sources we make available to you."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):63
msgid "Linux 2.4 is maintained by Willy Tarreau. Linus Torvalds, the original creator of Linux, handed maintainership of the Linux 2.4 branch over to Marcelo Tosatti when Linus went off to start developing the newer 2.6 kernel tree. Marcelo did a fine job of keeping 2.4 stable and secure, and has since handed over maintainership to Willy. Note that only security and bug fixes are accepted into the 2.4 kernel tree. Actual development happens in the Linux 2.6 kernel tree."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):72
msgid "Linux 2.6 is maintained by Andrew Morton, who works closely with Linus Torvalds to deliver a fast, powerful, and feature-packed Linux kernel. Development is happening at incredible pace and this kernel tree is now very mature."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):79
msgid "Some of the more uncommon system architectures are not fully compatible with Linux 2.6, and some users prefer the tried-and-tested Linux 2.4 kernel. However, please note that Linux 2.4 is currently not being developed further - only bug and security fixes are being included in the newer releases. If you are able to, we suggest that you upgrade to Linux 2.6. You may find the <uri link=\"/doc/en/migration-to-2.6.xml\">migration document</uri> useful."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):93
msgid "Supported kernel packages"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):95
msgid "genkernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):98
msgid "<c>Genkernel</c> is a kernel toolset that can be used to autodetect your hardware and configure your kernel automatically. This is usually recommended for users who do not feel comfortable about compiling a kernel manually."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):104
msgid "For more information, please read the <uri link=\"/doc/en/genkernel.xml\">Gentoo Linux Genkernel Guide</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):112
msgid "General purpose: gentoo-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):115
msgid "For most users, we recommend the <c>gentoo-sources</c> kernel. <c>gentoo-sources</c> is a kernel based on Linux 2.6, lightly patched to fix security problems, kernel bugs, and to increase compatibility with the more uncommon system architectures."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):122
msgid "The <c>gentoo-sources</c> package absorbs most of the resources of the Gentoo kernel team. They are brought to you by a group of talented developers, which can count on the expertise of popular kernel hacker Greg Kroah-Hartman, maintainer of udev and responsible for the USB and PCI subsystems of the official Linux kernel."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):133
msgid "For servers: hardened-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):136
msgid "<c>hardened-sources</c> is based on the official Linux kernel and is targeted at our users running Gentoo on server systems. It provides patches for the various subprojects of Gentoo Hardened (such as support for <uri link=\"http://www.nsa.gov/selinux/\">LSM/SELinux</uri> and <uri link=\"http://grsecurity.net\">grsecurity</uri>), together with stability/security-enhancements. Check <uri>http://www.gentoo.org/proj/en/hardened/</uri> for more information."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(impo):146
msgid "This kernel provides powerful patches for enhanced security. Please read the <uri link=\"/proj/en/hardened/\">documentation</uri> before you use it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):154
msgid "ck-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):157
msgid "<c>ck-sources</c> is Con Kolivas's kernel patch set. This patchset is primarily designed to improve system responsiveness and interactivity and is configurable for varying workloads (from servers to desktops). The patchset includes a different scheduler, BFS, designed to keep systems responsive and smooth even when under heavy load. Support and information is available at <uri>http://kernel.kolivas.org</uri> and in <c>#ck</c> on <c>irc.oftc.net</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):169
msgid "git-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):172
msgid "The <c>git-sources</c> package tracks daily snapshots of the upstream development kernel tree. You should run these kernels if you are interested in kernel development or testing. Bugreports should go to the <uri link=\"http://bugzilla.kernel.org/\">Linux Kernel Bug Tracker</uri> or LKML (Linux Kernel Mailing List)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):183
msgid "xen-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):186
msgid "<c>xen-sources</c> lets you run multiple operating systems on a single physical system. You can create virtual environments in which one or more guest operating systems run on a <uri link=\"http://www.xensource.com\">Xen</uri>-powered host operating system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):193
msgid "For more information on Xen and Gentoo, read the <uri link=\"/doc/en/xen-guide.xml\">Gentoo Xen Guide</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):201
msgid "Architecture dependent kernels"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):204
msgid "<c>cell-sources</c>, <c>mips-sources</c>, <c>sparc-sources</c>, and <c>xbox-sources</c> are, as their names suggest, patched to run best on specific architectures. They also contain some of the patches for hardware and features support from the other patch sets mentioned above and below."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):216
msgid "Unsupported kernel packages"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):220
msgid "Now I'm going to try to briefly describe some of the other <path>sys-kernel/*-sources</path> which you saw scroll by when you ran <c>emerge -s sources</c>. Let's take them in alphabetical order. These kernels are provided as a courtesy only and the various patch sets are not supported by the Gentoo kernel team."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):232
msgid "mm-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):235
msgid "The <c>mm-sources</c> are based on the <c>vanilla-sources</c> and contain Andrew Morton's patch set. They include the experimental and bleeding-edge features that are going to be included in the official kernel (or that are going to be rejected because they set your box on fire). They are known to be always moving at a fast pace and can change radically from one week to the other; kernel hackers use them as a testing ground for new stuff."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):244
msgid "If you really want to live on the edge and you think <c>vanilla-sources</c> are for wussies, then try out <c>mm-sources</c>. Be warned that this kernel is highly experimental and doesn't always work as expected."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):254
msgid "pf-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):257
msgid "The <c>pf-sources</c> kernel brings together parts of several different kernel patches. It includes the BFS patchset from <c>ck-sources</c>, the <c>tuxonice</c> patches, <uri link=\"http://www.linuximq.net\">LinuxIMQ</uri>, and the <uri link=\"http://algo.ing.unimo.it/people/paolo/disk_sched/patches/\">BFQ</uri> I/O <uri link=\"http://kerneltrap.org/Linux/Budget_Fair_Queuing_IO_Scheduler\">scheduler</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):270
msgid "openvz-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):273
msgid "OpenVZ is a server virtualization solution built on Linux. OpenVZ creates isolated, secure virtual private servers (VPSs) or virtual environments on a single physical server enabling better server utilization and ensuring that applications do not conflict. For more information, see <uri>http://www.openvz.org</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):284
msgid "tuxonice-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):287
msgid "The <c>tuxonice-sources</c> (formerly <c>suspend2-sources</c>) are patched with both genpatches which includes the patches found in gentoo-sources, and <uri link=\"http://www.tuxonice.net\">TuxOnIce</uri> which is an improved implementation of suspend-to-disk for the Linux kernel, formerly known as <e>suspend2</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):295
msgid "This kernel is recommended for laptop users who often rely on being able to suspend their laptop and resume work elsewhere."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):303
msgid "usermode-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):306
msgid "<c>usermode-sources</c> are the User Mode Linux kernel patches. This kernel is designed to allow Linux to run within Linux to run within Linux to ... User Mode Linux is intended for testing and virtual server support. For more information about this amazing tribute to the stability and scalability of Linux, see <uri>http://user-mode-linux.sourceforge.net</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):314
msgid "For more information on UML and Gentoo, read the <uri link=\"/doc/en/uml.xml\">Gentoo UML Guide</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):322
msgid "vanilla-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):325
msgid "The next kernel sources that many of you will probably be familiar with as Linux users are the <c>vanilla-sources</c>. These are the official kernel sources released on <uri>http://www.kernel.org/</uri>. Please note that we do not patch these kernels at all - these are purely for people who wish to run a completely unmodified Linux kernel. We recommend that you use <c>gentoo-sources</c> instead."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):334
msgid "Two versions of the kernel can be found under this package: 2.4 and 2.6."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):341
msgid "zen-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):344
msgid "<c>zen-sources</c> is designed for desktop systems. It includes code not found in the mainline kernel. The zen kernel has patches that add new features, support additional hardware, and contain various tweaks for desktops."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):355
msgid "Previously provided kernel packages"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):357
msgid "aa-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):360
msgid "<c>aa-sources</c> was a heavily modified kernel with all kinds of patches. The upstream maintainer has stopped releasing kernel patchsets, this package was removed as it went out of date."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):369
msgid "alpha-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):372
msgid "<c>alpha-sources</c> was a 2.4 kernel with patches applied to improve hardware compatibility for the Alpha architecture. These patches have been developed and included in the mainline kernel. Alpha users can now run any recent kernel with no need for extra patches."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):382
msgid "development-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):385
msgid "<c>development-sources</c>, the official 2.6 kernel from kernel.org, can now be found under the <c>vanilla-sources</c> package."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):393
msgid "gentoo-dev-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):396
msgid "<c>gentoo-dev-sources</c>, a 2.6 kernel patched with bug, security and stability fixes, can now be found under the <c>gentoo-sources</c> package."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):404
msgid "grsec-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):407
msgid "The <c>grsec-sources</c> kernel source used to be patched with the latest grsecurity updates (grsecurity version 2.0 and up) which included, amongst other security-related patches, support for PaX. As grsecurity patches are included in <c>hardened-sources</c>, this package is no longer in Portage."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):417
msgid "hardened-dev-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):420
msgid "<c>hardened-dev-sources</c> can now be found under the <c>hardened-sources</c> package."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):428
msgid "hppa-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):431
msgid "<c>hppa-sources</c> was a 2.6 kernel with patches applied to improve hardware compatibility for the HPPA architecture. These patches have been developed and included in the mainline kernel. HPPA users can now run any recent kernel with no need for extra patches."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):441
msgid "rsbac-dev-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):444
msgid "The <c>rsbac-dev-sources</c> kernels can now be found under the <c>rsbac-sources</c> package."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):452
msgid "rsbac-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):455
msgid "<c>rsbac-sources</c> was a 2.6-based kernel. It contained patches to use Rule Set Based Access Controls (<uri link=\"http://www.rsbac.org\">RSBAC</uri>). It has been removed due to lack of maintainers. Use <c>hardened-sources</c> if you need additional security features."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):465
msgid "selinux-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):468
msgid "<c>selinux-sources</c>, a 2.4 kernel including lots of security enhancements, has been obsoleted by security development in the 2.6 tree. SELinux functionality can be found in the <c>hardened-sources</c> package."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):477
msgid "sh-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):480
msgid "<c>sh-sources</c> was a 2.6 kernel with patches applied to improve hardware compatibility for the SuperH architecture. These patches have been developed and included in the mainline kernel. SuperH users can now run any recent kernel with no need for extra patches."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):490
msgid "uclinux-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):493
msgid "The <c>uclinux-sources</c> are meant for CPUs without MMUs as well as embedded devices. For more information, see <uri>http://www.uclinux.org</uri>. Lack of security patches as well as hardware to test on were the reasons this is no longer in the tree."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(title):503
msgid "win4lin-sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(p):506
msgid "<c>win4lin-sources</c> were patched to support the userland win4lin tools that allow Linux users to run many Microsoft Windows (TM) applications at almost native speeds. This was removed due to security issues."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//gentoo-kernel.xml(None):0
msgid "translator-credits"
msgstr ""

