msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(guide:redirect):5
msgid "/proj/en/vps/vserver-howto.xml"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(title):6
msgid "Gentoo Linux-VServer Howto"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(mail:link):9
msgid "hollow@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(mail):9
msgid "Benedikt Boehm"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(author:title):11
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(mail:link):12
msgid "fox2mike@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(mail):12
msgid "Shyam Mani"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(abstract):15
msgid "In this Howto you will learn to setup a basic virtual server using the Linux-VServer Technology"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(version):24
msgid "1.6"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(date):25
msgid "2006-10-06"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(title):28
msgid "Moved"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(p):32
msgid "This document has been moved to a <uri link=\"/proj/en/vps/vserver-howto.xml\">new location</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//vserver-howto.xml(None):0
msgid "translator-credits"
msgstr ""

