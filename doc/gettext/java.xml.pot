msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-05-18 19:18+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):6
msgid "Gentoo Java Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(author:title):8 ../../gentoo/xml/htdocs/doc/en//java.xml(author:title):11
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(mail:link):9
msgid "nichoj@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(mail):9
msgid "Joshua Nichols"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(mail:link):12
msgid "karltk@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(mail):12
msgid "Karl Trygve Kalleberg"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(author:title):14
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(mail:link):15
msgid "nightmorph@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(mail):15
msgid "Joshua Saddler"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(abstract):18
msgid "This guide will introduce you to Java and explain how to use Java with Gentoo Linux."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(version):27
msgid "2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(date):28
msgid "2011-05-09"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):31
msgid "What is Java?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):33 ../../gentoo/xml/htdocs/doc/en//java.xml(title):158
msgid "Overview"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):36
msgid "Java is a programming language developed by engineers of Sun Microsystems. The language is object-oriented and designed to run on multiple platforms without the need of recompiling code for each platform. Although Java can be compiled as a native program, much of Java's popularity can be attributed to its portability, along with other features such as garbage collection. To make platform independence possible the Java compiler compiles the Java code to an intermediate representation called \"Java bytecode\" that runs on a JRE (Java Runtime Environment) and not directly on the operating system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):47
msgid "In order to run Java bytecode, one needs to have a JRE (Java Runtime Environment) installed. A JRE provides core libraries, a platform dependent Java Virtual Machine, plugins for browsers, among other things. A JDK (Java Development Kit) adds programming tools, such as a bytecode compiler and a debugger."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):60
msgid "Installing a Virtual Machine"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):62
msgid "The choices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):65
msgid "Gentoo provides numerous Runtime Environments (JREs) and Development Kits (JDKs). Among the current choices, we have:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(th):72
msgid "Vendor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(th):73
msgid "JDK"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(th):74
msgid "JRE"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):77
msgid "Sun's Java Kit"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):78
msgid "dev-java/sun-jdk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):79
msgid "dev-java/sun-jre-bin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):82
msgid "The IBM Java Kit"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):83
msgid "dev-java/ibm-jdk-bin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):84
msgid "dev-java/ibm-jre-bin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):87
msgid "BEA WebLogic's J2SE Development Kit"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):88
msgid "dev-java/jrockit-jdk-bin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):91
msgid "The IcedTea Open Java Kit"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):92
msgid "dev-java/icedtea6-bin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):104
msgid "Installing a JRE/JDKs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):107
msgid "To install your profile's default JDK, you can run <c>emerge virtual/jdk</c>. Or to install your profile's default JRE, you can <c>emerge virtual/jre</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):112
msgid "Some JDKs and JREs, including the Sun packages, require accepting an End User License Agreement, or EULA. If its license (such as dlj-1.1) is not listed in ACCEPT_LICENSE in <path>/etc/make.conf</path>, then you won't be able to install the JDK/JRE. For more information on how to add acceptable licenses to <path>make.conf</path>, please read the <uri link=\"/doc/en/handbook/handbook-x86.xml?part=2&amp;chap=1#doc_chap4\">Portage Handbook</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):122
msgid "To avoid any restrictive license hassle, consider installing <c>icedtea6-bin</c>, which is an open Java implementation from the OpenJDK project."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(note):128
msgid "A JDK also includes a JRE, so if you install a JDK you shouldn't have to also have to install a JRE."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):136
msgid "Installing fetch-restricted virtual machines"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):139
msgid "Some of the JDKs and JREs require you to jump through a few hoops before installing. Simply emerge the packages as you normally would. The ebuilds will then instruct you where to go and what to download."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):145
msgid "You should download the indicated file(s) into <path>/usr/portage/distfiles</path>. Once there, you can rerun the emerge command, at which point the JRE/JDK will be begin to install."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):156
msgid "Configuring your virtual machine"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):161
msgid "Gentoo has the ability to have multiple JDKs and JREs installed without causing conflicts."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):166
msgid "Using the <c>java-config</c> tool, you can set the system-wide default (provided you have root access). Users can also use <c>java-config</c> to set up their own personal default."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(note):172
msgid "You can also use <e>eselect</e> to change the system and user vm. See <c>eselect java-vm help</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):180
msgid "Setting a default virtual machine"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):183
msgid "Running the command <c>java-config --list-available-vms</c> will give you a list of all JREs and JDKs installed on your system. Here is an example of output:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):189
msgid "Listing available VMs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):189
#, no-wrap
msgid "\n# <i>java-config --list-available-vms</i>\nThe following VMs are available for generation-2:\n1)      IcedTea6-bin 1.4.1 [icedtea6-bin]\n2)      Sun JDK 1.5.0.20 [sun-jdk-1.5] <comment>(Build Only)</comment>\n*)      Sun JDK 1.6.0.16 [sun-jdk-1.6]\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(note):197
msgid "VMs marked as Build Only may contain security vulnerabilities and/or be EOL. Gentoo recommends not setting these VMs as either your system or user VM. Please see <uri link=\"java.xml#build-only\">Build Only VM</uri> for more information."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):204
msgid "The <e>*</e> indicates this is the current active vm (system-vm or user-vm when set). The name in the brackets (<e>[]</e>) is the handle or ID for that particular VM. You use the handle or the number to <c>java-config --set-system-vm</c>. Here is an example of how to set the system VM."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):211
msgid "Setting the System VM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):211
#, no-wrap
msgid "\n<comment>(By handle (preferred))</comment>\n# <i>java-config --set-system-vm sun-jdk-1.6</i>\nNow using sun-jdk-1.6 as your generation-2 system JVM\n<comment>(By number)</comment>\n# <i>java-config --set-system-vm 3</i>\nNow using sun-jdk-1.6 as your generation-2 system JVM\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):220
msgid "As a regular user, you can use <c>java-config --set-user-vm</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(note):224
msgid "You no longer have to <c>source</c> the profile for updates to the user/system VM take place."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):232
msgid "Build Only VM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):235
msgid "Some virtual machines are flagged as build-only due to being EOL and/or containing security vulnerabilities. These virtual machines will not automatically be used by Gentoo for the running of applications using Gentoo launchers but will still be available for use by Gentoo's build environment as some packages may require them for building. The setting of these virtual machines as either your system or user VM is strongly discouraged as these VMs will then be used when running the <path>/usr/bin/{java,javac,..}</path> executables and will also be used by any packages not using Gentoo's launcher scripts."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):250
msgid "Preferred Build VM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):253
msgid "While merging Java packages, the VM used for building can sometimes be different from the one currently set as the system VM."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):258
msgid "This merge time VM switching is needed when, for example, your system-vm is set to a 1.6 VM and the package you are merging requires a 1.5 VM. While merging it will select and use a 1.5 VM, leaving your system-vm choice intact."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):264
msgid "To define which VM is selected when a switch is needed, we have created a list of <e>default/supported VMs</e> per arch. You can find them in <path>/usr/share/java-config-2/config/jdk-defaults.conf</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):270
msgid "You can override these defaults (and even your selected system VM) in <path>/etc/java-config-2/build/jdk.conf</path> and have complete control over which VM will get used for merging. Some examples:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):276 ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):281 ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):286
msgid "Example /etc/java-config-2/build/jdk.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):276
#, no-wrap
msgid "\n<comment>(I always want it to use a sun-jdk, ie sun-jdk-1.4 for 1.4, sun-jdk-1.5 for 1.5, etc)</comment>\n*=sun-jdk\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):281
#, no-wrap
msgid "\n<comment>(Always use sun-jdk-1.5 wherever possible, except for when a 1.4 or 1.3 VM is explicitly required)</comment>\n*=sun-jdk-1.5\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):286
#, no-wrap
msgid "\n<comment># For 1.3 I prefer sun-jdk 1.4 but when it is not available, use ibm-jdk-bin,\n# For 1.5, use sun-jdk </comment>\n1.3=sun-jdk-1.4 ibm-jdk-bin\n1.5=sun-jdk\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(warn):293
msgid "You do not <e>have</e> to edit this file. If you change these options to use a unsupported VM, things could possibly break. Because of the wide variety of available VMs, we do not have the resources to test and verify every package works on all of them. Bugs reported with a unsupported VM won't be prioritized as much as bugs present within supported VMs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):306
msgid "Compilers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):310
msgid "The standard Java compiler used for building is <c>javac</c>, which comes with each JDK. In addition to configuring the VM used at build time, it is also possible configure which compiler is used. Essentially, you define a list your preference for which compiler to use in <path>/etc/java-config-2/build/compilers.conf</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):318
msgid "/etc/java-config-2/build/compilers.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):318
#, no-wrap
msgid "\n# If the ebuild supports it\n# it will check the COMPILERS var front to back and\n# use the first compiler that is installed\n\nCOMPILERS=\"ecj-X.Y jikes javac\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):326
msgid "Some compilers don't support all possible -target and -source arguments. Therefore, each compiler in the list is checked to see if it can support the desired -source/-target. javac will work in all cases, so if no other suitable compiler is found, it will be used instead."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):333
msgid "More details about each compiler are provided below:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(th):339
msgid "Name"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(th):340
msgid "Handle"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(th):341
msgid "Package"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(th):342
msgid "Description"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):345 ../../gentoo/xml/htdocs/doc/en//java.xml(ti):346
msgid "javac"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):347
msgid "N/A"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):348
msgid "This is the default compiler that will be used, and comes with each JDK."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):353 ../../gentoo/xml/htdocs/doc/en//java.xml(ti):354
msgid "jikes"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):355
msgid "dev-java/jikes"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):356
msgid "Jikes was originally developed by IBM. Anecdotally, it is generally quicker than javac. Note however, that it is more pedantic, and will fail under a few circumstances where javac has no issue. It also does not support Java 1.5 syntax yet."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):364
msgid "Eclipse Compiler for Java"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):365
msgid "ecj"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):366
msgid "dev-java/eclipse-ecj"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(ti):367
msgid "ECJ is the compiler used by the Eclipse software development kit. It is very full featured, and is pretty fast. It does support Java 1.5 syntax."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):379
msgid "Setting a default CLASSPATH"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(warn):383
msgid "The options explained in this section should be considered deprecated and will most likely be removed in the future. We strongly recommend against using these, because your Java projects or application should ideally manage their own classpaths. If you choose to specify a default CLASSPATH, some applications may behave unexpectedly, because classes they weren't expecting would be on the classpath."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):392
msgid "<c>java-config</c> can also be used to set a system-wide default CLASSPATH, as well a user-specific default CLASSPATH."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):397
msgid "First, you will want to list available Java libraries installed on your system that might want to be put in your CLASSPATH. Here is an example of output:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):402
msgid "Listing classes"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):402
#, no-wrap
msgid "\n# <i>java-config --list-available-packages</i>\n[xerces-2] The next generation of high performance, fully compliant XML parsers in the Apache Xerces family (/usr/share/xerces-2/package.env)\n[junit] Simple framework to write repeatable tests (/usr/share/junit/package.env)\n[bsh] BeanShell: A small embeddable Java source interpreter (/usr/share/bsh/package.env)\n[bcel] The Byte Code Engineering Library: analyze, create, manipulate Java class files (/usr/share/bcel/package.env)\n[log4j] A low-overhead robust logging package for Java (/usr/share/log4j/package.env)\n...\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):412
msgid "Again, the names in brackets (<e>[]</e>) are the IDs that you have to pass to <c>java-config --set-system-classpath</c>. Here is an example:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):417
msgid "Setting classpaths"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):417
#, no-wrap
msgid "\n# <i>java-config --set-system-classpath log4j,xerces-2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(note):421
msgid "The current directory (<path>.</path>) will not be part of the system classpath, as that should be added in your system's login profile."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):426
msgid "You will have to update your environment by logging out, then in again or sourcing <path>/etc/profile</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):431
msgid "For users, <c>java-config --set-user-classpath</c> will create <path>~/.gentoo/java-env-classpath</path>, which you should then source from your shell's profile."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):437
msgid "Sourcing user specific classpath"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):443
msgid "If you really want a system wide or user default classpath you can add something like the following to your shell's profile. But we would advise against it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):449
msgid "Setting classpath"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):449
#, no-wrap
msgid "\n# <i>export CLASSPATH=\"${CLASSPATH}:$(java-config --classpath log4j,xerces-2)\"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):458
msgid "Java Browser Plugins"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):460
msgid "Installing a plugin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):463
msgid "You can install a Java plugin for your web browser by emerging a Java VM with the <c>nsplugin</c> USE flag set."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(note):468
msgid "<c>nsplugin</c> is not available for all architectures. Check for available plugins on your arch before trying to install a VM by running <c>emerge -pv &lt;java-vm&gt;</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):474
msgid "Portage will allow you to install multiple versions of Java plugins, though only one will be used by your browser. You can check the list of available plugins by running:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):480 ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):549
msgid "Viewing available plugins"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):480
#, no-wrap
msgid "\n# <i>eselect java-nsplugin list</i>\n   [1]   sun-jre-bin-1.6\n   [2]   icedtea6-bin\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):486
msgid "In this example, <c>sun-jre-bin</c> is selected for the browser plugin."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):490
msgid "Selecting a plugin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):490
#, no-wrap
msgid "\n# <i>eselect java-nsplugin set sun-jre-bin-1.6</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):494
msgid "Verify that the correct plugin was selected:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):498 ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):573
msgid "Verifying the correct plugin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):498
#, no-wrap
msgid "\n# <i>eselect java-nsplugin list</i>\n   [1]   sun-jre-bin-1.6  current\n   [2]   icedtea6-bin\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):504
msgid "Java.com also provides a link to <uri link=\"http://java.com/en/download/installed.jsp\">verify your installed plugin</uri>. Additionally, if you are using a Mozilla-based browser, you can verify your Java plugin by typing <c>about:plugins</c> into the address bar."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):514
msgid "Plugins on multilib systems"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):517
msgid "If you are running a mixed 64-bit and 32-bit multilib system (for example, on AMD64), you can use 64-bit and 32-bit Java plugins. Unless you have a pressing need to run 32-bit Java applications, we recommend using native 64-bit plugins on 64-bit web browsers."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):524
msgid "There are several native 64-bit browser plugins available. The default JDK/JRE pair, <c>sun-jdk</c> and <c>sun-jre-bin</c>, both include browser plugins. Just emerge one of them with the <c>nsplugin</c> USE flag enabled."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):530
msgid "Installing a 64-bit plugin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):530
#, no-wrap
msgid "\n# <i>echo \"dev-java/sun-jre-bin nsplugin\" &gt;&gt; /etc/portage/package.use</i>\n# <i>emerge sun-jre-bin</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):535
msgid "To use a 32-bit plugin on a 32-bit browser, you will need to emerge <c>emul-linux-x86-java</c> with the <c>nsplugin</c> USE flag enabled."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):540
msgid "Installing a 32-bit plugin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):540
#, no-wrap
msgid "\n# <i>echo \"app-emulation/emul-linux-x86-java nsplugin\" &gt;&gt; /etc/portage/package.use</i>\n# <i>emerge emul-linux-x86-java</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):545
msgid "Next, check which plugins are available:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):549
#, no-wrap
msgid "\n# <i>eselect java-nsplugin list</i>\nAvailable 32-bit Java browser plugins\n  [1]   emul-linux-x86-java-1.5\n  [2]   emul-linux-x86-java-1.6\nAvailable 64-bit Java browser plugins\n  [1]   icedtea6-bin\n  [2]   sun-jre-bin-1.6\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):559
msgid "Now select the right plugin for your browsers:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre:caption):563
msgid "Selecting plugins"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):563
#, no-wrap
msgid "\n<comment>(Choose the plugins for 32-bit and 64-bit browsers)</comment>\n# <i>eselect java-nsplugin set 32bit emul-linux-x86-java-1.6</i>\n# <i>eselect java-nsplugin set 64bit sun-jre-bin-1.6</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):569
msgid "Verify the correct plugin was selected:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(pre):573
#, no-wrap
msgid "\n# <i>eselect java-nsplugin list</i>\nAvailable 32-bit Java browser plugins\n  [1]   emul-linux-x86-java-1.5\n  [2]   emul-linux-x86-java-1.6  current\nAvailable 64-bit Java browser plugins\n  [1]   icedtea6-bin\n  [2]   sun-jre-bin-1.6  current\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):588
msgid "USE flags for use with Java"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):590
msgid "Setting USE flags"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(p):593
msgid "For more information regarding USE flags, refer to the <uri link=\"/doc/en/handbook/handbook-x86.xml?part=2&amp;chap=2\">USE flags</uri> chapter from the Gentoo Handbook."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):602
msgid "The flags"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):606
msgid "The <b>java</b> flag adds support for Java in a variety of programs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):607
msgid "The <b>nsplugin</b> flag adds support for Mozilla-like browsers (including Firefox). You will need this for viewing Java applets in your Mozilla-like browser."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):612
msgid "The <b>source</b> flag installs a zip of the source code of a package. This is traditionally used for IDEs to 'attach' source to the libraries you are using."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):617
msgid "The <b>jce</b> flag adds support for the Java Cryptography Engine"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):618
msgid "For Java packages, the <b>doc</b> flag will build API documentation using javadoc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):629
msgid "Additional resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):631
msgid "Off-line resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):635
msgid "java-config man page"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(title):642
msgid "Online resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):646
msgid "The <uri link=\"http://www.gentoo.org/proj/en/java/\">Java Project Page</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):650
msgid "The <uri link=\"http://news.gmane.org/gmane.linux.gentoo.java\">gentoo-java</uri>, <uri link=\"http://news.gmane.org/gmane.linux.gentoo.user\">gentoo-user</uri>, and <uri link=\"http://news.gmane.org/gmane.linux.gentoo.devel\">gentoo-dev</uri> mailing list archives"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):659
msgid "<uri link=\"irc://irc.gentoo.org/gentoo\">#gentoo</uri> and <uri link=\"irc://irc.gentoo.org/gentoo-java\">#gentoo-java</uri> on IRC"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(uri:link):665
msgid "http://en.wikipedia.org/wiki/Java_programming_language"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(uri):665
msgid "Wikipedia's entry for Java"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//java.xml(li):668
msgid "If you have suggestions or questions regarding this document, please email the Gentoo Java team: <mail>java@gentoo.org</mail>"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//java.xml(None):0
msgid "translator-credits"
msgstr ""

