msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):6
msgid "Gentoo Linux Bluetooth Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(author:title):8 ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(author:title):17
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail:link):9
msgid "deathwing00@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail):9
msgid "Ioannis Aslanidis"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(author:title):11 ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(author:title):14
msgid "Contributor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail:link):12
msgid "puggy@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail):12
msgid "Douglas Russell"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail:link):15
msgid "marcel@holtmann.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail):15
msgid "Marcel Holtmann"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail:link):18
msgid "fox2mike@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail):18
msgid "Shyam Mani"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(author:title):20 ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(author:title):23
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail:link):21
msgid "rane@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail):21
msgid "Łukasz Damentko"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(mail:link):24
msgid "nightmorph"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(abstract):27
msgid "This guide will explain how to successfully install a host Bluetooth device, configure the kernel properly, explain all the possibilities that the Bluetooth interconnection offers and how to have some fun with Bluetooth."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(version):37
msgid "1.11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(date):38
msgid "2009-07-16"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):41 ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):504
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):43
msgid "What is Bluetooth?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):46
msgid "Bluetooth is an industrial specification that provides users a way to connect and exchange information between devices like personal computers, PDAs or mobile phones. Using the Bluetooth technology, users can achieve wireless voice and data transmission between devices at a low cost. Bluetooth also offers the possibility to create small wireless LANs and to synchronize devices."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):57
msgid "About the content of this guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):60
msgid "The first part of this guide explains how to configure the system kernel, identify the Bluetooth devices installed on the system and detected by the kernel and install the necessary basic Bluetooth tools."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):66
msgid "The second part covers how to detect remote devices and how to establish a connection from or to them by either setting up radio frequency communication (RFCOMM)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):72
msgid "The last part of the guide lists in detail applications that can take advantage of all the possibilities offered by the Bluetooth technology."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):82
msgid "Configuring the system"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):84
msgid "Kernel Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):87
msgid "As the latest Linux stable kernel is 2.6, the configuration will be done for these series of the kernel. Most Bluetooth devices are connected to a USB port, so USB will be enabled too. Please refer to the <uri link=\"/doc/en/usb-guide.xml\">Gentoo Linux USB Guide</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):94
msgid "Configuration for 2.6 kernels"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):94
#, no-wrap
msgid "\nNetworking ---&gt;\n\n&lt;*&gt; Bluetooth subsystem support  ---&gt;\n\n--- Bluetooth subsystem support\n&lt;M&gt;   L2CAP protocol support\n&lt;M&gt;   SCO links support\n&lt;M&gt;   RFCOMM protocol support\n[*]     RFCOMM TTY support\n&lt;M&gt;   BNEP protocol support\n[*]     Multicast filter support\n[*]     Protocol filter support\n&lt;M&gt;   HIDP protocol support\n\nBluetooth device drivers  ---&gt;\n&lt;M&gt; HCI USB driver\n[*]   SCO (voice) support\n&lt;M&gt; HCI UART driver\n[*]   UART (H4) protocol support\n[*]   BCSP protocol support\n[*]   Transmit CRC with every BCSP packet\n&lt;M&gt; HCI BCM203x USB driver\n&lt;M&gt; HCI BPA10x USB driver\n&lt;M&gt; HCI BlueFRITZ! USB driver\n<comment>(The four drivers below are for PCMCIA Bluetooth devices and will only\nshow up if you have also selected PCMCIA support in your kernel.)</comment>\n&lt;M&gt; HCI DTL1 (PC Card) driver\n&lt;M&gt; HCI BT3C (PC Card) driver\n&lt;M&gt; HCI BlueCard (PC Card) driver\n&lt;M&gt; HCI UART (PC Card) device driver\n<comment>(The driver below is intended for HCI Emulation software.)</comment>\n&lt;M&gt; HCI VHCI (Virtual HCI device) driver\n\n<comment>(Move back three levels to Device Drives and then check if USB is\nenabled. This is required if you use a Bluetooth dongle, which are mostly USB\nbased.)</comment>\nUSB support  ---&gt;\n\n&lt;*&gt; Support for Host-side USB\n--- USB Host Controller Drivers\n&lt;M&gt; EHCI HCD (USB 2.0) support\n[ ]   Full speed ISO transactions (EXPERIMENTAL)\n[ ]   Root Hub Transaction Translators (EXPERIMENTAL)\n&lt;*&gt; OHCI HCD support\n&lt;*&gt; UHCI HCD (most Intel and VIA) support\n&lt; &gt; SL811HS HCD support\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):143
msgid "Now we'll reboot with our new kernel. If everything went fine, we will have a system that is Bluetooth ready."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(impo):148
msgid "Your USB device may have two modes the default of which may not be HCI, but HID. If this is your case, use <c>hid2hci</c> to switch to HCI mode. Your system will not remember this change when you next reboot."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):154
msgid "Checking the Bluetooth devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):154
#, no-wrap
msgid "\n<comment>(One way to check for the device)</comment>\n# <i>cat /proc/bus/usb/devices | grep -e^[TPD] | grep -e Cls=e0 -B1 -A1</i>\n<comment>(The Cls=e0(unk. ) identifies the Bluetooth adapter.)</comment>\nT:  Bus=02 Lev=02 Prnt=03 Port=00 Cnt=01 Dev#=  4 Spd=12  MxCh= 0\nD:  Ver= 1.10 Cls=e0(unk. ) Sub=01 Prot=01 MxPS=64 #Cfgs=  1\nP:  Vendor=0a12 ProdID=0001 Rev= 5.25\n<comment>(Some might show up on lsusb from sys-apps/usbutils)</comment>\n# <i>lsusb</i>\nBus 003 Device 002: ID 046d:c00e Logitech, Inc. Optical Mouse\nBus 003 Device 001: ID 0000:0000\nBus 002 Device 002: ID 0db0:1967 Micro Star International Bluetooth Dongle\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):173
msgid "BlueZ - The Bluetooth Stack"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):175
msgid "Installing BlueZ"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):178
msgid "Now that the device is detected by the kernel, we need a layer that lets applications communicate with the Bluetooth device. BlueZ provides the official Linux Bluetooth stack. The ebuilds that provide what we need are <c>bluez-libs</c> and <c>bluez-utils</c>. Devices that need Broadcom firmware files or the like may need <c>bluez-firmware</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):186
msgid "Installing bluez-libs and bluez-utils"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):186
#, no-wrap
msgid "\n# <i>emerge net-wireless/bluez-libs net-wireless/bluez-utils</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):193
msgid "BlueZ configuration and PIN pairing"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):196
msgid "Now it's time to see if the Bluetooth device is being picked up correctly by the system. We start up the required Bluetooth services first."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):201
msgid "Running hciconfig"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):201
#, no-wrap
msgid "\n<comment>(Start up Bluetooth)</comment>\n# <i>/etc/init.d/bluetooth start</i>\n* Starting Bluetooth ...\n*     Starting hcid ...                                                 [ ok ]\n*     Starting sdpd ...                                                 [ ok ]\n*     Starting rfcomm ...                                               [ ok ]\n\n# <i>hciconfig</i>\nhci0:   Type: USB\n        BD Address: 00:01:02:03:04:05 ACL MTU: 192:8  SCO MTU: 64:8\n        DOWN\n        RX bytes:131 acl:0 sco:0 events:18 errors:0\n        TX bytes:565 acl:0 sco:0 commands:17 errors:0\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):217
msgid "This shows that the Bluetooth device has been recognised. As you might have noticed the device is <e>DOWN</e>. Let's configure it so that we can bring it up. The configuration file is at <path>/etc/bluetooth/hcid.conf</path>. The required changes to the config file are shown below. For additional details please refer to <c>man hcid.conf</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):225
msgid "Editing /etc/bluetooth/hcid.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):225
#, no-wrap
msgid "\n<comment>(Recommended changes to be made to the file are shown)</comment>\n\n# HCId options\noptions {\n        # Automatically initialize new devices\n        autoinit yes;\n\n<comment>(Change security to \"auto\")</comment>\n        # Security Manager mode\n        #   none - Security manager disabled\n        #   auto - Use local PIN for incoming connections\n        #   user - Always ask user for a PIN\n        #\n        security auto;\n\n        # Pairing mode\n        pairing multi;\n\n<comment>(You only need a pin helper if you are using &lt;=bluez-libs-2.x and &lt;=bluez-utils-2.x)\n(Change pin_helper to use /etc/bluetooth/pin-helper)</comment>\n        # PIN helper\n        pin_helper /etc/bluetooth/pin-helper;\n}\n\n# Default settings for HCI devices\ndevice {\n<comment>(Set your device name here, you can call it anything you want)</comment>\n        # Local device name\n        #   %d - device id\n        #   %h - host name\n        name \"BlueZ at %h (%d)\";\n\n        # Local device class\n        class 0x3e0100;\n\n        # Inquiry and Page scan\n        iscan enable; pscan enable;\n\n        # Default link mode\n        lm accept;\n\n        # Default link policy\n        lp rswitch,hold,sniff,park;\n\n<comment>(Leave as is, if you don't know what exactly these do)</comment>\n        # Authentication and Encryption (Security Mode 3)\n        #auth enable;\n        #encrypt enable;\n}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):277
msgid "After that, we have to configure the Bluetooth device PIN. That will help in pairing this device with another one."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):282
msgid "Editing /etc/bluetooth/pin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):282
#, no-wrap
msgid "\n<comment>(Replace 123456 with your desired pin number.)</comment>\n123456\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(impo):287
msgid "This number (of your choice) must be the same in all your hosts with Bluetooth devices so they can be paired. This number must also be kept secret since anyone with knowledge of this number can essentially establish connections with your devices."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(note):294
msgid "Beginning with <c>&gt;=bluez-libs-3.x</c> and <c>&gt;=bluez-utils-3.x</c>, pin helpers have been replaced by passkey agents. There are a few different graphical passkey agents available to help manage your PIN, such as <c>bluez-gnome</c> and <c>kdebluetooth</c>. You can also use <c>passkey-agent</c> (found in <c>bluez-utils</c>) from the command line."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):305
msgid "Services configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):308
msgid "Now that we have concluded with the configuration of BlueZ, it's time to restart the necessary services."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):313
msgid "Starting the Bluetooth daemons"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):313
#, no-wrap
msgid "\n# <i>/etc/init.d/bluetooth restart</i>\n<comment>(We can also add it to the default runlevel.)</comment>\n# <i>rc-update add bluetooth default</i>\n * bluetooth added to runlevel default\n * rc-update complete.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):321
msgid "Let's be sure that the Bluetooth daemons started correctly. If we can see that both <c>hcid</c> and <c>sdpd</c> are running, then we configured Bluetooth the right way. After that, we can see if the devices are now up and running with the configured options."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):328
msgid "Checking whether Bluetooth daemons started correctly"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):328
#, no-wrap
msgid "\n<comment>(Check to see if the services are running)</comment>\n# <i>ps -ae | grep hcid</i>\n26050 ?        00:00:00 hcid\n# <i>ps -ae | grep sdpd</i>\n26054 ?        00:00:00 sdpd\n\n# <i>hciconfig -a</i>\nhci0:   Type: USB\n        BD Address: 00:0A:0B:0C:0D:0E ACL MTU: 192:8 SCO MTU: 64:8\n        UP RUNNING PSCAN ISCAN\n        RX bytes:125 acl:0 sco:0 events:17 errors:0\n        TX bytes:565 acl:0 sco:0 commands:17 errors:0\n        Features: 0xff 0xff 0x0f 0x00 0x00 0x00 0x00 0x00\n        Packet type: DM1 DM3 DM5 DH1 DH3 DH5 HV1 HV2 HV3\n        Link policy: RSWITCH HOLD SNIFF PARK\n        Link mode: SLAVE ACCEPT\n        Name: 'BlueZ at bluehat (0)'\n        Class: 0x3e0100\n        Service Classes: Networking, Rendering, Capturing, Object Transfer,\n        Audio\n        Device Class: Computer, Uncategorized\n        HCI Ver: 1.1 (0x1) HCI Rev: 0x1e7 LMP Ver: 1.1 (0x1) LMP Subver: 0x1e7\n        Manufacturer: Cambridge Silicon Radio (10)\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):359
msgid "Detecting and Connecting to Remote Devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):361
msgid "Detecting Bluetooth devices in other hosts"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):364
msgid "At this point we are now ready to detect Bluetooth devices installed in other machines. This is independent of the host Operating System. We will make use of the <c>hcitool</c> command for the same."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):370
msgid "Checking for local devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):370
#, no-wrap
msgid "\n# <i>hcitool dev</i>\nDevices:\n        hci0    00:01:02:03:04:05\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):376
msgid "Scanning for remote devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):376
#, no-wrap
msgid "\n# <i>hcitool scan</i>\nScanning ...\n        00:0A:0B:0C:0D:0E       Grayhat\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):382
msgid "Inquiring remote devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):382
#, no-wrap
msgid "\n# <i>hcitool inq</i>\nInquiring ...\n        00:0A:0B:0C:0D:0E       clock offset: 0x5579    class: 0x72010c\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):388
msgid "Now that we know the MAC address of the remote Bluetooth devices, we can check if we paired them correctly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):393
msgid "Running l2ping"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):393
#, no-wrap
msgid "\n# <i>l2ping 00:0A:0B:0C:0D:0E</i>\nPing: 00:0A:0B:0C:0D:0E from 00:01:02:03:04:05 (data size 20) ...\n20 bytes from 00:0A:0B:0C:0D:0E id 200 time 69.85ms\n20 bytes from 00:0A:0B:0C:0D:0E id 201 time 9.97ms\n20 bytes from 00:0A:0B:0C:0D:0E id 202 time 56.86ms\n20 bytes from 00:0A:0B:0C:0D:0E id 203 time 39.92ms\n4 sent, 4 received, 0% loss\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):406
msgid "Setting up Radio Frequency Communication (RFCOMM)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(note):409
msgid "Please note that setting up radio frequency communication is optional."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):413
msgid "We can establish a radio frequency connection to another Bluetooth device using the <c>rfcomm</c> command. To make things a little easier especially for users with multiple devices that support Bluetooth, it is advisable to make a few changes to the default rfcomm config at <path>/etc/bluetooth/rfcomm.conf</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):420
msgid "The whole segment of the config starting from <c>rfcomm0 {</c> and ending with <c>}</c> is the config for the device that will establish a connection at <path>/dev/rfcomm0</path>. In this case, we will only show one example, rfcomm0. You can add more devices as you see fit."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):427
msgid "Editing /etc/bluetooth/rfcomm.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):427
#, no-wrap
msgid "\n<comment>(Only changes that might be needed are shown)</comment>\nrfcomm0 {\n        # Automatically bind the device at startup\n        <comment>(Creates the device node, /dev/rfcomm0 at start up)</comment>\n        bind yes;\n\n        # Bluetooth address of the device\n        <comment>(Enter the address of the device you want to connect to)</comment>\n        device 00:0A:0B:0C:0D:0E;\n\n}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):441
msgid "After configuring RFCOMM, we can connect to any device. Since we've made the required settings to the <path>/etc/bluetooth/rfcomm.conf</path> file, we just issue the command shown below. In case you've not made changes to the config file, an alternative method is also shown in the code listing that follows"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):448
msgid "Establishing an RFCOMM connection"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):448
#, no-wrap
msgid "\n<comment>(The 0 refers to the rfcomm0 in the config file)</comment>\n# <i>rfcomm connect 0 </i>\nConnected /dev/rfcomm0 to 00:0A:0B:0C:0D:0E on channel 1\nPress CTRL-C for hangup\n\n<comment>(If you did not edit /etc/bluetooth/rfcomm.conf)</comment>\n# <i>rfcomm connect 0 00:0A:0B:0C:0D:0E 1</i>\nConnected /dev/rfcomm0 to 00:0F:DE:69:50:24 on channel 1\nPress CTRL-C for hangup\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):460
msgid "The first parameter after the connect command is the RFCOMM TTY device node that will be used (usually 0). The second parameter is the MAC address of the remote device. The third parameter is optional and specifies the channel to be used. Please, note that in order to connect to a device, that device must be listening for incoming connections. To do that, we have to explicitly tell it to listen. We can cancel the communication at any moment by just hitting CTRL+C."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):470
msgid "Listening for incoming RFCOMM connections"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):470
#, no-wrap
msgid "\n# <i>rfcomm listen 0 1</i>\nWaiting for connection on channel 1\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):475
msgid "In a similar way to the connect command, the listen command can receive two parameters. The first one explicits the RFCOMM TTY device node (usually 0) that will be used to accept a connection, while the second is the channel that will be used."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):482
msgid "Each time you call the <c>rfcomm</c> command, you can also specify the physical device you want to use. Below you can see a small example specifiying the physical device on the above two commands."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):488
msgid "RFCOMM connections specifying physical device"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):488
#, no-wrap
msgid "\n# <i>rfcomm -i hci0 listen 0 1</i>\nWaiting for connection on channel 1\n<comment>(To listen to a determined device) </comment>\n# <i>rfcomm -i hci0 connect 0 00:0A:0B:0C:0D:0E 1</i>\n<comment>(To use a determined device when connecting to another one)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):502
msgid "Desktop Applications for Bluetooth"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):507
msgid "We have quite a few Bluetooth applications that run on the desktop and this chapter has been divided into 3 parts, one each for Gnome, KDE and Miscellaneous applications."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):516
msgid "For Gnome"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):519
msgid "If you are a gnome user, you will most probably go with <c>gnome-bluetooth</c>. It provides the most basic yet most used functionalities, as you can see below."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):525
msgid "<c>gnome-bluetooth-manager</c>: To manage Bluetooth remote devices."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):526
msgid "<c>gnome-obex-send</c>: To send files to other devices."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):527
msgid "<c>gnome-obex-server</c>: To receive files."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):530
msgid "Installing gnome-bluetooth"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):530
#, no-wrap
msgid "\n# <i>emerge gnome-bluetooth</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):534
msgid "This adds menu entries under Applications &gt; System Tools from where you can easily start up the manager or File sharing to transfer files between devices."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):539
msgid "To transfer files (the easy way):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):544
msgid "From the Phone to the Computer - Send the file from the phone via Bluetooth and it will be picked up and saved to your <path>/home</path> always."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):558
msgid "<c>gnome-phone-manager</c> is a nifty app that you can use to send and receive messages to and from your phone, using only your system. You do not have to touch your phone to read or send messages since all that happens through the application. You are also notified of a new message on your screen if the option is enabled under Preferences. Installation is a breeze as always."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):566
msgid "Installing gnome-phone-manager"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):566
#, no-wrap
msgid "\n# <i>emerge gnome-phone-manager</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):573
msgid "For KDE"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):576
msgid "KDE makes use of <c>kdebluetooth</c> and provides more utilities than its Gnome counterpart as seen below."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):582
msgid "<c>kbluetoothd</c>: Bluetooth Meta Server."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):583
msgid "<c>kbtsearch</c>: Bluetooth device/service search utility."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):584
msgid "<c>khciconfig</c>: KDE Bluetooth Monitor."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):585
msgid "<c>kioclient</c>: KIO command line client."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):586
msgid "<c>qobexclient</c>: Swiss army knife for obex testing/development."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):587
msgid "<c>kbtobexclient</c>: A KDE Bluetooth Framework Application."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):590
msgid "<c>kbemusedsrv</c>: KDE Bemused Server."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):591
msgid "<c>kbtobexsrv</c>: KDE OBEX Push Server for Bluetooth."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):592
msgid "<c>kbluepin</c>: A KDE KPart Application."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):593
msgid "<c>auth-helper</c>: A helper program for kbtobexsrv that sends an authentication request for a given ACL link."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre:caption):599
msgid "Installing kdebluetooth"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(pre):599
#, no-wrap
msgid "\n# <i>emerge kdebluetooth</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):606
msgid "Other Interesting Applications"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):610
msgid "<c>app-mobilephone/obexftp</c>: File transfer over OBEX for mobile phones"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):613
msgid "<c>app-mobilephone/bemused</c>: Bemused is a system which allows you to control your music collection from your phone, using Bluetooth."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):617
msgid "<c>app-pda/multisync</c>: Multisync allows you to sync contacts, calendar entries and notes from your mobile phone with your computer, over a Bluetooth connection (amongst other things). It includes such features as backing up this information and restoring it later, and syncing with the Evolution e-mail client. You will need the <c>irmc</c> USE flag set to ensure that <c>multisync</c> has Bluetooth support."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(li):625
msgid "<c>net-wireless/opd</c> and <c>net-wireless/ussp-push</c> are command line tools (server and client) that can be used to send files to your mobile phone."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(title):637
msgid "Acknowledgements"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(p):641
msgid "Special thanks to <mail link=\"marcel@holtmann.org\">Marcel Holtmann</mail> for his time and dedication to the Bluetooth development and for reviewing this guide. And big thanks to <mail link=\"puggy@gentoo.org\">Douglas Russell</mail> for performing additional hardware tests and improving this guide."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//bluetooth-guide.xml(None):0
msgid "translator-credits"
msgstr ""

