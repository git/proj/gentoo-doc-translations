msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):6
msgid "Gentoo/MIPS Linux Hardware Requirements"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(mail:link):9
msgid "kumba@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(mail):9
msgid "Joshua Kinard"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(author:title):11 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(author:title):14
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(mail:link):12
msgid "redhatter@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(mail):12
msgid "Stuart Longland"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(mail:link):15
msgid "mattst88"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(mail):15
msgid "Matt Turner"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(abstract):18
msgid "This document provides an overview of the status of various MIPS-based systems for installing Gentoo Linux. If you find errors or omissions please email the maintainer or an editor."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(version):28
msgid "2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(date):29
msgid "2011-08-14"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):32
msgid "Silicon Graphics Systems &ndash; Stable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(p):36
msgid "The following systems are known to be very stable overall. Just about all the core hardware is supported in these systems, and there are very few outstanding stability issues at this time."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):45
msgid "IP22: Indy, Indigo 2 and Challenge S"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):52
msgid "<e>(on Indy and Challenge S)</e> R4000, R4400, R4600, R5000"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):55
msgid "<e>(on Indigo 2)</e> R4000, R4400"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):63 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):361
msgid "32-bit: <e>Stable</e>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):64 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):148 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):167 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):212
msgid "64-bit: <e>Experimental</e>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):70
msgid "At present, only the Newport (XL) graphics framebuffer is supported in Linux. Therefore on systems with other framebuffers, you will need to use serial console."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):75
msgid "The Challenge S has its RJ-45 connector on a daughter board (Mezz) linked with an unsupported SCSI adapter, a WD33C95A. In order for this network connector to function, this SCSI bus needs to be activated, however it is not yet supported by Linux. As a result of this, the Challenge S can only get network connectivity via its AUI connector."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):82
msgid "Indigo2 EISA Support isn't very well tested and may not work with all hardware. Hardware that it is known to function with is listed, and as more compatible hardware is discovered, it will be added. Please note that hardware requiring DMA support is currently not functional. This includes EISA/ISA-based soundcards. EISA Video cards requiring an x86-compatible BIOS are also not functional."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):98
msgid "IP32: O2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):105
msgid "<b>Stable:</b> R5000, RM5200"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):106
msgid "<b>Experimental:</b> RM7000"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):107
msgid "<b>Unsupported:</b> R10000"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):113 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):147 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):166 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):211
msgid "32-bit: <e>Unsupported in Kernel</e>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):114
msgid "64-bit: <e>Stable</e>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):124
msgid "Silicon Graphics Systems &ndash; Experimental/Unstable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(p):128
msgid "The systems listed here are known to run Linux, however in many cases, there are known stability issues and major gaps in the hardware support. In short, it will be a rough and bumpy ride."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):137
msgid "IP27: Origin 200 and 2000"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):141
msgid "<b>Processor Options <e>(Experimental)</e>:</b> R10000, R12000"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):156
msgid "IP28: Indigo 2 Impact (a.k.a Indigo 2 R10k)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):160
msgid "<b>Processor Options <e>(Experimental)</e>:</b> R10000"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):185
msgid "MIPS R10000 Microprocessor User's Manual (See <e>PDF Pages 51-55</e>) <uri>http://techpubs.sgi.com/library/manuals/2000/007-2490-001/pdf/007-2490-001.pdf</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):190
msgid "Post to NetBSD sgimips Mailing List on 29 Jun 2000 <uri>http://mail-index.netbsd.org/port-sgimips/2000/06/29/0006.html</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):173
msgid "IP28 Indigo2 Impact support (classified as an Indigo2 system w/ an R10000 Processor) is <e>very experimental</e>. Most of the work was done by Peter Fuerst, who maintains patches on his <uri link=\"http://www.pfrst.de/download.html\">website</uri>. This kernel is <e>not</e> intended for stable, day-to-day use. The Indigo2 IP28 system suffers from an issue known as <e>Speculative Execution</e>, a feature of the R10000 Processor that is problematic on machines that are <e>Non Cache Coherent</e>, such as the IP28 Indigo2 and on R10000/R12000-based IP32 O2 systems. Details on the nature of Speculative Execution, and the issues it poses to the Indigo2 can be found at the following URLs: <placeholder-1/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):203
msgid "IP30: Octane &ndash; Dead"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):207
msgid "<b>Processor Options (Experimental):</b> R10000, R12000, R14000A"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):218
msgid "Octane support in Linux should be regarded as broken. Stanislaw Skowronek did the reverse engineering and bring up. <uri link=\"http://www.linux-mips.org/~skylark/\">His page</uri> is available but has not been updated since October 2006."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):224
msgid "The serial ports on the Octane currently only support 9600 baud rate, 8-bits, no parity, 1 stop bit. Other baud rates do not currently work at present."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):229
msgid "Dead due to issues porting the IRQ handling to newer Linux kernels. Help is welcome."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):242
msgid "Silicon Graphics Systems &ndash; Unsupported"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(p):246
msgid "Just about all of these systems are totally unsupported at the kernel level &ndash; making a full Linux system totally impossible. Little is known about a lot of the systems listed here &ndash; therefore any support in the near future is highly unlikely. You may wish to keep an eye on the <uri link=\"http://www.linux-mips.org\">Linux/MIPS website</uri> for any news of ports &ndash; either that, or start your own. :-)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):256
msgid "IP12: Iris Indigo (R3000 CPU)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):257
msgid "IP20: Iris Indigo (R4000 or R4400 CPU)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):258
msgid "IP26: Indigo 2 Power (R8000 CPU)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):259
msgid "IP34: Fuel (R14000A or R16000 CPU)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):260
msgid "IP35: Origin 3000 (R14000 or R14000A CPU)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):261
msgid "IP45: Origin 300 (R14000 CPU)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):262
msgid "IP53: Origin 350 &amp; Tezro (R16000 CPU)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):270
msgid "Silicon Graphics Accessories"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(p):274
msgid "As with most systems, there is quite wide array of peripherals that one can obtain for Silicon Graphics systems. While a lot of these work in IRIX, Linux support is a bit touch-and-go on some of these items. The following table lists the support for each device."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(th):283
msgid "Stable Support"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(th):284
msgid "Experimental Support"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(th):285
msgid "Unsupported/Untested"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(ti):288
msgid "Indy/Indigo2: XL Graphics Card (Newport)<br/> Indy/Indigo2: SGI Seeq Ethernet 10Mbps<br/> Indy/Indigo2: WD33C93 SCSI<br/> Indy/Indigo2: HAL2 Sound/Audio<br/> Indy/Indigo2: Parallel Port<br/> Indy/Indigo2: Serial Port<br/> O2: SGI MACE Ethernet 10/100Mbps<br/> O2: Adaptec AIC7880 SCSI<br/> O2: Serial Port<br/> O2: GBE Framebuffer (4MB Framebuffer memory only!)<br/> Indigo2 ISA: Parallel Port Card (PC-Style)<br/> Indigo2 ISA: NE2000 Compatible 10Mbps NIC (RTL8019)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(ti):302
msgid "Indy: I2C/VINO subsystem &amp; IndyCam<br/> Indigo2 EISA: 3COM 3c597 10/100Mbps<br/> O2: PCI Slot<br/> Origin: IOC3 Ethernet 10/100Mbps<br/> Origin: QLogic ISP1020 SCSI<br/> O2 PCI: ALi 5273 USB 2.0 (Req. Kernel &gt;=2.6.8.1)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(ti):310
msgid "(1)Indy/Indigo2: Impact &amp; Extreme/XZ graphics chipsets<br/> Indy: Phobos G130 10/100Mbps NIC<br/> Indy ThunderLAN card<br/> Indigo2: GIO64 Slots<br/> Indigo2: Phobos G160 10/100Mbps NIC<br/> Challenge S: WD33C95A SCSI Adapter/RJ-45 Daughter Card<br/> O2: VICE Subsystem Octane: Keyboard<br/> Octane: Mardi Gras (MGRAS) Graphics<br/> Octane: QLogic ISP1040B SCSI<br/> Octane: RAD1 Audio<br/> Octane: SMP Support<br/> Octane: V6/V8/V10/V12 Graphics (Odyssey)<br/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):333
msgid "Cobalt Servers &ndash; Stable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(p):337
msgid "The Cobalt servers are a set of machines all based around the QED RM523[01] CPU. They came in two varieties, the RaQ &ndash; a rackmounted model, and the Qube, a small desktop model (1ft. cube). The big difference between these systems and the SGI systems above, is that these systems are <e>little endian</e> (<c>mipsel</c>) as opposed to <e>big endian</e> (<c>mips</c>)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(p):345
msgid "These were also sold under an OEM arragement to other companies, examples of this are the Gateway Microserver (Qube 2) and the Seagate NasRaQ (RaQ 2)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):353
msgid "Qube/Raq 2800 (a.k.a Qube/Raq 2)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):357
msgid "<b>Processor:</b> QED RM5231 @ 250MHz"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):362
msgid "64-bit: <e>Highly Experimental</e>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):372
msgid "Cobalt Servers &ndash; Experimental"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):374
msgid "Raq 2700 (a.k.a Raq 1)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):378 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):407
msgid "<b>Processor:</b> QED RM5230 @ 150MHz"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):382
msgid "32-bit: <e>Experimental</e>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):383 ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):412
msgid "64-bit: <e>Untested In Gentoo</e>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):393
msgid "Cobalt Servers &ndash; Unsupported"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(title):395
msgid "Qube 2700 (a.k.a Qube 1)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(p):398
msgid "The Qube 2700 was the first of this whole subarchitecture. Unfortunately, it lacks a serial port, making installation at present, damn near impossible. You may wish to have a look at <uri link=\"http://web.archive.org/web/20060716160139/http://metzner.org/projects/qube/\"> Jan Metzner's page</uri> for more information."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(li):411
msgid "32-bit: <e>Untested In Gentoo</e>"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//mips-requirements.xml(None):0
msgid "translator-credits"
msgstr ""

