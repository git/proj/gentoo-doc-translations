msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(guide:redirect):5
msgid "/doc/en/security/"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(title):6
msgid "Obsolete Gentoo Linux Security Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(author:title):7
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(mail:link):8
msgid "neysx@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(mail):8
msgid "Xavier Neys"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(abstract):11
msgid "This was a step-by-step guide for hardening Gentoo Linux."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(version):15
msgid "1.0"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(date):16
msgid "2005-06-01"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(title):19
msgid "Obsolete"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(p):23
msgid "This guide has been replaced by the <uri link=\"/doc/en/security/\">Gentoo Security Handbook</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//gentoo-security.xml(None):0
msgid "translator-credits"
msgstr ""

