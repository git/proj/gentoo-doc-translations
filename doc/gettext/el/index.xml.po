msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: 2010-10-21 23:56+0600\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../gentoo/xml/htdocs/doc/en//index.xml(version):10
msgid "11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(title):12
msgid "Gentoo Documentation Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(title):19
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(p):22
msgid ""
"Welcome to the Gentoo Documentation Resources. This page will give you a "
"quick overview on the documentation we provide. Most documentation is "
"released under the <uri link=\"http://creativecommons.org/licenses/by-"
"sa/2.5\">Creative Commons - Attribution / Share Alike</uri> license, an open "
"documentation license. All documentation should also be available in a "
"printer-friendly format. To view this document, append <path>?"
"style=printable</path> to the URL. You'll also find a \"Print\" link on the "
"top-right corner of each document."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(p):32
msgid ""
"If you find bugs in our documentation or have proposals, please use our <uri "
"link=\"http://bugs.gentoo.org\">Bugtracking System</uri> and fill in a bug "
"report for \"Documentation\" or \"Doc Other\". These bug reports will then "
"be handled by the <uri link=\"/proj/en/gdp/\">Gentoo Documentation Project</"
"uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(title):42
msgid "Documentation Repository"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(p):45
msgid ""
"Our documentation is <uri link=\"index.xml#doc_chap2\">categorized</uri> and "
"hierarchically navigable. If you want a quick overview of all available "
"documentation without the additional information, please use our <uri link="
"\"list.xml\">Documentation Listing</uri> page. We also have a <uri link="
"\"list.xml?desc=1\">full listing</uri> of all our documents with their "
"descriptions."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):59
msgid "faq"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):60
msgid "install"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):61
msgid "desktop"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):62
msgid "upgrade"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):63
msgid "gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):64
msgid "sysadmin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):65
msgid "gentoodev"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):66
msgid "project"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//index.xml(catid):67
msgid "other"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//index.xml(None):0
msgid "translator-credits"
msgstr ""
