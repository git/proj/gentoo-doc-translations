msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: 2010-10-21 23:56+0600\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):6
msgid "Gentoo System Monitoring with Nagios"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(mail:link):9
msgid "swift@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(mail):9
msgid "Sven Vermeulen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(abstract):12
msgid ""
"For system and network monitoring, the Nagios tool is a popular choice "
"amongst various free software users. This guide helps you discover Nagios "
"and how you can integrate it with your existing Gentoo infrastructure."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(version):20
msgid "2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(date):21
msgid "2010-03-02"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):24
#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):225
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):26
msgid "System Monitoring"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):29
msgid ""
"Single system users usually don't need a tool to help them identify the "
"state of their system. However, when you have a couple of systems to "
"administer, you will require an overview of your systems' health: do the "
"partitions still have sufficient free space, is your CPU not overloaded, how "
"many people are logged on, are your systems up to date with the latest "
"security fixes, etc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):37
msgid ""
"System monitoring tools, such as the Nagios software we discuss here, offer "
"an easy way of dealing with the majority of metrics you want to know about "
"your system. In larger environments, often called \"enterprise environments"
"\", the tools aggregate the metrics of the various systems onto a single "
"location, allowing for centralized monitoring management."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):48
msgid "About Nagios"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):51
msgid ""
"The <uri link=\"http://www.nagios.org\">Nagios</uri> software is a popular "
"software tool for host, service and network monitoring for Unix (although it "
"can also capture metrics from the Microsoft Windows operating system "
"family). It supports:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):59
msgid ""
"obtaining metrics for local system resources, such as diskspace, CPU usage, "
"memory consumption, ..."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):63
msgid ""
"discovering service availability (such as SSH, SMTP and other protocols),"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):66
msgid ""
"assuming network outages (when a group of systems that are known to be "
"available on a network are all unreachable),"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):72
msgid "and more."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):76
msgid ""
"Basically, the Nagios software consists of a core tool (which manages the "
"metrics), a web server module (which manages displaying the metrics) and a "
"set of plugins (which obtain and send the metrics to the core tool)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):85
msgid "About this Document"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):88
msgid ""
"The primary purpose of this document is to introduce you, Gentoo users, to "
"the Nagios software and how you can integrate it within your Gentoo "
"environment. The guide is not meant to describe Nagios in great detail - I "
"leave this up to the documentation editors of Nagios itself."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):99
msgid "Setting Up Nagios"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):101
#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):121
msgid "Installing Nagios"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):104
msgid ""
"Before you start installing Nagios, draw out and decide which system will "
"become your master Nagios system (i.e. where the Nagios software is fully "
"installed upon and where all metrics are stored) and what kind of metrics "
"you want to obtain. You will not install Nagios on every system you want to "
"monitor, but rather install Nagios on the master system and the TODO on the "
"systems you want to receive metrics from."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):113
msgid "Install the Nagios software on your central server:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(warn):117
msgid ""
"Nagios 3 is currently still ~arch masked, so you first need to unmask it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):121
#, no-wrap
msgid ""
"\n"
"# <i>emerge nagios</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):125
msgid ""
"Follow the instructions the ebuild displays at the end of the installation "
"(i.e. adding <c>nagios</c> to your active runlevel, configuring web server "
"read access and more)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):131
msgid "Really. Read it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):140
msgid "Restricting Access to the Nagios Web Interface"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):143
msgid ""
"The Nagios web interface allows for executing commands on the various "
"systems monitored by the Nagios plugins. For this purpose (and also because "
"the metrics can have sensitive information) it is best to restrict access to "
"the interface."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):149
msgid ""
"For this purpose, we introduce two access restrictions: one on IP level "
"(from what systems can a user connect to the interface) and a basic "
"authentication one (using the username / password scheme)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):155
msgid ""
"First, edit <path>/etc/apache2/modules/99_nagios3.conf</path> and edit the "
"<c>allow from</c> definitions:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):160
msgid "Allow from definitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):160
#, no-wrap
msgid ""
"\n"
"<comment>(Example allowing access from the local host and the local network)</comment>\n"
"Order allow,deny\n"
"Allow from 127.0.0.1 192.168.1.1/24\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):166
msgid ""
"Next, create an Apache authorization table where you define the users who "
"have access to the interface as well as their authorizations. The "
"authentication definition file is called <path>.htaccess</path> and contains "
"where the authentication information itself is stored."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):173
msgid "Example .htaccess file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):173
#, no-wrap
msgid ""
"\n"
"AuthName \"Nagios Access\"\n"
"AuthType Basic\n"
"AuthUserFile /etc/nagios/auth.users\n"
"Require valid-user\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):180
msgid ""
"Place this file inside the <path>/usr/share/nagios/htdocs</path> and <path>/"
"usr/lib/nagios/cgi-bin</path> directories."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):185
msgid ""
"Create the <path>/etc/nagios/auth.users</path> file with the necessary user "
"credentials. By default, the Gentoo nagios ebuild defines a single user "
"called <c>nagiosadmin</c>. Let's create that user first:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):191
msgid "Creating the nagiosadmin user"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):191
#, no-wrap
msgid ""
"\n"
"# <i>htpasswd2 -c /etc/nagios/auth.users nagiosadmin</i>\n"
"<comment>(Apache needs read access to auth.users)</comment>\n"
"# <i>chown nagios:nagios /etc/nagios/auth.users</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):200
msgid "Accessing Nagios"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):203
msgid ""
"Once Nagios and its dependencies are installed, fire up Apache and Nagios:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):207
msgid "Starting the services"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):207
#, no-wrap
msgid ""
"\n"
"# <i>/etc/init.d/nagios start</i>\n"
"# <i>/etc/init.d/apache2 start</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):212
msgid ""
"Next, fire up your browser and connect to <uri link=\"http://localhost/nagios"
"\">http://localhost/nagios</uri>. Log on as the <c>nagiosadmin</c> user and "
"navigate to the <e>Host Detail</e> page. You should be able to see the "
"monitoring states for the local system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):223
msgid "Installing Client Daemons"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):228
msgid "There are various methods available to monitor remote hosts."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):233
msgid ""
"Use the <e>NRPE</e> daemon on the remote host and have Nagios communicate to "
"this daemon using the <path>check_nrpe</path> plugin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):237
msgid "Use a password-less SSH connection to execute the command remotely"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):240
msgid "Trap SNMP events and create Nagios alerts from it"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):245
msgid ""
"We focus on using the NRPE method as it is the most popular one and leave "
"the other methods as an interesting excercise."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):253
msgid "Installing NRPE"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):256
msgid ""
"With NRPE, each remote host runs a daemon (the NRPE deamon) which allows the "
"main Nagios system to query for certain metrics. One can run the NRPE daemon "
"by itself or use an inetd program. I'll leave the inetd method as a nice "
"exercise to the reader and give an example for running NRPE by itself."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):263
msgid "First install the NRPE plugin:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):267
msgid "Installing the NRPE plugin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):267
#, no-wrap
msgid ""
"\n"
"# <i>emerge nagios-nrpe</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):271
msgid ""
"Next, edit <path>/etc/nagios/nrpe.cfg</path> to allow your main Nagios "
"system to access the NRPE daemon and customize the installation to your "
"liking. Another important change to the <path>nrpe.cfg</path> file is the "
"list of commands that NRPE supports. For instance, to use <c>nagios-nrpe</c> "
"version 2.12 with Nagios 3, you'll need to change the paths from <path>/usr/"
"nagios/libexec</path> to <path>/usr/lib/nagios/plugins</path>. Finally, "
"launch the NRPE daemon:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):280
msgid "Launching the NRPE daemon"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):280
#, no-wrap
msgid ""
"\n"
"# <i>/etc/init.d/nrpe start</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):284
msgid ""
"Finally, we need to configure the main Nagios system to connect to this "
"particular NRPE instance and request the necessary metrics. To introduce you "
"to Nagios' object syntax, our next section will cover this a bit more "
"throroughly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):293
msgid "Configuring a Remote Host"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(note):296
msgid ""
"The following hands-on tutorial is an example, used to introduce the user to "
"Nagios' object model. Do not see this as the \"Best Practice\" for "
"configuring Nagios."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):302
msgid ""
"First, edit <path>/etc/nagios/nagios.cfg</path> and place a <c>cfg_dir</c> "
"directive. This will tell Nagios to read in all object configuration files "
"in the said directory - in our example, the directory will contain the "
"definitions for remote systems."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):309
msgid "Editing /etc/nagios/nagios.cfg"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):309
#, no-wrap
msgid ""
"\n"
"cfg_dir=/etc/nagios/objects/remote\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):313
msgid ""
"Create the directory and start with the first file, <path>nrpe-command.cfg</"
"path>. In this file, we configure a Nagios command called <c>check_nrpe</c> "
"which will be used to trigger a plugin (identified by the placeholder <c>"
"$ARG1$</c>) on the remote system (identified by the placeholder <c>"
"$HOSTADDRESS$</c>). The <c>$USER1$</c> variable is a default pointer to the "
"Nagios installation directory (for instance, <path>/usr/nagios/libexec</"
"path>)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):323
msgid "Defining the check_nrpe command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):323
#, no-wrap
msgid ""
"\n"
"# <i>nano /etc/nagios/objects/remote/nrpe-command.cfg</i>\n"
"\n"
"define command {\n"
"  command_name check_nrpe\n"
"  command_line $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$\n"
"}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):332
msgid ""
"Next, create a file <path>nrpe-hosts.cfg</path> where we define the remote "
"host(s) to monitor. In this example, we define two remote systems:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):337
msgid "Defining two remote hosts"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):337
#, no-wrap
msgid ""
"\n"
"# <i>nano /etc/nagios/objects/remote/nrpe-hosts.cfg</i>\n"
"\n"
"define host {\n"
"  use linux-server\n"
"  host_name webber\n"
"  alias Gentoo Linux Web Server\n"
"  address 192.168.2.1\n"
"}\n"
"\n"
"define host {\n"
"  use linux-server\n"
"  host_name isync\n"
"  alias Gentoo Linux RSync server\n"
"  address 192.168.2.2\n"
"}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):355
msgid ""
"Finally, define the service(s) you want to check on these hosts. As a prime "
"example, we run the system load test and disk usage plugins:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre:caption):360
msgid "Define the services to check"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(pre):360
#, no-wrap
msgid ""
"\n"
"# <i>nano /etc/nagios/objects/remote/nrpe-services.cfg</i>\n"
"\n"
"define service {\n"
"  use generic-service\n"
"  host_name webber,isync\n"
"  service_description Current Load\n"
"  check_command check_nrpe!check_load\n"
"}\n"
"\n"
"define service {\n"
"  use generic-service\n"
"  host_name webber,isync\n"
"  service_description Root Partition\n"
"  check_command check_nrpe!check_disk\n"
"}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):378
msgid ""
"That's it. If you now check the service details on the Nagions monitoring "
"site you'll see that the remote hosts are connected and are transmitting "
"their monitoring metrics to the Nagios server."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):387
msgid "Using Passwordless SSH Connection"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):390
msgid ""
"Just as we did by creating the <c>check_nrpe</c> command, we can create a "
"command that executes a command remotely through a passwordless SSH "
"connection. We leave this up as an interesting exercise to the reader."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):396
msgid "A few pointers and tips:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):401
msgid ""
"Make sure the passwordless SSH connection is set up for a dedicated user "
"(definitely not root) - most checks you want to execute do not need root "
"privileges anyway"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):406
msgid ""
"Creating a passwordless SSH key can be accomplished with <c>ssh-keygen</c>, "
"you install a key on the destination system by adding the public key to the "
"<path>.ssh/authorized_keys</path> file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):417
msgid "More Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):419
msgid "Adding Gentoo Checks"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(p):422
msgid ""
"It is quite easy to extend Nagios to include Gentoo-specific checks, such as "
"security checks (GLSAs). Gentoo developer Wolfram Schlich has a "
"<c>check_glsa.sh</c> script <uri link=\"http://dev.gentoo.org/~wschlich/misc/"
"nagios/nagios-plugins-extra/nagios-plugins-extra-4/plugins/\">available</"
"uri> amongst others."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(title):433
msgid "Nagios Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):437
msgid "<uri>http://www.nagios.org</uri>, the official Nagios website"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):440
msgid ""
"<uri>http://www.nagiosexchange.org</uri>, where you can find Nagios plugins "
"and addons"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):444
msgid ""
"<uri>http://www.nagiosforge.org</uri>, where developers host Nagios addon "
"projects"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(li):448
msgid ""
"<uri>http://www.nagioswiki.org</uri>, for more information and tutorials on "
"Nagios"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//nagios-guide.xml(None):0
msgid "translator-credits"
msgstr ""
