msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):7
msgid "Nano Basics Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(author:title):9
msgid "Gentoo Doc Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(author):9
msgid "Sherman Boyd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(author:title):12
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(mail:link):13
msgid "swift@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(mail):13
msgid "Sven Vermeulen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(abstract):16
msgid "This guide is meant to be a simple introduction to nano. It will quickly help you to become familiar with its basic operation."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(version):21
msgid "1.6"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(date):22
msgid "2006-01-05"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):25
msgid "Nano Basics"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):27
msgid "Purpose"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):30
msgid "This guide was written to cover basic operations in nano, and is meant to be very concise. For more information about nano check out:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(uri):36 ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(uri):153
msgid "http://www.nano-editor.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):42
msgid "Opening and creating files"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):45
msgid "Opening and creating files is simple in nano, simply type:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(pre:caption):49
msgid "Creating or opening a file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(pre):49
#, no-wrap
msgid "\n# <i>nano filename</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):53
msgid "Nano is a modeless editor so you can start typing immediately to insert text. If you are editing a configuration file like <path>/etc/fstab</path> use the <c>-w</c> switch, for example:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(pre:caption):59
msgid "Disable the word wrapping"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(pre):59
#, no-wrap
msgid "\n# <i>nano -w /etc/fstab</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(warn):63
msgid "It is very, very important that you use the <c>-w</c> switch when opening a config file. Failure to do so may keep your system from booting or cause other bad things."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):72
msgid "Saving and exiting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):75
msgid "If you want to save the changes you've made, press <c>Ctrl+O</c>. To exit nano, type <c>Ctrl+X</c>. If you ask nano to exit from a modified file, it will ask you if you want to save it. Just press <c>N</c> in case you don't, or <c>Y</c> in case you do. It will then ask you for a filename. Just type it in and press <c>Enter</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):83
msgid "If you accidentally confirmed that you want to save the file but you actually don't, you can always cancel by pressing <c>Ctrl+C</c> when you're prompted for a filename."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):92
msgid "Cutting and pasting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):95
msgid "To cut a single line, you use <c>Ctrl+K</c> (hold down <c>Ctrl</c> and then press <c>K</c>). The line disappears. To paste it, you simply move the cursor to where you want to paste it and punch <c>Ctrl+U</c>. The line reappears. To move multiple lines, simply cut them with several <c>Ctrl+K</c>s in a row, then paste them with a single <c>Ctrl+U</c>. The whole paragraph appears wherever you want it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):104
msgid "If you need a little more fine-grained control, then you have to mark the text. Move the cursor to the beginning of the text you want to cut. Hit <c>Ctrl+6</c> (or <c>Alt+A</c>). Now move your cursor to the end of the text you want to cut: the marked text gets highlighted. If you need to cancel your text marking, simply hit <c>Ctrl+6</c> again. Press <c>Ctrl+K</c> to cut the marked text. Use <c>Ctrl+U</c> to paste it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):116
msgid "Searching for text"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):119
msgid "Searching for a string is easy as long as you think <e>\"WhereIs\"</e> instead of <e>\"Search\"</e>. Simply hit <c>Ctrl+W</c>, type in your search string, and press <c>Enter</c>. To search for the same string again, hit <c>Alt+W</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(note):125
msgid "In nano's help texts the Ctrl-key is represented by a caret (<c>^</c>), so <c>Ctrl+W</c> is shown as <c>^W</c>, and so on. The Alt-key is represented by an <c>M</c> (from \"Meta\"), so <c>Alt+W</c> is shown as <c>M-W</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):134
msgid "More options"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):137
msgid "If you're interested in tweaking nano, be sure to read <path>/etc/nanorc</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(title):144
msgid "Wrap up"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(p):147
msgid "That is all! Thanks to kiyose and quazion from #gentoo. Again, for more information about nano check out:"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//nano-basics-guide.xml(None):0
msgid "translator-credits"
msgstr ""

