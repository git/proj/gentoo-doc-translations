msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(guide:redirect):5
msgid "/proj/en/apache/doc/upgrading.xml"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(title):6
msgid "Troubleshooting Apache"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(mail):9
msgid "neysx"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(abstract):12
msgid "This document describes the procedure end-users should follow to safely upgrade their apache installation."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(version):21
msgid "3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(date):22
msgid "2008-03-23"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(title):25
msgid "Moved"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(p):29
msgid "See <uri>/proj/en/apache/doc/upgrading.xml</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//apache-upgrading.xml(None):0
msgid "translator-credits"
msgstr ""

