msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):8
msgid "Gentoo Sudo(ers) Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(author:title):10
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(mail:link):11
msgid "swift@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(mail):11
msgid "Sven Vermeulen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(abstract):14
msgid "When you want some people to perform certain administrative steps on your system without granting them total root access, using sudo is your best option. With sudo you can control who can do what. This guide offers you a small introduction to this wonderful tool."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(version):25
msgid "2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(date):26
msgid "2011-08-13"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):29
msgid "About Sudo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):31
msgid "Granting Permissions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):34
msgid "The <c>app-admin/sudo</c> package allows the system administrator to grant permission to other users to execute one or more applications they would normally have no right to. Unlike using the <e>setuid</e> bit on these applications <c>sudo</c> gives a more fine-grained control on <e>who</e> can execute a certain command and <e>when</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):42
msgid "With <c>sudo</c> you can make a clear list <e>who</e> can execute a certain application. If you would set the setuid bit, any user would be able to run this application (or any user of a certain group, depending on the permissions used). You can (and probably even should) require the user to provide a password when he wants to execute the application."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):53
msgid "Logging Activity"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):56
msgid "One additional advantage of <c>sudo</c> is that it can log any attempt (successful or not) to run an application. This is very useful if you want to track who made that one fatal mistake that took you 10 hours to fix :)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):65
msgid "Configuring Sudo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):68
msgid "The <c>sudo</c> configuration is managed by the <path>/etc/sudoers</path> file. This file should never be edited through <c>nano&nbsp;/etc/sudoers</c> or <c>vim&nbsp;/etc/sudoers</c> or any other editor you might like. When you want to alter this file, you should use <c>visudo</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):75
msgid "This tool makes sure that no two system administrators are editing this file at the same time, preserves the permissions on the file and performs some syntax checking to make sure you make no fatal mistakes in the file."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):84
msgid "About this Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):87
msgid "This guide is meant as a quick introduction. The <c>sudo</c> package is a lot more powerful than what is described in this guide. It has special features for editing files as a different user (<c>sudoedit</c>), running from within a script (so it can background, read the password from standard in instead of the keyboard, ...), etc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):95
msgid "Please read the <c>sudo</c> and <c>sudoers</c> manual pages for more information."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):105
msgid "Sudoers Syntax"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):107
msgid "Basic Syntax"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):110
msgid "The most difficult part of <c>sudo</c> is the <path>/etc/sudoers</path> syntax. The basic syntax is like so:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):115
msgid "Basic /etc/sudoers syntax"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):115
#, no-wrap
msgid "\nuser  host = commands\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):119
msgid "This syntax tells <c>sudo</c> that the user, identified by <e>user</e> and logged in on the system <e>host</e> can execute any of the commands listed in <e>commands</e> as the root user. A more real-life example might make this more clear: allow the user <e>swift</e> to execute <c>emerge</c> if he is logged in on localhost:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):127
msgid "Live /etc/sudoers examples"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):127
#, no-wrap
msgid "\nswift  localhost = /usr/bin/emerge\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(note):131
msgid "The hostname must match what the <c>hostname</c> command returns."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):135
msgid "A <brite>big warning</brite> is in place though: do not allow a user to run an application that can allow people to elevate privileges. For instance, allowing users to execute <c>emerge</c> as root can indeed grant them full root access to the system because <c>emerge</c> can be manipulated to change the live file system to the user's advantage. If you do not trust your <c>sudo</c> users, don't grant them any rights."
msgstr ""

#. Wrappers are no real advantage here either, see #71750
#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):148
msgid "The user name can also be substituted with a group name - in this case you should start the group name with a <c>%</c> sign. For instance, to allow any one in the <c>wheel</c> group to execute <c>emerge</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):154
msgid "Allowing the wheel group members to execute emerge"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):154
#, no-wrap
msgid "\n%wheel  localhost = /usr/bin/emerge\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):158
msgid "You can extend the line to allow for several commands (instead of making a single entry for each command). For instance, to allow the same user to not only run <c>emerge</c> but also <c>ebuild</c> and <c>emerge-webrsync</c> as root:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):164
msgid "Multiple commands"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):164
#, no-wrap
msgid "\nswift  localhost = /usr/bin/emerge, /usr/bin/ebuild, /usr/sbin/emerge-webrsync\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):168
msgid "You can also specify a precise command and not only the tool itself. This is useful to restrict the use of a certain tool to a specified set of command options. The <c>sudo</c> tool allows shell-style wildcards (AKA meta or glob characters) to be used in pathnames as well as command line arguments in the sudoers file. Note that these are <e>not</e> regular expressions."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):176
msgid "Let us put this to the test:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):180
msgid "Attempt to update the system using sudo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):180
#, no-wrap
msgid "\n$ <i>sudo emerge -uDN world</i>\n\nWe trust you have received the usual lecture from the local System\nAdministrator. It usually boils down to these three things:\n\n    #1) Respect the privacy of others.\n    #2) Think before you type.\n    #3) With great power comes great responsibility.\n\nPassword: <comment>(Enter the user password, not root!)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):193
msgid "The password that <c>sudo</c> requires is the user's own password. This is to make sure that no terminal that you accidentally left open to others is abused for malicious purposes."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):199
msgid "You should know that <c>sudo</c> does not alter the <c>${PATH}</c> variable: any command you place after <c>sudo</c> is treated from <e>your</e> environment. If you want the user to run a tool in for instance <path>/sbin</path> he should provide the full path to <c>sudo</c>, like so:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):206
msgid "Using the full path to a tool"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):206
#, no-wrap
msgid "\n$ <i>sudo /usr/sbin/emerge-webrsync</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):213
msgid "Using Aliases"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):216
msgid "In larger environments having to enter all users over and over again (or hosts, or commands) can be a daunting task. To ease the administration of <path>/etc/sudoers</path> you can define <e>aliases</e>. The format to declare aliases is quite simple:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):223
msgid "Declaring aliases in /etc/sudoers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):223
#, no-wrap
msgid "\nHost_Alias hostalias = hostname1, hostname2, ...\nUser_Alias useralias = user1, user2, ...\nCmnd_Alias cmndalias = command1, command2, ...\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):229
msgid "One alias that always works, for any position, is the <c>ALL</c> alias (to make a good distinction between aliases and non-aliases it is recommended to use capital letters for aliases). As you might undoubtedly have guessed, the <c>ALL</c> alias is an alias to all possible settings."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):236
msgid "A sample use of the <c>ALL</c> alias to allow <e>any</e> user to execute the <c>shutdown</c> command if he is logged on locally is:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):241
msgid "Allowing any user to execute shutdown"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):241
#, no-wrap
msgid "\nALL  localhost = /sbin/shutdown\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):245
msgid "Another example is to allow the user <c>swift</c> to execute the <c>emerge</c> command as root, regardless of where he is logged in from:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):250
msgid "Allowing a user to run an application regardless of his location"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):250
#, no-wrap
msgid "\nswift   ALL = /usr/bin/emerge\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):254
msgid "More interesting is to define a set of users who can run software administrative applications (such as <c>emerge</c> and <c>ebuild</c>) on the system and a group of administrators who can change the password of any user, except root!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):260
msgid "Using aliases for users and commands"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):260
#, no-wrap
msgid "\nUser_Alias  <i>SOFTWAREMAINTAINERS</i> = swift, john, danny\nUser_Alias  <i>PASSWORDMAINTAINERS</i> = swift, sysop\nCmnd_Alias  <i>SOFTWARECOMMANDS</i>    = /usr/bin/emerge, /usr/bin/ebuild\nCmnd_Alias  <i>PASSWORDCOMMANDS</i>    = /usr/bin/passwd [a-zA-Z0-9_-]*, !/usr/bin/passwd root\n\n<i>SOFTWAREMAINTAINERS</i>  localhost = <i>SOFTWARECOMMANDS</i>\n<i>PASSWORDMAINTAINERS</i>  localhost = <i>PASSWORDCOMMANDS</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):273
msgid "Non-Root Execution"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):276
msgid "It is also possible to have a user run an application as a different, non-root user. This can be very interesting if you run applications as a different user (for instance <c>apache</c> for the web server) and want to allow certain users to perform administrative steps as that user (like killing zombie processes)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):283
msgid "Inside <path>/etc/sudoers</path> you list the user(s) in between <c>(</c>&nbsp;and&nbsp;<c>)</c> before the command listing:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):288
msgid "Non-root execution syntax"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):288
#, no-wrap
msgid "\nusers  hosts = (run-as) commands\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):292
msgid "For instance, to allow <c>swift</c> to run the <c>kill</c> tool as the <c>apache</c> or <c>gorg</c> user:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):297
msgid "Non-root execution example"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):297
#, no-wrap
msgid "\nCmnd_Alias KILL = /bin/kill, /usr/bin/pkill\n\nswift   ALL = (apache, gorg) KILL\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):303
msgid "With this set, the user can run <c>sudo&nbsp;-u</c> to select the user he wants to run the application as:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):308
msgid "Running pkill as the apache user"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):308
#, no-wrap
msgid "\n$ <i>sudo -u apache pkill apache</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):312
msgid "You can set an alias for the user to run an application as using the <c>Runas_Alias</c> directive. Its use is identical to the other <c>_Alias</c> directives we have seen before."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):321
msgid "Passwords and Default Settings"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):324
msgid "By default, <c>sudo</c> asks the user to identify himself using his own password. Once a password is entered, <c>sudo</c> remembers it for 5 minutes, allowing the user to focus on his tasks and not repeatedly re-entering his password."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):331
msgid "Of course, this behavior can be changed: you can set the <c>Defaults:</c> directive in <path>/etc/sudoers</path> to change the default behavior for a user."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):337
msgid "For instance, to change the default 5 minutes to 0 (never remember):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):341
msgid "Changing the timeout value"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):341
#, no-wrap
msgid "\nDefaults:swift  timestamp_timeout=0\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):345
msgid "A setting of <c>-1</c> would remember the password indefinitely (until the system reboots)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):350
msgid "A different setting would be to require the password of the user that the command should be run as and not the users' personal password. This is accomplished using <c>runaspw</c>. In the following example we also set the number of retries (how many times the user can re-enter a password before <c>sudo</c> fails) to <c>2</c> instead of the default 3:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):358
msgid "Requiring the root password instead of the user's password"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):358
#, no-wrap
msgid "\nDefaults:john   runaspw, passwd_tries=2\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):362
msgid "Another interesting feature is to keep the <c>DISPLAY</c> variable set so that you can execute graphical tools:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):367
msgid "Keeping the DISPLAY variable alive"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):367
#, no-wrap
msgid "\nDefaults:john env_keep=DISPLAY\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):371
msgid "You can change dozens of default settings using the <c>Defaults:</c> directive. Fire up the <c>sudo</c> manual page and search for <c>Defaults</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):376
msgid "If you however want to allow a user to run a certain set of commands without providing any password whatsoever, you need to start the commands with <c>NOPASSWD:</c>, like so:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):382
msgid "Allowing emerge to be ran as root without asking for a password"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):382
#, no-wrap
msgid "\nswift     localhost = NOPASSWD: /usr/bin/emerge\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):391
msgid "Using Sudo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):393
msgid "Listing Privileges"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):396
msgid "To inform yourself what your capabilities are, run <c>sudo&nbsp;-l</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):400
msgid "Listing capabilities"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):400
#, no-wrap
msgid "\n$ <i>sudo -l</i>\nUser swift may run the following commands on this host:\n    (root)   /usr/libexec/xfsm-shutdown-helper\n    (root)   /usr/bin/emerge\n    (root)   /usr/bin/passwd [a-zA-Z0-9_-]*\n    (root)   !/usr/bin/passwd root\n    (apache) /usr/bin/pkill\n    (apache) /bin/kill\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):411
msgid "If you have any command in <path>/etc/sudoers</path> that does not require you to enter a password, it will not require a password to list the entries either. Otherwise you might be asked for your password if it isn't remembered."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(title):420
msgid "Prolonging the Password Timeout"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):423
msgid "By default, if a user has entered his password to authenticate himself to <c>sudo</c>, it is remembered for 5 minutes. If the user wants to prolong this period, he can run <c>sudo&nbsp;-v</c> to reset the time stamp so that it will take another 5 minutes before <c>sudo</c> asks for the password again."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre:caption):430
msgid "Prolonging the password timeout"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(pre):430
#, no-wrap
msgid "\n$ <i>sudo -v</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(p):434
msgid "The inverse is to kill the time stamp using <c>sudo&nbsp;-k</c>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//sudo-guide.xml(None):0
msgid "translator-credits"
msgstr ""

