msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(guide:lang):5
msgid "en"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):6
msgid "Gentoo Info Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(author:title):8 ../../gentoo/xml/htdocs/doc/en//info-guide.xml(mail):9
msgid "Chris White"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(mail:link):9
msgid "chriswhite@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(abstract):12
msgid "This guide is meant to show how to navigate info pages using the info command."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(version):20
msgid "1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(date):21
msgid "2006-03-28"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):24
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):26
msgid "What is info?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):29
msgid "Most of you may be familiar with the <c>man</c> documentation system. While man is good with quickly looking up items, it lacks structure in linking man pages together. This is where <c>info</c> comes in. Info pages are made using the <c>texinfo</c> tools, and can link with other pages, create menus and ease navigation in general. The next section will look at how info pages are laid out."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):41
msgid "Info pages layout"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):44
msgid "The main info pages are held in <path>/usr/share/info</path>. Unlike the man style directory layout, <path>/usr/share/info</path> contains what is largely a rather extensive collection of files. These files have the following format:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):50
msgid "info file format"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):50
#, no-wrap
msgid "\npagename.info[-node].gz\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):54
msgid "<c>pagename</c> is the actual name of the page (example: <c>wget</c>). <c>[-node]</c> is an optional construct that designates another node level (generally these are referenced to by the toplevel of the info document in question). In order to save space these info pages are compressed using the <c>gzip</c> compression scheme. Additional info pages can be listed with the <c>INFOPATH</c> environment variable (usually set through the various <path>/etc/env.d/</path> files). To get started, it's important to note the <path>/usr/share/info/dir</path> file. This special file is used when info is ran with no parameters. It contains a listing of all info pages available for users to browse. To begin looking at navigating around in info, we'll go ahead and bring it up with no arguments:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):68
msgid "Starting up info"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):68
#, no-wrap
msgid "\n$ <i>info</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):72
msgid "Now in the next chapter we'll look at dealing with basic info navigation."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):80
msgid "Working with info pages"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):82
msgid "Browsing with menus"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):85
msgid "Now that info is started, we're given a screen similar to this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):89
msgid "Sample info screen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):89
#, no-wrap
msgid "\nFile: dir,      Node: Top       This is the top of the INFO tree\n\n  This (the Directory node) gives a menu of major topics.\n  Typing \"q\" exits, \"?\" lists all Info commands, \"d\" returns here,\n  \"h\" gives a primer for first-timers,\n  \"mEmacs&lt;Return&gt;\" visits the Emacs manual, etc.\n\n  In Emacs, you can click mouse button 2 on a menu item or cross reference\n  to select it.\n\n* Menu:\n\nUser Interface Toolkit\n* GDK: (gdk).           The General Drawing Kit\n* GTK: (gtk).           The GIMP Toolkit\n\nGNU programming tools\n* Autoconf v2.1: (autoconf).         Create source code configuration scripts.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):110
msgid "Right now there are a bunch of entries with an asterisk before them. These are menu items for navigating through different node levels. There are two ways of selecting menus. We'll look at the first now and the other way later. First off, we'll go ahead and look at the <c>wget</c> info page. To do so, use the down arrow key until you reach the area indicated by the blue highlighting:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):118
msgid "Navigating to the wget info menu entry"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):118
#, no-wrap
msgid "\nNetwork Applications\n* GnuTLS: (gnutls).                     Package for Transport Layer Security.\n* <i>Wget: (wget).</i>         The non-interactive network downloader.\n* certtool: (gnutls)Invoking certtool.  Manipulate certificates and keys.\n* gnutls-cli: (gnutls)Invoking gnutls-cli.      GNU TLS test client.\n* gnutls-cli-debug: (gnutls)Invoking gnutls-cli-debug.  GNU TLS debug client.\n* gnutls-serv: (gnutls)Invoking gnutls-serv.    GNU TLS test server.\n* srptool: (gnutls)Invoking srptool.    Simple SRP password tool.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):129
msgid "Once you get to this area, hit the <c>ENTER</c> key to select the menu item. This will bring up the info page for <c>wget</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):134
msgid "The wget info page"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):134
#, no-wrap
msgid "\nFile: wget.info,  Node: Top,  Next: Overview,  Up: (dir)\n\nWget 1.10.2\n***********\n\nThis manual documents version 1.10.2 of GNU Wget, the freely available\nutility for network downloads.\n\n   Copyright (C) 1996-2005 Free Software Foundation, Inc.\n\n* Menu:\n\n* Overview::            Features of Wget.\n* Invoking::            Wget command-line arguments.\n* Recursive Download::  Downloading interlinked pages.\n* Following Links::     The available methods of chasing links.\n* Time-Stamping::       Mirroring according to time-stamps.\n* Startup File::        Wget's initialization file.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):155
msgid "Now that we have an info page up, the next section will look at basic navigation."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):163
msgid "Basic navigation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):166
msgid "In terms of nodes, this is considered the <c>Top</c> node for the wget page. Consider the <c>Top</c> node to be the same as the table of contents for that particular info page. Now to navigate the actual page itself, you have a couple of different methods. First off is the standard info method. This is using the <c>SPACE</c> key to move forward a page and the <c>BACKSPACE/DELETE</c> keys to move back a page. This is the recommended method as it automatically advances/retreats to the appropriate node in the document. This allows for a somewhat linear browsing for those used to man pages. Another way is through the <c>PAGE UP/PAGE DOWN</c> keys. These work, but they will not advance/retreat like <c>SPACE/BACKSPACE/DELETE</c> will. If you want to skip entire nodes without using <c>SPACE/BACKSPACE/DELETE</c>, you can also use the <c>[</c> (advance backwards) and <c>]</c> (advance forwards) keys."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):181
msgid "As mentioned earlier, there are 2 ways of navigating menus. The other way will now be described here. The numbers <c>1-9</c> can be used to reference to the first-ninth menu entries in a document. This can be used to quickly peruse through documents. For example, we'll use <c>3</c> to reach the <c>Recursive Download</c> menu entry. So press <c>3</c> and it will bring up the <c>Recursive Download</c> screen:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):190
msgid "Resulting Recursive Download screen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):190
#, no-wrap
msgid "\nFile: wget.info,  Node: Recursive Download,  Next: Following Links,  Prev: Invoking,  Up: Top\n\n3 Recursive Download\n********************\n\nGNU Wget is capable of traversing parts of the Web (or a single HTTP or\nFTP server), following links and directory structure.  We refer to this\nas to \"recursive retrieval\", or \"recursion\".\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):201
msgid "Now we're at the <c>Recursive Download</c> screen. Here is a good time to note a few things. First off the top header section. This header shows the navigation capable from this particular screen. The page indicated by <c>Next: </c> can be accessed by pressing the <c>n</c> key, and the page indicated by <c>Prev: </c> can be accessed by pressing the <c>p</c> key. Please note that this will only work for the same level. If overused you could round up in totally unrelated content. It's better to use <c>SPACE/BACKSPACE/DELETE/[/]</c> to navigate in a linear fashion."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):212
msgid "If for some reason you get lost, there are a few ways to get out. First is the <c>t</c> key. This will take you straight to the toplevel (table of contents) for the particular info page you're browsing. If you want to return to the last page you looked out, you can do so with the <c>l</c> key. If you want to go to the above level, you can do so with the <c>u</c> key. Now that you have some idea of navigating a page, the next chapter will look at searching for content."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):226
msgid "Searching through info"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):228
msgid "Navigating to other info pages"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):231
msgid "Now that you can navigate an individual info page, it's important to look at accessing other info pages. The first obvious way is to go to the info page through the <c>dir</c> index listing of info pages. To get to the <c>dir</c> index from deep within a document, simply press the <c>d</c> key. From there you can search for the appropriate page you want. However, if you know the actual page, there is an easier way through the <c>Goto node (g key)</c> command. To go to an info page by name, type <c>g</c> to bring up the prompt and enter the name of the page in parentheses:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):242
msgid "Going to an info page by name"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):242
#, no-wrap
msgid "\n* Startup File::        Wget's initialization file.\n* Examples::            Examples of usage.\n* Various::             The stuff that doesn't fit anywhere else.\n* Appendices::          Some useful references.\n* Copying::             You may give out copies of Wget and of this manual.\n--zz-Info: (wget.info.gz)Top, 24 lines --Top-------------------------------\nGoto node: <i>(libc)</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):252
msgid "This will bring up the libc page as shown here:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):256
msgid "Result of the Goto node command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):256
#, no-wrap
msgid "\nFile: libc.info,  Node: Top,  Next: Introduction,  Prev: (dir),  Up: (dir)\n\nMain Menu\n*********\n\nThis is Edition 0.10, last updated 2001-07-06, of `The GNU C Library\nReference Manual', for Version 2.3.x of the GNU C Library.\n\n* Menu:\n\n* Introduction::                 Purpose of the GNU C Library.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):270
msgid "Now that we know how to go to info pages by name, the next section will look at searching for pieces of information using the info page's index."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):278
msgid "Searching using an index"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):281
msgid "In this example we'll see how to lookup the <c>printf</c> function of the c library using the <c>libc</c> info page's index. You should still be at the libc info page from the last section, and if not, use the Goto node command to do so. To utilize the index search, hit the <c>i</c> key to bring up the prompt, then enter your search term. We'll do so for <c>printf</c> below:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):289
msgid "Entering an index search query"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):289
#, no-wrap
msgid "\n* Character Set Handling::       Support for extended character sets.\n* Locales::                      The country and language can affect the\n                                   behavior of library functions.\n* Message Translation::          How to make the program speak the user's\n                                   language.\n--zz-Info: (libc.info.gz)Top, 1291 lines --Top-- Subfile: libc.info-1.gz-----\nIndex entry: <i>printf</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):299
msgid "After pressing enter upon completion of our query, we're brought to the <c>libc</c> definition for <c>printf</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):304
msgid "Result of the index search query"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):304
#, no-wrap
msgid "\nFile: libc.info,  Node: Formatted Output Functions,  Next: Dynamic Output,  Prev: Other Output Conversions,  Up: Formatted Output\n\n12.12.7 Formatted Output Functions\n----------------------------------\n\nThis section describes how to call <i>`printf'</i> and related functions.\nPrototypes for these functions are in the header file `stdio.h'.\nBecause these functions take a variable number of arguments, you _must_\ndeclare prototypes for them before using them.  Of course, the easiest\nway to make sure you have all the right prototypes is to just include\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):317
msgid "We've now successfully performed a search using the <c>libc</c> info page index. However, sometimes what we want is in the page itself. The next section will look at performing searches within the page."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):326
msgid "Searching using the search command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):329
msgid "Starting from the previous location at the <c>Formatted Output Functions</c> node, we'll look at searching for the <c>sprintf</c> variation of the <c>printf</c> function. To perform a search, press the <c>s</c> key to bring up the search prompt, and then enter the query (sprintf in this case):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):336
msgid "Entering a search query"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):336
#, no-wrap
msgid "\n -- Function: int wprintf (const wchar_t *TEMPLATE, ...)\n     The `wprintf' function prints the optional arguments under the\n     control of the wide template string TEMPLATE to the stream\n     `stdout'.  It returns the number of wide characters printed, or a\n--zz-Info: (libc.info.gz)Formatted Output Functions, 127 lines --Top-- Subfile: libc.info-3.gz--\nSearch for string []: <i>sprintf</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):345
msgid "Hit <c>ENTER</c> and it will show the result of the query:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre:caption):349
msgid "Result of the search query"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(pre):349
#, no-wrap
msgid "\n-- Function: int <i>sprintf</i> (char *S, const char *TEMPLATE, ...)\n     This is like `printf', except that the output is stored in the\n     character array S instead of written to a stream.  A null\n     character is written to mark the end of the string.\n\n     The `sprintf' function returns the number of characters stored in\n     the array S, not including the terminating null character.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):359
msgid "And we have the function we need."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):368 ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):370
msgid "Conclusion"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):373
msgid "This concludes the overview of using info to view info pages. As always comments are both welcome and appreciated. Clicking on the my name (Chris White) on the right side will send me an email."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(title):382
msgid "Additional Program Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):385
msgid "In order to make things easier for those that wish to browse info pages through a more friendly graphical interface, the following are available:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(li):391
msgid "app-text/info2html - Convert info pages to a browse-able HTML format"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(li):392
msgid "app-text/pinfo - <c>ncurses</c> based info viewer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(li):393
msgid "app-text/tkinfo - a <c>tcl/tk</c> based info browser"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(li):394
msgid "app-vim/info - a <c>vim</c> based info browser"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(p):397
msgid "The <c>KDE</c> browser <c>Konqueror</c> also allows you to browse info pages through the <c>info: </c> URI."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//info-guide.xml(None):0
msgid "translator-credits"
msgstr ""

