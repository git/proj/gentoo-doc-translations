# Azamat H. Hackimov <azamat.hackimov@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: 2010-02-09 01:03+0500\n"
"Last-Translator: Azamat H. Hackimov <azamat.hackimov@gmail.com>\n"
"Language-Team: Russian <gentoo-doc-ru@gentoo.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.0\n"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):6
msgid "Gentoo Top-Level Management Structure"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(author:title):8
msgid "Author"
msgstr "автор"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(mail:link):9
msgid "drobbins@gentoo.org"
msgstr "drobbins@gentoo.org"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(mail):9
msgid "Daniel Robbins"
msgstr "Daniel Robbins"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(author:title):11
msgid "Editor"
msgstr "редактор"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(mail:link):12
msgid "pauldv@gentoo.org"
msgstr "pauldv@gentoo.org"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(mail):12
msgid "Paul de Vrieze"
msgstr "Paul de Vrieze"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(abstract):16
msgid "Gentoo top-level management structure (work in progress)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(version):22
msgid "1.4"
msgstr "1.4"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(date):23
msgid "2003-07-20"
msgstr "2003-07-20"

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):27
msgid "Gentoo top-level management structure"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(brite):33
msgid ""
"As of May 2005, this document is no longer valid but kept for historical "
"purposes."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):40
msgid "What was the purpose of the new management structure?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):43
msgid ""
"The purpose of the new management structure was to solve chronic management, "
"coordination and communication issues in the Gentoo project. In particular, "
"we had no clearly defined top-level management structure, and no official, "
"regular meetings to communicate status updates between developers serving in "
"critical roles. In general, most communication took place on irc and "
"irregularly via email. There was also little to no accountability, even at a "
"high level, to complete projects on time."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):53
msgid ""
"Because of this prior state of affairs, it was difficult to set goals and "
"track the status of projects. This lack of communication and coordination "
"also made it difficult for top-level developers to manage their own "
"projects. In addition, we had other chronic problem of not having clearly-"
"defined roles and scopes of executive decision-making authority for top-"
"level developers, which resulted in many top-level developers doubting that "
"they even have the authority to manage their own projects and sub-projects. "
"While this had *never* been the intention of top-level developers, it is the "
"unfortunate result of an unstructured development process: no one knew what "
"was going on, and everyone deferred to the Chief Architect for all executive "
"decisions."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):67
msgid ""
"Clearly, a plan was needed to swiftly and permanently address these issues "
"by increasing communication, coordination, and accountability. Roles and "
"scopes of executive decision-making authority needed to be defined for top "
"developers so that they have a clear mandate as well as accountability to "
"manage their projects and thus ensure their projects completed their "
"appointed work efficiently and on-schedule."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):79
msgid "Initial implementation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):82
msgid ""
"Initial implementation of the new management structure began by creating "
"official top-level management structure. This management structure consists "
"of the Chief Architect and a group of developers that will be given the "
"title of Top-level Managers. Top-level Managers will be accountable for the "
"projects they manage, and be responsible for communicating the status of "
"their projects to the rest of the Top-level Managers and Chief Architect, as "
"well as other responsibilities detailed later in this document."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):92
msgid ""
"All the top-level projects in the Gentoo project are in the process of being "
"clearly defined, including goals, sub-projects, members, roadmap and "
"schedules. The \"Hardened Gentoo\" page at <uri>http://www.gentoo.org/proj/"
"en/hardened/</uri> is an excellent example of such a top-level project "
"definition, and many other top-level projects currently exist today."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):101
msgid ""
"Certain executive decision-making authority will be granted to these "
"projects, as agreed upon by the Chief Architect, Top-level Managers and "
"project members. Then, as part of the initial implementation of the new "
"management structure, a Top-level Manager or managers will be officially "
"adopt projects and ebuild herds. These managers will be responsible for "
"tracking the status of the project, ensuring that the project meets targets "
"and is generally managed properly. Manager responsibilities are described in "
"detail later in this document."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):115
msgid "Management team"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):118
msgid ""
"The current top-level management team is as follows (in no particular order):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):123
msgid "Seemant Kulleen (seemant)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):124
msgid "Jay Pfeifer (pfeifer)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):125
msgid "Joshua Brindle (method)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):126
msgid "Kurt Lieber (klieber)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):127
msgid "Pieter Van den Abeele (pvdabeel)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):128
msgid "Jon Portnoy (avenj)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):129
msgid "Paul de Vrieze (pauldv)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):130
msgid "Sven Vermeulen (swift)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):136
msgid "Management charter"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):139
msgid ""
"1) Constructive, professional communication: All communication should be "
"focused on improving the management of Gentoo projects, should be "
"constructive in nature and should be shared in a friendly, professional "
"manner."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):146
msgid ""
"2) Accountability to peers: Allow fellow members of this list to hold us "
"accountable to follow-through on projects and meet deadlines. Keep fellow "
"members accountable."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):152
msgid ""
"3) Management of projects: empower managers to have the authority and "
"strategic direction necessary to properly manage thier projects and efforts "
"to ensure projects complete their appointed work on time."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):158
msgid ""
"4) Results: our expectation is that our efforts, when properly executed, "
"will result in the Gentoo project's ability to meet deadlines, have much "
"better communication and coordination throughout the entire project, higher "
"overall quality and a more positive experience for all."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):168
msgid "Manager responsibilities"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):171
msgid ""
"Every top-level Gentoo project will have a clearly defined scope, and "
"clearly defined and explicit executive decision-making authority that will "
"be granted to managers of the project to exercise and/or delegate as they "
"see fit. Both the scope and any necessary decision-making authority must be "
"agreed upon by both the Chief Architect and project members. The scope and "
"executive authority of a project can be expanded over time as required as "
"approved by the Top-level Managers and Chief Architect."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):181
msgid ""
"In addition to decision-making authority, managers have the following "
"responsibilities:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):187
msgid ""
"Keep a complete list of all projects and efforts you are managing, and "
"associated gentoo.org project page up-to-date"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):192
msgid ""
"Manage and track the status of these efforts. This includes active direction "
"as well as passive tracking of progress."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):197
msgid ""
"Define clear goals, roadmaps and timelines (preliminary if necessary) for "
"every effort."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):202
msgid "Proactively identify efforts that have problems and need help."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):204
msgid ""
"Ensure that your efforts are completed on-time, or that any efforts that are "
"behind-schedule are reported in a timely manner."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):209
msgid ""
"Remain focused. Make sure that you are not managing more than you can handle."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):214
msgid ""
"Fulfill formal communication and coordination responsibilities required by "
"top-level managers (weekly meetings, etc.)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):219
msgid ""
"Fulfill formal communication and coordination responsibilities required by "
"individual efforts (project meetings, communication with project members, "
"etc.) This is important as our management of projects means that we have the "
"responsibility not only to communicate with our peers but also those who we "
"are managing. This communication should be frequent, have a formal component "
"(planned meetings, official status updates, etc.) and model good management "
"practices to members of our teams."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(li):229
msgid ""
"<b>RECURSIVE FUNCTIONALITY</b>: At an appropriate time, implement these "
"management practices for *sub*-projects (define managers, clear sub-project "
"goals, grant executive authority) with you serving as primary authority."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):239
msgid "Meetings, logs and status updates"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):242
msgid ""
"Currently, weekly manager meetings are held on irc, and all Gentoo "
"developers are welcome to join the meeting channel and view the meeting in "
"progress. In addition, after the meeting completes, the floor is opened for "
"questions and comments by developers. The weekly meetings generally take "
"place on Monday. After the meeting and open-floor session is concluded, a "
"log of the meeting is posted to the gentoo-core mailing list."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):251
msgid ""
"Status updates are very important to the proper functioning of our "
"management structure. Currently, managers make an effort to email each other "
"weekly status updates. Soon, we hope to post weekly status updates to our "
"respective project pages so that the public can be apprised on the status of "
"every top-level project."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(note):259
msgid ""
"Inability to attend due to time zone can be addressed by posting the full "
"IRC log to gentoo-managers and allowing non-attending members to post ideas, "
"comments and follow-ups."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):268
msgid "Decision-making"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):271
msgid ""
"Currently, GLEPs (Gentoo Linux Enhancement Proposals) can be approved or "
"rejected by the appropriate top-level manager under which the GLEP falls. If "
"there is no clearly-defined manager under which the GLEP falls, the GLEP "
"will be voted upon by the Managers and Chief Architect, and must be approved "
"unanimously. In all cases, a public, written explanation must be provided "
"detailing why the GLEP was approved or rejected, either by the manager who "
"approved/rejected it, or the head of the GLEP sub-project (Grant Goodyear) "
"if the GLEP was voted upon by the management team. This summary is meant to "
"reflect the decision that was made by some of the managers at an early "
"manager meeting."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):283
msgid ""
"Currently, there is no formal general voting procedure in place. In the "
"interim, any item to be voted upon must be approved by \"votable\" by the "
"Chief Architect. Before voting takes place, all managers must have an "
"opportunity to present their ideas before the other managers, with the "
"general originator(s) of the idea having the opportunity to present first. "
"After that, the Chief Architect and Managers can present their ideas, with "
"the Chief Architect having the opportunity to present last. After this has "
"happened, voting can take place, and the item will be approved on an "
"unanimous vote. Managers or the Chief Architect can choose to abstain from "
"voting, and the vote can still pass with abstainers as long as at least 50% "
"of the members have voted. The voting must take place at an official "
"managers meeting. Non-attending managers are allowed to vote via email. The "
"vote must be officially tallied and posted to the managers list."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):298
msgid ""
"The reason for the \"Chief Architect approval\" clause it to prevent the "
"voting process from being abused by allowing voting items that make no "
"sense, such as those that begin with a \"Should we continue to,\" where a "
"\"nay\" result would result in a change in existing policy, as well as "
"preventing managers for requesting that every small decision be voted upon. "
"We currently have no clear policy to determine what is a \"votable\" item, "
"and without this policy there needs to be some method to determine what is "
"\"votable\" and what affects some immutable part of Gentoo."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):309
msgid ""
"This section is subject to additional clarification and refinement in the "
"future, as is the rest of this document. The purpose of this section is to "
"document our currently-existing procedures rather than define ideal or "
"\"final\" procedures."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(title):319
msgid "Top-level projects"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(p):323
msgid ""
"It is the responsibility of the <uri link=\"/proj/en/metastructure/"
"\">metastructure</uri> top-level project to officially document and get "
"changes approved to our management structure. The <uri link=\"/proj/en/"
"metastructure/projects.xml\">official top-level project list</uri> can be "
"viewed, and can be expected to change/expand slightly at the new management "
"structure is implemented."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//management-structure.xml(None):0
msgid "translator-credits"
msgstr ""
