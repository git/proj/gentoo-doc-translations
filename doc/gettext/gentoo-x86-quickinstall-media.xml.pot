msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(version):7
msgid "5"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(date):8
msgid "2009-10-04"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(title):11
msgid "Installation Media"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):14
msgid "Download a CD from one of our <uri link=\"/main/en/mirrors.xml\">mirrors</uri>. You can find the minimal CD ISO in <path>releases/x86/current-iso/</path>. The <e>minimal</e> installation CD is only useful for Internet-based installations. This guide will use the minimal CD."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):21
msgid "<uri link=\"/doc/en/faq.xml#isoburning\">Burn</uri> the CD and boot it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(title):28
msgid "Booting the CD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):31
msgid "Press <c>F2</c> at the boot screen to find out what boot options exist. You can either start <c>gentoo</c> or <c>gentoo-nofb</c>, the latter disables the framebuffer. If you booted the LiveCD, don't forget to add the <c>nox</c> option to prevent the X graphical environment from starting. Several options allow to enable or disable some features. If all goes well, your hardware will be detected and all modules will be loaded. If the kernel fails to boot properly or if your computer hangs during the boot procedure, you may have to experiment with different configurations. The safest way is probably to use the <c>nodetect</c> option and then load required modules explicitly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre:caption):43
msgid "Boot the minimal CD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre):43
#, no-wrap
msgid "\nGentoo Linux Installation LiveCD                     http://www.gentoo.org\nEnter to Boot; F1 for kernels  F2 for options.\nboot: <i>gentoo-nofb</i>\n  <comment>(or in case of problems)</comment>\nboot: <i>gentoo-nofb nodetect</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(title):54
msgid "Optional: loading modules"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):57
msgid "If you used the <c>nodetect</c> option, once booted, load the required modules. You need to enable networking and have access to your disks. The <c>lspci</c> command can help you identify your hardware."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre:caption):63
msgid "Load required modules"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre):63
#, no-wrap
msgid "\nlivecd root # <i>lspci</i>\n<comment>(Use lspci's output to identify required modules)</comment>\n\n<comment>(The following is an example, adapt it to your hardware)</comment>\nlivecd root # <i>modprobe 3w-9xxx</i>\nlivecd root # <i>modprobe r8169</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(title):75
msgid "Network Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):78
msgid "If your network does not work already, you can use <c>net-setup</c> to configure your network. You might need to load support for your network card using <c>modprobe</c> prior to the configuration. If you have ADSL, use <c>pppoe-setup</c> and <c>pppoe-start</c>. For PPTP support, first edit <path>/etc/ppp/chap-secrets</path> and <path>/etc/ppp/options.pptp</path> and then use <c>pptp &lt;server&nbsp;ip&gt;</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):87
msgid "For wireless access, use <c>iwconfig</c> to set the wireless parameters and then use either <c>net-setup</c> again or run <c>ifconfig</c>, <c>dhcpcd</c> and/or <c>route</c> manually."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):93
msgid "If you are behind a proxy, do not forget to initialize your system using <c>export http_proxy</c>, <c>ftp_proxy</c> and <c>RSYNC_PROXY</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre:caption):98
msgid "Configure networking the guided way"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre):98
#, no-wrap
msgid "\nlivecd root # <i>net-setup eth0</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):102
msgid "Alternatively, you can start networking manually. The following example assigns the IP address 192.168.1.10 to your PC and defines 192.168.1.1 as your router and name server."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre:caption):108
msgid "Configure networking the manual way"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre):108
#, no-wrap
msgid "\nlivecd root # <i>ifconfig eth0 192.168.1.10/24</i>\nlivecd root # <i>route add default gw 192.168.1.1</i>\nlivecd root # <i>echo nameserver 192.168.1.1 &gt; /etc/resolv.conf</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):114
msgid "The installation CD allows you to start an <c>sshd</c> server, add additional users, run <c>irssi</c> (a command-line chat client) and surf the web using <c>links</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(title):123
msgid "Optional: connect to your new box over ssh"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):126
msgid "The most interesting feature is of course <c>sshd</c>. You can start it and then connect from another machine and cut and paste commands from this guide."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre:caption):131
msgid "Start sshd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre):131
#, no-wrap
msgid "\nlivecd root # <i>time /etc/init.d/sshd start</i>\n * Generating hostkey ...\n<comment>(sshd generates the key and displays more output)</comment>\n * starting sshd ...                            [ok]\n\nreal   0m13.688s\nuser   0m9.420s\nsys    0m0.090s\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):142
msgid "Now, set the root password on the liveCD so that you can connect to it from another PC. Please note that allowing root to connect over ssh is not recommended under normal circumstances. If you can't trust your local network, use a long and complex password, you should use it only once as it will disappear after your first reboot."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre:caption):150
msgid "Set the root password"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre):150
#, no-wrap
msgid "\nlivecd root # <i>passwd</i>\nNew UNIX password: <comment>type_a_password</comment>\nRetype new UNIX password: <comment>type_a_password</comment>\npasswd: password updated successfully\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(p):157
msgid "Now, you can start a terminal on another PC and connect to your new box, follow the rest of this guide in another window, and cut and paste commands."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre:caption):162
msgid "Connect to your new box from another PC"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(pre):162
#, no-wrap
msgid "\n<comment>(Use the IP address of your new box)</comment>\n$ <i>ssh root@192.168.1.10</i>\nThe authenticity of host '192.168.1.10 (192.168.1.10)' can't be established.\nRSA key fingerprint is 96:e7:2d:12:ac:9c:b0:94:90:9f:40:89:b0:45:26:8f.\nAre you sure you want to continue connecting (yes/no)? <i>yes</i>\nWarning: Permanently added '192.168.1.10' (RSA) to the list of known hosts.\nPassword: <comment>type_the_password</comment>\n"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//gentoo-x86-quickinstall-media.xml(None):0
msgid "translator-credits"
msgstr ""

