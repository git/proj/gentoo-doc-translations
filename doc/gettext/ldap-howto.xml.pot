msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):6
msgid "Gentoo Guide to OpenLDAP Authentication"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(author:title):8 ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(author:title):11
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail:link):9
msgid "sj7trunks@pendulus.net"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail):9
msgid "Benjamin Coles"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail:link):12
msgid "swift"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(author:title):14 ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(author:title):17 ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(author:title):20 ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(author:title):23
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail:link):15
msgid "tseng@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail):15
msgid "Brandon Hale"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail:link):18
msgid "bennyc@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail):18
msgid "Benny Chuang"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail:link):21
msgid "jokey"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(mail:link):24
msgid "nightmorph"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(abstract):27
msgid "This guide introduces the basics of LDAP and shows you how to setup OpenLDAP for authentication purposes between a group of Gentoo boxes."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(version):36
msgid "6"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(date):37
msgid "2011-08-15"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):40
msgid "Getting Started with OpenLDAP"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):42
msgid "What is LDAP?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):45
msgid "LDAP stands for <e>Lightweight Directory Access Protocol</e>. Based on X.500 it encompasses most of its primary functions, but lacks the more esoteric functions that X.500 has. Now what is this X.500 and why is there an LDAP?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):52
msgid "X.500 is a model for Directory Services in the OSI concept. It contains namespace definitions and the protocols for querying and updating the directory. However, X.500 has been found to be overkill in many situations. Enter LDAP. Like X.500 it provides a data/namespace model for the directory and a protocol too. However, LDAP is designed to run directly over the TCP/IP stack. See LDAP as a slim-down version of X.500."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):65
msgid "I don't get it. What is a directory?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):68
msgid "A directory is a specialized database designed for frequent queries but infrequent updates. Unlike general databases they don't contain transaction support or roll-back functionality. Directories are easily replicated to increase availability and reliability. When directories are replicated, temporary inconsistencies are allowed as long as they get synchronised eventually."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):81
msgid "How is information structured?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):84
msgid "All information inside a directory is structured hierarchically. Even more, if you want to enter data inside a directory, the directory must know how to store this data inside a tree. Lets take a look at a fictional company and an Internet-like tree:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):91
msgid "Organisational structure for GenFic, a Fictional Gentoo company"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):91
#, no-wrap
msgid "\ndc:         com\n             |\ndc:        genfic         <comment>(Organisation)</comment>\n          /      \\\nou:   People   servers    <comment>(Organisational Units)</comment>\n      /    \\     ..\nuid: ..   John            <comment>(OU-specific data)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):101
msgid "Since you don't feed data to the database in this ascii-art like manner, every node of such a tree must be defined. To name such nodes, LDAP uses a naming scheme. Most LDAP distributions (including OpenLDAP) already contain quite a number of predefined (and general approved) schemes, such as the inetorgperson, a frequently used scheme to define users."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):109
msgid "Interested users are encouraged to read the <uri link=\"http://www.openldap.org/doc/admin24/\">OpenLDAP Admin Guide</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):117
msgid "So... What's the Use?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):120
msgid "LDAP can be used for various things. This document focuses on centralised user management, keeping all user accounts in a single LDAP location (which doesn't mean that it's housed on a single server, LDAP supports high availability and redundancy), yet other goals can be achieved using LDAP as well."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(li):128
msgid "Public Key Infrastructure"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(li):129
msgid "Shared Calendar"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(li):130
msgid "Shared Addressbook"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(li):131
msgid "Storage for DHCP, DNS, ..."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(li):132
msgid "System Class Configuration Directives (keeping track of several server configurations)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(li):136
msgid "..."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):144
msgid "Configuring OpenLDAP"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):146
msgid "Initial Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(note):149
msgid "In this document we use the genfic.com address as an example. You will ofcourse have to change this. However, make sure that the top node is an official top level domain (net, com, cc, be, ...)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):155
msgid "Let's first emerge OpenLDAP:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):159
msgid "Install OpenLDAP"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):159
#, no-wrap
msgid "\n# <i>emerge openldap</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):163
msgid "Now generate an encrypted password we'll use later on:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):167
msgid "Generate password"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):167
#, no-wrap
msgid "\n# <i>slappasswd</i>\nNew password: <i>my-password</i>\nRe-enter new password: <i>my-password</i>\n{SSHA}EzP6I82DZRnW+ou6lyiXHGxSpSOw2XO4\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):174
msgid "Now edit the LDAP Server config at <path>/etc/openldap/slapd.conf</path>. Below we'll give a sample configuration file to get things started. For a more detailed analysis of the configuration file, we suggest that you work through the OpenLDAP Administrator's Guide."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):181 ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):564 ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):587
msgid "/etc/openldap/slapd.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):181
#, no-wrap
msgid "\ninclude\t/etc/openldap/schema/core.schema\ninclude /etc/openldap/schema/cosine.schema\ninclude /etc/openldap/schema/inetorgperson.schema\ninclude /etc/openldap/schema/nis.schema\ninclude\t/etc/openldap/schema/misc.schema\n\npidfile /var/run/openldap/slapd.pid\nargsfile /var/run/openldap/slapd.args\n\nserverID 0 <comment>Used in case of replication</comment>\nloglevel 0\n\n<comment>## Access Controls</comment>\naccess to dn.base=\"\" by * read\naccess to dn.base=\"cn=Subschema\" by * read\naccess to *\n  by self write\n  by users read\n  by anonymous read\n\n<comment>## Database definition</comment>\ndatabase hdb\nsuffix \"dc=genfic,dc=com\"\ncheckpoint 32 30\nrootdn \"cn=Manager,dc=genfic,dc=com\"\nrootpw \"{SSHA}EzP6I82DZRnW+ou6lyiXHGxSpSOw2XO4\" <comment># See earlier slappasswd command</comment>\ndirectory \"/var/lib/openldap-ldbm\"\nindex objectClass eq\n\n<comment>## Synchronisation (pull from other LDAP server)</comment>\nsyncrepl rid=000\n  provider=ldap://ldap2.genfic.com\n  type=refreshAndPersist\n  retry=\"5 5 300 +\"\n  searchbase=\"dc=genfic,dc=com\"\n  attrs=\"*,+\"\n  bindmethod=\"simple\"\n  binddn=\"cn=ldapreader,dc=genfic,dc=com\"\n  credentials=\"ldapsyncpass\"\n\nindex entryCSN eq\nindex entryUUID eq\n\nmirrormode TRUE\n\noverlay syncprov\nsyncprov-checkpoint 100 10\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):231
msgid "Next we edit the LDAP Client configuration file:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):235
msgid "/etc/openldap/ldap.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):235
#, no-wrap
msgid "\n# <i>nano -w /etc/openldap/ldap.conf</i>\n<comment>(Add the following...)</comment>\n\nBASE         dc=genfic, dc=com\nURI          ldap://ldap.genfic.com:389/ ldap://ldap1.genfic.com:389/ ldap://ldap2.genfic.com:389/\nTLS_REQCERT  allow\nTIMELIMIT    2\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):245
msgid "Now edit <path>/etc/conf.d/slapd</path> and set the following OPTS line:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):249
msgid "/etc/conf.d/slapd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):249
#, no-wrap
msgid "\nOPTS=\"-h 'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock'\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):253
msgid "Finally, create the <path>/var/lib/openldap-ldbm</path> structure:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):257
msgid "Preparing the openldap-ldbm location"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):257
#, no-wrap
msgid "\n~# <i>mkdir -p /var/lib/openldap-ldbm</i>\n~# <i>chown ldap:ldap /var/lib/openldap-ldbm</i>\n~# <i>chmod 700 /var/lib/openldap-ldbm</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):263
msgid "Start slapd:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):267
msgid "Starting SLAPd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):267
#, no-wrap
msgid "\n# <i>/etc/init.d/slapd start</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):271
msgid "You can test with the following command:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):275
msgid "Test the SLAPd daemon"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):275
#, no-wrap
msgid "\n# <i>ldapsearch -x -D \"cn=Manager,dc=genfic,dc=com\" -W</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):279
msgid "If you receive an error, try adding <c>-d 255</c> to increase the verbosity and solve the issue you have."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):289
msgid "Replication"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):291
msgid "If you need high availability"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):294
msgid "If your environment requires high availability, then you need to setup replication of changes across multiple LDAP systems. Replication within OpenLDAP is, in this guide, set up using a specific replication account (<c>ldapreader</c>) which has read rights on the primary LDAP server and which pulls in changes from the primary LDAP server to the secundary."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):302
msgid "This setup is then mirrored, allowing the secundary LDAP server to act as a primary. Thanks to OpenLDAP's internal structure, changes are not re-applied if they are already in the LDAP structure."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):311
msgid "Setting Up Replication"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):314
msgid "To setup replication, first setup a second OpenLDAP server, similarly as above. However take care that, in the configuration file,"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(li):320
msgid "the <e>sync replication provider</e> is pointing to the <e>other</e> system"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(li):323
msgid "the <e>serverID</e> of each OpenLDAP system is different"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):328
msgid "Next, create the synchronisation account. We will create an LDIF file (the format used as data input for LDAP servers) and add it to each LDAP server:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):333
msgid "Creating the ldapreader account"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):333
#, no-wrap
msgid "\n~# <i>slappasswd -s myreaderpassword</i>\n {SSHA}XvbdAv6rdskp9HgFaFL9YhGkJH3HSkiM\n\n~# <i>cat ldapreader.ldif</i>\ndn: cn=ldapreader,dc=genfic,dc=com\nuserPassword: {SSHA}XvbdAv6rdskp9HgFaFL9YhGkJH3HSkiM\nobjectClass: organizationalRole\nobjectClass: simpleSecurityObject\ncn: ldapreader\ndescription: LDAP reader used for synchronization\n\n~# <i>ldapadd -x -W -D \"cn=Manager,dc=genfic,dc=com\" -f ldapreader.ldif</i>\nPassword: <comment>enter the administrative password</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):354
msgid "Client Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):356
msgid "Migrate existing data to ldap"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):359
msgid "Configuring OpenLDAP for centralized administration and management of common Linux/Unix items isn't easy, but thanks to some tools and scripts available on the Internet, migrating a system from a single-system administrative point-of-view towards an OpenLDAP-based, centralized managed system isn't hard either."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):367
msgid "Go to <uri link=\"http://www.padl.com/OSS/MigrationTools.html\">http://www.padl.com/OSS/MigrationTools.html</uri> and fetch the scripts there. You'll need the migration tools and the <c>make_master.sh</c> script."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):374
msgid "Next, extract the tools and copy the <c>make_master.sh</c> script inside the extracted location:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):379
msgid "Extracting the MigrationTools"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):379
#, no-wrap
msgid "\n~# <i>mktemp -d</i>\n/tmp/tmp.zchomocO3Q\n~# <i>cd /tmp/tmp.zchomocO3Q</i>\n~# <i>tar xvzf /path/to/MigrationTools.tgz</i>\n~# <i>mv /path/to/make_master.sh MigrationTools-47</i>\n~# <i>cd MigrationTools-47</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):388
msgid "The next step now is to migrate the information of your system to OpenLDAP. The <c>make_master.sh</c> script will do this for you, after you have provided it with the information regarding your LDAP structure and environment."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):394
msgid "At the time of writing, the tools require the following input:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(th):400
msgid "Input"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(th):401
msgid "Description"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(th):402
msgid "Example"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):405
msgid "LDAP BaseDN"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):406
msgid "The base location (root) of your tree"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):407
msgid "dc=genfic,dc=com"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):410
msgid "Mail domain"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):411
msgid "Domain used in e-mail addresses"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):412
msgid "genfic.com"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):415
msgid "Mail host"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):416
msgid "FQDN of your mail server infrastructure"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):417
msgid "smtp.genfic.com"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):420
msgid "LDAP Root DN"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):421
msgid "Administrative account information for your LDAP structure"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):422
msgid "cn=Manager,dc=genfic,dc=com"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):425
msgid "LDAP Root Password"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(ti):426
msgid "Password for the administrative account, cfr earlier <c>slappasswd</c> command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):434
msgid "The tool will also ask you which accounts and settings you want to migrate."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):441
msgid "Configuring PAM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):444
msgid "First, we will configure PAM to allow LDAP authorization. Install <c>sys-auth/pam_ldap</c> so that PAM supports LDAP authorization, and <c>sys-auth/nss_ldap</c> so that your system can cope with LDAP servers for additional information (used by <path>nsswitch.conf</path>)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):451
msgid "Installing pam_ldap and nss_ldap"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):451
#, no-wrap
msgid "\n# <i>emerge pam_ldap nss_ldap</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):455
msgid "Now add the following lines in the right places to <path>/etc/pam.d/system-auth</path>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):460
msgid "/etc/pam.d/system-auth"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):460
#, no-wrap
msgid "\n<comment># Note: only add them. Don't kill stuff already in there or your box won't let you login again!</comment>\n\nauth       sufficient   pam_ldap.so use_first_pass\naccount    sufficient   pam_ldap.so\npassword   sufficient   pam_ldap.so use_authtok use_first_pass\nsession    optional     pam_ldap.so\n\n<comment># Example file:</comment>\n#%PAM-1.0\n\nauth       required     pam_env.so\nauth       <i>sufficient</i>   pam_unix.so try_first_pass likeauth nullok\n<i>auth       sufficient   pam_ldap.so use_first_pass</i>\nauth       required     pam_deny.so\n\n<i>account    sufficient   pam_ldap.so</i>\naccount    required     pam_unix.so\n\npassword   required     pam_cracklib.so difok=2 minlen=8 dcredit=2 ocredit=2 try_first_pass retry=3\npassword   <i>sufficient</i>   pam_unix.so try_first_pass use_authtok nullok md5 shadow\n<i>password   sufficient   pam_ldap.so use_authtok use_first_pass</i>\npassword   required     pam_deny.so\n\nsession    required     pam_limits.so\nsession    required     pam_unix.so\n<i>session    optional     pam_ldap.so</i>\n\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):490
msgid "Now change <path>/etc/ldap.conf</path> to read:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):494
msgid "/etc/ldap.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):494
#, no-wrap
msgid "\n<comment>#host 127.0.0.1</comment>\n<comment>#base dc=padl,dc=com</comment>\n\nsuffix          \"dc=genfic,dc=com\"\n<comment>#rootbinddn uid=root,ou=People,dc=genfic,dc=com</comment>\nbind_policy soft\nbind_timelimit 2\nldap_version 3\nnss_base_group ou=Group,dc=genfic,dc=com\nnss_base_hosts ou=Hosts,dc=genfic,dc=com\nnss_base_passwd ou=People,dc=genfic,dc=com\nnss_base_shadow ou=People,dc=genfic,dc=com\npam_filter objectclass=posixAccount\npam_login_attribute uid\npam_member_attribute memberuid\npam_password exop\nscope one\ntimelimit 2\nuri ldap://ldap.genfic.com/ ldap://ldap1.genfic.com ldap://ldap2.genfic.com\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):516
msgid "Next, copy over the (OpenLDAP) <path>ldap.conf</path> file from the server to the client so the clients are aware of the LDAP environment:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):521
msgid "Copying over the OpenLDAP ldap.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):521
#, no-wrap
msgid "\n<comment>(Substitute ldap-server with your LDAP server name)</comment>\n# <i>scp ldap-server:/etc/openldap/ldap.conf /etc/openldap</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):526
msgid "Finally, configure your clients so that they check the LDAP for system accounts:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre:caption):531
msgid "/etc/nsswitch.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):531
#, no-wrap
msgid "\npasswd:         files ldap\ngroup:          files ldap\nshadow:         files ldap\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):537
msgid "If you noticed one of the lines you pasted into your <path>/etc/ldap.conf</path> was commented out (the <c>rootbinddn</c> line): you don't need it unless you want to change a user's password as superuser. In this case you need to echo the root password to <path>/etc/ldap.secret</path> in plaintext. This is <brite>DANGEROUS</brite> and should be chmoded to 600. What you might want to do is keep that file blank and when you need to change someones password thats both in the ldap and <path>/etc/passwd</path>, put the pass in there for 10 seconds while changing the users password and remove it when done."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):553
msgid "LDAP Server Security Settings"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):555
msgid "OpenLDAP permissions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):558
msgid "If we take a look at <path>/etc/openldap/slapd.conf</path> you'll see that you can specify the ACLs (permissions if you like) of what data users can read and/or write:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):564
#, no-wrap
msgid "\naccess to *\n  by dn=\"uid=root,ou=People,dc=genfic,dc=com\" write\n  by users read\n  by anonymous auth\n\naccess to attrs=userPassword,gecos,description,loginShell\n  by self write\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):574
msgid "This gives you access to everything a user should be able to change. If it's your information, then you got write access to it; if it's another user their information then you can read it; anonymous people can send a login/pass to get logged in. There are four levels, ranking them from lowest to greatest: <c>auth search read write</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):582
msgid "The next ACL is a bit more secure as it blocks normal users to read other people their shadowed password:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(pre):587
#, no-wrap
msgid "\naccess to attrs=\"userPassword\"\n  by dn=\"uid=root,ou=People,dc=genfic,dc=com\" write\n  by dn=\"uid=John,ou=People,dc=genfic,dc=com\" write\n  by anonymous auth\n  by self write\n  by * none\n\naccess to *\n  by dn=\"uid=root,ou=People,dc=genfic,dc=com\" write\n  by dn=\"uid=John,ou=People,dc=genfic,dc=com\" write\n  by * search\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):601
msgid "This example gives root and John access to read/write/search for everything in the the tree below <path>dc=genfic,dc=com</path>. This also lets users change their own <path>userPassword</path>'s. As for the ending statement everyone else just has a search ability meaning they can fill in a search filter, but can't read the search results. Now you can have multiple acls but the rule of the thumb is it processes from bottom up, so your toplevel should be the most restrictive ones."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):616
msgid "Working with OpenLDAP"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):618
msgid "Maintaining the directory"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):621
msgid "You can start using the directory to authenticate users in apache/proftpd/qmail/samba. You can manage it with phpldapadmin, diradm, jxplorer, or lat, which provide easy management interfaces."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(title):632
msgid "Acknowledgements"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(p):636
msgid "We would like to thank Matt Heler for lending us his box for the purpose of this guide. Thanks also go to the cool guys in #ldap @ irc.freenode.net"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//ldap-howto.xml(None):0
msgid "translator-credits"
msgstr ""

