msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//doc-languages.xml(version):7
msgid "2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//doc-languages.xml(date):8
msgid "2009-06-21"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//doc-languages.xml(title):11
msgid "Available Languages"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//doc-languages.xml(p):14
msgid "Our documentation is also available in the following languages:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//doc-languages.xml(p):18
msgid "<uri link=\"/doc/cs/__FILE__\">Czech</uri> | <uri link=\"/doc/en/__FILE__\">English</uri> | <uri link=\"/doc/fr/__FILE__\">French</uri> | <uri link=\"/doc/de/__FILE__\">German</uri> | <uri link=\"/doc/it/__FILE__\">Italian</uri> | <uri link=\"/doc/ja/__FILE__\">Japanese</uri> | <uri link=\"/doc/pl/__FILE__\">Polish</uri> | <uri link=\"/doc/zh_cn/__FILE__\">Simplified Chinese</uri> | <uri link=\"/doc/es/__FILE__\">Spanish</uri>"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//doc-languages.xml(None):0
msgid "translator-credits"
msgstr ""

