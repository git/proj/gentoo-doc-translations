msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(guide:redirect):3
msgid "/doc/en/handbook/handbook-mips.xml"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(title):4 ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(title):20
msgid "Gentoo MIPS Installation Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(author:title):6
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(mail:link):7
msgid "swift@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(mail):7
msgid "Sven Vermeulen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(abstract):10
msgid "Replacement document for the installation guides."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(version):16
msgid "1.1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(date):17
msgid "2004-03-06"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(p):24
msgid "As of 2004.0, the individual architecture-specific installation guides have been obsoleted in favor of the new (and extended) <uri link=\"/doc/en/handbook/handbook-mips.xml\">Gentoo Handbook</uri>. For more information please read up on the <uri link=\"/doc/en/handbook\">Gentoo Handbook Project</uri> page."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//gentoo-mips-install.xml(None):0
msgid "translator-credits"
msgstr ""

