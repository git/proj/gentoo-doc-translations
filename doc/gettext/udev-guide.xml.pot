msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-18 15:18+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):6
msgid "Gentoo udev Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(mail:link):9
msgid "swift@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(mail):9
msgid "Sven Vermeulen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(author:title):11
msgid "Contributor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(mail:link):12
msgid "greg_g@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(mail):12
msgid "Gregorio Guidi"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(author:title):14
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(mail:link):15
msgid "nightmorph"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(abstract):18
msgid "This document explains what udev is and how you can use udev to fit your needs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(version):26
msgid "9"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(date):27
msgid "2011-09-11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):30
msgid "What is udev?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):32
msgid "The /dev Directory"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):35
msgid "When Linux-users talk about the hardware on their system in the vicinity of people who believe Linux is some sort of virus or brand of coffee, the use of \"slash dev slash foo\" will return a strange look for sure. But for the fortunate user (and that includes you) using <path>/dev/hda1</path> is just a fast way of explaining that we are talking about the primary master IDE, first partition. Or aren't we?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):44
msgid "We all know what a device file is. Some even know why device files have special numbers when we take a closer look at them when we issue <c>ls -l</c> in <path>/dev</path>. But what we always take for granted is that the primary master IDE disk is referred to as <path>/dev/hda</path>. You might not see it this way, but this is a flaw by design."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):52
msgid "Think about hotpluggable devices like USB, IEEE1394, hot-swappable PCI, ... What is the first device? And for how long? What will the other devices be named when the first one disappears? How will that affect ongoing transactions? Wouldn't it be fun that a printing job is suddenly moved from your supernew laserprinter to your almost-dead matrix printer because your mom decided to pull the plug of the laserprinter which happened to be the first printer?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):61
msgid "Enter <e>udev</e>. The goals of the udev project are both interesting and needed:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(li):67
msgid "Runs in userspace"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(li):68
msgid "Dynamically creates/removes device files"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(li):69
msgid "Provides consistent naming"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(li):70
msgid "Provides a user-space API"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):73
msgid "Every time a change happens within the device structure, the kernel emits a <e>uevent</e> which gets picked up by udev. udev then follows the rules as declared in the <path>/etc/udev/rules.d</path> and <path>/lib/udev/rules.d</path> directories. Based on the information contained within the uevent, it finds the rule or rules it needs to trigger and performs the required actions. These actions can be creating or deleting device files, but can also trigger the loading of particular firmware files into the kernel memory."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):89
msgid "Using udev on Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):91
msgid "Requirements"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):94
msgid "udev is meant to be used in combination with a 2.6 kernel (like <c>gentoo-sources</c> with the default 10.0 profile). If you're using such a kernel then you just should have no issues whatsoever with using udev as the necessary support is built-in in all stable <c>sys-apps/baselayout</c> versions. Normally, udev should already be installed on your system, but if this is not the case, then it is easy to install:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):103
msgid "Installing udev"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):103
#, no-wrap
msgid "\n# <i>emerge udev</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):107
msgid "Kernelwise, be sure to activate the following options:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):111
msgid "Required kernel options"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):111
#, no-wrap
msgid "\nGeneral Setup ---&gt;\n  <comment>(Make sure the following item is *not* enabled)</comment>\n  [ ] enable deprecated sysfs features to support old userspace tools\n\nFile Systems ---&gt;\n  [*] Inotify support for userspace\n  Pseudo filesystems ---&gt;\n    [*] Virtual memory file system support (former shm fs)\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):122
msgid "If you use <c>genkernel</c>, you don't need to do anything special. Genkernel sets up udev by default."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):132
msgid "Known Issues"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):134
msgid "Missing device node files at boot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):137
msgid "If you can't boot successfully because you get an error about <path>/dev/null</path> not found, or because the initial console is missing, the problem is that you lack some device files that must be available <e>before</e><path>/dev</path> is mounted and handled by udev. This is common on Gentoo machines installed from old media."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):145
msgid "If you run <c>sys-apps/baselayout-1.8.12</c> or later, this problem is alleviated since the boot process should still manage to complete. However, to get rid of those annoying warnings, you should create the missing device nodes as described below."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):152
msgid "To see which devices nodes are present before the <path>/dev</path> filesystem is mounted, run the following commands:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):157
msgid "Listing device nodes available at boot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):157
#, no-wrap
msgid "\n# <i>mkdir test</i>\n# <i>mount --bind / test</i>\n# <i>cd test/dev</i>\n# <i>ls</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):164
msgid "The devices needed for a successful boot are <path>/dev/null</path> and <path>/dev/console</path>. If they didn't show up in the previous test, you have to create them manually. Issue the following commands in the <path>test/dev/</path> directory:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):171
msgid "Creating necessary device node files"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):171
#, no-wrap
msgid "\n# <i>mknod -m 660 console c 5 1</i>\n# <i>mknod -m 660 null c 1 3</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):176
msgid "When you're finished, don't forget to unmount the <path>test/</path> directory:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):180
msgid "Unmounting the test/ directory"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):180
#, no-wrap
msgid "\n# <i>cd ../..</i>\n# <i>umount test</i>\n# <i>rmdir test</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):189
msgid "udev and nvidia"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):192
msgid "If you use the proprietary driver from nVidia and the X server fails to start on a udev-only system, then make sure you have the <c>nvidia</c> module listed in <path>/etc/conf.d/modules</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):201
msgid "No Consistent Naming between DevFS and udev"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):204
msgid "Even though our intention is to have a consistent naming scheme between both dynamical device management solutions, sometimes naming differences do occur."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):209
msgid "One reported clash is with a HP Smart Array 5i RAID controller (more precisely the <c>cciss</c> kernel module). With udev, the devices are named <path>/dev/cciss/cXdYpZ</path> with X, Y and Z regular numbers. With devfs, the devices are <path>/dev/hostX/targetY/partZ</path> or symlinked from <path>/dev/cciss/cXdY</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):217
msgid "If this is the case, don't forget to update your <path>/etc/fstab</path> and bootloader configuration files accordingly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):222
msgid "The same happens with all-round symlinks that used to exist in <path>/dev</path>, such as <path>/dev/mouse</path>, which <c>udev</c> doesn't create anymore. Be certain to check your X configuration file and see if the Device rule for your mouse points to an existing device file."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):229
msgid "Another issue is the difference in naming of terminals between devfs and udev. While devfs calls its terminals <c>tty</c>, udev calls them <c>vc</c> and <c>tty</c>. This could lead to a problem in case you are restricting root logins from consoles using <path>/etc/securetty</path>. You will need to make sure that both <c>tty1</c> and <c>vc/1</c> are listed in <path>/etc/securetty</path> to ensure that root can login using the console."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):241
msgid "Block device renaming"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):244
msgid "Recent versions of udev (104 and up) along with newer kernel versions (2.6.19 and up) may change your disc device names, due to a change in the kernel's libata implementation. A CD-RW device at <path>/dev/hdc</path> may be changed to <path>/dev/sr0</path>. While this is not normally a problem, it may cause issues for some applications that are hardcoded to look for devices at other locations. For example, <c>media-sound/rip</c> expects to find discs at <path>/dev/cdrom</path>, which becomes a problem if you use a newer kernel and udev renames your device to <path>/dev/cdrom1</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):255
msgid "To work around these issues, you must edit <path>/etc/udev/rules.d/70-persistent-cd.rules</path> and assign the correct name to the device."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):261
msgid "For more information on writing udev rules, be sure to read Daniel Drake's <uri link=\"http://www.reactivated.net/udevrules.php\">guide</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):269
msgid "Network device renaming"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):272
msgid "Sometimes unplugging and replugging a network device (like a USB WiFi card) can rename your net device each time, incrementing the number by one."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):277
msgid "When this happens, you'll see it become <c>wlan0</c>, <c>wlan1</c>, <c>wlan2</c>, etc. This is because udev is adding additional rules to its rules file, instead of reloading the existing rules. Since udev watches its rules directory via inotify, you need inotify support in your kernel config:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):284
msgid "Enabling inotify support in the kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):284
#, no-wrap
msgid "\nFile systems ---&gt;\n    [*] Inotify file change notification support\n    [*]   Inotify support for userspace\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):290
msgid "Now udev will retain proper names for your network devices."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):297
msgid "udev loads modules in an unpredictable order"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):300
msgid "Sometimes udev loads modules in an undesired, unpredictable, or seemingly random order. This is especially common for systems that have multiple devices of the same type, as well as multimedia devices. This can affect the assigned numbers of devices; for example, sound cards may sometimes swap numbers."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):307
msgid "There are a few solutions to fix device numbers and/or module load order. Ideally, you can just use module parameters to specify your desired device number. Some modules, such as ALSA, include the \"index\" parameter. Modules that use the index parameter can be adjusted as shown. This example is for a system with two sound cards. The card with an index of 0 is designated as the first card. Once the parameters are changed, the module config files must be updated."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):316
msgid "Specifying module parameters"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):316
#, no-wrap
msgid "\n# <i>echo \"option snd-ice1724 index=0\" &gt;&gt; /etc/modprobe.d/alsa.conf</i>\n# <i>echo \"option snd-ymfpci index=1\" &gt;&gt; /etc/modprobe.d/alsa.conf</i>\n# <i>update-modules</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):322
msgid "The above example is the preferred solution, but not all modules support parameters such as index. For these modules, you'll have to force the correct module load order. First, you must stop udev from autoloading the modules by blacklisting them. Be sure to use the exact name of the module being loaded. For PCI devices, you'll need to use the module names obtained from the output of <c>lspci -k</c>, available in the <c>pciutils</c> package. The following example uses DVB modules."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):332
msgid "Blacklisting modules"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):332
#, no-wrap
msgid "\n# <i>echo \"blacklist b2c2-flexcop-pci\" &gt;&gt; /etc/modprobe.d/dvb</i>\n# <i>echo \"blacklist budget\" &gt;&gt; /etc/modprobe.d/dvb</i>\n# <i>update-modules</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):338
msgid "Next, load the modules in the correct order. Add them to <path>/etc/conf.d/modules</path><e>in the exact order you want them loaded</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):344
msgid "Loading modules in the correct order"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):344
#, no-wrap
msgid "\n# <i>nano -w /etc/conf.d/modules</i>\n\nmodules=\"<i>budget b2c2-flexcop-pci</i>\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):353
msgid "Other issues"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):356
msgid "Support for the framebuffer devices (<path>/dev/fb/*</path>) comes with the kernel starting from version 2.6.6-rc2."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):361
msgid "For kernels older than 2.6.4 you have to explicitly include support for the <path>/dev/pts</path> filesystem."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre:caption):366
msgid "Enabling the /dev/pts filesystem"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(pre):366
#, no-wrap
msgid "\nFile systems ---&gt;\n  Pseudo filesystems ---&gt;\n    [*] /dev/pts file system for Unix98 PTYs\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(title):377
msgid "Resources &amp; Acknowledgements"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):381
msgid "The udev talk on the Linux Symposium (Ottawa, Ontario Canada - 2003) given by Greg Kroah-Hartman (IBM Corporation) provided a solid understanding on the udev application."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):387
msgid "<uri link=\"http://webpages.charter.net/decibelshelp/LinuxHelp_UDEVPrimer.html\">Decibel's UDEV Primer</uri> is an in-depth document about udev and Gentoo."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(p):392
msgid "<uri link=\"http://www.reactivated.net/udevrules.php\">Writing udev rules</uri> by fellow Gentoo developer Daniel Drake is an excellent document to learn how to customize your udev installation."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//udev-guide.xml(None):0
msgid "translator-credits"
msgstr ""

