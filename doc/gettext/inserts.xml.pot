msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-08-02 23:45+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(inserts:lang):5
msgid "en"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(version):6
msgid "3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):7
msgid "Content"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):8
msgid "Credits"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):9
msgid "Updated <docdate/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):10
msgid "The <uri link=\"$originalversion\">original version</uri> of this document was last updated <docdate/>"
msgstr ""

#. Doc not in /doc/LL/metadoc.xml
#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):12
msgid "This translation is not maintained anymore"
msgstr ""

#. Doc in /doc/LL/metadoc.xml but not in /doc/en/metadoc.xml
#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):14
msgid "The original version of this translation is not maintained anymore"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):15
msgid "Summary"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):16
msgid "Figure"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):17
msgid "Note"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):18
msgid "Warning"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):19
msgid "Important"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):20
msgid "Code Listing"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):21
msgid "Gentoo Linux Documentation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):22
msgid "Printable Linux Documentation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):23
msgid "Printable Linux Project"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):25
msgid "Print"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):26
msgid "View a printer-friendly version"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):27
msgid "View all"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):28
msgid "View all handbook in one page"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):29
msgid "Home"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):30
msgid "The contents of this document, unless otherwise expressly stated, are licensed under the <uri link=\"http://creativecommons.org/licenses/by-sa/2.5\">CC-BY-SA-2.5</uri> license. The <uri link=\"http://www.gentoo.org/main/en/name-logo.xml\"> Gentoo Name and Logo Usage Guidelines </uri> apply."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):33
msgid "Link to other book part not available"
msgstr ""

#. Metadoc stuff
#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):35
msgid "Gentoo Linux Documentation Categories"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):36
msgid "Members"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):37
msgid "Member"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):38
msgid "Lead"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):39
msgid "Name"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):40
msgid "Nickname"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):41
msgid "E-mail"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):42
msgid "Position"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):43
msgid "Files"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):44
msgid "Filename"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):45
msgid "Version"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):46
msgid "Original Version"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):47
msgid "Editing"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):48
msgid "N/A"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):49
msgid "Bugs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):50
msgid "Showstoppers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):51
msgid "Normal Bugs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):52
msgid "Document"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):53
msgid "Bug ID"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):54
msgid "Untranslated files"
msgstr ""

#. Disclaimer and redirect stuff
#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):57
msgid "Disclaimer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):58
msgid "The original version of this article was first published on IBM developerWorks, and is property of Westtech Information Services. This document is an updated version of the original article, and contains various improvements made by the Gentoo Linux Documentation team.<br/> This document is not actively maintained."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):65
msgid "This handbook has been replaced by a newer version and is not maintained anymore."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):68
msgid "This document is a work in progress and should not be considered official yet."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):71
msgid "This document is not valid and is not maintained anymore."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(insert):74
msgid "You should be redirected to <uri link=\"$redirect\">the new version</uri> within a few seconds."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//inserts.xml(None):0
msgid "translator-credits"
msgstr ""

