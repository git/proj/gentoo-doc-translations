msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-21 23:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):6
msgid "Gentoo BIND Guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(mail:link):9
msgid "peratu@carrosses.com"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(mail):9
msgid "Vicente Olivert Riera"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(author:title):11
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(mail:link):12
msgid "nightmorph"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(abstract):15
msgid "This guide will teach you how install and configure BIND for your domain and your local network."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(version):20
msgid "2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(date):21
msgid "2009-08-21"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):24
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):28
msgid "This tutorial will show you how to install and configure BIND, the most used DNS server on Internet. We will configure <c>bind</c> for your domain using different configurations, one for your local network and one for the rest of the world. We will use views to do that. One view for your internal zone (your local network) and other view for the external zone (rest of the world)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):41
msgid "Data used in the examples"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(th):47
msgid "Keyword"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(th):48
msgid "Explanation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(th):49
msgid "Example"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):52
msgid "YOUR_DOMAIN"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):53
msgid "Your domain name"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):54
msgid "gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):57
msgid "YOUR_PUBLIC_IP"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):58
msgid "The public ip that ISP gives to you"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):59
msgid "204.74.99.100"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):62
msgid "YOUR_LOCAL_IP"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):63
msgid "The local ip address"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):64
msgid "192.168.1.5"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):67
msgid "YOUR_LOCAL_NETWORK"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):68
msgid "The local network"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):69
msgid "192.168.1.0/24"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):72
msgid "SLAVE_DNS_SERVER"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):73
msgid "The ip address of the slave DNS server for your domain."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):74
msgid "209.177.148.228"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):77
msgid "ADMIN"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):78
msgid "The DNS server administrator's name."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):79
msgid "root"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):82
msgid "MODIFICATION"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):83
msgid "The modification date of the file zone, with a number added"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(ti):84
msgid "2009062901"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(figure:link):88
msgid "/images/docs/local-network-map.png"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(figure:short):88
msgid "network"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(figure:caption):88
msgid "Network example"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):95
msgid "Configuring BIND"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):97
msgid "Installation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):100
msgid "First, install <c>net-dns/bind</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):104
msgid "Installing bind"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):104
#, no-wrap
msgid "\n# <i>emerge net-dns/bind</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):111
msgid "Configuring /etc/bind/named.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):114
msgid "The first thing to configure is <path>/etc/bind/named.conf</path>. The first part of this step is specifying bind's root directory, the listening port with the IPs, the pid file, and a line for ipv6 protocol."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):120
msgid "options section"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):120
#, no-wrap
msgid "\noptions {\n        directory \"/var/bind\";\n\n        listen-on-v6 { none; };\n        listen-on port 53 { 127.0.0.1; YOUR_LOCAL_IP; };\n\n        pid-file \"/var/run/named/named.pid\";\n};\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):131
msgid "The second part of <path>named.conf</path> is the internal view used for our local network."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):136
msgid "Internal view"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):136
#, no-wrap
msgid "\nview \"internal\" {\n        match-clients { YOUR_LOCAL_NETWORK; localhost; };\n        recursion yes;\n\n        zone \"YOUR_DOMAIN\" {\n                type master;\n                file \"pri/YOUR_DOMAIN.internal\";\n                allow-transfer { any; };\n        };\n};\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):149
msgid "The third part of <path>named.conf</path> is the external view used to resolve our domain name for the rest of the world and to resolve all other domain names for us (and anyone who wants to use our DNS server)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):155 ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):185
msgid "External view"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):155
#, no-wrap
msgid "\nview \"external\" {\n        match-clients { any; };\n        recursion no;\n\n        zone \".\" IN {\n                type hint;\n                file \"named.ca\";\n        };\n\n        zone \"127.in-addr.arpa\" IN {\n                type master;\n                file \"pri/127.zone\";\n                allow-update { none; };\n                notify no;\n        };\n\n        zone \"YOUR_DOMAIN\" {\n                type master;\n                file \"pri/YOUR_DOMAIN.external\";\n                allow-query { any; };\n                allow-transfer { SLAVE_DNS_SERVER; };\n        };\n};\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):181
msgid "The final part of <path>named.conf</path> is the logging policy."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):185
#, no-wrap
msgid "\nlogging {\n        channel default_syslog {\n                file \"/var/log/named/named.log\" versions 3 size 5m;\n                severity debug;\n                print-time yes;\n                print-severity yes;\n                print-category yes;\n        };\n       category default { default_syslog; };\n};\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):198
msgid "The <path>/var/log/named/</path> directory must be exist and belong to <c>named</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):203
msgid "Creating the log file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):203
#, no-wrap
msgid "\n# <i>mkdir -p /var/log/named/</i>\n# <i>chmod 770 /var/log/named/</i>\n# <i>touch /var/log/named/named.log</i>\n# <i>chmod 660 /var/log/named/named.log</i>\n# <i>chown -R named /var/log/named/</i>\n# <i>chgrp -R named /var/log/named/</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):215
msgid "Creating the internal zone file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):218
msgid "We use the hostnames and IP adresses of the picture network example. Note that almost all (not all) domain names finish with \".\" (dot)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):223
msgid "/var/bind/pri/YOUR_DOMAIN.internal"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):223
#, no-wrap
msgid "\n$TTL 2d\n@\tIN SOA\tns.YOUR_DOMAIN.\tADMIN.YOUR_DOMAIN. (\n\tMODIFICATION\t; serial\n\t3h\t; refresh\n\t1h\t; retry\n\t1w\t; expiry\n\t1d )\t; minimum\n\nYOUR_DOMAIN.\t\tIN MX\t0 mail.YOUR_DOMAIN.\nYOUR_DOMAIN.\t\tIN TXT\t\"v=spf1 ip4:YOUR_PUBLIC_IP/32 mx ptr mx:mail.YOUR_DOMAIN ~all\"\nYOUR_DOMAIN.\t\tIN NS\tns.YOUR_DOMAIN.\nYOUR_DOMAIN.\t\tIN NS\tSLAVE_DNS_SERVER\nwww.YOUR_DOMAIN.\tIN A\t192.168.1.3\nns.YOUR_DOMAIN.\t\tIN A\t192.168.1.5\nmail.YOUR_DOMAIN.\tIN A\t192.168.1.3\nrouter.YOUR_DOMAIN.\tIN A\t192.168.1.1\nhell.YOUR_DOMAIN.\tIN A\t192.168.1.3\nheaven.YOUR_DOMAIN.\tIN A\t192.168.1.5\ndesktop.YOUR_DOMAIN.\tIN A\t192.168.1.4\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):248
msgid "Creating the external zone file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):251
msgid "Here we only have the subdomains we want for external clients (www, mail and ns)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):256
msgid "/var/bind/pri/YOUR_DOMAIN.external"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):256
#, no-wrap
msgid "\n$TTL 2d\n@\tIN SOA\tns.YOUR_DOMAIN.\tADMIN.YOUR_DOMAIN. (\n\tMODIFICATION\t;serial\n\t3h\t;refresh\n\t1h\t;retry\n\t1w\t;expiry\n\t1d )\t;minimum\n\nYOUR_DOMAIN.\t\tIN MX\t0 mail.YOUR_DOMAIN.\nYOUR_DOMAIN.\t\tIN TXT\t\"v=spf1 ip4:YOUR_PUBLIC_IP/32 mx ptr mx:mail.YOUR_DOMAIN ~all\"\nYOUR_DOMAIN.\t\tIN NS\tns.YOUR_DOMAIN.\nYOUR_DOMAIN.\t\tIN NS\tSLAVE_DNS_SERVER\nwww.YOUR_DOMAIN.\tIN A\tYOUR_PUBLIC_IP\nns.YOUR_DOMAIN.\t\tIN A\tYOUR_PUBLIC_IP\nmail.YOUR_DOMAIN.\tIN A\tYOUR_PUBLIC_IP\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):277
msgid "Finishing configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):280
msgid "You'll need to add <c>named</c> to the default runlevel:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):284
msgid "Add to default runlevel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):284
#, no-wrap
msgid "\n# <i>rc-update add named default</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):293
msgid "Configuring clients"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):297
msgid "Now you can use your own DNS server in all machines of your local network to resolve domain names. Modify the <path>/etc/resolv.conf</path> file of all machines of your local network."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):303
msgid "Editing /etc/resolv.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):303
#, no-wrap
msgid "\nsearch YOUR_DOMAIN\nnameserver YOUR_DNS_SERVER_IP\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):308
msgid "Note that YOUR_DNS_SERVER_IP is the same as YOUR_LOCAL_IP we used in this document. In the picture the example is 192.168.1.5."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):318
msgid "Testing"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):322
msgid "We are able to test our new DNS server. First, we need to start the service."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):326
msgid "Starting the service manually"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):326
#, no-wrap
msgid "\n# <i>/etc/init.d/named start</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):330
msgid "Now, we are going to make some <c>host</c> commands to some domains. We can use any computer of our local network to do this test. If you don't have <c>net-dns/host</c> installed you can use <c>ping</c> instead. Otherwise, first run <c>emerge host</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):337
msgid "Performing the test"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):337
#, no-wrap
msgid "\n$ <i>host www.gentoo.org</i>\nwww.gentoo.org has address 209.177.148.228\nwww.gentoo.org has address 209.177.148.229\n\n$ <i>host hell</i>\nhell.YOUR_DOMAIN has address 192.168.1.3\n\n$ <i>host router</i>\nrouter.YOUR_DOMAIN has address 192.168.1.1\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(title):354
msgid "Protecting the server with iptables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(p):358
msgid "If you use iptables to protect your server, you can add these rules for DNS service."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre:caption):363
msgid "Iptables rules"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(pre):363
#, no-wrap
msgid "\niptables -A INPUT -p udp --sport 53 -m state --state ESTABLISHED,RELATED -j ACCEPT\niptables -A INPUT -p udp --dport 53 -j ACCEPT\niptables -A INPUT -p tcp --sport 53 -j ACCEPT\niptables -A INPUT -p tcp --dport 53 -j ACCEPT\n"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en//bind-guide.xml(None):0
msgid "translator-credits"
msgstr ""

