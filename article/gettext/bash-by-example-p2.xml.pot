msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:11+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):6
msgid "Bash by example, Part 2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(mail:link):9
msgid "drobbins@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(mail):9
msgid "Daniel Robbins"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(abstract):12
msgid "In his introductory article on bash, Daniel Robbins walked you through some of the scripting language's basic elements and reasons for using bash. In this, the second installment, Daniel picks up where he left off and looks at bash's basic constructs like conditional (if-then) statements, looping, and more."
msgstr ""

#. The original version of this article was published on IBM developerWorks,
#. and is property of Westtech Information Services. This document is an updated
#. version of the original article, and contains various improvements made by the
#. Gentoo Linux Documentation team
#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(version):24
msgid "1.4"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(date):25
msgid "2005-10-09"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):28
msgid "More bash programming fundamentals"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):30
msgid "Accepting arguments"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):33
msgid "Let's start with a brief tip on handling command-line arguments, and then look at bash's basic programming constructs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):38
msgid "In the sample program in the <uri link=\"/doc/en/articles/bash-by-example-p1.xml\">introductory article</uri>, we used the environment variable \"$1\", which referred to the first command-line argument. Similarly, you can use \"$2\", \"$3\", etc. to refer to the second and third arguments passed to your script. Here's an example:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):46
msgid "Referring to arguments passed to the script"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):46
#, no-wrap
msgid "\n#!/usr/bin/env bash\n\necho name of script is $0\necho first argument is $1\necho second argument is $2\necho seventeenth argument is $17\necho number of arguments is $#\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):56
msgid "The example is self explanatory except for two small details. First, \"$0\" will expand to the name of the script, as called from the command line, and \"$#\" will expand to the number of arguments passed to the script. Play around with the above script, passing different kinds of command-line arguments to get the hang of how it works."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):64
msgid "Sometimes, it's helpful to refer to all command-line arguments at once. For this purpose, bash features the \"$@\" variable, which expands to all command-line parameters separated by spaces. We'll see an example of its use when we take a look at \"for\" loops, a bit later in this article."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):74
msgid "Bash programming constructs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):77
msgid "If you've programmed in a procedural language like C, Pascal, Python, or Perl, then you're familiar with standard programming constructs like \"if\" statements, \"for\" loops, and the like. Bash has its own versions of most of these standard constructs. In the next several sections, I will introduce several bash constructs and demonstrate the differences between these constructs and others you are already familiar with from other programming languages. If you haven't programmed much before, don't worry. I include enough information and examples so that you can follow the text."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):91
msgid "Conditional love"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):94
msgid "If you've ever programmed any file-related code in C, you know that it requires a significant amount of effort to see if a particular file is newer than another. That's because C doesn't have any built-in syntax for performing such a comparison; instead, two stat() calls and two stat structures must be used to perform the comparison by hand. In contrast, bash has standard file comparison operators built in, so determining if \"<path>/tmp/myfile</path> is readable\" is as easy as checking to see if \"<c>$myvar</c> is greater than 4\"."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):104
msgid "The following table lists the most frequently used bash comparison operators. You'll also find an example of how to use every option correctly. The example is meant to be placed immediately after the \"if\". For example:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):110
msgid "Bash comparison operator"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):110
#, no-wrap
msgid "\nif [ -z \"$myvar\" ]\nthen\n     echo \"myvar is not defined\"\nfi\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):117
msgid "Sometimes, there are several different ways that a particular comparison can be made. For example, the following two snippets of code function identically:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):122
msgid "Two ways of making comparison"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):122
#, no-wrap
msgid "\nif [ \"$myvar\" -eq 3 ]\nthen \n     echo \"myvar equals 3\"\nfi\n\nif [ \"$myvar\" = \"3\" ]\nthen\n     echo \"myvar equals 3\"\nfi\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):134
msgid "In the above two comparisons do exactly the same thing, but the first uses arithmetic comparison operators, while the second uses string comparison operators."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):143
msgid "String comparison caveats"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):146
msgid "Most of the time, while you can omit the use of double quotes surrounding strings and string variables, it's not a good idea. Why? Because your code will work perfectly, unless an environment variable happens to have a space or a tab in it, in which case bash will get confused. Here's an example of a fouled-up comparison:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):154
msgid "Fouled-up comparison example"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):154
#, no-wrap
msgid "\nif [ $myvar = \"foo bar oni\" ]\nthen\n     echo \"yes\"\nfi\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):161
msgid "In the above example, if myvar equals \"foo\", the code will work as expected and not print anything. However, if myvar equals \"foo bar oni\", the code will fail with the following error:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):167
msgid "Error when variable contains spaces"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):167
#, no-wrap
msgid "\n[: too many arguments\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):171
msgid "In this case, the spaces in \"$myvar\" (which equals \"foo bar oni\") end up confusing bash. After bash expands \"$myvar\", it ends up with the following comparison:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):177
msgid "Ending comparison"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):177
#, no-wrap
msgid "\n[ foo bar oni = \"foo bar oni\" ]\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):181
msgid "Because the environment variable wasn't placed inside double quotes, bash thinks that you stuffed too many arguments in-between the square brackets. You can easily eliminate this problem by surrounding the string arguments with double-quotes. Remember, if you get into the habit of surrounding all string arguments and environment variables with double-quotes, you'll eliminate many similar programming errors. Here's how the \"foo bar oni\" comparison should have been written:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):191
msgid "Proper way of comparison writing"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):191
#, no-wrap
msgid "\nif [ \"$myvar\" = \"foo bar oni\" ]\nthen\n    echo \"yes\"\nfi\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):198
msgid "The above code will work as expected and will not create any unpleasant surprises."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(note):203
msgid "If you want your environment variables to be expanded, you must enclose them in double quotes, rather than single quotes. Single quotes disable variable (as well as history) expansion."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):212
msgid "Looping constructs: \"for\""
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):215
msgid "OK, we've covered conditionals, now it's time to explore bash looping constructs. We'll start with the standard \"for\" loop. Here's a basic example:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):220
msgid "Basic for example"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):220
#, no-wrap
msgid "\n#!/usr/bin/env bash\n\nfor x in one two three four\ndo\n    echo number $x\ndone\n\n<comment>Output:</comment>\nnumber one\nnumber two \nnumber three \nnumber four\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):235
msgid "What exactly happened? The \"for x\" part of our \"for\" loop defined a new environment variable (also called a loop control variable) called \"$x\", which was successively set to the values \"one\", \"two\", \"three\", and \"four\". After each assignment, the body of the loop (the code between the \"do\" ... \"done\") was executed once. In the body, we referred to the loop control variable \"$x\" using standard variable expansion syntax, like any other environment variable. Also notice that \"for\" loops always accept some kind of word list after the \"in\" statement. In this case we specified four English words, but the word list can also refer to file(s) on disk or even file wildcards. Look at the following example, which demonstrates how to use standard shell wildcards:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):248
msgid "Using standard shell wildcards"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):248
#, no-wrap
msgid "\n#!/usr/bin/env bash\n\nfor myfile in /etc/r*\ndo\n    if [ -d \"$myfile\" ] \n    then\n      echo \"$myfile (dir)\"\n    else\n      echo \"$myfile\"\n    fi\ndone\n\noutput:\n\n/etc/rc.d (dir)\n/etc/resolv.conf\n/etc/resolv.conf~\n/etc/rpc\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):269
msgid "The above code looped over each file in <path>/etc</path> that began with an \"r\". To do this, bash first took our wildcard /etc/r* and expanded it, replacing it with the string <path>/etc/rc.d</path><path>/etc/resolv.conf</path><path>/etc/resolv.conf~</path><path>/etc/rpc</path> before executing the loop. Once inside the loop, the \"-d\" conditional operator was used to perform two different actions, depending on whether myfile was a directory or not. If it was, a \" (dir)\" was appended to the output line."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):279
msgid "We can also use multiple wildcards and even environment variables in the word list:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):284
msgid "Multiple wildcards and environment variables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):284
#, no-wrap
msgid "\nfor x in /etc/r??? /var/lo* /home/drobbins/mystuff/* /tmp/${MYPATH}/*\ndo\n    cp $x /mnt/mydira\ndone\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):291
msgid "Bash will perform wildcard and variable expansion in all the right places, and potentially create a very long word list."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):296
msgid "While all of our wildcard expansion examples have used absolute paths, you can also use relative paths, as follows:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):301
msgid "Using relative paths"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):301
#, no-wrap
msgid "\nfor x in ../* mystuff/*\ndo\n     echo $x is a silly file\ndone\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):308
msgid "In the above example, bash performs wildcard expansion relative to the current working directory, just like when you use relative paths on the command line. Play around with wildcard expansion a bit. You'll notice that if you use absolute paths in your wildcard, bash will expand the wildcard to a list of absolute paths. Otherwise, bash will use relative paths in the subsequent word list. If you simply refer to files in the current working directory (for example, if you type <c>for x in *</c>), the resultant list of files will not be prefixed with any path information. Remember that preceding path information can be stripped using the <c>basename</c> executable, as follows:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):320
msgid "Stripping preceding path with basename"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):320
#, no-wrap
msgid "\nfor x in /var/log/*\ndo\n    echo `basename $x` is a file living in /var/log\ndone\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):327
msgid "Of course, it's often handy to perform loops that operate on a script's command-line arguments. Here's an example of how to use the \"$@\" variable, introduced at the beginning of this article:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):333
msgid "Example use of $@ variable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):333
#, no-wrap
msgid "\n#!/usr/bin/env bash\n\nfor thing in \"$@\"\ndo\n    echo you typed ${thing}.\ndone\n\n<comment>output:</comment>\n\n$ allargs hello there you silly\nyou typed hello.\nyou typed there.\nyou typed you.\nyou typed silly.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):353
msgid "Shell arithmetic"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):356
msgid "Before looking at a second type of looping construct, it's a good idea to become familiar with performing shell arithmetic. Yes, it's true: You can perform simple integer math using shell constructs. Simply enclose the particular arithmetic expression between a \"$((\" and a \"))\", and bash will evaluate the expression. Here are some examples:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):364
msgid "Counting in bash"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):364
#, no-wrap
msgid "\n$ <i>echo $(( 100 / 3 ))</i>\n33\n$ <i>myvar=\"56\"</i>\n$ <i>echo $(( $myvar + 12 ))</i>\n68\n$ <i>echo $(( $myvar - $myvar ))</i>\n0\n$ <i>myvar=$(( $myvar + 1 ))</i>\n$ <i>echo $myvar</i>\n57\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):377
msgid "Now that you're familiar performing mathematical operations, it's time to introduce two other bash looping constructs, \"while\" and \"until\"."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):385
msgid "More looping constructs: \"while\" and \"until\""
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):388
msgid "A \"while\" statement will execute as long as a particular condition is true, and has the following format:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):393
msgid "While statement template"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):393
#, no-wrap
msgid "\nwhile [ condition ]\ndo\n    statements\ndone\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):400
msgid "\"While\" statements are typically used to loop a certain number of times, as in the following example, which will loop exactly 10 times:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):405
msgid "Looping the statement 10 times"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):405
#, no-wrap
msgid "\nmyvar=0\nwhile [ $myvar -ne 10 ]\ndo\n    echo $myvar\n    myvar=$(( $myvar + 1 ))\ndone\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):414
msgid "You can see the use of arithmetic expansion to eventually cause the condition to be false, and the loop to terminate."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):419
msgid "\"Until\" statements provide the inverse functionality of \"while\" statements: They repeat as long as a particular condition is false. Here's an \"until\" loop that functions identically to the previous \"while\" loop:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):425
msgid "Until loop example"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):425
#, no-wrap
msgid "\nmyvar=0\nuntil [ $myvar -eq 10 ]\ndo\n    echo $myvar\n    myvar=$(( $myvar + 1 ))\ndone\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):437
msgid "Case statements"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):440
msgid "\"Case\" statements are another conditional construct that comes in handy. Here's an example snippet:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):445
msgid "Example case statement snippet"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):445
#, no-wrap
msgid "\ncase \"${x##*.}\" in\n     gz)\n           gzunpack ${SROOT}/${x}\n           ;;\n     bz2)\n           bz2unpack ${SROOT}/${x}\n           ;;\n     *)\n           echo \"Archive format not recognized.\"\n           exit\n           ;;\nesac\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):460
msgid "Above, bash first expands \"${x##*.}\". In the code, \"$x\" is the name of a file, and \"${x##*.}\" has the effect of stripping all text except that following the last period in the filename. Then, bash compares the resultant string against the values listed to the left of the \")\"s. In this case, \"${x##*.}\" gets compared against \"gz\", then \"bz2\" and finally \"*\". If \"${x##*.}\" matches any of these strings or patterns, the lines immediately following the \")\" are executed, up until the \";;\", at which point bash continues executing lines after the terminating \"esac\". If no patterns or strings are matched, no lines of code are executed; however, in this particular code snippet, at least one block of code will execute, because the \"*\" pattern will catch everything that didn't match \"gz\" or \"bz2\"."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):477
msgid "Functions and namespaces"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):480
msgid "In bash, you can even define functions, similar to those in other procedural languages like Pascal and C. In bash, functions can even accept arguments, using a system very similar to the way scripts accept command-line arguments. Let's take a look at a sample function definition and then proceed from there:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):487
msgid "Sample function definition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):487
#, no-wrap
msgid "\ntarview() {\n    echo -n \"Displaying contents of $1 \"\n    if [ ${1##*.} = tar ]\n    then\n        echo \"(uncompressed tar)\"\n        tar tvf $1\n    elif [ ${1##*.} = gz ]\n    then\n        echo \"(gzip-compressed tar)\"\n        tar tzvf $1\n    elif [ ${1##*.} = bz2 ]\n    then\n        echo \"(bzip2-compressed tar)\"\n        cat $1 | bzip2 -d | tar tvf -\n    fi\n}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(note):506
msgid "Another case: The above code could have been written using a \"case\" statement. Can you figure out how?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):511
msgid "Above, we define a function called \"tarview\" that accepts one argument, a tarball of some kind. When the function is executed, it identifies what type of tarball the argument is (either uncompressed, gzip-compressed, or bzip2-compressed), prints out a one-line informative message, and then displays the contents of the tarball. This is how the above function should be called (whether from a script or from the command line, after it has been typed in, pasted in, or sourced):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):521
msgid "Calling the above function"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):521
#, no-wrap
msgid "\n$ <i>tarview shorten.tar.gz</i>\nDisplaying contents of shorten.tar.gz (gzip-compressed tar)\ndrwxr-xr-x ajr/abbot         0 1999-02-27 16:17 shorten-2.3a/\n-rw-r--r-- ajr/abbot      1143 1997-09-04 04:06 shorten-2.3a/Makefile\n-rw-r--r-- ajr/abbot      1199 1996-02-04 12:24 shorten-2.3a/INSTALL\n-rw-r--r-- ajr/abbot       839 1996-05-29 00:19 shorten-2.3a/LICENSE\n....\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):531
msgid "As you can see, arguments can be referenced inside the function definition by using the same mechanism used to reference command-line arguments. In addition, the \"$#\" macro will be expanded to contain the number of arguments. The only thing that may not work completely as expected is the variable \"$0\", which will either expand to the string \"bash\" (if you run the function from the shell, interactively) or to the name of the script the function is called from."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(note):540
msgid "Use'em interactively: Don't forget that functions, like the one above, can be placed in your ~/.bashrc or ~/.bash_profile so that they are available for use whenever you are in bash."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):549
msgid "Namespace"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):552
msgid "Often, you'll need to create environment variables inside a function. While possible, there's a technicality you should know about. In most compiled languages (such as C), when you create a variable inside a function, it's placed in a separate local namespace. So, if you define a function in C called myfunction, and in it define a variable called \"x\", any global (outside the function) variable called \"x\" will not be affected by it, eliminating side effects."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):562
msgid "While true in C, this isn't true in bash. In bash, whenever you create an environment variable inside a function, it's added to the global namespace. This means that it will overwrite any global variable outside the function, and will continue to exist even after the function exits:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):569
msgid "Variable handling in bash"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):569
#, no-wrap
msgid "\n#!/usr/bin/env bash\n\nmyvar=\"hello\"\n\nmyfunc() {\n\n    myvar=\"one two three\"\n    for x in $myvar\n    do\n        echo $x\n    done\n}\n\nmyfunc\n\necho $myvar $x\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):588
msgid "When this script is run, it produces the output \"one two three three\", showing how \"$myvar\" defined in the function clobbered the global variable \"$myvar\", and how the loop control variable \"$x\" continued to exist even after the function exited (and also would have clobbered any global \"$x\", if one were defined)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):595
msgid "In this simple example, the bug is easy to spot and to compensate for by using alternate variable names. However, this isn't the right approach; the best way to solve this problem is to prevent the possibility of clobbering global variables in the first place, by using the \"local\" command. When we use \"local\" to create variables inside a function, they will be kept in the local namespace and not clobber any global variables. Here's how to implement the above code so that no global variables are overwritten:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre:caption):605
msgid "Assuring no global variables will be overwritten"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(pre):605
#, no-wrap
msgid "\n#!/usr/bin/env bash\n\nmyvar=\"hello\"\n\nmyfunc() {\n    local x\n    local myvar=\"one two three\"\n    for x in $myvar\n    do\n        echo $x\n    done\n}\n\nmyfunc\n\necho $myvar $x\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):624
msgid "This function will produce the output \"hello\" -- the global \"$myvar\" doesn't get overwritten, and \"$x\" doesn't continue to exist outside of myfunc. In the first line of the function, we create x, a local variable that is used later, while in the second example (local myvar=\"one two three\"\") we create a local myvar and assign it a value. The first form is handy for keeping loop control variables local, since we're not allowed to say \"for local x in $myvar\". This function doesn't clobber any global variables, and you are encouraged to design all your functions this way. The only time you should not use \"local\" is when you explicitly want to modify a global variable."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):639
msgid "Wrapping it up"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(p):642
msgid "Now that we've covered the most essential bash functionality, it's time to look at how to develop an entire application based in bash. In my next installment, we'll do just that. See you then!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):653
msgid "Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(title):655
msgid "Useful links"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(li):659
msgid "Read <uri link=\"/doc/en/articles/bash-by-example-p1.xml\"> Bash by example: Part 1</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(li):663
msgid "Read <uri link=\"/doc/en/articles/bash-by-example-p3.xml\">Bash by example: Part 3</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(li):668
msgid "Visit <uri link=\"http://www.gnu.org/software/bash/bash.html\">GNU's bash home page</uri>"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p2.xml(None):0
msgid "translator-credits"
msgstr ""

