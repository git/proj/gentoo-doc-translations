msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:11+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(title):7
msgid "Prompt magic"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(author:title):9
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(mail:link):10
msgid "drobbins@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(mail):10
msgid "Daniel Robbins"
msgstr ""

#. <author title="Editor">
#.   <mail link="nightmorph@gentoo.org">Joshua Saddler</mail>
#. </author>
#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(abstract):16
msgid "Why stick with the standard boring shell prompt when you can easily make it colorful and more informative? In this tip, Daniel Robbins will show you how to get your shell prompt just the way you like it, as well as how to dynamically update your X terminal's title bar."
msgstr ""

#. The original version of this article was first published on IBM 
#. developerWorks, and is property of Westtech Information Services. This 
#. document is an updated version of the original article, and contains
#. various improvements made by the Gentoo Linux Documentation team
#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(version):28
msgid "1.2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(date):29
msgid "2005-10-21"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(title):32
msgid "Enhancing the system prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):36
msgid "As Linux/UNIX people, we spend a lot of time working in the shell, and in many cases, this is what we have staring back at us:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):41
msgid "The normal user prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):41
#, no-wrap
msgid "\nbash-2.04$\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):45
msgid "If you happen to be root, you're entitled to the \"prestige\" version of this beautiful prompt:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):50
msgid "The root prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):50
#, no-wrap
msgid "\nbash-2.04#\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):54
msgid "These prompts are not exactly pretty. It's no wonder that several Linux distributions have upgraded their default prompts that add color and additional information to boot. However, even if you happen to have a modern distribution that comes with a nice, colorful prompt, it may not be perfect. Maybe you'd like to add or change some colors, or add (or remove) information from the prompt itself. It isn't hard to design your own colorized, tricked-out prompt from scratch."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(title):67
msgid "Prompt basics"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):70
msgid "Under bash, you can set your prompt by changing the value of the <c>PS1</c> environment variable, as follows:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):75
msgid "Altering the environment variable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):75
#, no-wrap
msgid "\n$ <i>export PS1=\"&gt; \"</i>\n&gt;\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):80
msgid "Changes take effect immediately, and can be made permanent by placing the <c>export</c> definition in your <path>~/.bashrc</path> file. <c>PS1</c> can contain any amount of plain text that you'd like:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):86
msgid "A custom prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):86
#, no-wrap
msgid "\n$ <i>export PS1=\"This is my super prompt &gt; \"</i>\nThis is my super prompt &gt;\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):91
msgid "While this is, um, interesting, it's not exactly useful to have a prompt that contains lots of static text. Most custom prompts contain information like the current username, working directory, or hostname. These tidbits of information can help you to navigate in your shell universe. For example, the following prompt will display your username and hostname:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):99
msgid "A more useful prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):99
#, no-wrap
msgid "\n$ <i>export PS1=\"\\u@\\H &gt; \"</i>\ndrobbins@freebox &gt;\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):104
msgid "This prompt is especially handy for people who log in to various machines under various differently-named accounts, since it acts as a reminder of what machine you're actually on and what privileges you currently have."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):110
msgid "In the above example, we told bash to insert the username and hostname into the prompt by using special backslash-escaped character sequences that bash replaces with specific values when they appear in the <c>PS1</c> variable. We used the sequences <c>\\u</c> (for username) and <c>\\H</c> (for the first part of the hostname). Here's a complete list of all special sequences that bash recognizes (you can find this list in the bash man page, in the \"PROMPTING\" section):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(th):122
msgid "Sequence"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(th):123
msgid "Description"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):126
msgid "\\a"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):127
msgid "The ASCII bell character (you can also type <c>\\007</c>)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):130
msgid "\\d"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):131
msgid "Date in \"Wed Sep 06\" format"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):134
msgid "\\e"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):135
msgid "ASCII escape character (you can also type <c>\\033</c>)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):138
msgid "\\h"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):139
msgid "First part of hostname (such as \"mybox\")"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):142
msgid "\\H"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):143
msgid "Full hostname (such as \"mybox.mydomain.com\")"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):146
msgid "\\j"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):147
msgid "The number of processes you've suspended in this shell by hitting <c>^Z</c>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):152
msgid "\\l"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):153
msgid "The name of the shell's terminal device (such as \"ttyp4\")"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):156
msgid "\\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):157
msgid "Newline"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):160
msgid "\\r"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):161
msgid "Carriage return"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):164
msgid "\\s"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):165
msgid "The name of the shell executable (such as \"bash\")"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):168
msgid "\\t"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):169
msgid "Time in 24-hour format (such as \"23:01:01\")"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):172
msgid "\\T"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):173
msgid "Time in 12-hour format (such as \"11:01:01\")"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):176
msgid "\\@"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):177
msgid "Time in 12-hour format with am/pm"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):180
msgid "\\u"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):181
msgid "Your username"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):184
msgid "\\v"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):185
msgid "Version of bash (such as 2.04)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):188
msgid "\\V"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):189
msgid "Bash version, including patchlevel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):192
msgid "\\w"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):193
msgid "Current working directory (such as <path>/home/drobbins</path>)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):196
msgid "\\W"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):197
msgid "The \"basename\" of the current working directory (such as \"drobbins\")"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):200
msgid "\\!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):201
msgid "Current command's position in the history buffer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):204
msgid "\\#"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):205
msgid "Command number (this will count up at each prompt, as long as you type something)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):211
msgid "\\\\$"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):212
msgid "If you are not root, inserts a <c>$</c>; if you are root, you get a <c>#</c>. If you delimit your string with <c>'</c> instead of <c>\"</c>, you should use a single backslash instead."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):219
msgid "\\xxx"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):220
msgid "Inserts an ASCII character based on three-digit number xxx (replace unused digits with zeros, such as <c>\\007</c>)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):226
msgid "\\\\"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):227
msgid "A backslash"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):230
msgid "\\["
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):231
msgid "This sequence should appear before a sequence of characters that don't move the cursor (like color escape sequences). This allows bash to calculate word wrapping correctly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):238
msgid "\\]"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(ti):239
msgid "This sequence should appear after a sequence of non-printing characters."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):245
msgid "So, there you have all of bash's special backslashed escape sequences. Play around with them for a bit to get a feel for how they work. After you've done a little testing, it's time to add some color."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(title):254
msgid "Colorization"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):257
msgid "Adding color is quite easy; the first step is to design a prompt without color. Then, all we need to do is add special escape sequences that'll be recognized by the terminal (rather than bash) and cause it to display certain parts of the text in color. Standard Linux terminals and X terminals allow you to set the foreground (text) color and the background color, and also enable \"bold\" characters if so desired. We get eight colors to choose from."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):266
msgid "Colors are selected by adding special sequences to <c>PS1</c> -- basically sandwiching numeric values between an <c>\\e[</c> (escape open-bracket) and an <c>m</c>. If we specify more than one numeric code, we separate each code with a semicolon. Here's an example color code:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):273
msgid "Adding color"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):273
#, no-wrap
msgid "\n\"\\e[0m\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):277
msgid "When we specify a zero as a numeric code, it tells the terminal to reset foreground, background, and boldness settings to their default values. You'll want to use this code at the end of your prompt, so that the text that you type in is not colorized. Now, let's take a look at the color codes. Check out this screenshot:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(figure:link):285
msgid "/images/docs/prompt-magic-colortable.gif"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(figure:caption):285
msgid "Color chart"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):287
msgid "To use this chart, find the color you'd like to use, and find the corresponding foreground (30-37) and background (40-47) numbers. For example, if you like green on a normal black background, the numbers are 32 and 40. Then, take your prompt definition and add the appropriate color codes. This:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):294
msgid "A basic custom prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):294
#, no-wrap
msgid "\n$ <i>export PS1=\"\\w&gt; \"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):298
msgid "becomes:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):302
msgid "The colorized prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):302
#, no-wrap
msgid "\n$ <i>export PS1=\"\\e[32;40m\\w&gt; \"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):306
msgid "So far, so good, but it's not perfect yet. After bash prints the working directory, we need to set the color back to normal with an <c>\\e[0m</c> sequence:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):312
msgid "A better colorized prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):312
#, no-wrap
msgid "\n$ <i>export PS1=\"\\e[32;40m\\w&gt; \\e[0m\"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):316
msgid "This definition will give you a nice, green prompt, but we still need to add a few finishing touches. We don't need to include the background color setting of 40, since that sets the background to black which is the default color anyway. Also, the green color is quite dim; we can fix this by adding a <c>1</c> color code, which enables brighter, bold text. In addition to this change, we need to surround all non-printing characters with special bash escape sequences, <c>\\[</c> and <c>\\]</c>. These sequences will tell bash that the enclosed characters don't take up any space on the line, which will allow word-wrapping to continue to work properly. Without them, you'll end up with a nice-looking prompt that will mess up the screen if you happen to type in a command that approaches the extreme right of the terminal. Here's our final prompt:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):330
msgid "A nice colorful prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):330
#, no-wrap
msgid "\n$ <i>export PS1=\"\\[\\e[32;1m\\]\\w&gt; \\[\\e[0m\\]\"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):334
msgid "Don't be afraid to use several colors in the same prompt, like so:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):338
msgid "Much more colorful"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):338
#, no-wrap
msgid "\n$ <i>export PS1=\"\\[\\e[36;1m\\]\\u@\\[\\e[32;1m\\]\\H&gt; \\[\\e[0m\\]\"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(title):345
msgid "Xterm fun"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):348
msgid "I've shown you how to add information and color to your prompt, but you can do even more. It's possible to add special codes to your prompt that will cause the title bar of your X terminal (such as rxvt or aterm) to be dynamically updated. All you need to do is add the following sequence to your <c>PS1</c> prompt:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):356
msgid "Updating the xterm title bar"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):356
#, no-wrap
msgid "\n\"\\e]2;titlebar\\a\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):360
msgid "Simply replace the substring <c>titlebar</c> with the text that you'd like to have appear in your xterm's title bar, and you're all set! You don't need to use static text; you can also insert bash escape sequences into your titlebar. Check out this example, which places the username, hostname, and current working directory in the titlebar, as well as defining a short, bright green prompt:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):369
msgid "An extremely useful xterm"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):369
#, no-wrap
msgid "\n$ <i>export PS1=\"\\[\\e]2;\\u@\\H \\w\\a\\e[32;1m\\]&gt;\\[\\e[0m\\] \"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):373
msgid "This is the particular prompt that I'm using in the color chart screenshot, above. I love this prompt, because it puts all the information in the title bar rather than in the terminal where it limits how much can fit on a line. By the way, make sure you surround your titlebar sequence with <c>\\[</c> and <c>\\]</c>, since as far as the terminal is concerned, this sequence is non-printing. The problem with putting lots of information in the title bar is that you will not be able to see info if you are using a non-graphical terminal, such as the system console. To fix this, you may want to add something like this to your <path>~/.bashrc</path>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):385
msgid "Adding usefulness to xterms and system consoles"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):385
#, no-wrap
msgid "\nif [ \"$TERM\" = \"linux\" ]\nthen\n    <comment># We're on the system console or maybe telnetting in</comment>\n    export PS1=\"\\[\\e[32;1m\\]\\u@\\H &gt; \\[\\e[0m\\]\"\nelse\n    <comment># We're not on the console, assume an xterm</comment>\n    export PS1=\"\\[\\e]2;\\u@\\H \\w\\a\\e[32;1m\\]&gt;\\[\\e[0m\\] \" \nfi\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):396
msgid "This bash conditional statement will dynamically set your prompt based on your current terminal settings. For consistency, you'll want to configure your <path>~/.bash_profile</path> so that it sources your <path>~/.bashrc</path> on startup. Make sure the following line is in your <path>~/.bash_profile</path>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre:caption):403
msgid "Editing bash_profile"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(pre):403
#, no-wrap
msgid "\nsource ~/.bashrc\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):407
msgid "This way, you'll get the same prompt setting whether you start a login or non-login shell."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):412
msgid "Well, there you have it. Now, have some fun and whip up some nifty colorized prompts!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(title):420
msgid "Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(li):424
msgid "<uri link=\"http://www.rxvt.org\">rxvt</uri> is a great little xterm that happens to have a good amount of documentation related to escape sequences tucked in the <path>doc</path> directory included in the source tarball."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(li):429
msgid "<uri link=\"http://aterm.sourceforge.net\">aterm</uri> is another terminal program, based on rxvt. It supports several nice visual features, like transparency and tinting."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(li):434
msgid "<uri link=\"http://bashish.mine.nu/BashishWiki/index.php/Main_Page\">bashish</uri> is a theme engine for all different kinds of terminals. Check out some <uri link=\"http://bashish.mine.nu/BashishWiki/index.php/Screenshots\">great screenshots</uri> of bashish in action!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(title):446
msgid "About the author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(p):449
msgid "Daniel Robbins lives in Albuquerque, New Mexico. He was the President/CEO of Gentoo Technologies Inc., the Chief Architect of the Gentoo Project and is a contributing author of several books published by MacMillan: Caldera OpenLinux Unleashed, SuSE Linux Unleashed, and Samba Unleashed. Daniel has been involved with computers in some fashion since the second grade when he was first exposed to the Logo programming language and a potentially lethal dose of Pac Man. This probably explains why he has since served as a Lead Graphic Artist at SONY Electronic Publishing/Psygnosis. Daniel enjoys spending time with his wife Mary and his new baby daughter, Hadassah. You can contact Daniel at <mail>drobbins@gentoo.org</mail>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/articles/prompt-magic.xml(None):0
msgid "translator-credits"
msgstr ""

