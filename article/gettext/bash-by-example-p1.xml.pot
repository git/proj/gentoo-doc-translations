msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:11+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):6
msgid "Bash by example, Part 1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(mail:link):9
msgid "drobbins@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(mail):9
msgid "Daniel Robbins"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(abstract):12
msgid "By learning how to program in the bash scripting language, your day-to-day interaction with Linux will become more fun and productive, and you'll be able to build upon those standard UNIX constructs (like pipelines and redirection) that you already know and love. In this three-part series, Daniel Robbins will teach you how to program in bash by example. He'll cover the absolute basics (making this an excellent series for beginners) and bring in more advanced features as the series proceeds."
msgstr ""

#. The original version of this article was published on IBM developerWorks,
#. and is property of Westtech Information Services. This document is an updated
#. version of the original article, and contains various improvements made by the
#. Gentoo Linux Documentation team
#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(version):27
msgid "1.3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(date):28
msgid "2005-10-09"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):31
msgid "Fundamental programming in the Bourne again shell (bash)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):33
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):36
msgid "You might wonder why you ought to learn Bash programming. Well, here are a couple of compelling reasons:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):44
msgid "You're already running it"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):47
msgid "If you check, you'll probably find that you are running bash right now. Even if you changed your default shell, bash is probably still running somewhere on your system, because it's the standard Linux shell and is used for a variety of purposes. Because bash is already running, any additional bash scripts that you run are inherently memory-efficient because they share memory with any already-running bash processes. Why load a 500K interpreter if you already are running something that will do the job, and do it well?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):60
msgid "You're already using it"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):63
msgid "Not only are you already running bash, but you're actually interacting with bash on a daily basis. It's always there, so it makes sense to learn how to use it to its fullest potential. Doing so will make your bash experience more fun and productive. But why should you learn bash programming? Easy, because you already think in terms of running commands, CPing files, and piping and redirecting output. Shouldn't you learn a language that allows you to use and build upon these powerful time-saving constructs you already know how to use? Command shells unlock the potential of a UNIX system, and bash is the Linux shell. It's the high-level glue between you and the machine. Grow in your knowledge of bash, and you'll automatically increase your productivity under Linux and UNIX -- it's that simple."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):80
msgid "Bash confusion"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):83
msgid "Learning bash the wrong way can be a very confusing process. Many newbies type <c>man bash</c> to view the bash man page, only to be confronted with a very terse and technical description of shell functionality. Others type <c>info bash</c> (to view the GNU info documentation), causing either the man page to be redisplayed, or (if they are lucky) only slightly more friendly info documentation to appear."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):92
msgid "While this may be somewhat disappointing to novices, the standard bash documentation can't be all things to all people, and caters towards those already familiar with shell programming in general. There's definitely a lot of excellent technical information in the man page, but its helpfulness to beginners is limited."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):100
msgid "That's where this series comes in. In it, I'll show you how to actually use bash programming constructs, so that you will be able to write your own scripts. Instead of technical descriptions, I'll provide you with explanations in plain English, so that you will know not only what something does, but when you should actually use it. By the end of this three-part series, you'll be able to write your own intricate bash scripts, and be at the level where you can comfortably use bash and supplement your knowledge by reading (and understanding!) the standard bash documentation. Let's begin."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):114
msgid "Environment variables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):117
msgid "Under bash and almost all other shells, the user can define environment variables, which are stored internally as ASCII strings. One of the handiest things about environment variables is that they are a standard part of the UNIX process model. This means that environment variables not only are exclusive to shell scripts, but can be used by standard compiled programs as well. When we \"export\" an environment variable under bash, any subsequent program that we run can read our setting, whether it is a shell script or not. A good example is the <c>vipw</c> command, which normally allows root to edit the system password file. By setting the <c>EDITOR</c> environment variable to the name of your favorite text editor, you can configure vipw to use it instead of vi, a handy thing if you are used to xemacs and really dislike vi."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):131
msgid "The standard way to define an environment variable under bash is:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):135
msgid "Defining environment variable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):135
#, no-wrap
msgid "\n$ <i>myvar='This is my environment variable!'</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):139
msgid "The above command defined an environment variable called \"myvar\" and contains the string \"This is my environment variable!\". There are several things to notice above: first, there is no space on either side of the \"=\" sign; any space will result in an error (try it and see). The second thing to notice is that while we could have done away with the quotes if we were defining a single word, they are necessary when the value of the environment variable is more than a single word (contains spaces or tabs)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(note):149
msgid "For extremely detailed information on how quotes should be used in bash, you may want to look at the \"QUOTING\" section in the bash man page. The existence of special character sequences that get \"expanded\" (replaced) with other values does complicate how strings are handled in bash. We will just cover the most often-used quoting functionality in this series."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):157
msgid "Thirdly, while we can normally use double quotes instead of single quotes, doing so in the above example would have caused an error. Why? Because using single quotes disables a bash feature called expansion, where special characters and sequences of characters are replaced with values. For example, the \"!\" character is the history expansion character, which bash normally replaces with a previously-typed command. (We won't be covering history expansion in this series of articles, because it is not frequently used in bash programming. For more information on it, see the \"HISTORY EXPANSION\" section in the bash man page.) While this macro-like functionality can come in handy, right now we want a literal exclamation point at the end of our environment variable, rather than a macro."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):171
msgid "Now, let's take a look at how one actually uses environment variables. Here's an example:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):176
msgid "Using environment variables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):176
#, no-wrap
msgid "\n$ <i>echo $myvar</i>\nThis is my environment variable!\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):181
msgid "By preceding the name of our environment variable with a $, we can cause bash to replace it with the value of myvar. In bash terminology, this is called \"variable expansion\". But, what if we try the following:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):187
msgid "First try to use variable expansion"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):187
#, no-wrap
msgid "\n$ <i>echo foo$myvarbar</i>\nfoo\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):192
msgid "We wanted this to echo \"fooThis is my environment variable!bar\", but it didn't work. What went wrong? In a nutshell, bash's variable expansion facility in got confused. It couldn't tell whether we wanted to expand the variable $m, $my, $myvar, $myvarbar, etc. How can we be more explicit and clearly tell bash what variable we are referring to? Try this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):200
msgid "Proper variable expansion"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):200
#, no-wrap
msgid "\n$ <i>echo foo${myvar}bar</i>\nfooThis is my environment variable!bar\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):205
msgid "As you can see, we can enclose the environment variable name in curly braces when it is not clearly separated from the surrounding text. While $myvar is faster to type and will work most of the time, ${myvar} can be parsed correctly in almost any situation. Other than that, they both do the same thing, and you will see both forms of variable expansion in the rest of this series. You'll want to remember to use the more explicit curly-brace form when your environment variable is not isolated from the surrounding text by whitespace (spaces or tabs)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):216
msgid "Recall that we also mentioned that we can \"export\" variables. When we export an environment variable, it's automatically available in the environment of any subsequently-run script or executable. Shell scripts can \"get to\" the environment variable using that shell's built-in environment-variable support, while C programs can use the getenv() function call. Here's some example C code that you should type in and compile -- it'll allow us to understand environment variables from the perspective of C:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):226
msgid "myvar.c -- a sample environment variable C program"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):226
#, no-wrap
msgid "\n#include &lt;stdio.h&gt;\n#include &lt;stdlib.h&gt;\n\nint main(void) {\n  char *myenvvar=getenv(\"EDITOR\");\n  printf(\"The editor environment variable is set to %s\\n\",myenvvar);\n}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):236
msgid "Save the above source into a file called <path>myenv.c</path>, and then compile it by issuing the command:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):241
msgid "Compiling the above source"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):241
#, no-wrap
msgid "\n$ <i>gcc myenv.c -o myenv</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):245
msgid "Now, there will be an executable program in your directory that, when run, will print the value of the <c>EDITOR</c> environment variable, if any. This is what happens when I run it on my machine:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):251
msgid "Running the above program"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):251
#, no-wrap
msgid "\n$ <i>./myenv</i>\nThe editor environment variable is set to (null)\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):256
msgid "Hmmm... because the <c>EDITOR</c> environment variable was not set to anything, the C program gets a null string. Let's try setting it to a specific value:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):261
msgid "Trying it with a specific value"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):261
#, no-wrap
msgid "\n$ <i>EDITOR=xemacs</i>\n$ <i>./myenv</i>\nThe editor environment variable is set to (null)\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):267
msgid "While you might have expected myenv to print the value \"xemacs\", it didn't quite work, because we didn't export the <c>EDITOR</c> environment variable. This time, we'll get it working:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):273
msgid "Same program after exporting variable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):273
#, no-wrap
msgid "\n$ <i>export EDITOR</i>\n$ <i>./myenv</i>\nThe editor environment variable is set to xemacs\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):279
msgid "So, you have seen with your very own eyes that another process (in this case our example C program) cannot see the environment variable until it is exported. Incidentally, if you want, you can define and export an environment variable using one line, as follows:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):286
msgid "Defining and exporting an environment variable in one command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):286
#, no-wrap
msgid "\n$ <i>export EDITOR=xemacs</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):290
msgid "It works identically to the two-line version. This would be a good time to show how to erase an environment variable by using <c>unset</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):295
msgid "Unsetting the variable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):295
#, no-wrap
msgid "\n$ <i>unset EDITOR</i>\n$ <i>./myenv</i>\nThe editor environment variable is set to (null)\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):304
msgid "Chopping strings overview"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):307
msgid "Chopping strings -- that is, splitting an original string into smaller, separate chunk(s) -- is one of those tasks that is performed daily by your average shell script. Many times, shell scripts need to take a fully-qualified path, and find the terminating file or directory. While it's possible (and fun!) to code this in bash, the standard <c>basename</c> UNIX executable performs this extremely well:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):316
msgid "Using basename"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):316
#, no-wrap
msgid "\n$ <i>basename /usr/local/share/doc/foo/foo.txt</i>\nfoo.txt\n$ <i>basename /usr/home/drobbins</i>\ndrobbins\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):323
msgid "<c>basename</c> is quite a handy tool for chopping up strings. It's companion, called <c>dirname</c>, returns the \"other\" part of the path that <c>basename</c> throws away:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):329
msgid "Using dirname"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):329
#, no-wrap
msgid "\n$ <i>dirname /usr/local/share/doc/foo/foo.txt</i>\n/usr/local/share/doc/foo\n$ <i>dirname /usr/home/drobbins/</i>\n/usr/home\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(note):336
msgid "Both <c>dirname</c> and <c>basename</c> do not look at any files or directories on disk; they are purely string manipulation commands."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):344
msgid "Command substitution"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):347
msgid "One very handy thing to know is how to create an environment variable that contains the result of an executable command. This is very easy to do:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):352
msgid "Creating an environment variable containing command result"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):352
#, no-wrap
msgid "\n$ <i>MYDIR=`dirname /usr/local/share/doc/foo/foo.txt`</i>\n$ <i>echo $MYDIR</i>\n/usr/local/share/doc/foo\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):358
msgid "What we did above is called <e>command substitution</e>. Several things are worth noticing in this example. On the first line, we simply enclosed the command we wanted to execute in back quotes. Those are not standard single quotes, but instead come from the keyboard key that normally sits above the Tab key. We can do exactly the same thing with bash's alternate command substitution syntax:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):366
msgid "Alternate command substitution syntax"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):366
#, no-wrap
msgid "\n$ <i>MYDIR=$(dirname /usr/local/share/doc/foo/foo.txt)</i>\n$ <i>echo $MYDIR</i>\n/usr/local/share/doc/foo\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):372
msgid "As you can see, bash provides multiple ways to perform exactly the same thing. Using command substitution, we can place any command or pipeline of commands in between <e>` `</e> or <e>$( )</e> and assign it to an environment variable. Handy stuff! Here's an example of how to use a pipeline with command substitution:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):380
msgid "Pipeline command substitution"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):380
#, no-wrap
msgid "\n$ <i>MYFILES=$(ls /etc | grep pa)</i>\n$ <i>echo $MYFILES</i>\npam.d passwd\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):389
msgid "Chopping strings like a pro"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):392
msgid "While <c>basename</c> and <c>dirname</c> are great tools, there are times where we may need to perform more advanced string \"chopping\" operations than just standard pathname manipulations. When we need more punch, we can take advantage of bash's advanced built-in variable expansion functionality. We've already used the standard kind of variable expansion, which looks like this: ${MYVAR}. But bash can also perform some handy string chopping on its own. Take a look at these examples:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):402
msgid "Examples of strings chopping"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):402
#, no-wrap
msgid "\n$ <i>MYVAR=foodforthought.jpg</i>\n$ <i>echo ${MYVAR##*fo}</i>\nrthought.jpg\n$ <i>echo ${MYVAR#*fo}</i>\nodforthought.jpg\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):410
msgid "In the first example, we typed ${MYVAR##*fo}. What exactly does this mean? Basically, inside the ${ }, we typed the name of the environment variable, two ##s, and a wildcard (\"*fo\"). Then, bash took <c>MYVAR</c>, found the longest substring from the beginning of the string \"foodforthought.jpg\" that matched the wildcard \"*fo\", and chopped it off the beginning of the string. That's a bit hard to grasp at first, so to get a feel for how this special \"##\" option works, let's step through how bash completed this expansion. First, it began searching for substrings at the beginning of \"foodforthought.jpg\" that matched the \"*fo\" wildcard. Here are the substrings that it checked:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):422
msgid "Substrings being checked"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):422
#, no-wrap
msgid "\nf       \nfo              MATCHES *fo\nfoo     \nfood\nfoodf           \nfoodfo          MATCHES *fo\nfoodfor\nfoodfort        \nfoodforth\nfoodfortho      \nfoodforthou\nfoodforthoug\nfoodforthought\nfoodforthought.j\nfoodforthought.jp\nfoodforthought.jpg\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):441
msgid "After searching the string for matches, you can see that bash found two. It selects the longest match, removes it from the beginning of the original string, and returns the result."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):447
msgid "The second form of variable expansion shown above appears identical to the first, except it uses only one \"#\" -- and bash performs an almost identical process. It checks the same set of substrings as our first example did, except that bash removes the shortest match from our original string, and returns the result. So, as soon as it checks the \"fo\" substring, it removes \"fo\" from our string and returns \"odforthought.jpg\"."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):456
msgid "This may seem extremely cryptic, so I'll show you an easy way to remember this functionality. When searching for the longest match, use ## (because ## is longer than #). When searching for the shortest match, use #. See, not that hard to remember at all! Wait, how do you remember that we are supposed to use the '#' character to remove from the *beginning* of a string? Simple! You will notice that on a US keyboard, shift-4 is \"$\", which is the bash variable expansion character. On the keyboard, immediately to the left of \"$\" is \"#\". So, you can see that \"#\" is \"at the beginning\" of \"$\", and thus (according to our mnemonic), \"#\" removes characters from the beginning of the string. You may wonder how we remove characters from the end of the string. If you guessed that we use the character immediately to the right of \"$\" on the US keyboard (\"%\"), you're right! Here are some quick examples of how to chop off trailing portions of strings:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):472
#, no-wrap
msgid "\n$ <i>MYFOO=\"chickensoup.tar.gz\"</i>\n$ <i>echo ${MYFOO%%.*}</i>\nchickensoup\n$ <i>echo ${MYFOO%.*}</i>\nchickensoup.tar\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):480
msgid "As you can see, the % and %% variable expansion options work identically to # and ##, except they remove the matching wildcard from the end of the string. Note that you don't have to use the \"*\" character if you wish to remove a specific substring from the end:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):487
msgid "Removing substrings from the end"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):487
#, no-wrap
msgid "\n<i>MYFOOD=\"chickensoup\"</i>\n$ <i>echo ${MYFOOD%%soup}</i>\nchicken\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):493
msgid "In this example, it doesn't matter whether we use \"%%\" or \"%\", since only one match is possible. And remember, if you forget whether to use \"#\" or \"%\", look at the 3, 4, and 5 keys on your keyboard and figure it out."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):499
msgid "We can use another form of variable expansion to select a specific substring, based on a specific character offset and length. Try typing in the following lines under bash:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):505
msgid "Selecting a specific substring"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):505
#, no-wrap
msgid "\n$ <i>EXCLAIM=cowabunga</i>\n$ <i>echo ${EXCLAIM:0:3}</i>\ncow\n$ <i>echo ${EXCLAIM:3:7}</i>\nabunga\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):513
msgid "This form of string chopping can come in quite handy; simply specify the character to start from and the length of the substring, all separated by colons."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):522
msgid "Applying string chopping"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):525
msgid "Now that we've learned all about chopping strings, let's write a simple little shell script. Our script will accept a single file as an argument, and will print out whether it appears to be a tarball. To determine if it is a tarball, it will look for the pattern \".tar\" at the end of the file. Here it is:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):532
msgid "mytar.sh -- a sample script"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):532
#, no-wrap
msgid "\n#!/bin/bash\n\nif [ \"${1##*.}\" = \"tar\" ]\nthen\n       echo This appears to be a tarball.\nelse\n       echo At first glance, this does not appear to be a tarball.\nfi\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):543
msgid "To run this script, enter it into a file called <path>mytar.sh</path>, and type <c>chmod 755 mytar.sh</c> to make it executable. Then, give it a try on a tarball, as follows:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):549
msgid "Trying the script"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):549
#, no-wrap
msgid "\n$ <i>./mytar.sh thisfile.tar</i>\nThis appears to be a tarball.\n$ <i>./mytar.sh thatfile.gz</i>\nAt first glance, this does not appear to be a tarball.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):556
msgid "OK, it works, but it's not very functional. Before we make it more useful, let's take a look at the \"if\" statement used above. In it, we have a boolean expression. In bash, the \"=\" comparison operator checks for string equality. In bash, all boolean expressions are enclosed in square brackets. But what does the boolean expression actually test for? Let's take a look at the left side. According to what we've learned about string chopping, \"${1##*.}\" will remove the longest match of \"*.\" from the beginning of the string contained in the environment variable \"1\", returning the result. This will cause everything after the last \".\" in the file to be returned. Obviously, if the file ends in \".tar\", we will get \"tar\" as a result, and the condition will be true."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):569
msgid "You may be wondering what the \"1\" environment variable is in the first place. Very simple -- $1 is the first command-line argument to the script, $2 is the second, etc. OK, now that we've reviewed the function, we can take our first look at \"if\" statements."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):579
msgid "If statements"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):582
msgid "Like most languages, bash has its own form of conditional. When using them, stick to the format above; that is, keep the \"if\" and the \"then\" on separate lines, and keep the \"else\" and the terminating and required \"fi\" in horizontal alignment with them. This makes the code easier to read and debug. In addition to the \"if,else\" form, there are several other forms of \"if\" statements:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):590
msgid "Basic form of if statement"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):590
#, no-wrap
msgid "\nif      [ condition ]\nthen\n        action\nfi\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):597
msgid "This one performs an action only if condition is true, otherwise it performs no action and continues executing any lines following the \"fi\"."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre:caption):602
msgid "Checking condition before continuing with commands following fi"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(pre):602
#, no-wrap
msgid "\nif [ condition ]\nthen \n        action\nelif [ condition2 ]\nthen\n        action2\n.\n.\n.\nelif [ condition3 ]\nthen\n\nelse\n        actionx\nfi\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):620
msgid "The above \"elif\" form will consecutively test each condition and execute the action corresponding to the first true condition. If none of the conditions are true, it will execute the \"else\" action, if one is present, and then continue executing lines following the entire \"if,elif,else\" statement."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):630
msgid "Next time"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(p):633
msgid "Now that we've covered the most basic bash functionality, it's time to pick up the pace and get ready to write some real scripts. In the next article, I'll cover looping constructs, functions, namespace, and other essential topics. Then, we'll be ready to write some more complicated scripts. In the third article, we'll focus almost exclusively on very complex scripts and functions, as well as several bash script design options. See you then!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):647
msgid "Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(title):649
msgid "Useful links"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(li):653
msgid "Read <uri link=\"http://www.gentoo.org/doc/en/articles/bash-by-example-p2.xml\">Bash by example: Part 2</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(li):658
msgid "Read <uri link=\"http://www.gentoo.org/doc/en/articles/bash-by-example-p3.xml\">Bash by example: Part 3</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(li):663
msgid "Visit <uri link=\"http://www.gnu.org/software/bash/bash.html\">GNU's bash home page</uri>"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/articles/bash-by-example-p1.xml(None):0
msgid "translator-credits"
msgstr ""

