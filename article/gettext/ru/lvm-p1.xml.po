msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:11+0600\n"
"PO-Revision-Date: 2009-10-12 06:16+0400\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):6
msgid "Learning Linux LVM, Part 1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(mail:link):9
msgid "drobbins@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(mail):9
msgid "Daniel Robbins"
msgstr ""

#. xmlified by: Joshua Saddler (nightmorph@gentoo.org)
#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(abstract):13
msgid ""
"In this article, Daniel introduces you to the concepts behind Linux LVM "
"(Logical Volume Management) and shows you how to get the latest kernel "
"patches and tools installed on your system. LVM allows you to create logical "
"volumes out of the physical storage resources on your machine. However, "
"unlike physical volumes, the logical volumes can be expanded and shrunk "
"while the system is still running, providing Linux system administrators "
"with the storage flexibility that they've until now only dreamed of."
msgstr ""

#. The original version of this article was first published on IBM
#. developerWorks, and is property of Westtech Information Services. This
#. document is an updated version of the original article, and contains
#. various improvements made by the Gentoo Linux Documentation team
#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(version):28
msgid "1.2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(date):29
msgid "2006-01-22"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):32
msgid "Storage management magic with Logical Volume Management"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):34
msgid "LVM intro"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):37
msgid ""
"In this series, I'm going to show you how to install and use the new Logical "
"Volume Management support built-in to the Linux 2.4 kernel. If you've never "
"used a form of LVM before, you're in for a treat; it's a wonderful "
"technology. Before we actually get LVM up and running, I'm going to explain "
"exactly what it is and how it works. Then, we'll be ready to test out LVM "
"and get the most out of it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):46
msgid ""
"If you're like me, then your experience with UNIX and Linux began on a PC "
"platform, rather than on large, commercial UNIX servers and workstations. On "
"the basic PC, we've always had to deal with partitioning our hard drives. PC "
"people are generally well-acquainted with tools such as <c>fdisk</c>, which "
"are used to create and delete primary and extended partitions on hard disks. "
"Hard disk partitioning is an annoying but accepted part of the process of "
"getting an operating system up and running."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):56
msgid ""
"Hard drive partitioning can be annoying because to do a good job you really "
"need to accurately estimate how much space you'll need for each partition. "
"If you make a poor estimation, your Linux system could possibly be crippled "
"-- to fix the problem, it's possible that you might even need to perform a "
"full system backup, wipe your hard drives clean, and then restore all your "
"data to a new (and presumably better) partition layout. Ick! These are "
"exactly the kinds of situations that sysadmins try their best to avoid in "
"the first place."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):66
msgid ""
"While partitions were once static storage regions, thankfully, we now have a "
"proliferation of PC repartitioning tools (PowerQuest's Partition Magic "
"product is one of the most popular). These tools allow you to boot your "
"system with a special disk and dynamically resize your partitions and "
"filesystems. Once you reboot, you have newly resized partitions, hopefully "
"getting you out of your storage crunch. These partition resizing tools are "
"great and solve the problem storage management for some. But are they "
"perfect? Not exactly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):76
msgid ""
"Tools like Partition Magic are great for workstations, but aren't really "
"adequate for servers. First of all, they require you to reboot your system. "
"This is something most sysadmins desperately try to avoid doing. What if you "
"simply can't reboot your machine every time your storage needs change, such "
"as if your storage needs change dramatically on a weekly basis? What happens "
"if you need to expand a filesystem so that it spans more than one hard "
"drive, or what do you do if you need to dynamically expand or shrink a "
"volume's storage capacity while allowing Apache to continue to serve Web "
"pages? In a highly available, dynamic environment, a basic partition resizer "
"just won't work. For these and other situations, Logical Volume Management "
"is an excellent (if not perfect) solution."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):93
msgid "Enter LVM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):96
msgid ""
"Now, let's take a look at how LVM solves these problems. To create an LVM "
"logical volume, we follow a three-step process. First, we need to select the "
"physical storage resources that are going to be used for LVM. Typically, "
"these are standard partitions but can also be Linux software RAID volumes "
"that we've created. In LVM terminology, these storage resources are called "
"\"physical volumes\". Our first step in setting up LVM involves properly "
"initializing these partitions so that they can be recognized by the LVM "
"system. This involves setting the correct partition type if we're adding a "
"physical partition, and running the pvcreate command."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):108
msgid ""
"Once we have one or more physical volumes initialized for use by LVM, we can "
"move on to step two -- creating a volume group. You can think of a volume "
"group as a pool of storage that consists of one or more physical volumes. "
"While LVM is running, we can add physical volumes to the volume group or "
"even remove them. However, we can't mount or create filesystems on a volume "
"group directly. Instead, we can tell LVM to create one or more \"logical "
"volumes\" using our volume group storage pool:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(figure:link):119
msgid "/images/docs/l-lvm-1.gif"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(figure:caption):119
msgid "A volume group is created out of physical volumes"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):121
msgid ""
"Creating an LVM logical volume is really easy, and once it's created we can "
"go ahead and put a filesystem on it, mount it, and start using the volume to "
"store our files. To create a logical volume, we use the <c>lvcreate</c> "
"command, specifying the name of our new volume, the size we'd like the "
"volume to be, and the volume group that we'd like this particluar logical "
"volume to be part of. The LVM system will then allocate storage from the "
"volume group we specify and create our new volume, which is now ready for "
"use. Once created, we can put an ext2 or ReiserFS filesystem on it, mount "
"it, and use it as we like."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(figure:link):133
msgid "/images/docs/l-lvm-2.gif"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(figure:caption):133
msgid "Creating two logical volumes from our existing volume group"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):138
msgid "Extents"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):141
msgid ""
"Behind the scenes, the LVM system allocates storage in equal-sized \"chunks"
"\", called extents. We can specify the particular extent size to use at "
"volume group creation time. The size of an extent defaults to 4Mb, which is "
"perfect for most uses. One of the beauties of LVM is that the physical "
"storage locations of the extents used for one of our logical volumes (in "
"other words, what disk they're stored on) can be dynamically changed while "
"our logical volume is mounted and in use! The LVM system ensures that our "
"logical volumes continue to operate perfectly while allowing the "
"administrator to physically change where everything is stored."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):153
msgid ""
"Of course, since everything is created out of equally-sized extents, it's "
"really easy to allocate some additional extents for an already-existing "
"logical volume -- in other words, dynamically \"grow\" the volume:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(figure:link):161
msgid "/images/docs/l-lvm-3.gif"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(figure:caption):161
msgid ""
"Adding additional extents from our volume group, expanding the size of our "
"logical volume"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):163
msgid ""
"Once the logical volume has been expanded, you can then expand your ext2 or "
"ReiserFS filesystem to take advantage of this new space. If you use a "
"program such as <c>resize_reiserfs</c>, this filesystem expansion can also "
"happen while the volume is mounted and being used! Truly amazing -- with LVM "
"and online filesystem expansion utilties, it's no longer necessary to reboot "
"your system or even drop to runlevel 1 to change your storage configuration."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):172
msgid ""
"The only time you need to shut down your system is when you need to add new "
"physical disks. Once new disks have been added, you then can add these new "
"physical volumes to your volume group(s) to create a fresh supply of extents."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):181
msgid "Setting up LVM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):184
msgid ""
"OK, let's get LVM installed. LVM consists of two parts: a kernel part and a "
"suite of user-space tools. To start, head over to the main LVM page (see "
"<uri link=\"#resources\">Resources</uri> later in this article) and download "
"the most recent version of the LVM tarball (currently lvm_0.9.1_beta3.tar."
"gz) you can find. The LVM tarball contains all the user-space tools, as well "
"as a bunch of kernel patches. Here is where things get interesting."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):193
msgid ""
"If you already have a 2.4-series kernel installed, you may already have LVM "
"support available on your system, and if not, it's a simple matter to "
"recompile your kernel to enable LVM support. However, you may not want to "
"use the LVM support included with your stock (or distribution-supplied) 2.4 "
"kernel. If you want to use the latest LVM version, you'll want to apply "
"patches from the LVM tarball to your current 2.4 kernel source tree. Here's "
"how to do it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):202
msgid ""
"To start, enter your kernel source directory (<path>/usr/src/linux</path>) "
"and create a directory called <path>extras</path>. Then, enter this "
"directory and extract your LVM tarball:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):208
msgid "Extracting the patches"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):208
#, no-wrap
msgid ""
"\n"
"# <i>cd /usr/src/linux</i>\n"
"# <i>mkdir extras</i>\n"
"# <i>cd extras</i>\n"
"# <i>tar xzvf /path/to/location/of/lvm_0.9.1_beta3.tar.gz</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):215
msgid ""
"Once you've done that, you'll notice a new directory in extras called "
"<path>LVM</path> that contains another directory named after the version of "
"LVM that you just unpacked. Enter these two directories to get to the LVM "
"sources:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):221
msgid "Getting to the LVM sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):221
#, no-wrap
msgid ""
"\n"
"# <i>cd LVM/0.9.1_beta3</i>\n"
"# <i>ls</i>\n"
"ABSTRACT      COPYING      INSTALL     Makefile     README    autoconf      config.status  kernel         make.tmpl.in\n"
"CHANGELOG     COPYING.LIB  KNOWN_BUGS  Makefile.in  TODO      config.cache  configure      lvm_input_msg  scripts\n"
"CONTRIBUTORS  FAQ          LVM-HOWTO   PATCHES      WHATSNEW  config.log    configure.in   make.tmpl      tools\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):229
msgid ""
"You'll see several text files, scripts, and source directories. You'll find "
"the installation instructions in the <path>INSTALL</path> file; I'll guide "
"you through this process. First, we'll want to run the configure script, as "
"follows:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):236
msgid "Configuring the LVM sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):236
#, no-wrap
msgid ""
"\n"
"# <i>./configure --prefix=/ --mandir=/usr/man</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):243
msgid "Patching"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):246
msgid ""
"After executing this command, the Makefiles will be created and configured "
"to install all the LVM tools in <path>/sbin</path> and the man pages in "
"<path>/usr/man</path>. If your man pages are in <path>/usr/share/man</path> "
"(as per FHS 2.1), then adjust the above path accordingly. And, if your "
"kernel sources aren't in <path>/usr/src/linux</path>, then add a <c>--with-"
"kernel_dir=/path/to/usr/src/linux</c> option to the line as well. Once the "
"configure script completes, we're ready to install the tools and generate a "
"patch for your current kernel. Let's patch the kernel first. Enter the "
"<path>PATCHES</path> directory:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):258
msgid "Changing directories"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):258
#, no-wrap
msgid ""
"\n"
"# <i>cd PATCHES</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):262
msgid ""
"Now, we're going to type <c>make</c>. The Makefile will generate a patch "
"specifically for our particular 2.4-series kernel sources:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):267
msgid "Creating the patch"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):267
#, no-wrap
msgid ""
"\n"
"# <i>make</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):271
msgid ""
"The patch will be named <c>lvm-[lvmversion]-[kernelversion].patch</c>. For "
"example, since I'm using version 0.9.1_beta3 of LVM and kernel 2.4.0-ac11, "
"my patch is called <c>lvm-0.9.1_beta3-2.4.0-ac11.patch</c>. You'll find it "
"in your current directory. Now, it's time to apply the patch. To do this, "
"you'll want to change directories to the location of your kernel sources and "
"use the patch command as shown:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):280
msgid "The patch command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):280
#, no-wrap
msgid ""
"\n"
"# <i>cd /usr/src/linux</i>\n"
"# <i>patch -l -p1 &lt; /usr/src/linux/extras/LVM/0.9.1_beta3/PATCHES/lvm-0.9.1_beta3-2.4.0-ac11.patch</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):285
msgid ""
"While the LVM INSTALL documentation doesn't mention it, I typically pass the "
"<c>-l</c> option to patch. This option allows the patch program to "
"compensate for any changes in whitespace (such as minor indentation changes) "
"that would ordinarily cause some hunks of the patch to fail. If the above "
"command completes without any \"FAILED\" lines, then you're ready to install "
"the user-space tools. If not, you'll need to scan your <path>/usr/src/linux</"
"path> directory for <c>.rej</c> files and insert the rejected hunks into the "
"sources by hand using a text editor -- ick! However, in most all situations "
"the patch will apply cleanly and you'll be ready to go."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):300
msgid "Configuring, compiling, and installing"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):303
msgid ""
"OK, you now have a kernel that's been patched so that it has the most "
"current LVM code available. Now, you'll want to configure your kernel so "
"that LVM support is enabled. I recommend that you compile LVM support "
"directly into the kernel rather than configuring it to compile as a module. "
"Fire up your favorite Linux kernel configuration method:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):311
msgid "Configuring the kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):311
#, no-wrap
msgid ""
"\n"
"# <i>cd /usr/src/linux</i>\n"
"# <i>make menuconfig</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):316
msgid ""
"You'll find the LVM options under the \"Multi-device support (RAID and "
"LVM)\" section. Once you enable the first option:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):321
msgid "RAID and LVM support"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):321
#, no-wrap
msgid ""
"\n"
"[*] Multiple devices driver support (RAID and LVM)\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):325
msgid ". . . you'll see the following option, which you should also enable:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):329
msgid "Enable LVM support"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):329
#, no-wrap
msgid ""
"\n"
"&lt;*&gt;   Logical volume manager (LVM) support\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):333
msgid ""
"Depending on your LVM version, there may be other LVM-related options that "
"you'll want to enable as well. Once you're done, save your kernel "
"configuration and perform your standard kernel compilation routine and "
"reboot. Congratulations -- you now have kernel LVM support enabled. Now, we "
"need to get the user-space tools compiled and installed. This step is easy:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):341
msgid "Creating the user-space tools"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):341
#, no-wrap
msgid ""
"\n"
"# <i>cd /usr/src/linux/extras/LVM/0.9.1_beta3</i>\n"
"# <i>make</i>\n"
"# <i>make install</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):347
msgid ""
"There's just one more step, and it's optional. If you're going to be doing "
"more than just testing out LVM, you'll want to add the following lines to "
"your startup rc scripts:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):353
msgid "Editing the startup rc script"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):353
#, no-wrap
msgid ""
"\n"
"/sbin/vgscan\n"
"/sbin/vgchange -a y\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):358
msgid ""
"These lines will scan for all available volume groups and activate them. "
"Then, add the following line to your shutdown rc script, and make sure that "
"it executes after all filesystems have been unmounted:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre:caption):364
msgid "Editing the shutdown rc script"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(pre):364
#, no-wrap
msgid ""
"\n"
"/sbin/vgchange -a n\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):368
msgid ""
"If you're just testing out LVM, then you can skip these steps. Just remember "
"that after every reboot, you'll need to type <c>vgscan</c> and <c>vgchange -"
"a y</c> as root before your logical volumes are available for use."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(p):374
msgid ""
"That's it for this article. Next article, I'll show you how to create your "
"own logical volumes and unleash the power of LVM. I'll see you then!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(title):382
msgid "Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(li):386
msgid ""
"Download the <uri link=\"ftp://sources.redhat.com/pub/lvm2\">LVM tarball</"
"uri> from RedHat."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(li):390
msgid ""
"The impatient will want to check out <uri link=\"http://www.linux.org/docs/"
"ldp/howto/LVM-HOWTO.html\">Heinz Mauelshagen's LVM HOWTO</uri>, which shows "
"you how to set up physical volumes, volume groups, and logical volumes. This "
"is something we'll cover in my next article."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(li):397
msgid ""
"There's also an interesting HOWTO that shows you <uri link=\"http://www.the-"
"infinite.org/archive/docs/lvm/howto-boot-off-root-lv.txt\"> how to set up "
"your root filesystem on a logical volume</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(li):402
msgid ""
"Andreas Dilger is involved with the Linux LVM project and has a nice-looking "
"<e>online</e><uri link=\"http://www-mddsp.enel.ucalgary.ca/People/adilger/"
"online-ext2/\">ext2 filesystem resizer</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(li):408
msgid ""
"<uri link=\"http://www.namesys.com\">ReiserFS</uri> is an excellent "
"filesystem (especially in combination with LVM). If you're using ReiserFS, "
"you'll want to grab the reiserfs-utils tarball, which contains a program "
"called <c>reiserfs_resize</c> -- allowing online resizing of ReiserFS "
"filesystems."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(li):415
msgid ""
"For more information on setting up Linux software RAID volumes, see <uri "
"link=\"/doc/en/articles/software-raid-p1.xml\">Part 1</uri> and <uri link=\"/"
"doc/en/articles/software-raid-p2.xml\">Part 2</uri> of Daniel's series on "
"software RAID."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(li):421
msgid ""
"For a refresher, see Daniel's <uri link=\"/doc/en/articles/linux-kernel-"
"compiling.xml\">tutorial on compiling the Linux kernel</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/articles/lvm-p1.xml(None):0
msgid "translator-credits"
msgstr ""
