msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:11+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):6
msgid "The gentoo.org redesign, Part 1: A site reborn"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(author:title):8
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(mail:link):9
msgid "drobbins@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(mail):9
msgid "Daniel Robbins"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(abstract):12
msgid "Have you ever woken up one morning and suddenly realized that your cute little personal development Web site isn't really that great? If so, you're in good company. In this series, Daniel Robbins shares his experiences as he redesigns the www.gentoo.org Web site using technologies like XML, XSLT, and Python. Along the way, you may find some excellent approaches to use for your next Web site redesign. In this article, Daniel creates a user-centric action plan and introduces pytext, an embedded Python interpreter."
msgstr ""

#. The original version of this article was first published on IBM
#. developerWorks, and is property of Westtech Information Services. This
#. document is an updated version of the original article, and contains
#. various improvements made by the Gentoo Linux Documentation team
#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(version):27
msgid "1.1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(date):28
msgid "2005-10-10"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):31
msgid "An unruly horde"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):35
msgid "Fellow software developer, may I ask you a question? Why is it that although many of us are intimately familiar with Web technologies such as HTML, CGI, Perl, Python, Java technology, and XML, our very own Web sites -- the ones devoted to our precious development projects -- look like they were thrown together by an unruly horde of hyperactive 12-year-olds? Why, oh why, is this so?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):44
msgid "Could it be because most of the time, we've left our Web site out to rot while we squander our precious time hacking away on our free software projects? The answer, at least in my case, is a most definite \"Yes.\""
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):50
msgid "When I'm not writing articles for IBM developerWorks or being a new dad, I'm feverishly working on the next release of Gentoo Linux, along with my skilled team of volunteers. And, yes, Gentoo Linux has its own Web site (see Resources). As of right now (March 2001), our Web site isn't that special; that's because we don't spend much time working on it because we're generally engrossed in improving Gentoo Linux itself. Sure, our site does have several admittedly cute logos that I whipped up using Xara X (see Resources), but when you look past the eye candy, our site leaves a lot to be desired. Maybe yours does too. If so, I have one thing to say to you -- welcome to the club."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):67
msgid "www.gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):71
msgid "In our case, our Web site dilemma exists because our project has been growing, and our Web site hasn't. Now that Gentoo Linux is approaching the 1.0 release (when it'll be officially ready for non-developers) and is growing in popularity, we need to start seriously looking at how our Web site can better serve its users. Here's a snapshot of www.gentoo.org:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(figure:link):80
msgid "/images/docs/l-redesign-01.gif"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(figure:caption):80
msgid "The current (March 2001) state of affairs at www.gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):82
msgid "As you can see, we have all the bare essentials -- a description of Gentoo Linux, a features list, a daily Changelog (automatically updated thanks to Python), and a bunch of important links (to the download sites, to our mailing list sign-up pages, and to cvsWeb). We also have links to three documentation resources -- the Gentoo Linux Install Guide and Development Guides, and Christian Zander's NVIDIA Troubleshooting Guide."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):91
msgid "However, while the site seems O.K., we're missing a lot of things. The most obvious is documentation -- our installation and development guides need a lot of work. And then we need to add an FAQ, new links, new user information...the list is endless."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):101
msgid "Content vs. display"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):104
msgid "And now we come to our second problem. Right now, all of our work is done in raw HTML; I hack away at the index.html file until it looks O.K. Even worse, our Web documentation is written in raw HTML. This isn't a good thing from a development perspective because our raw content (consisting of paragraphs, sections, chapters) is garbled together with a bunch of display-related HTML tags. This, of course, makes it difficult to change both the content and the look of our site. While this approach has worked so far, it is bound to cause problems as our site continues to grow."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):115
msgid "Clearly, we need to be using better technologies behind the scenes. Instead of using HTML directly, we need to start using things like XML, XSLT, and Python. The goal is to automate as much as possible so that we can add and expand our site with ease. If we do our job well, even major future changes to our site should be relatively painless."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):126
msgid "A strategy!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):129
msgid "It was clear that we had a lot of work ahead of us. In fact, there was so much to be done that I didn't know where to begin. Just as I was trying to sort out everything in my head, I came across Laura Wonnacott's \"Site Savvy\" InfoWorld column (see <uri link=\"#resources\">Resources</uri>). In it, she explained the concept of \"user-centric\" design -- how to improve a Web site while keeping the needs of your target audience (in this case, Gentoo Linux users and developers) in focus. Reading the article and taking a look at the \"Handbook of User-Centered Design\" link from the article helped me to formulate a strategy -- an action plan -- for the redesign:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):142
msgid "First, clearly define the official goal of the Web site -- in writing. What's it there for, and what's it supposed to do?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):146
msgid "Identify the different categories of users who will be using your site -- your target audience. Rank them in order of priority: Which ones are most important to you?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):151
msgid "Set up a system for getting feedback from your target audience, so they can let you know what you're doing right and wrong."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):155
msgid "Evaluate the feedback, and use it to determine what parts of the site need to be improved or redesigned. Tackle high-priority sections first."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):159
msgid "Once you've selected the part of the site to improve, get to work! During your implementation, make sure that the content and design of the new section caters specifically to the needs of your target audience and fixes all known deficiencies."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):165
msgid "When the section redesign is complete, add it to your live site, even if it has a look that's markedly different from your current site. This way, your users can begin benefitting from the newly redesigned section immediately. If there's a problem with the redesign, you'll get user feedback more quickly. Finally, making incremental improvements to your site (rather than revamping the whole site and then rolling it out all at once -- surprise!) will help prevent your users from feeling alienated by your (possibly dramatic) site changes."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):175
msgid "After completing step 6, jump to step 4 and repeat."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):181
msgid "The mission statement"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):184
msgid "I was happy to discover that we already had step 3 in place. We had received several e-mail suggestions from visitors to the site, and our developer mailing list also served as a way of exchanging suggestions and comments. However, I had never really completed steps 1 or 2. While the answers may seem obvious, I did find it helpful to actually sit down and write out our mission statement:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):192
msgid "www.gentoo.org exists to assist those who use and develop for Gentoo Linux by providing relevant, up-to-date information about Gentoo Linux and Linux in general, focusing on topics related to Gentoo Linux installation, use, administration, and development. As the central hub for all things Gentoo, the site should also feature important news relevant to Gentoo Linux users and developers. In addition to catering to Gentoo Linux users and developers, www.gentoo.org has the secondary purpose of meeting the needs of potential Gentoo Linux users, providing the information they need to decide whether Gentoo Linux is right for them."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):207
msgid "The target audience"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):210
msgid "So far, so good. Now for step 2 -- defining our target audience:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):214
msgid "www.gentoo.org has three target audiences -- Gentoo Linux developers, users, and potential users. While no one group is absolutely a higher priority than another, right now the needs of Gentoo Linux developers are our highest priority, followed by Gentoo Linux users, and then potential users. This is because Gentoo Linux is currently in a prerelease state. When Gentoo Linux reaches version 1.0, Gentoo Linux users and potential users will also become a priority."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):227
msgid "Comments and suggestions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):230
msgid "O.K., now it's time to evaluate the suggestions and comments we've collected:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):234
msgid "Over the past few months, we've received a number of suggestions from Web site visitors. Overwhelmingly, people are requesting better documentation -- for both developers and users. Several developers have asked if we could create a mailing list that would be devoted exclusively to describing CVS commits."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):241
msgid "Interestingly, we've also received a couple of e-mails asking whether Gentoo Linux is a commercial or free product. I'm guessing that because our main logo is inscribed with the name \"Gentoo Technologies, Inc.\" (our legal corporation name), people assume that we have a commercial focus. Modifying our logo so that it reads \"Gentoo Linux\" and adding small opening paragraph to the main page explaining that we are a free software project should help."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):253
msgid "The improvement list"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):256
msgid "O.K., now let's turn these suggestions into a list of possible improvements:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):263
msgid "Implementation: update logo and add free software blurb"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):264
msgid "Goal: to clearly state that we are a free software project"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):265
msgid "Target group: potential users"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):266 ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):274
msgid "Difficulty: medium"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):261
msgid "Revamp main page <placeholder-1/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):271
msgid "Implementation: new XML/XSLT system, verbose documentation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):272
msgid "Goal: to make it easier for users to install Gentoo Linux"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):273
msgid "Target group: new users"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):269
msgid "Improve basic user documentation <placeholder-1/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):279
msgid "Implementation: new XML/XSLT system, CVS guide, dev guide, Portage guide"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):280
msgid "Goal: to help our developers to do a great job"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):281 ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):289
msgid "Target group: developers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):282
msgid "Difficulty: hard"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):277
msgid "Improve/create developer documentation <placeholder-1/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):287
msgid "Implementation: use our existing mailman mailing list manager"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):288
msgid "Goal: to better inform our developers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):290
msgid "Difficulty: easy"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):285
msgid "Add a CVS mailing list <placeholder-1/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):298
msgid "A selection!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):301
msgid "Two things leap out from the list, for different reasons. The first is the CVS mailing list -- this one is a no-brainer because it's so easy to implement. Often, it makes sense to implement the easiest changes first so that users can benefit from them right away."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):308
msgid "The second big thing that leaps out from the list is the need for developer documentation. This is a longer-term project that will require much more work. From my conversations with the other developers, we all appear to be in agreement that some kind of XML/XSL approach is the right solution."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):318
msgid "The XML/XSL prototype"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):321
msgid "To help start the process, I developed a prototype XML syntax to be used for all our online documentation. By using this XML syntax (called \"guide\"), our documentation will be clearly organized into paragraphs, sections, and chapters (using XML tags like &lt;section&gt;, &lt;chapter&gt;, etc.) while remaining free of any display-related tags. To create the HTML for display on our site, I created a prototype set of XSL transforms. By using an XSLT processor such as Sablotron, our guide XML files can be converted into HTML as follows:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre:caption):331
msgid "Converting guide XML files to HTML"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre):331
#, no-wrap
msgid "\ndevguide.xml + guide.xsl ---XSLT processor---&gt; devguide.html\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):335
msgid "The great thing about this XML/XSLT approach is that it separates our raw content (XML) from the display-related information contained in the guide.xsl (XSLT) file. If we ever need to update the look of our Web pages, we simply modify the guide.xsl file and run all our XML through the XSLT processor (Sablotron), creating updated HTML pages. Or, if we need to add a few chapters to the development guide, we can modify devguide.xml. Once we're done, we then run the XML through Sablotron, which then spits out a fully-formatted devguide.html file with several added chapters. Think of XML as the content and XSLT as the display-related formatting macros."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):347
msgid "While our entire team is convinced that XML/XSLT is the way to go, we haven't yet agreed upon an official XML syntax. Achim, our development lead, suggested that we use docbook instead of rolling our own XML syntax. However, the prototype guide XML format has helped to start the decision-making process. Because we developers are going to be the ones using the XML/XSL on a daily basis, it's important to choose a solution that we're comfortable with and meets all of our needs. By my next article, I should have a working XML/XSL doc system to show off to you."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):361
msgid "Technology demo: pytext"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):364
msgid "For the most part, our current Web site isn't using any new or super-cool technologies that are worth mentioning. However, there's one notable exception -- our tiny pytext embedded Python interpreter."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):370
msgid "Like many of you, I'm a huge Python fan and much prefer it over other scripting languages, so when it came time to add some dynamic content to our Web site, I naturally wanted to use Python. And, as you probably know, when coding dynamic HTML content, it's usually much more convenient to embed the language commands inside the HTML, rather than the other way around. Thus, the need for an embedded Python interpreter that can take a document like this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre:caption):379
msgid "Source document"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre):379
#, no-wrap
msgid "\n&lt;p&gt;\nYeah, sure; I got some questions:&lt;br&gt;\n&lt;!--code\nnames=[\"bob\",\"jimmy\",\"ralph\"]\nitems=[\"socks\",\"lunch\",\"accordion\"]\nfor x in items:\nfor y in names:\nprint \"Anyone seen\",y+\"'s\",x+\"?&lt;br&gt;\"\n--&gt;\nSee, told you so.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):392
msgid "....and transform it into this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre:caption):396
msgid "Target document"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre):396
#, no-wrap
msgid "\n&lt;p&gt;\nYeah, sure; I got some questions:&lt;br&gt;\nAnyone seen bob's socks?&lt;br&gt;\nAnyone seen jimmy's socks?&lt;br&gt;\nAnyone seen ralph's socks?&lt;br&gt;\nAnyone seen bob's lunch?&lt;br&gt;\nAnyone seen jimmy's lunch?&lt;br&gt;\nAnyone seen ralph's lunch?&lt;br&gt;\nAnyone seen bob's accordion?&lt;br&gt;\nAnyone seen jimmy's accordion?&lt;br&gt;\nAnyone seen ralph's accordion?&lt;br&gt;\nSee, told you so.\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):411
msgid "Here's the source code for pytext:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre:caption):415
msgid "The pytext embedded Python interpreter"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre):415
#, no-wrap
msgid "\n#!/usr/bin/env python\n\n# pytext 2.1\n# Copyright 1999-2001 Daniel Robbins\n# Distributed under the GPL\n\nimport sys\n\ndef runfile(myarg):\n   \"interprets a text file with embedded elements\"\n   mylocals={}\n   try:\n      a=open(myarg,'r')\n   except IOError:\n      sys.stderr.write(\"!!! Error opening \"+myarg+\"!\\n\")\n      return\n   mylines=a.readlines()\n   a.close()\n   pos=0\n   while pos&lt;len(mylines):\n      if mylines[pos][0:8]==\"&lt;!--code\":\n  mycode=\"\"\n  pos=pos+1\n  while (pos&lt;len(mylines)) and (mylines[pos][0:3]!=\"--&gt;\"):\n       mycode=mycode+mylines[pos]\n       pos=pos+1\n  exec(mycode,globals(),mylocals)\n       else:\n  sys.stdout.write(mylines[pos])\n       pos=pos+1\n\nif len(sys.argv)&gt;1:\n   for x in sys.argv[1:]:\n       runfile(x)\n   sys.exit(0)\nelse:\n   sys.stderr.write\n     (\"pytext 2.1 -- Copyright 1999-2001 Daniel Robbins. \")\n   sys.stderr.write\n     (\"Distributed under the\\nGNU Public License\\n\\n\")\n   sys.stderr.write\n     (\"Usage: \"+sys.argv[0]+\" file0 [file1]...\\n\")\n   sys.exit(1)\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):464
msgid "How pytext works"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):467
msgid "Here's how it works. It scans each input line, and most of the time, each input line is simply echoed to stdout. However, if pytext encounters a line beginning with &lt;!--code, then the contents of every line up to the first line beginning with --&gt; are appended to a string called mycode. Pytext then executes the mycode string using the built-in exec() function, effectively creating an embedded Python interpreter."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):476
msgid "There's something really beautiful about this particular implementation -- we call exec() in such a way that all modifications to the global and local namespaces are saved. This makes it possible to import a module or define a variable in one embedded block, and then access this previously-created object in a later block, as this example clearly demonstrates:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre:caption):484
msgid "Sample code"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre):484
#, no-wrap
msgid "\n&lt;!--code\nimport os\nfoo=23\n--&gt;\n\nHello\n\n&lt;!--code\nprint foo\nif os.path.exists(\"/tmp/mytmpfile\"):\nprint \"it exists\"\nelse:\nprint \"I don't see it\"\n--&gt;\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):501
msgid "Handy, eh? pytext serves is an excellent demonstration of the power of Python, and is an extremely useful tool for Python fans. For our current site, we call pytext from a cron job, using it to periodically generate the HTML code for our main page Changelog:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre:caption):508
msgid "Generating the HTML code for main page"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(pre):508
#, no-wrap
msgid "\npytext index.ehtml &gt; index.html\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(p):512
msgid "That's it for now; I'll see you next time when we'll take a look at the first stage of the www.gentoo.org redesign!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(title):522
msgid "Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):531
msgid "In <uri link=\"/doc/en/articles/l-redesign-2.xml\">Part 2</uri>, Daniel shows off the new documentation system and sets up a daily CVS-log mailing list (May 2001)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):536
msgid "In <uri link=\"/doc/en/articles/l-redesign-3.xml\">Part 3</uri>, he creates a new look for the site (July 2001)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):540
msgid "In <uri link=\"/doc/en/articles/l-redesign-4.xml\">Part 4</uri>, Daniel completes the conversion to XML/XSLT, fixes a host of Netscape 4.x browser compatibility bugs, and adds an auto-generated XML Changelog to the site (Aug 2001)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):527
msgid "Read the other articles in this developerWorks series about the redesign of the www.gentoo.org Web site using technologies like XML, XSLT, and Python: <placeholder-1/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):548
msgid "If you haven't started using Python yet, you're only hurting yourself. Find it at <uri>http://www.python.org</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):552
msgid "Laura Wonnacott's <uri link=\"http://www.infoworld.com/articles/op/xml/01/03/05/010305opsavvy.xml\">Site Savvy</uri> column appears regularly on InfoWorld.com."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):557
msgid "Check out <uri link=\"http://www.xara.com/\">Xara.com</uri>, the home of Xara X -- an excellent vector drawing package for Windows. With virtually no bloat and blazing speed, it has my personal recommendation."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):562
msgid "Learn more about XSLT at <uri>http://www.xslt.com</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(li):563
msgid "When you wake up, check out Sablotron, a fast XSLT processor available from <uri link=\"http://www.gingerall.com\">Gingerall</uri>"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/articles/l-redesign-1.xml(None):0
msgid "translator-credits"
msgstr ""

