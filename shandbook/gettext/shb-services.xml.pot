msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(version):10
msgid "1.5"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(date):11
msgid "2008-06-13"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):14
msgid "Apache"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):17
msgid "Apache comes with a pretty decent configuration file but again, we need to improve some things, like binding Apache to one address and preventing it from leaking information. Below are the options that you should apply the configuration file."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):24
msgid "If you did not disable <c>ssl</c> in your <path>/etc/make.conf</path> before installing Apache, you should have access to an ssl enabled server. Inside <path>/etc/apache2/vhosts.d</path> example configuration files can be found. These are working examples and it is best to verify those or disable them."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):31
msgid "It is important to define your configuration(s) to listen to a particular IP address (rather than all available IP addresses on your system). For instance, for the <path>00_default_vhost.conf</path> file:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):37
msgid "/etc/apache2/vhosts.d/00_default_vhost.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):37
#, no-wrap
msgid "\n<comment># Make it listen on your ip</comment>\nListen 127.0.0.1\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):42
msgid "We also recommend you to disable showing any information about your Apache installation to the world. By default, the configuration will add server version and virtual host name to server-generated pages. To disable this, change the <c>ServerSignature</c> variable to <c>Off</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):49
msgid "/etc/apache2/modules.d/00_default_settings.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):49
#, no-wrap
msgid "\nServerSignature Off\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):53
msgid "Apache is compiled with <c>--enable-shared=max</c> and <c>--enable-module=all</c>. This will by default enable all modules, so you should comment out all modules in the <c>LoadModule</c> section (<c>LoadModule</c> and <c>AddModule</c>) that you do not use in the main <path>/etc/apache2/httpd.conf</path> configuration file. Restart the service by executing <c>/etc/init.d/apache2 restart</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):62
msgid "Documentation is available at <uri>http://www.apache.org</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):70
msgid "Bind"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):73
msgid "One can find documentation at the <uri link=\"http://www.isc.org/products/BIND/bind9.html\">Internet Software Consortium</uri>. The BIND 9 Administrator Reference Manual is also in the <path>doc/arm</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):80
msgid "The newer BIND ebuilds support chrooting out of the box. After emerging <c>bind</c> follow these simple instructions:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):85
msgid "Chrooting BIND"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):85
#, no-wrap
msgid "\n# <i>emerge --config bind</i>\n<comment>(Before running the above command you might want to change the chroot\ndirectory in /etc/conf.d/named. Otherwise /chroot/dns will be used.)</comment>\n\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):94
msgid "Djbdns"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):97
msgid "Djbdns is a DNS implementation on the security of which its author is willing to bet <uri link=\"http://cr.yp.to/djbdns/guarantee.html\">money</uri>. It is very different from how Bind 9 works but worth a try. More information can be obtained from <uri>http://www.djbdns.org</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):108
msgid "FTP"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):111
msgid "Generally, using FTP (File Transfer Protocol) is a bad idea. It uses unencrypted data (ie. passwords are sent in clear text), listens on 2 ports (normally port 20 and 21), and attackers are frequently looking for anonymous logins for trading warez. Since the FTP protocol contains several security problems you should instead use <c>sftp</c> or HTTP. If this is not possible, secure your services as well as you can and prepare yourself."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):123
msgid "Mysql"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):126
msgid "If you only need local applications to access the <c>mysql</c> database, uncomment the following line in <path>/etc/mysql/my.cnf</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):131
msgid "Disable network access"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):131
#, no-wrap
msgid "\nskip-networking\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):135
msgid "Then we disable the use of the LOAD DATA LOCAL INFILE command. This is to prevent against unauthorized reading from local files. This is relevant when new SQL Injection vulnerabilities in PHP applications are found."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):141
msgid "Disable LOAD DATA LOCAL INFILE in the [mysqld] section"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):141
#, no-wrap
msgid "\nset-variable=local-infile=0\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):145
msgid "Next, we must remove the sample database (test) and all accounts except the local <c>root</c> account."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):150
msgid "Removing sample database and all unnecessary users"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):150
#, no-wrap
msgid "\nmysql&gt; <i>drop database test;</i>\nmysql&gt; <i>use mysql;</i>\nmysql&gt; <i>delete from db;</i>\nmysql&gt; <i>delete from user where not (host=\"localhost\" and user=\"root\");</i>\nmysql&gt; <i>flush privileges;</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(warn):158
msgid "Be careful with the above if you have already configured user accounts."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(note):162
msgid "If you have been changing passwords from the MySQL prompt, you should always clean out <path>~/.mysql_history</path> and <path>/var/log/mysql/mysql.log</path> as they store the executed SQL commands with passwords in clear text."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):172
msgid "Proftpd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):175
msgid "Proftpd has had several security problems, but most of them seem to have been fixed. Nonetheless, it is a good idea to apply some enhancements:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):180
msgid "/etc/proftpd/proftpd.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):180
#, no-wrap
msgid "\nServerName \"My ftp daemon\"\n#Don't show the ident of the server\nServerIdent on \"Go away\"\n\n#Makes it easier to create virtual users\nRequireValidShell off\n\n#Use alternative password and group file (passwd uses crypt format)\nAuthUserFile \"/etc/proftpd/passwd\"\nAuthGroupFile \"/etc/proftpd/group\"\n\n# Permissions\nUmask 077\n\n# Timeouts and limitations\nMaxInstances 30\nMaxClients 10 \"Only 10 connections allowed\"\nMaxClientsPerHost 1 \"You have already logged on once\"\nMaxClientsPerUser 1 \"You have already logged on once\"\nTimeoutStalled 10\nTimeoutNoTransfer 20\nTimeoutLogin 20\n\n#Chroot everyone\nDefaultRoot ~\n\n#don't run as root\nUser  nobody\nGroup nogroup\n\n#Log every transfer\nTransferLog /var/log/transferlog\n\n#Problems with globbing\nDenyFilter \\*.*/\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):218
msgid "One can find documentation at <uri>http://www.proftpd.org</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):225
msgid "Pure-ftpd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):228
msgid "Pure-ftpd is an branch of the original trollftpd, modified for security reasons and functionality by Frank Dennis."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):233
msgid "Use virtual users (never system accounts) by enabling the <c>AUTH</c> option. Set this to <c>-lpuredb:/etc/pureftpd.pdb</c> and create your users by using <c>/usr/bin/pure-pw</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):239
msgid "/etc/conf.d/pure-ftpd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):239
#, no-wrap
msgid "\nAUTH=\"-lpuredb:/etc/pureftpd.pdb\"\n\n## Misc. Others ##\nMISC_OTHER=\"-A -E -X -U 177:077 -d -4 -L100:5 -I 15\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):246
msgid "Configure your <c>MISC_OTHER</c> setting to deny anonymous logins (<c>-E</c>), chroot everyone (<c>-A</c>), prevent users from reading or writing to files beginning with a . (dot) (<c>-X</c>), max idle time (<c>-I</c>), limit recursion (<c>-L</c>), and a reasonable <c>umask</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(warn):253
msgid "Do <e>not</e> use the <c>-w</c> or <c>-W</c> options! If you want to have a warez site, stop reading this guide!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):258
msgid "One can find documentation at <uri>http://www.pureftpd.org</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):265
msgid "Vsftpd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):268
msgid "Vsftpd (short for very secure ftp) is a small ftp daemon running a reasonably default configuration. It is simple and does not have as many features as pureftp and proftp."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):274
msgid "/etc/vsftpd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):274
#, no-wrap
msgid "\nanonymous_enable=NO\nlocal_enable=YES\n\n#read only\nwrite_enable=NO\n\n#enable logging of transfers\nxferlog_std_format=YES\n\nidle_session_timeout=20\ndata_connection_timeout=20\nnopriv_user=nobody\n\nchroot_list_enable=YES\nchroot_list_file=/etc/vsftpd/chrootlist\n\nls_recurse_enable=NO\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):294
msgid "As you can see, there is no way for this service to have individual permissions, but when it comes to anonymous settings it is quite good. Sometimes it can be nice to have an anonymous ftp server (for sharing open source), and vsftpd does a really good job at this."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):304
msgid "Netqmail"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):307
msgid "Netqmail is often considered to be a very secure mail server. It is written with security (and paranoia) in mind. It does not allow relaying by default and has not had a security hole since 1996. Simply <c>emerge netqmail</c> and go configure!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):316
msgid "Samba"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):319
msgid "Samba is a protocol to share files with Microsoft/Novell networks and it should <e>not</e> be used over the Internet. Nonetheless, it still needs securing."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):325
msgid "/etc/samba/smb.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):325
#, no-wrap
msgid "\n[global]\n  #Bind to an interface\n  interfaces = eth0 10.0.0.1/32\n\n  #Make sure to use encrypted password\n  encrypt passwords = yes\n  directory security mask = 0700\n\n  #allow traffic from 10.0.0.*\n  hosts allow = 10.0.0.\n\n  #Enables user authentication\n  #(don't use the share mode)\n  security = user\n\n  #Disallow privileged accounts\n  invalid users = root @wheel\n\n  #Maximum size smb shows for a share (not a limit)\n  max disk size = 102400\n\n  #Uphold the password policy\n  min password length = 8\n  null passwords = no\n\n  #Use PAM (if added support)\n  obey pam restrictions = yes\n  pam password change = yes\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):356
msgid "Make sure that permissions are set correct on every share and remember to read the <uri link=\"http://www.samba.org\">documentation</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):361
msgid "Now restart the server and add the users who should have access to this service. This is done though the command <path>/usr/bin/smbpasswd</path> with the parameter <c>-a</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):370
msgid "ssh"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):373
msgid "The only securing that OpenSSH needs is turning on a stronger authentication based on public key encryption. Too many sites (like <uri>http://www.sourceforge.net</uri>, <uri>http://www.php.net</uri> and <uri>http://www.apache.org</uri>) have suffered unauthorized intrusion due to password leaks or bad passwords."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):381
msgid "/etc/ssh/sshd_config"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):381
#, no-wrap
msgid "\n#Only enable version 2\nProtocol 2\n\n#Disable root login. Users have to su to root\nPermitRootLogin no\n\n#Turn on Public key authentication\nPubkeyAuthentication yes\nAuthorizedKeysFile      .ssh/authorized_keys\n\n#Disable .rhost and normal password authentication\nHostbasedAuthentication no\nPasswordAuthentication no\nPermitEmptyPasswords no\n\n#Only allow userin the wheel or admin group to login\nAllowGroups wheel admin\n\n#In those groups only allow the following users\n#The @&lt;domainname&gt; is optional but replaces the\n#older AllowHosts directive\nAllowUsers kn@gentoo.org bs@gentoo.org\n\n#Logging\nSyslogFacility AUTH\nLogLevel INFO\n\n<comment>(Change this to your address)</comment>\nListenAddress 127.0.0.1\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):413
msgid "Also verify that you don't have <c>UsePAM yes</c> in your configuration file as it overrides the public key authentication mechanism, or you can disable either <c>PasswordAuthentication</c> or <c>ChallengeResponseAuthentication</c>. More information about these options can be found in the <path>sshd_config</path> manual page."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):421
msgid "Now all that your users have to do is create a key (on the machine they want to login from) with the following command:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):426
msgid "Create a DSA keypair"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):426
#, no-wrap
msgid "\n# <i>/usr/bin/ssh-keygen -t dsa</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):430
msgid "And type in a pass phrase."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):434
msgid "Output of ssh-keygen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):434
#, no-wrap
msgid "\nGenerating public/private dsa key pair.\nEnter file in which to save the key (/home/kn/.ssh/id_dsa):<i>[Press enter]</i>\nCreated directory '/home/kn/.ssh'.\nEnter passphrase (empty for no passphrase): <i>[Enter passphrase]</i>\nEnter same passphrase again: <i>[Enter passphrase again]</i>\nYour identification has been saved in /home/kn/.ssh/id_dsa.\nYour public key has been saved in /home/kn/.ssh/id_dsa.pub.\nThe key fingerprint is:\n07:24:a9:12:7f:83:7e:af:b8:1f:89:a3:48:29:e2:a4 kn@knielsen\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):446
msgid "This will add two files in your <path>~/.ssh/</path> directory called <path>id_dsa</path> and <path>id_dsa.pub</path>. The file called <path>id_dsa</path> is your private key and should be kept from other people than yourself. The other file <path>id_dsa.pub</path> is to be distributed to every server that you have access to. Add the key to the users home directory in <path>~/.ssh/authorized_keys</path> and the user should be able to login:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):455
msgid "Adding the id_dsa.pub file to the authorized_keys file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):455
#, no-wrap
msgid "\n$ <i>scp id_dsa.pub other-host:/var/tmp/currenthostname.pub</i>\n$ <i>ssh other-host</i>\npassword:\n$ <i>cat /var/tmp/currenthostname.pub &gt;&gt; ~/.ssh/authorized_keys</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):462
msgid "Now your users should guard this private key well. Put it on a media that they always carry with them or keep it on their workstation (put this in the <uri link=\"?part=1&amp;chap=1#security_policies\">password</uri> policy)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):468
msgid "For more information go to the <uri link=\"http://www.openssh.org\">OpenSSH</uri> web site."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):476
msgid "Using xinetd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):479
msgid "xinetd is a replacement for <c>inetd</c> (which Gentoo does not have), the Internet services daemon. It supports access control based on the address of the remote host and the time of access. It also provide extensive logging capabilities, including server start time, remote host address, remote user name, server run time, and actions requested."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):487
msgid "As with all other services it is important to have a good default configuration. But since <c>xinetd</c> is run as root and supports protocols that you might not know how they work, we recommend not to use it. But if you want to use it anyway, here is how you can add some security to it:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):494
msgid "Install xinetd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):494
#, no-wrap
msgid "\n# <i>emerge xinetd tcp-wrappers</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):498
msgid "And edit the configuration file:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):502
msgid "/etc/xinetd.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):502
#, no-wrap
msgid "\ndefaults\n{\n only_from = localhost\n instances = 10\n log_type = SYSLOG authpriv info\n log_on_success = HOST PID\n log_on_failure = HOST\n cps = 25 30\n}\n\n# This will setup pserver (cvs) via xinetd with the following settings:\n# max 10 instances (10 connections at a time)\n# limit the pserver to tcp only\n# use the user cvs to run this service\n# bind the interfaces to only 1 ip\n# allow access from 10.0.0.*\n# limit the time developers can use cvs from 8am to 5pm\n# use tpcd wrappers (access control controlled in\n# <i>/etc/hosts.allow</i> and <i>/etc/hosts.deny</i>)\n# max_load on the machine set to 1.0\n# The disable flag is per default set to no but I like having\n# it in case of it should be disabled\nservice cvspserver\n{\n socket_type = stream\n protocol = tcp\n instances = 10\n protocol = tcp\n wait = no\n user = cvs\n bind = 10.0.0.2\n only_from = 10.0.0.0\n access_times = 8:00-17:00\n server = /usr/sbin/tcpd\n server_args = /usr/bin/cvs --allow-root=/mnt/cvsdisk/cvsroot pserver\n max_load = 1.0\n log_on_failure += RECORD\n disable = no\n}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):544
msgid "For more information read <c>man 5 xinetd.conf</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(title):552
msgid "X"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):555
msgid "By default Xorg is configured to act as an Xserver. This can be dangerous since X uses unencrypted TCP connections and listens for xclients."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(impo):560
msgid "If you do not need this service disable it!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):564
msgid "But if you depend on using your workstation as a Xserver use the <c>/usr/X11R6/bin/xhost</c> command with caution. This command allows clients from other hosts to connect and use your display. This can become handy if you need an X application from a different machine and the only way is through the network, but it can also be exploited by an attacker. The syntax of this command is <c>/usr/X11R6/bin/xhost +hostname</c>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(warn):573
msgid "Do not ever use the <c>xhost +</c> feature! This will allow any client to connect and take control of your X. If an attacker can get access to your X, he can log your keystrokes and take control over your desktop. If you have to use it always remember to specify a host."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):580
msgid "A more secure solution is to disable this feature completely by starting X with <c>startx -- -nolisten tcp</c> or disable it permanently in the configuration."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):585
msgid "/usr/X11R6/bin/startx"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):585
#, no-wrap
msgid "\ndefaultserverargs=\"-nolisten tcp\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):589
msgid "To make sure that <path>startx</path> does not get overwritten when emerging a new version of Xorg you must protect it. Add the following line to <path>/etc/make.conf</path>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):595
msgid "/etc/make.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):595
#, no-wrap
msgid "\nCONFIG_PROTECT_MASK=\"/usr/X11R6/bin/startx\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):599
msgid "If you use a graphical login manager you need a different approach."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):603
msgid "For <c>gdm</c> (Gnome Display Manager)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):607
msgid "/etc/X11/gdm/gdm.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):607
#, no-wrap
msgid "\n[server-Standard]\ncommand=/usr/X11R6/bin/X -nolisten tcp\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(p):612
msgid "For <c>xdm</c> (X Display Manager) and <c>kdm</c> (Kde Display Manager)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre:caption):616
msgid "/etc/X11/xdm/Xservers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(pre):616
#, no-wrap
msgid "\n:0 local /usr/bin/X11/X -nolisten tcp\n"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/security//shb-services.xml(None):0
msgid "translator-credits"
msgstr ""

