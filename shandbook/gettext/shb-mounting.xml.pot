msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(version):10
msgid "1.0"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(date):11
msgid "2005-05-31"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(title):14
msgid "Mounting partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(p):17
msgid "When mounting an <c>ext2</c>, <c>ext3</c>, or <c>reiserfs</c> partition, you have several options you can apply to the file <path>/etc/fstab</path>. The options are:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(li):24
msgid "<c>nosuid</c> - Will ignore the SUID bit and make it just like an ordinary file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(li):28
msgid "<c>noexec</c> - Will prevent execution of files from this partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(li):31
msgid "<c>nodev</c> - Ignores devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(p):36
msgid "Unfortunately, these settings can easily be circumvented by executing a non-direct path. However, setting <path>/tmp</path> to noexec will stop the majority of exploits designed to be executed directly from <path>/tmp</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(pre:caption):42
msgid "/etc/fstab"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(pre):42
#, no-wrap
msgid "\n/dev/sda1 /boot ext2 noauto,noatime 1 1\n/dev/sda2 none swap sw 0 0\n/dev/sda3 / reiserfs notail,noatime 0 0\n/dev/sda4 /tmp reiserfs notail,noatime,nodev,nosuid,noexec 0 0\n/dev/sda5 /var reiserfs notail,noatime,nodev 0 0\n/dev/sda6 /home reiserfs notail,noatime,nodev,nosuid 0 0\n/dev/sda7 /usr reiserfs notail,noatime,nodev,ro 0 0\n/dev/cdroms/cdrom0 /mnt/cdrom iso9660 noauto,ro 0 0\nproc /proc proc defaults 0 0\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(warn):54
msgid "Placing <path>/tmp</path> in <c>noexec</c> mode can prevent certain scripts from executing properly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(note):59
msgid "For disk quotas see <uri link=\"?part=1&amp;chap=5#quotas\">the Quotas section</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(note):63
msgid "I do not set <path>/var</path> to <c>noexec</c> or <c>nosuid</c>, even if files normally are never executed from this mount point. The reason for this is that netqmail is installed in <path>/var/qmail</path> and must be allowed to execute and access one SUID file. I setup <path>/usr</path> in read-only mode since I never write anything there unless I want to update Gentoo. Then I remount the file system in read-write mode, update and remount again."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(note):72
msgid "Even if you do not use netqmail, Gentoo still needs the executable bit set on <path>/var/tmp</path> since ebuilds are made here. But an alternative path can be setup if you insist on having <path>/var</path> mounted in <c>noexec</c> mode."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/security//shb-mounting.xml(None):0
msgid "translator-credits"
msgstr ""

