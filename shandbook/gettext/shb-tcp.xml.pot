msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(version):10
msgid "1.1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(date):11
msgid "2010-04-26"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(title):14
msgid "TCP Wrappers"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(p):17
msgid "This is a way of controlling access to services normally run by inetd (which Gentoo does not have), but it can also be used by xinetd and other services."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(note):22
msgid "The service should be executing tcpd in its server argument (in xinetd). See the chapter on xinetd for more information."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(pre:caption):27
msgid "/etc/hosts.deny"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(pre):27
#, no-wrap
msgid "\nALL:PARANOID\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(pre:caption):31
msgid "/etc/hosts.allow"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(pre):31
#, no-wrap
msgid "\nALL: LOCAL @wheel\ntime: LOCAL, .gentoo.org\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(p):36
msgid "As you can see the format is very similar to the one in <path>/etc/security/access.conf</path>. Tcpd supports a specific service; it does not overlap with <path>/etc/security/access.conf</path>. These settings only apply to services using tcp wrappers."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(p):43
msgid "It is also possible to execute commands when a service is accessed (this can be used when activating relaying for dial-in users) but it is not recommended, since people tend to create more problems than they are trying to solve. An example could be that you configure a script to send an e-mail every time someone hits the deny rule, but then an attacker could launch a DoS attack by keep hitting the deny rule. This will create a lot of I/O and e-mails so don't do it!. Read the <c>man 5 hosts_access</c> for more information."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/security//shb-tcp.xml(None):0
msgid "translator-credits"
msgstr ""

