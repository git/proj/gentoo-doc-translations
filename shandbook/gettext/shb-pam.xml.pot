msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(version):10
msgid "1.1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(date):11
msgid "2006-08-03"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(title):14
msgid "PAM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(p):17
msgid "PAM is a suite of shared libraries that provide an alternative way providing user authentication in programs. The <c>pam</c> USE flag is turned on by default. Thus the PAM settings on Gentoo Linux are pretty reasonable, but there is always room for improvement. First install cracklib."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(pre:caption):24
msgid "Installing cracklib"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(pre):24
#, no-wrap
msgid "\n# <i>emerge cracklib</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(pre:caption):28
msgid "/etc/pam.d/passwd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(pre):28
#, no-wrap
msgid "\nauth     required pam_unix.so shadow nullok\naccount  required pam_unix.so\npassword required pam_cracklib.so difok=3 retry=3 minlen=8 dcredit=-2 ocredit=-2\npassword required pam_unix.so md5 use_authtok\nsession  required pam_unix.so\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(p):36
msgid "This will add the cracklib which will ensure that the user passwords are at least 8 characters and contain a minimum of 2 digits, 2 other characters, and are more than 3 characters different from the last password. This forces the user to choose a good password (password policy). Check the <uri link=\"http://www.kernel.org/pub/linux/libs/pam/Linux-PAM-html/sag-pam_cracklib.html\">PAM</uri> documentation for more options."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(pre:caption):45
msgid "/etc/pam.d/sshd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(pre):45
#, no-wrap
msgid "\nauth     required pam_unix.so nullok\nauth     required pam_shells.so\nauth     required pam_nologin.so\nauth     required pam_env.so\naccount  required pam_unix.so\npassword required pam_cracklib.so difok=3 retry=3 minlen=8 dcredit=-2 ocredit=-2 use_authtok\npassword required pam_unix.so shadow md5\nsession  required pam_unix.so\nsession  required pam_limits.so\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(p):57
msgid "Every service not configured with a PAM file in <path>/etc/pam.d</path> will use the rules in <path>/etc/pam.d/other</path>. The defaults are set to <c>deny</c>, as they should be. But I like to have a lot of logs, which is why I added <c>pam_warn.so</c>. The last configuration is <c>pam_limits</c>, which is controlled by <path>/etc/security/limits.conf</path>. See the <uri link=\"?part=1&amp;chap=5#limits_conf\">/etc/security/limits.conf</uri> section for more on these settings."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(pre:caption):67
msgid "/etc/pam.d/other"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(pre):67
#, no-wrap
msgid "\nauth     required pam_deny.so\nauth     required pam_warn.so\naccount  required pam_deny.so\naccount  required pam_warn.so\npassword required pam_deny.so\npassword required pam_warn.so\nsession  required pam_deny.so\nsession  required pam_warn.so\n"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/security//shb-pam.xml(None):0
msgid "translator-credits"
msgstr ""

