msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:56+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(version):10
msgid "1.1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(date):11
msgid "2005-10-13"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(title):14
msgid "Keeping up-to-date"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(p):17
msgid "Once you have successfully installed your system and ensured a good level of security you are not done. Security is an ongoing process; the vast majority of intrusions result from known vulnerabilities in unpatched systems. Keeping your system up-to-date is the single most valuable step you can take to greater security."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(p):25
msgid "If you have a recent version of <c>portage</c> installed, you can first sync your portage tree with <c>emerge --sync</c> and then issue the command <c>glsa-check --list</c> to check if your system is up to date security-wise. <c>glsa-check</c> is part of <c>app-portage/gentoolkit</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(pre:caption):32
msgid "Example output of glsa-check -l"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(pre):32
#, no-wrap
msgid "\n# <i>glsa-check -l</i>\nWARNING: This tool is completely new and not very tested, so it should not be\nused on production systems. It's mainly a test tool for the new GLSA release\nand distribution system, it's functionality will later be merged into emerge\nand equery.\nPlease read http://www.gentoo.org/proj/en/portage/glsa-integration.xml\nbefore using this tool AND before reporting a bug.\n\n[A] means this GLSA was already applied,\n[U] means the system is not affected and\n[N] indicates that the system might be affected.\n\n200406-03 [N] sitecopy: Multiple vulnerabilities in included libneon ( net-misc/sitecopy )\n200406-04 [U] Mailman: Member password disclosure vulnerability ( net-mail/mailman )\n.......\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(warn):50
msgid "The <c>glsa-check</c> is still experimental, so if security really is your top priority it would be wise to double check the list with other sources."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(p):55
msgid "All lines with a <c>[A]</c> and <c>[U]</c> can be almost safely ignored as the system is not affected by this GLSA."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(impo):60
msgid "Please note that the usual <c>emerge -vpuD world</c> will not pick up all package updates. You need to use <c>glsa-check</c> if you want to make sure all GLSAs are fixed on your system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(pre:caption):66
msgid "Check all GLSAs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(pre):66
#, no-wrap
msgid "\n<comment>(Check if your system is affected by GLSAs)</comment>\n# <i>glsa-check -t all</i>\nWARNING: This tool is completely new and not very tested, so it should not be\nused on production systems. It's mainly a test tool for the new GLSA release\nand distribution system, it's functionality will later be merged into emerge\nand equery.\nPlease read http://www.gentoo.org/proj/en/portage/glsa-integration.xml\nbefore using this tool AND before reporting a bug.\n\nThis system is affected by the following GLSA:\n200504-06\n200510-08\n200506-14\n200501-35\n200508-12\n200507-16\n\n<comment>(See what packages would be emerged)</comment>\n# <i>glsa-check -p $(glsa-check -t all)</i>\n     <comment>(partial output)</comment>\nChecking GLSA 200504-06\nThe following updates will be performed for this GLSA:\n     app-arch/sharutils-4.2.1-r11 (4.2.1-r10)\n\n     **********************************************************************\n\n     Checking GLSA 200510-08\n     The following updates will be performed for this GLSA:\n          media-libs/xine-lib-1.1.0-r5 (1.1.0-r4)\n\n<comment>(Apply required fixes)</comment>\n# <i>glsa-check -f $(glsa-check -t all)</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(p):101
msgid "If you have upgraded a running service, you should not forget to restart it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(p):105
msgid "Keeping your <uri link=\"/doc/en/kernel-upgrade.xml\">kernel up-to-date</uri> is also recommended."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(p):110
msgid "If you want an email each time a GLSA is released subscribe to the <c>gentoo-announce</c> mailing list. Instructions for joining it and many other great mailing lists can be found <uri link=\"/main/en/lists.xml\">Gentoo Linux Mailing List Overview</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(p):117
msgid "Another great security resource is the <uri link=\"http://www.securityfocus.com/archive/1\">Bugtraq mailing list</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/security//shb-uptodate.xml(None):0
msgid "translator-credits"
msgstr ""

