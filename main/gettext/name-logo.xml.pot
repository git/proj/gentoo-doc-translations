msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-05-18 19:19+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(guide:link):6
msgid "/main/en/name-logo.xml"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(guide:lang):6
msgid "en"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):7
msgid "Gentoo Name and Logo Usage Guidelines"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(author):9
msgid "The Gentoo Foundation"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(abstract):13
msgid "This document covers the guidelines on the Gentoo Name and Logo usage."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(version):21
msgid "1.8"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(date):22
msgid "2005-12-11"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):25
msgid "Preliminaries"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):29
msgid "This document describes proper use of the Gentoo name, the \"g\" logo and any other associated marks for software and computer-related efforts that are not under the direction of the Gentoo Foundation, Inc. If you have any questions about these guidelines, please contact <mail link=\"trustees@gentoo.org\">Gentoo Foundation, Inc.</mail>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):37
msgid "Gentoo Foundation, Inc. reserve the right to change these terms from time to time at their sole discretion. For that reason, we encourage you to review this statement periodically."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):48
msgid "Definitions"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):50
msgid "The Gentoo project"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):53
msgid "The volunteer development efforts currently directed by Gentoo Foundation, Inc., a not-for-profit corporation."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):61
msgid "Content produced by the Gentoo project"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):64
msgid "Any content that is copyright Gentoo Foundation, Inc."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):71
msgid "The \"g\" logo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(fig:link):74
msgid "/images/glogo-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):79
msgid "Gentoo artwork"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):82
msgid "All artwork produced by <e>the Gentoo project</e> (see above definition)."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):89
msgid "Gentoo community site"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):92
msgid "A non-profit website whose primary objective is to provide any Gentoo user with additional information on Gentoo and Gentoo-related topics."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):102
msgid "Ownership of marks"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):106
msgid "The name \"Gentoo\" and the \"g\" logo are currently trademarks of Gentoo Foundation, Inc."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):116
msgid "Use of Gentoo name"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):120
msgid "You are permitted to use the Gentoo name in computer-related content, provided that:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):126
msgid "You acknowledge that the name \"Gentoo\" is a trademark of Gentoo Foundation, Inc., and"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):130
msgid "you do not entitle any software project or computer-related product \"Gentoo\" or have \"Gentoo\" appear within its name, and"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):134
msgid "the fully-qualified domain name for your software project or computer-related products does not contain \"Gentoo\", and"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(e):139
msgid "you clearly state that the content, project, site, product or any other type of item with which the \"Gentoo\" name is associated is not part of the Gentoo project and is not directed or managed by Gentoo Foundation, Inc."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):147
msgid "We do not consider these restrictions to apply to the <uri link=\"http://www.obsession.se/gentoo/\">Gentoo file manager</uri> project, developed by Emil Bink, as it is not an operating system project."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):158
msgid "Use of Gentoo Logo and artwork"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):162
msgid "You are permitted to use the Gentoo \"g\" logo and artwork copyright Gentoo Foundation, Inc. (hereafter called \"Gentoo artwork\") under the following conditions:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):171
msgid "Commercial Use"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):174
msgid "Commercial use of the Gentoo \"g\" logo and Gentoo artwork is allowed for <e>any software or computer hardware product</e> that <e>contains</e> or is <e>based upon</e> content produced by the Gentoo project, or any project or service referencing Gentoo or the Gentoo Project, provided that the following conditions are met:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):183
msgid "You acknowledge that the \"g\" logo is a trademark of Gentoo Foundation, Inc. and that any Gentoo artwork is copyright Gentoo Foundation, Inc., and"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):187
msgid "the artwork used is <e>not</e> the primary (largest) mark displayed on the product, and is thus intended to convey that the product contains \"content produced by the Gentoo project\", but is <e>not itself a product being sold or supported</e> by Gentoo Foundation, Inc."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(e):196
msgid "Commercial use of the Gentoo \"g\" logo and Gentoo artwork for any other purpose is expressly denied."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):205
msgid "Non-Commercial Use"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):208
msgid "Non-commercial use of the \"g\" logo and Gentoo artwork is permitted provided that the following conditions are met:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):214
msgid "You acknowledge that the \"g\" logo is a trademark of Gentoo Foundation, Inc., and that any Gentoo artwork is copyright Gentoo Foundation, Inc, and,"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):218
msgid "the \"g\" logo and Gentoo artwork are used in content that pertain to \"the Gentoo project\", as directed by the Gentoo Foundation, Inc., and not any effort outside or beyond the authority of the Gentoo Foundation, Inc., and"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(e):224
msgid "you clearly state that the content, project, site, product or any other type of item with which the \"g\" logo or Gentoo artwork is associated is not part of the Gentoo project and is not directed or managed by Gentoo Foundation, Inc."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(title):238
msgid "Gentoo Community Projects"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):242
msgid "Gentoo Foundation, Inc. grant Gentoo community sites, as defined above, the right to use the Gentoo name in their project name and fully qualified domain name provided that:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):249
msgid "the website clearly states, on each page, that the project is no official Gentoo project by labelling itself as a \"news site\", \"fan site\", \"unofficial site\" or \"community site\", and"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):254
msgid "the website does not closely mimic the name or layout of one of our official websites, and"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(li):258
msgid "you acknowledge that the name \"Gentoo\" is a trademark of Gentoo Foundation, Inc."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(p):264
msgid "Any other use of the Gentoo name, logo and artwork remains bound by the beforementioned guidelines."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/main/en//name-logo.xml(None):0
msgid "translator-credits"
msgstr ""

