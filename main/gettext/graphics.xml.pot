msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:13+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(title):6
msgid "Gentoo Linux Graphics"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(author:title):8
msgid "Previous Chief Architect"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(mail:link):9
msgid "drobbins@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(mail):9
msgid "Daniel Robbins"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(author:title):11 ../../gentoo/xml/htdocs/main/en//graphics.xml(author:title):14
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(mail:link):12
msgid "curtis119@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(mail):12
msgid "Curtis Napier"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(mail):15
msgid "sping"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(abstract):18
msgid "Listing of various graphical resources available for use under the terms of the Gentoo Name and Logo Usage Guidelines"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(version):25
msgid "5"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(date):26
msgid "2011-05-04"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(title):29
msgid "Gentoo Branded Artwork"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(title):31
msgid "Terms of Use"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(p):34
msgid "The graphics on this page are written by various authors who grant you permission to use and modify them as you see fit. The use of the Gentoo name and logo are governed by the <uri link=\"/main/en/name-logo.xml\">Gentoo Name and Logo Usage Guidelines</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(title):44
msgid "Contribute"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(p):47
msgid "If you want to contribute a graphic to Gentoo, please use the <uri link=\"http://forums.gentoo.org\">Gentoo Forums</uri> first so others can comment and help you improve the quality of the graphic. Then contact <mail link=\"www@gentoo.org\">the Gentoo WWW team</mail> and ask for your graphic to be included."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(p):55
msgid "The team may decide not to include your artwork if it is too close to existing graphics, is not available in sufficient resolutions, does not fit the Gentoo spirit (for instance because it is offensive) or any other reason."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(p):61
msgid "Including a graphic on the page can take some time, please be patient."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(title):68
msgid "Logos and graphical building blocks"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(p):71
msgid "For logos and other graphical building blocks please check the <uri link=\"/proj/en/desktop/artwork/artwork.xml\">Gentoo Artwork page</uri>, instead."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(title):79
msgid "Gentoo Linux Badges"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(p):86
msgid "Put a <e>Powered by Gentoo</e> image on your Gentoo powered web sites or use a <e>Gentoo Badge</e> on your web page, blog, forum signature or elsewhere and link back to <uri link=\"http://www.gentoo.org\">http://www.gentoo.org</uri> - help us spread the word! Tell others how happy you are with Gentoo Linux."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):96
msgid "/images/powered-show.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):99
msgid "/images/powered-by-gentoo.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):100
msgid "/images/powered-by-gentoo2.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):101
msgid "/images/powered-by-gentoo3.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):102
msgid "/images/powered-by-gentoo4.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):105
msgid "/images/ralph_gentoo_badge.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):108
msgid "/images/gentoo-badge.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):112
msgid "<uri link=\"/images/powered-small.png\">Small</uri> - <uri link=\"/images/powered.png\">Medium</uri> - <uri link=\"/images/powered-big.png\">Large</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):117
msgid "by Sascha Schwabbauer"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):120
msgid "by Ralph Jacobs"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):123
msgid "by Ryan Viljoen"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):129
msgid "/images/gentoo-badge2.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):132
msgid "/images/gentoo-badge3.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):135
msgid "/images/szbence-badge1.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):136
msgid "/images/szbence-badge2.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):137
msgid "/images/szbence-badge3.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):138
msgid "/images/szbence-badge4.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):139
msgid "/images/szbence-badge5.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):140
msgid "/images/szbence-badge6.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):143
msgid "/images/hardened-badge.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):147
msgid "by <uri link=\"http://forums.gentoo.org/profile.php?mode=viewprofile&amp;u=36364\">m@o</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):150
msgid "by <uri link=\"http://forums.gentoo.org/profile.php?mode=viewprofile&amp;u=26433\">wolven</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):153
msgid "by <uri link=\"http://forums.gentoo.org/viewtopic-t-7763-start-50.html\">Szabó Bence</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):156
msgid "by <uri link=\"http://www.liquidustech.com/\">Matthew Summers</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(title):165
msgid "Gentoo Linux Wallpapers"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(p):173
msgid "Use one of these <e>Gentoo Wallpapers</e> to beautify your desktop. Show every one that you run Gentoo Linux and run it with pride."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):185
msgid "/images/backgrounds/gentoo120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):188
msgid "/images/backgrounds/gentoo-cycle-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):192
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo640x480.jpg\">640x680</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo1280x1024.jpg\">1280x1024</uri>&nbsp;<br/> by <uri link=\"http://forums.gentoo.org/viewtopic.php?t=6745\">mksoft</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):204
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-cycle-640x480.jpg\">640x480</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-cycle-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-cycle-1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-cycle-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo-cycle-1280x1024.jpg\">1280x1024</uri>&nbsp;<br/> by <uri link=\"http://forums.gentoo.org/viewtopic.php?t=6868\">mksoft</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):219
msgid "/images/backgrounds/gentoo-ice-light2-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):222
msgid "/images/backgrounds/gentoo-ice-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):226
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-ice-light2-640x480.jpg\">640x480</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-ice-light2-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-ice-light2-1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-ice-light2-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo-ice-light2-1280x1024.jpg\">1280x1024</uri>&nbsp;<br/> by <uri link=\"http://forums.gentoo.org/viewtopic.php?t=7993\">mksoft</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):238
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-ice-640x480.jpg\">640x480</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-ice-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-ice-1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-ice-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo-ice-1280x1024.jpg\">1280x1024</uri>&nbsp;<br/> by <uri link=\"http://forums.gentoo.org/viewtopic.php?t=7672\">mksoft</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):253
msgid "/images/backgrounds/gentoo-box-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):256
msgid "/images/backgrounds/gentoo-minimal-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):260
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-box-640x480.jpg\">640x480</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-box-1024x768.png\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-box-1152x864.png\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-box-1600x1200.png\">1600x1200</uri>&nbsp;<br/> by Luca Martinetti"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):269
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-minimal-800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-minimal-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-minimal-1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-minimal-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo-minimal-1280x1024.jpg\">1280x1024</uri>&nbsp;<br/> by <uri link=\"http://forums.gentoo.org/viewtopic-t-365758.html\">Brian Wigginton</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):285
msgid "/images/backgrounds/gentoo-amd64_1-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):288
msgid "/images/backgrounds/gentoo-amd64_2-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):292
msgid "5:4 @ <uri link=\"/images/backgrounds/gentoo-amd64_1-1280x1024.jpg\">1280x1024</uri><br/> by Tedder Wayne"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):298
msgid "5:4 @ <uri link=\"/images/backgrounds/gentoo-amd64_2-1280x1024.jpg\">1280x1024</uri><br/> by Tedder Wayne"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):307
msgid "/images/backgrounds/gentoo-glass-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):310
msgid "/images/backgrounds/gentoo-causticswp-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):314
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-glass-800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-glass-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-glass-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> by <uri link=\"http://kde-look.org/content/show.php/Gentoo+Glass?content=29181\">Robert Krig</uri>, licensed under <uri link=\"http://www.fsf.org/licenses/gpl.html\">GPL</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):322
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-causticswp-800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-causticswp-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-causticswp-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> by <uri link=\"http://kde-look.org/content/show.php/Gentoo+Water?content=29183\">Robert Krig</uri>, licensed under <uri link=\"http://www.fsf.org/licenses/gpl.html\">GPL</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):335
msgid "/images/backgrounds/gentoo-chojindsl_2-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):338
msgid "/images/backgrounds/gentoo-chojindsl_1-240x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):342
msgid "<uri link=\"/images/backgrounds/gentoo-chojindsl_2.blend\">Original Blender file</uri><br/> 4:3 @ <uri link=\"/images/backgrounds/gentoo-chojindsl_2-800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-chojindsl_2-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-chojindsl_2-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> by <uri link=\"http://kde-look.org/content/show.php/Red+Green+Blue?content=29187\">Robert Krig</uri>, licensed under <uri link=\"http://www.fsf.org/licenses/gpl.html\">GPL</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):352
msgid "<br/> 4:3 @ <uri link=\"/images/backgrounds/gentoo-chojindsl_1-1600x1200.jpg\">3200x1200</uri>&nbsp;<br/> by <uri link=\"http://kde-look.org/content/show.php/Red+Green+Blue+%28Dual+Desktop%29?content=29186\">Robert Krig</uri>, licensed under <uri link=\"http://www.fsf.org/licenses/gpl.html\">GPL</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):363
msgid "/images/backgrounds/gentoo-water-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):366
msgid "/images/backgrounds/cow-push-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):370
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-water-800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-water-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-water-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> by <uri link=\"http://kde-look.org/content/show.php/Gentoo+Water?content=29180\">Robert Krig</uri>, licensed under <uri link=\"http://www.fsf.org/licenses/gpl.html\">GPL</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):378
msgid "4:3 @ <uri link=\"/images/backgrounds/cow-push-800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/cow-push-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/cow-push-1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/cow-push-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> by <uri link=\"http://forums.gentoo.org/profile.php?mode=viewprofile&amp;u=100549\">Tero Konttila</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):391
msgid "/images/backgrounds/cow-push2-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):394
msgid "/images/backgrounds/macosx-gentoo-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):398
msgid "4:3 @ <uri link=\"/images/backgrounds/cow-push2-800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/cow-push2-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/cow-push2-1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/cow-push2-1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> by <uri link=\"http://forums.gentoo.org/profile.php?mode=viewprofile&amp;u=100549\">Tero Konttila</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):408
msgid "4:3 @ <uri link=\"/images/backgrounds/macosx-gentoo-800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/macosx-gentoo-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/macosx-gentoo-1152x864.jpg\">1152x864</uri>&nbsp;<br/> by Przemysław Pazik"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):419
msgid "/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-120x90.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):422
msgid "/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-120x90.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):426
msgid "<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515.svg\">Original SVG</uri><br/> 4:3 @ <uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-1024x768.png\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-1152x864.png\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-1600x1200.png\">1600x1200</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-4:3.svg\">SVG</uri>&nbsp;<br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-1280x1024.png\">1280x1024</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-5:4.svg\">SVG</uri>&nbsp;<br/> 8:5 @ <uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-1440x900.png\">1440x900</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-big-logo-20060515-8:5.svg\">SVG</uri>&nbsp;<br/> by Marius Morawski, licensed under <uri link=\"http://www.gnu.org/licenses/gpl-2.0.html\">GPL version 2</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):445
msgid "<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-20060515.svg\">Original SVG</uri><br/> 4:3 @ <uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-1024x768.png\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-1152x864.png\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-1600x1200.png\">1600x1200</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-4:3.svg\">SVG</uri>&nbsp;<br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-1280x1024.png\">1280x1024</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-5:4.svg\">SVG</uri>&nbsp;<br/> 8:5 @ <uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-1440x900.png\">1440x900</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-freebsd-wallpaper-dark-skies-8:5.svg\">SVG</uri>&nbsp;<br/> by Marius Morawski, licensed under <uri link=\"http://www.gnu.org/licenses/gpl-2.0.html\">GPL version 2</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):467
msgid "/images/backgrounds/gentoo-abducted-120x90.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):470
msgid "/images/backgrounds/larry-cave-cow-120x90.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):474
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-abducted-800x600.png\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-abducted-1024x768.png\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-abducted-1152x864.png\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-abducted-1600x1200.png\">1600x1200</uri><br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo-abducted-1280x1024.png\">1280x1024</uri>&nbsp;<br/> 8:5 @ <uri link=\"/images/backgrounds/gentoo-abducted-1680x1050.png\">1680x1050</uri>&nbsp;<br/> by <uri link=\"http://peach.smartart.it/wallpaper/gentoo-abducted\">Matteo 'Peach' Pescarin</uri> and <uri link=\"http://new.myfonts.com/person/Ethan_Paul_Dunham/\">Ethan Dunham</uri><br/> licensed under <uri link=\"http://creativecommons.org/licenses/by-sa/2.5/\">CC-BY-SA/2.5</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):491
msgid "4:3 @ <uri link=\"/images/backgrounds/larry-cave-cow-1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/larry-cave-cow-1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/larry-cave-cow-1600x1200.jpg\">1600x1200</uri><br/> 5:4 @ <uri link=\"/images/backgrounds/larry-cave-cow-1280x1024.jpg\">1280x1024</uri>&nbsp;<br/><br/> by <uri link=\"http://kde-look.org/content/show.php/Larry+the+%28Cave%29Cow?content=18639\">Sami Tikka</uri>, licensed under <uri link=\"http://creativecommons.org/licenses/by-nc-sa/2.0/\">CC-BY-NC-SA/2.0</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):506
msgid "/proj/en/pr/releases/10.0/images/wallpaper/purple/fullhdthumb.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):509
msgid "/proj/en/pr/releases/10.0/images/wallpaper/blue/fullhdthumb.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):513
msgid "<uri link=\"/proj/en/pr/releases/10.0/graphics.xml\">Gentoo 10.0 LiveDVD Graphics</uri><br/> 4:3 @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1280x960.jpg\">1280x960</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> 8:5 @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1280x800.jpg\">1280x800</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1440x900.jpg\">1440x900</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1920x1200.jpg\">1920x1200</uri>&nbsp;<br/> 16:9 @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1280x720.jpg\">1280x720</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1600x900.jpg\">1600x900</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1920x1080.jpg\">1920x1080</uri>&nbsp;<br/> odd @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/purple/1024x600.jpg\">1024x600</uri>&nbsp;"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):536
msgid "<uri link=\"/proj/en/pr/releases/10.0/graphics.xml\">Gentoo 10.0 LiveDVD Graphics</uri><br/> 4:3 @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/800x600.jpg\">800x600</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1024x768.jpg\">1024x768</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1152x864.jpg\">1152x864</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1280x960.jpg\">1280x960</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1600x1200.jpg\">1600x1200</uri>&nbsp;<br/> 8:5 @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1280x800.jpg\">1280x800</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1440x900.jpg\">1440x900</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1920x1200.jpg\">1920x1200</uri>&nbsp;<br/> 16:9 @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1280x720.jpg\">1280x720</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1600x900.jpg\">1600x900</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1920x1080.jpg\">1920x1080</uri>&nbsp;<br/> odd @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/blue/1024x600.jpg\">1024x600</uri>&nbsp;"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):562
msgid "/proj/en/pr/releases/10.0/images/wallpaper/red/thumb.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(fig:link):565
msgid "/images/backgrounds/gentoo-larry-bg-250x156.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):569
msgid "<uri link=\"/proj/en/pr/releases/10.0/graphics.xml\">Gentoo 10.0 LiveDVD Graphics</uri><br/> 8:5 @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/red/1280x800.jpg\">1280x800</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/red/1440x900.jpg\">1440x900</uri>&nbsp;<uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/red/1920x1200.jpg\">1920x1200</uri>&nbsp;<br/> odd @ <uri link=\"/proj/en/pr/releases/10.0/images/wallpaper/red/1024x600.jpg\">1024x600</uri>&nbsp;"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//graphics.xml(ti):580
msgid "4:3 @ <uri link=\"/images/backgrounds/gentoo-larry-bg-800x600.png\">800x600</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1024x768.png\">1024x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1152x864.png\">1152x864</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1280x960.png\">1280x960</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1600x1200.png\">1600x1200</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-4:3.svg\">SVG</uri>&nbsp;<br/> 5:4 @ <uri link=\"/images/backgrounds/gentoo-larry-bg-1280x1024.png\">1280x1024</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-5:4.svg\">SVG</uri>&nbsp;<br/> 8:5 @ <uri link=\"/images/backgrounds/gentoo-larry-bg-1280x800.png\">1280x800</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1440x900.png\">1440x900</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1680x1050.png\">1680x1050</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1920x1200.png\">1920x1200</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-8:5.svg\">SVG</uri>&nbsp;<br/> 16:9 @ <uri link=\"/images/backgrounds/gentoo-larry-bg-1280x720.png\">1280x720</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1366x768.png\">1366x768</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1600x900.png\">1600x900</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-1920x1080.png\">1920x1080</uri>&nbsp;<uri link=\"/images/backgrounds/gentoo-larry-bg-16:9.svg\">SVG</uri>&nbsp;<br/><br/> by <uri link=\"https://wilber.deviantart.com/\">Dávid Kótai</uri>, <uri link=\"http://peach.smartart.it/\">Matteo Pescarin</uri> and <uri link=\"http://new.myfonts.com/person/Ethan_Paul_Dunham/\">Ethan Dunham</uri><br/> licensed under <uri link=\"http://creativecommons.org/licenses/by-sa/2.5/\">CC-BY-SA/2.5</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/main/en//graphics.xml(None):0
msgid "translator-credits"
msgstr ""

