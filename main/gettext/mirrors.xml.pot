msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:51+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(mainpage:redirect):7
msgid "mirrors2.xml"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(title):8
msgid "Gentoo Linux Mirrors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(author:title):10
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(mail:link):11
msgid "jforman@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(mail):11
msgid "Jeffrey Forman"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(version):14
msgid "0"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(title):17
msgid "Gentoo Download Mirrors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(title):20
msgid "Other Mirrors:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(uri:link):23
msgid "http://distfiles.gentoo.org/"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(uri):23
msgid "Gentoo distfiles mirror (Global)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(uri:link):24
msgid "Please upgrade to"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(uri):24
msgid "app-portage/mirrorselect-2.0.0 or higher"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/main/en//mirrors.xml(None):0
msgid "translator-credits"
msgstr ""

