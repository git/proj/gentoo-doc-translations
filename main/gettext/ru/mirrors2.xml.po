msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:51+0600\n"
"PO-Revision-Date: 2009-10-12 06:14+0400\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(title):9
msgid "Gentoo Linux Mirrors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(author:title):11
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(mail:link):12
msgid "jforman@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(mail):12
msgid "Jeffrey Forman"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(author:title):14
#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(author:title):17
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(mail:link):15
msgid "fox2mike@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(mail):15
msgid "Shyam Mani"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(mail:link):18
msgid "astinus@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(mail):18
msgid "Alex Howells"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(abstract):21
msgid "List of Gentoo Mirrors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(version):27
msgid "2.1"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(date):28
msgid "2009-12-03"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(title):31
msgid "Gentoo Download Full Mirrors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(p):35
msgid ""
"The following organizations provide a full source mirror of all files "
"related to Gentoo, including installation CDs, LiveCDs and GRP package sets."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(impo):40
msgid ""
"These mirrors are download mirrors. The rsync-mirrors listed here are "
"<e>not</e> for individual use (i.e. <c>emerge --sync</c>) as that would "
"download the full mirror instead of just the Portage tree."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(note):46
msgid "* indicates mirrors that support IPv6"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(title):55
msgid "Other Mirrors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(uri:link):60
msgid "http://ibiblio.org/pub/Linux/MIRRORS.html"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(uri):60
msgid "Ibiblio.org(worldwide ibiblio mirrors)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(title):69
msgid "Partial Mirrors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(p):73
msgid ""
"These mirrors are not complete mirrors. Some provide only distfiles, while "
"others provide only installation media. Please use one of the full mirrors "
"if you cannot find what you're looking for."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(title):88
msgid "Mirroring Resources"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(uri:link):93
msgid "/doc/en/rsync.xml"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(uri):93
msgid "How to set up a Gentoo rsync mirror"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(uri:link):94
msgid "/doc/en/source_mirrors.xml"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(uri):94
msgid "How to set up a Gentoo source mirror"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/main/en//mirrors2.xml(None):0
msgid "translator-credits"
msgstr ""
