msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-10-28 22:39+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):6
msgid "Gentoo Linux Screenshots"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(author:title):8
msgid "Previous Chief Architect"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(mail:link):9
msgid "drobbins"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(author:title):11 ../../gentoo/xml/htdocs/main/en//shots.old.xml(author:title):14 ../../gentoo/xml/htdocs/main/en//shots.old.xml(author:title):15
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(mail:link):12
msgid "swift"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(author):14
msgid "Sergey Kuleshov"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(mail:link):16
msgid "dabbott"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(abstract):19
msgid "Screenshots of Gentoo Linux in action."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(version):27
msgid "1.44"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(date):28
msgid "2010-08-10"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):41
msgid "Here they are... click for larger PNG versions!"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):44
msgid "Disclaimer"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):47
msgid "The screenshots on this page are contributed by various Gentoo users. Information about the themes used, background images, etc. is not available through Gentoo. You will need to contact the author of the screenshot (if you can find this person) for more information."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):54
msgid "You can always try locating the authors on our #gentoo chat channel on irc.freenode.net or through the <uri link=\"http://forums.gentoo.org\">Gentoo Forums</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):64
msgid "E17 EFL overlay (Chopinzee)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):71
msgid "The window manager is E17, built from the official EFL overlay with <c>itask-ng</c>. The window manager theme is A-SBlack2.edj for the main, grey.edj for the clock, and detour-glossy-red.edj for the <c>itask-ng</c> bar. The wallpaper is Black_wall.edj <c>gimp</c>ed to properly render at 1920x1080. The terminals are <c>x11-terms/terminal</c>, GTK theme is \"crude.\" The application running in the busy shot is <c>mixxx</c>. The compositor is <c>ecomp</c> with the <c>ecomorph</c> module. The icons on the <c>itask-ng</c> bar are \"Token Light.\""
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):82
msgid "/images/shots/chopinzee-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):88
msgid "Simple is Beautiful (whtwtr)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):95
msgid "Simple is beautiful <e>but</e> it must be functional and provide the user with tangible information."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):100
msgid "Shawn's box currently runs <c>compiz</c> as the window manager with <c>xfce4-panel</c>. The wallpaper is plain old #000000 (black). Two <c>app-admin/conky-1.8.0</c> instances render the desktop eye-candy. The first <c>conky</c> instance powers the ring meters. The rings represent cpu usage (large white rings), memory usage (green ring), and file system usage (yellow rings). The second <c>conky</c> instance runs the bottom info bar with uptime, temps, up/download speeds, calendar and curent time. The globe image is taken from die.net and is a \"Mollweide\" projection of the earth's day/night shadow and He uses <c>cron</c> plus a small script to update the image every hour. (The clouds are updated every three hours.) Cool, eh?"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):113
msgid "/images/shots/whtwtr-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):119
msgid "Xfce from the xfce-dev overlay (GentooApologetin)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):126
msgid "Mona's custom Xfce environment is from the xfce-dev overlay. It's on an ~amd64 desktop running <c>compiz-fusion</c>, modded Salmon gtk2 theme, recolored Humanity Colors icon theme, <c>conky</c> and <c>pcmanfm2</c> for the desktop, <c>lxterminal</c> and a fine-tuned claws-mail instance. It's completely lightweight and runs stable enough as her everyday main system."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):134
msgid "/images/shots/mona-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):140
msgid "Gnome Lover's Dream (shpaq)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):147
msgid "The judges liked Michał's balance between icon theme, background, custom <c>conky</c> fonts and the whole look of his desktop."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):152
msgid "/images/shots/shpaq-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):158
msgid "Simple Elegance (nm)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):165
msgid "The window manager is <c>dwm</c>. Terminal emulator: <c>urxvt</c>. Wallpaper: of unknown origin. Font: Proggy Tiny Slashed Zero."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):170
msgid "/images/shots/nm-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):176
msgid "(Almost) default GNOME"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):179
msgid "Bartek's shot is another of the screenshot contest winners. Bartek got this decent look by using the depth illusion of a 3D wallpaper along with clever arrangement of pretty icons. This is good example of how a default theme can be used to create a beautiful screenshot."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):186
msgid "/images/shots/gnome-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):192
msgid "Elegant black WM"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):195
msgid "Another winner of Gentoo Screenshot Contest is Mikołaj, a.k.a. mklimek, who is a KDE user so far, and he doesn't intend to change desktops. On his screenshot you can see some KDE utilities and icons . . . which looks kinda Gnome-like."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):201
msgid "/images/shots/rash-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):207
msgid "Space trip"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):210
msgid "Robert's screenshot demonstrates an appealing use of blue hues. Excluding <c>cairo-clock</c>, his KDE runs only Qt-based applications: <c>kopete</c>, <c>konsole</c>, and <c>amarok</c>. Robert also cares about his <c>irssi</c> theme, which is distinguished by green colors."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):217
msgid "/images/shots/sshotpw4-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):223
msgid "Even more gentooish"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):226
msgid "That is probably the most \"Gentooish\" screenshot here ever and personally for me it is the most beautiful. Alexander runs Gnome with the Lila icon set, which not only looks pretty but also befits a Gentoo workstation."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):232
msgid "/images/shots/Bildschirmfoto-small.png"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(title):238
msgid "Emerge that!"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(p):241
msgid "As you can tell, Massimiliano is going to build <c>media-video/dvdauthor</c> on his KDE box."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(fig:link):246
msgid "/images/shots/darkgentoo2sf2-small.png"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/main/en//shots.old.xml(None):0
msgid "translator-credits"
msgstr ""

