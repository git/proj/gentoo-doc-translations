msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-10-28 22:39+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):6
msgid "Gentoo Mailing Lists"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(author:title):8 ../../gentoo/xml/htdocs/main/en//lists.xml(author:title):11
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail:link):9
msgid "lcars"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail):9
msgid "Andrea Barisani"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail:link):12
msgid "cybersystem"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail):12
msgid "Sascha Schwabbauer"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(author:title):14 ../../gentoo/xml/htdocs/main/en//lists.xml(author:title):17 ../../gentoo/xml/htdocs/main/en//lists.xml(author:title):20 ../../gentoo/xml/htdocs/main/en//lists.xml(author:title):23
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail:link):15
msgid "curtis119"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail):15
msgid "Curtis Napier"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail:link):18
msgid "klieber"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail):18
msgid "Kurt Lieber"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail:link):21 ../../gentoo/xml/htdocs/main/en//lists.xml(ti):177 ../../gentoo/xml/htdocs/main/en//lists.xml(ti):187 ../../gentoo/xml/htdocs/main/en//lists.xml(ti):207 ../../gentoo/xml/htdocs/main/en//lists.xml(ti):307
msgid "robbat2"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail):21
msgid "Robin H. Johnson"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(mail:link):24
msgid "nightmorph"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(abstract):27
msgid "Gentoo public mailing lists"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(version):35
msgid "7.02"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(date):36
msgid "2011-09-13"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):39
msgid "Mailing Lists"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):43
msgid "The Gentoo free software project has a number of public mailing lists, covering a variety of Gentoo-related subjects. Our mailing lists are powered by <uri link=\"http://mlmmj.mmj.dk\">mlmmj</uri> and provides <c>List-Id:</c> mail headers and <c>[listname]</c> subject prefixes to comply with modern mailing list manager standards and conventions."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):51
msgid "One interacts with <c>mlmmj</c> via email. To subscribe to a list, send an empty email to:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(note):60
msgid "Replace 'listname' with the actual name of the list that you want to subscribe to, e.g.: <c>gentoo-user+subscribe@lists.gentoo.org</c> to subscribe to the <c>gentoo-user</c> mailing list."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):66
msgid "You will then recieve a subscription confirmation request (double opt-in) from the list manager, that you must reply to if you wish to subscribe."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):71
msgid "Once subscribed to the list, you can post to it by sending an email to:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):79
msgid "To unsubscribe from a list, send an empty email to:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(note):87
msgid "You must use the identical address that you subscribed with to unsubscribe successfully. If your email address is now forwarded/rewritten beyond your control, please contact the list owner via <c>listname+owner@lists.gentoo.org</c> with a request for manual removal."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):94
msgid "You will then recieve a unsubscription confirmation request (double opt-in) from the list manager, that you must reply to if you wish to be unsubscribed."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):99
msgid "All of our lists also have a corresponding digest list. Digest lists will mail a single email to you every couple of days, rather than individual emails for each post. If you are subscribed to the digest variant and wish to be unsubscribed, you must specifically unsubscribe from the digest variant. Using the email address that you wish to (un)subscribe, send an empty email to the following addresses to subscribe and unsubscribe from mailing lists respectively:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):114
msgid "Some users may want to post to the list, but not actually receive mail from it (such as those who read lists via an alternate method, such as gmane). These users may subscribe to the \"nomail\" option of each list:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):125
msgid "You can learn more about the capabilities of mlmmj by sending an empty mail to the following address:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(impo):134
msgid "You can also learn more about the Gentoo mailing list system, including etiquette and acceptable behavior, by reading the short Gentoo <uri link=\"#faq\">mailing list FAQ</uri> that appears later in this document."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):140
msgid "Very few of the Gentoo lists are moderated: only the -announce lists are fully moderatored. Additionally, some of our high-traffic lists have moderators in place for spam and unsubscribe requests (matching mail via regex). The moderators can be reached by emailing: <c>listname+moderators@lists.gentoo.org</c>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):147
msgid "To get on a \"Restricted\" lists, you must be subscribed by an administrator; the normal subscribe process is disabled."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):155
msgid "Primary Gentoo Mailing Lists"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(th):160 ../../gentoo/xml/htdocs/main/en//lists.xml(th):444 ../../gentoo/xml/htdocs/main/en//lists.xml(th):561 ../../gentoo/xml/htdocs/main/en//lists.xml(th):619
msgid "List name"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(th):161 ../../gentoo/xml/htdocs/main/en//lists.xml(th):445 ../../gentoo/xml/htdocs/main/en//lists.xml(th):562 ../../gentoo/xml/htdocs/main/en//lists.xml(th):620
msgid "Description"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(th):162 ../../gentoo/xml/htdocs/main/en//lists.xml(th):446 ../../gentoo/xml/htdocs/main/en//lists.xml(th):563
msgid "Moderators"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):166
msgid "General Gentoo user support and discussion mailing list"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):171
msgid "General Gentoo announcements list (new releases, security fixes) [moderated]"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):172
msgid "a3li,craig,dberkholz,falco,jaervosz,keytoaster,klieber,py,rajiv,rbu,underling,vorlon"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):176
msgid "General Gentoo developer discussion mailing list"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):181
msgid "Gentoo development-specific announcements list [moderated]"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):182
msgid "dberkholz,robbat2,vapier"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):186
msgid "For the discussion of non-technical matters in Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):191
msgid "For the discussion of security issues and fixes"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):196
msgid "For documentation contributions, suggestions, improvements and translations"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):201
msgid "Subscribe to this list if you want to be notified on changes regarding our documentation"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):206
msgid "Subscribe to this list if you want to be notified on changes in the CVS, SVN and Git trees. This is a high-traffic read-only list!"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):211
msgid "For Gentoo Linux/PowerPC user support and discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):216
msgid "For Gentoo Linux/PowerPC developer discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):221
msgid "For Gentoo Linux/Alpha user support and discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):226
msgid "For Gentoo Linux/AMD64 user support and discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):231
msgid "Discussions about running Gentoo on the HPPA architecture"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):236
msgid "Discussions about running Gentoo on the MIPS architecture"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):241
msgid "For Gentoo Linux/Sparc user support and discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):246
msgid "Discussion about Gentoo/BSD"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):251
msgid "Discussion about the <uri link=\"/proj/en/gentoo-alt/\">Gentoo on Alternate Platforms Project</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):256
msgid "Release announcements for gentoo-sources, vesafb-tng, fbsplash and discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):261
msgid "Discussion on powersaving, pcmcia and other laptop-related stuff"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):266
msgid "Mailing list devoted to Gentoo on the desktop"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):271
msgid "Discussions about improving the Gentoo desktop experience"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):276
msgid "For a security hardened version of Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):281
msgid "Portage Internals and Portage Interface Development Discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):286
msgid "Mailinglist dedicated to catalyst"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):291
msgid "Discussions about Gentoo in production environments"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):296
msgid "Discussion about Gentoo Linux administration issues"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):301
msgid "Discussions about Gentoo in clustered environments"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):306
msgid "Discussion and help with ebuild developement issues for users"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):311
msgid "Discussion of web configuration and administration related to Gentoo's web tools"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):316
msgid "For Gentoo Linux/embedded user and developer discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):321
msgid "Mailinglist for the Gentoo release management team"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):326
msgid "Mailinglist for the output of the Gentoo weekly autobuilds output from the release management team"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):331
msgid "Mailinglist for all Gentoo public-relation discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):336
msgid "Discussions about QA and its improvement within Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):341
msgid "Gentoo Developer Relations mailing list"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):346
msgid "Gentoo User Relations mailing list"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):351
msgid "Gentoo Council mailing list"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):356
msgid "Announcements and discussion among Gentoo mirror admins and developers regarding releases and other issues"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):361
msgid "Discussion of perl on Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):366
msgid "Discussion about Java on Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):371
msgid "Discussion on science-related applications and integration in Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):376
msgid "Discussion about the <uri link=\"/proj/en/desktop/accessibility/\">Gentoo Accessibility Project</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):381
msgid "Discussion within United Kingdom developers and UK based events organisation"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):386
msgid "Discussion within Australian developers and local events organisation"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):391
msgid "Discussion on Gentoo activities related to Google's Summer of Code"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):396
msgid "Discussion about Lisp on Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):401
msgid "Discussion about VDR on Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):406
msgid "The Gentoo NFP/Trustees Mailing list"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):407
msgid "calchan,dabbott,fox2mike,neddyseagoon,quantumsummers,rich0,robbat2"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):411
msgid "Discussion about migration of primary Gentoo repositories to alternate SCMs."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):416
msgid "Discussion about the Gentoo Package Manager Specification."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):421
msgid "Discussion about <uri link=\"/proj/en/guis/\">Gentoo specific gius development and user support</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):426
msgid "Discussion about development of genkernel"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):431
msgid "Discussions centering around the Python ecosystem in Gentoo Linux"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):439
msgid "Non-English Mailing Lists"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):450
msgid "deutschsprachige Gentoo User Diskussionsliste"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):451
msgid "dertobi123"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):455
msgid "Brazilian Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):460
msgid "Greek Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):465
msgid "Lista para la ayuda y discusion de usuarios hispano-hablantes de Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):470
msgid "French Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):475
msgid "Hungarian Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):480
msgid "Indonesian Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):485
msgid "Dutch Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):490
msgid "Polish Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):495
msgid "Czech and Slovak Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):500
msgid "Russian Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):505
msgid "Turkish Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):510
msgid "German Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):515
msgid "Greek Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):520
msgid "Lista de correo dedicada a la traduccion y creacion de documentacion en Espanol de Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):528
msgid "Finnish Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):533
msgid "French Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):538
msgid "Italian Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):543
msgid "Polish Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):548
msgid "Russian Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):556
msgid "Other Mailing Lists"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):567
msgid "For the discussion of libconf development"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):572
msgid "Special-purpose list for the Gentoo Bug Wranglers. This mailing list is by invite only. If you are interested in joining, simply get active on bugzilla and help our existing members wrangle bugs. You'll get noticed and invited to be a bug-wrangler in due course."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):582
msgid "For testing of the list management software (restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):587
msgid "Internal Gentoo developer discussions (non-development, restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):592
msgid "Legally required announcements of the Gentoo Foundation to foundation members (restricted &amp; moderated list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):593
msgid "dabbott,neddyseagoon,quantumsummers,rich0,robbat2"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):597
msgid "Internal Gentoo Infrastructure project list (restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):602
msgid "Internal Gentoo Foundation Trustees discussions (restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):610
msgid "Inactive, Closed Gentoo Mailing Lists"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):613
msgid "Archives still exist but these lists are closed for new posts."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):624
msgid "Indonesian Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):628
msgid "Lithuanian Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):632
msgid "Hungarian Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):636
msgid "Dutch Gentoo Documentation Translation List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):640
msgid "Gentoo Forums translations list"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):644
msgid "For discussion of document translation issues"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):648
msgid "Discussions about running Gentoo on the ARM architecture"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):652
msgid "Gentoo Weekly Newsletter"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):656
msgid "German Gentoo Weekly Newsletter"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):660
msgid "Spanish Gentoo Weekly Newsletter"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):664
msgid "French Gentoo Weekly Newsletter"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):668
msgid "Dutch Gentoo Weekly Newsletter"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):672
msgid "Polish Gentoo Weekly Newsletter"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):676
msgid "Gentoo Weekly Newsletter - administratriva for English authors (restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):680
msgid "Gentoo Weekly Newsletter - administratriva for German translations (restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):684
msgid "Gentoo Weekly Newsletter - administratriva for Spanish translations (restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):688
msgid "Gentoo Monthly Newsletter (shared with gentoo-gwn)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):692
msgid "German Gentoo Monthly Newsletter (shared with gentoo-gwn-de)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):696
msgid "Spanish Gentoo Monthly Newsletter (shared with gentoo-gwn-es)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):700
msgid "French Gentoo Monthly Newsletter (shared with gentoo-gwn-fr)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):704
msgid "Dutch Gentoo Monthly Newsletter (shared with gentoo-gwn-nl)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):708
msgid "Polish Gentoo Monthly Newsletter (shared with gentoo-gwn-pl)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):712
msgid "Gentoo Monthly Newsletter - administratriva for English authors (shared with gentoo-gwn-admin, restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):716
msgid "Gentoo Monthly Newsletter - administratriva for German translations (shared with gentoo-gwn-admin-de, restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):720
msgid "Gentoo Monthly Newsletter - administratriva for Spanish translations (shared with gentoo-gwn-admin-es, restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):724
msgid "Discussion of programming language support and related issues in Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):728
msgid "For Gentoo Linux/ia64 user support and discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):732
msgid "Discussion about the <uri link=\"/proj/en/scire/\">Systems Configuration, Installation and Replication Environment Project</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):739
msgid "Discussion about Gentoo media packages"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):743
msgid "Discussion about Gentoo for Xbox"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):747
msgid "For Gentoo cygwin user support and discussion"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):751
msgid "Dedicated to the development of the new Gentoo website"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):755
msgid "Discussion about the <uri link=\"/proj/en/releng/installer/\">Gentoo Linux Installer Project</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):762
msgid "Discussions about improving the performance of Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):766
msgid "Discussion about Gentoo Extreme Security project"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):770
msgid "Discussion about GNUstep on Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):774
msgid "Discussion about Gentoo on OSX project (merged to gentoo-alt list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):778
msgid "Commit emails for CVS (merged to gentoo-commits list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):782
msgid "Discussions of the Gentoo Proctors project (restricted list)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):786
msgid "Korean Gentoo User Mailing List"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):790
msgid "Discussion about development of the Gentoo GNAP project"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):794
msgid "Announcements about the Gentoo Tenshi project"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(ti):798
msgid "Discussion about development of the Gentoo Tenshi project"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):807
msgid "Archives"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):811
msgid "Gentoo mailing list archives are collected on <br/><uri link=\"http://archives.gentoo.org\">archives.gentoo.org</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):816
msgid "The following sites also host archives of most of the mailing lists.<br/><uri link=\"http://news.gmane.org/search.php?match=gentoo\">Gmane</uri><br/><uri link=\"http://marc.theaimsgroup.com/\">MARC: Mailing list ARChives</uri><br/><uri link=\"http://www.mail-archive.com\">Mail-Archive</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):828
msgid "Mailing List Mini-FAQ"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):837
msgid "To reduce spam, all of our lists are configured to only allow posts from official subscriber email addresses. Fortunately, <c>mlmmj</c> supports \"nomail\" subscriptions, allowing you to register alternate email addresses that can be used only for posting to the list. Here's an example of how this works. Let's say you subscribed to the <c>gentoo-dev</c> list as <c>jim@home.com</c>, but you'd also like to post to the list using your <c>james@work.com</c> email address. To do this, send a message (as <c>james@work.com</c>) to <c>gentoo-dev+subscribe-nomail@lists.gentoo.org</c> You should then be allowed to post to <c>gentoo-dev</c> using both your home and work email addresses."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):849
msgid "In line with the original spam reduction goal, if you post to the list from a non-subscribed address, your mail will be delivered to <path>/dev/null</path>. You will <b>not</b> receive any bounce message from our servers. This prevents spammers from forging your address to get a bounce delivered to you."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):861
msgid "Unsubscribe from the normal list and then subscribe to the digest list. For list <c>listname</c>, this would be done by sending empty emails to the following two addresses:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):875
msgid "To filter incoming mail arriving from list <c>listname</c>, use the following <c>procmail</c> recipe:"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(pre:caption):880
msgid "Sample procmail recipe"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(pre):880
#, no-wrap
msgid "\n:0:\n* ^List-Id:.*listname\\.gentoo\\.org\nMail/listname\n"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):886
msgid "This is identical to how you'd filter incoming <e>Mailman</e> mailing list manager emails."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):895
msgid "HTML email is discouraged, but not forbidden (there are some MUA, specifically web-based that make it very hard to disable HTML entirely). Beware that some users may have their recieving side configured to hide HTML entirely, so it might look like you are ignored, especially if you sent HTML-only email. In order of preference, you should endeavour to send: text/plain, multipart with text/plain before text/html, text/html. MIME is acceptable and widely used."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):904
msgid "Please do not send any vacation or out of office messages to the list. In the interests of reducing list spam, if you set any auto-responding message that goes to the lists, we will unsubscribe your account from ALL lists. Any previously subscribed addresses that send mail server messages to the lists will also be removed."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):912
msgid "Do not cross-post when sending messages to a list! Sending the same message to more than one list at a time leads to fragmentation of the thread when some people reply on one list, and some on another list. There's no guarantee that the recipients of your message are on both lists. Choose just one list when sending a message."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(note):920
msgid "For general email list etiquette, <uri link=\"http://www.dtcc.edu/cs/rfc1855.html\">these guidelines</uri> are an excellent primer."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):930
msgid "If you need additional help beyond the above, please <uri link=\"http://bugs.gentoo.org/enter_bug.cgi?product=Gentoo%20Infrastructure&amp;version=unspecified&amp;component=Mailing%20Lists&amp;rep_platform=All&amp;op_sys=Linux&amp;priority=P2&amp;bug_severity=normal&amp;bug_status=NEW&amp;alias=&amp;bug_file_loc=http%3A%2F%2F&amp;short_desc=&amp;comment=&amp;commentprivacy=0&amp;keywords=&amp;dependson=&amp;blocked=&amp;bit-25=1&amp;maketemplate=Remember%20values%20as%20bookmarkable%20template&amp;form_name=enter_bug&amp;assigned_to=infra-bugs%40gentoo.org\"> file a bug</uri>, or contact <c>listname+owner@lists.gentoo.org</c>"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(title):940
msgid "Privacy policy &amp; disclaimer"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):944
msgid "The mailing lists are public forums, except for lists explicitly otherwise. For lists explicitly marked private or restricted, all of the following except web archives are applicable."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):950
msgid "All emails sent to the lists are distributed both to the list subscribers and copied to the <uri link=\"http://archives.gentoo.org/\">Gentoo archives</uri>, for people to browse or search without the need to be subscribed."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):956
msgid "There may be other places where lists are distributed — please make sure you never send any confidential or unlicensed material to the lists. This includes things like e-mail addresses. Of particular note is the fact that spammers, viruses, worms etc have been known to abuse e-mail addresses posted to our mailing lists."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):964
msgid "Gentoo maintains the mailing lists in good faith and will take steps to curb all noticed abuse and maintain uninterrupted normal service. At the same time, Gentoo is not responsible for all mailing list posts or anything that may happen in relation to them."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//lists.xml(p):971
msgid "These policies are based on the <uri link=\"http://www.debian.org/MailingLists/disclaimer\"> policies of the Debian mailing lists</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/main/en//lists.xml(None):0
msgid "translator-credits"
msgstr ""

