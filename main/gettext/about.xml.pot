msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:51+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/main/en//about.xml(title):6 ../../gentoo/xml/htdocs/main/en//about.xml(title):36
msgid "About Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(author:title):7
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail:link):8
msgid "drobbins@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail):8
msgid "Daniel Robbins"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(author:title):10 ../../gentoo/xml/htdocs/main/en//about.xml(author:title):13 ../../gentoo/xml/htdocs/main/en//about.xml(author:title):16 ../../gentoo/xml/htdocs/main/en//about.xml(author:title):19 ../../gentoo/xml/htdocs/main/en//about.xml(author:title):22
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail:link):11
msgid "curtis119@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail):11
msgid "Curtis Napier"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail:link):14
msgid "peesh@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail):14
msgid "Jorge Paulo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail:link):17
msgid "klieber@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail):17
msgid "Kurt Lieber"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail:link):20
msgid "fox2mike@gmail.com"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail):20
msgid "Shyam Mani"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail:link):23
msgid "swift@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(mail):23
msgid "Sven Vermeulen"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(abstract):26
msgid "About Gentoo, an overview."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(version):32
msgid "1.7"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(date):33
msgid "2007-09-17"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(fig:link):40
msgid "/images/poster.jpg"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(title):45
msgid "What is Gentoo?"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(p):48
msgid "Gentoo is a free operating system based on either Linux or FreeBSD that can be automatically optimized and customized for just about any application or need. Extreme configurability, performance and a top-notch user and developer community are all hallmarks of the Gentoo experience."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(p):55
msgid "Thanks to a technology called Portage, Gentoo can become an ideal secure server, development workstation, professional desktop, gaming system, embedded solution or something else -- whatever you need it to be. Because of its near-unlimited adaptability, we call Gentoo a <b>meta</b>distribution."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(p):62
msgid "Of course, Gentoo is more than just the software it provides. It is a community built around a distribution which is driven by more than 300 developers and thousands of users. The distribution project provides the means for the users to enjoy Gentoo: documentation, infrastructure (mailinglists, site, forums ...), release engineering, software porting, quality assurance, security followup, hardening and more."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(p):71
msgid "To advise on and help with Gentoo's global development, a <uri link=\"/proj/en/council/\">7-member council</uri> is elected on a yearly basis which decides on global issues, policies and advancements in the Gentoo project."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(title):81
msgid "What is Portage?"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(p):84
msgid "<b>Portage</b> is the heart of Gentoo, and performs many key functions. For one, Portage is the <e>software distribution</e> system for Gentoo. To get the latest software for Gentoo, you type one command: <c>emerge --sync</c>. This command tells Portage to update your local \"Portage tree\" over the Internet. Your local Portage tree contains a complete collection of scripts that can be used by Portage to create and install the latest Gentoo packages. Currently, we have <uri link=\"http://packages.gentoo.org/categories\">more than 10000 packages</uri> in our Portage tree, with updates and new ones being <uri link=\"http://packages.gentoo.org\">added all the time</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(p):96
msgid "Portage is also a <e>package building and installation</e> system. When you want to install a package, you type <c>emerge packagename</c>, at which point Portage automatically builds a custom version of the package to your exact specifications, optimizing it for your hardware and ensuring that the optional features in the package that you want are enabled -- and those you don't want aren't."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//about.xml(p):105
msgid "Portage also keeps your system <e>up-to-date</e>. Typing <c>emerge -uD world</c> -- one command -- will ensure that all the packages that <e>you</e> want on your system are updated automatically."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/main/en//about.xml(None):0
msgid "translator-credits"
msgstr ""

