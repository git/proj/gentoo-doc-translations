msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-10-28 22:39+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):6
msgid "Gentoo Stores"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(author:title):8 ../../gentoo/xml/htdocs/main/en//stores.xml(author:title):12
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(mail:link):9
msgid "robbat2"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(mail):9
msgid "Robin H. Johnson"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(mail:link):13
msgid "dabbott"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(mail):13
msgid "David Abbott"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(abstract):17
msgid "This document lists the places that Gentoo merchandise may be purchased."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(version):23
msgid "17"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(date):24
msgid "2011-10-15"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):27
msgid "Vendor Information"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):31
msgid "There are many types of business that are providing solutions based on Gentoo, from a simple pre-load of Gentoo for a desktop PC, to a complex multi-vendor application environment."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):37
msgid "If you simply don't have the ability to download the large DVD or CD images, then you may wish to purchase a Gentoo DVD or CD."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):47
msgid "Approved Vendors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):51
msgid "Gentoo merchanise may be purchased from the following vendors who have been licenced to use the Gentoo Logo by either the <uri link=\"http://www.gentoo.org/foundation/en/\">Gentoo Foundation Inc</uri> (in the USA) or <uri link=\"http://www.gentoo-ev.org\">Friends of Gentoo e.V.</uri> within the European Union"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):59
msgid "Approved Vendors make a small contribution to Gentoo from sales of Gentoo related merchandise."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):67
msgid "Americas"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):70
msgid "<uri link=\"http://www.cafepress.com/officialgentoo/\">Offical Gentoo Cafepress Store</uri> located in the USA"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):75
msgid "<uri link=\"http://www.case-badges.com/gentoo-logo-3d-domed-computer-case-badges-p-212.html\"> Techiant, LLC</uri> for Case Badges located in the USA"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):84
msgid "Asia"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):87
msgid "<uri link=\"http://www.zyxware.com/requestcd?title=gentoo\"> Zyxware Technologies</uri> located in India"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):96
msgid "Europe"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):99
msgid "<uri link=\"http://22258.spreadshirt.net/de/DE/Shop\"> Offical Friends of Gentoo e.V Store</uri> located in Germany"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):104
msgid "<uri link=\"http://www.linuxpusher.com/distributions/Gentoo-Linux\"> LinuxPusher</uri> located in Denmark"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):110
msgid "<uri link=\"http://www.penguenci.com/index.php?route=product/manufacturer&amp;manufacturer_id=39\"> Penguenci</uri> located in Turkey"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):121
msgid "If you buy something from a licenced vendor and you like it, tell all your friends."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):125
msgid "If you have problems with quality or the vendor, please tell <mail link=\"trustees@gentoo.org\">us</mail> and the vendor. We do not undertake to resolve individual issues but we take the quality of goods bearing our logo very seriously indeed."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):136
msgid "Gentoo Linux DVDs"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):144
msgid "http://www.jbox.ca/software/linux-distributions/gentoo/"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):144
msgid "Stormfront Gentoo Section"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):153
msgid "http://www.distribuicoeslinux.com.br/"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):153
msgid "Brazilian Store of Linux Media"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):161
msgid "Unlicensed Vendors"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):163
msgid "Information"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):166
msgid "If you would like a listing here, please email the <mail link=\"trustees@gentoo.org\">trustees</mail>. A listing on this page is included in the <uri link=\"name-logo.xml\">license</uri>. Gentoo must defend its registered marks to prevent them becoming public domain and we try to do it in the friendy way that is the hallmark of the open source community."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):178
msgid "Gentoo Linux CDs"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):186
msgid "http://www.cheapbytes.com"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):186
msgid "CheapBytes"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):190
msgid "http://edmunds-enterprises.com"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):190
msgid "Edmunds Enterprises"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):194
msgid "http://www.frozentech.com"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):194
msgid "Frozentech"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):199
msgid "http://www.osdisc.com/cgi-bin/view.cgi/products/linux/gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):199
msgid "OSDisc.com"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):203
msgid "http://www.thelinuxstore.ca"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):203
msgid "The Linux Store"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):211
msgid "http://getlinux.leprado.com"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):211
msgid "Getlinux (FR)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):215
msgid "http://www.linux2go.de"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):215
msgid "Linux2Go (DE)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):219
msgid "http://www.cheaplinux.de/systeme/gentoo.php"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):219
msgid "CheapLinux (DE)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):223
msgid "http://www.linux-distro.co.uk"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):223
msgid "Linux-Distro (UK)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):231
msgid "http://shop.linuxit.com.au/"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):231
msgid "Linux Information Technology (Australia)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):235
msgid "http://www.lsl.com.au/"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):235
msgid "Linux System Labs (Australia)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):239
msgid "http://www.linuxcdmall.com/gentoo.html"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):239
msgid "Linux CD Mall (New Zealand)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):246
msgid "Computers with Gentoo Linux pre-installed"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):249
msgid "The following companies sell computers that come pre-installed with Gentoo Linux."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):259
msgid "http://linuxcertified.com/gentoo-laptop.html"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):259
msgid "Linux Certified, Inc."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):263
msgid "http://www.microway.com"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):263
msgid "Microway, Inc."
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):271
msgid "http://www.vgcomputing.com.au"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):271
msgid "VG Computing"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri:link):279
msgid "http://www.linux-service.be"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(uri):279
msgid "Linux Service (Belgium)"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(title):286
msgid "Want to be listed on this page?"
msgstr ""

#: ../../gentoo/xml/htdocs/main/en//stores.xml(p):289
msgid "For vendor listing on this page, Gentoo requires that you sell our media at no \"profit\" as described by the GPL (reasonable cost to cover expenses) or make an agreement with Gentoo for the listing. That agreement could be one where you donate some amount to Gentoo or could be free, at Gentoo's discretion. Vendors supporting open source will be considered heavily for free listing. We do not list commercial vendors who do not give back to Gentoo because we already have a Gentoo store. If you are making a profit, contact the <mail link=\"trustees@gentoo.org\">Gentoo foundation</mail> to negotiate an agreement."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/main/en//stores.xml(None):0
msgid "translator-credits"
msgstr ""

