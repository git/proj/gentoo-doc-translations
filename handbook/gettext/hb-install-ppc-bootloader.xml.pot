msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-05-18 19:19+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(abstract):10
msgid "Several bootloaders exist. Each one of them has its own way of configuration. In this chapter we'll describe all possibilities for you and step you through the process of configuring a bootloader to your needs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(version):17
msgid "12"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(date):18
msgid "2011-05-09"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):21
msgid "Making your Choice"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):23 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):51
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):26
msgid "Now that your kernel is configured and compiled and the necessary system configuration files are filled in correctly, it is time to install a program that will fire up your kernel when you start the system. Such a program is called a <e>bootloader</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):33
msgid "The bootloader that you use will depend upon the type of PPC machine you have."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):37
msgid "If you are using a NewWorld Apple or IBM machine, you need to use <uri link=\"#yaboot\">yaboot</uri>. OldWorld Apple machines have two options, <uri link=\"#bootx\">BootX</uri> (recommended) and <uri link=\"#quik\">quik</uri>. The Pegasos does not require a bootloader, but you will need to emerge <uri link=\"#bootcreator\">bootcreator</uri> to create SmartFirmware boot menus."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):49
msgid "Default: Using yaboot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(impo):54
msgid "yaboot can only be used on NewWorld Apple and IBM systems!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):58
msgid "In order to find the boot devices, yaboot needs access to the device nodes created by udev on startup and the sysfs filesystem. These two filesystems are found at <path>/dev</path> and <path>sys</path> respectively. To do this, you will need to \"bind mount\" these filesystems from the Installation CD's root to the <path>/dev</path> and <path>/sys</path> mount points inside the chroot. If you have already bind mounted these filesystems, there is no need to do it again."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):68
msgid "Bind-mounting the device and sysfs filesystems"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):68
#, no-wrap
msgid "\n# <i>exit </i> # this will exit the chroot\n# <i>mount --rbind /dev /mnt/gentoo/dev</i>\n# <i>mount --rbind /sys /mnt/gentoo/sys</i>\n# <i>chroot /mnt/gentoo /bin/bash</i>\n# <i>/usr/sbin/env-update &amp;&amp; source /etc/profile </i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):76
msgid "To set up yaboot, you can use <c>yabootconfig</c> to automatically create a configuration file for you. If you are installing Gentoo on a G5 (where <c>yabootconfig</c> does not always work), or you plan to boot from FireWire or USB, you will need to manually configure yaboot."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(note):83
msgid "You will need to manually edit the <path>yaboot.conf</path> when using <c>genkernel</c>, even if <c>yabootconfig</c> is used. The kernel image section of <path>yaboot.conf</path> should be modified as follows (using <path>vmlinux</path> and <path>initrd</path> as the name of kernel and <path>initrd</path> image):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):91
msgid "Adding genkernel boot arguments to yaboot.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):91
#, no-wrap
msgid "\n<comment>###########################################################\n## This section can be duplicated if you have more than one\n## kernel or set of boot options - replace the image and initrd\n## with the exact filename of your kernel and initrd image.\n###########################################################</comment>\nimage=/boot/<keyval id=\"genkernel-name\"></keyval>\n  label=Linux\n  root=/dev/ram0\n  partition=3\n  initrd=/boot/<keyval id=\"genkernel-initrd\"></keyval>\n  <comment># You can add additional kernel arguments to append such as \n  # rootdelay=10 for a USB/Firewire Boot</comment>\n  append=\"real_root=/dev/sda3\"  \n  read-only\n<comment>##########################################################</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(uri:link):110
msgid "#yabootconfig"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(uri):110 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):119
msgid "Default: Using yabootconfig"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(uri:link):112
msgid "#manual_yaboot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(uri):112 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):172
msgid "Alternative: Manual yaboot Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):122
msgid "<c>yabootconfig</c> will auto-detect the partitions on your machine and will set up dual and triple boot combinations with Linux, Mac OS, and Mac OS X."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):127
msgid "To use <c>yabootconfig</c>, your drive must have an Apple_Bootstrap partition, and <path>/etc/fstab</path> must be configured to reflect your Linux partitions (note that the Bootstrap partition should <e>not</e> be in your fstab). These steps should have already been completed before, but check <path>/etc/fstab</path> before proceeding. Now, install <c>yaboot</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):135 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):179
msgid "Installing yaboot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):135 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):179
#, no-wrap
msgid "\n# <i>emerge yaboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):139
msgid "Now exit the chroot and run <c>yabootconfig --chroot /mnt/gentoo</c>. First, the program will confirm the location of the bootstrap partition. If you are using the suggested disk partitioning scheme, your bootstrap partition should be <path>/dev/sda2</path>. Type <c>Y</c> if the output is correct. If not, double check your <path>/etc/fstab</path>. <c>yabootconfig</c> will then scan your system setup, create <path>/etc/yaboot.conf</path> and run <c>mkofboot</c> for you. <c>mkofboot</c> is used to format the Apple_Bootstrap partition, and install the yaboot configuration file into it. After this enter the chroot again."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):151
msgid "Re-enter the chroot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):151
#, no-wrap
msgid "\n# <i>chroot /mnt/gentoo /bin/bash</i>\n# <i>/usr/sbin/env-update &amp;&amp; source /etc/profile</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):156
msgid "You should verify the contents of <path>/etc/yaboot.conf</path>. If you make changes to <path>/etc/yaboot.conf</path> (like setting the default/boot OS), make sure to rerun <c>ybin -v</c> to apply changes to the Apple_Bootstrap partition. Whenever you make a change to yaboot.conf, like when testing a new kernel, always remember to run <c>ybin -v</c> to update the bootstrap partition."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):165
msgid "Now continue with <uri link=\"#reboot\">Rebooting the System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):175
msgid "First, install <c>yaboot</c> on your system:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):183
msgid "An example <path>yaboot.conf</path> file is given below, but you will need to alter it to fit your needs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):188
msgid "/etc/yaboot.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):188
#, no-wrap
msgid "\n<comment>## /etc/yaboot.conf\n##\n## run: \"man yaboot.conf\" for details. Do not make changes until you have!!\n## see also: /usr/share/doc/yaboot/examples for example configurations.\n##\n## For a dual-boot menu, add one or more of:\n## bsd=/dev/sdaX, macos=/dev/sdaY, macosx=/dev/sdaZ\n\n## The bootstrap partition:</comment>\n\nboot=/dev/sda2\n\n<comment>## ofboot is the Open Firmware way to specify the bootstrap partition.\n## If this isn't defined, yaboot fails on the G5 and some G4s (unless \n## you pass the necessary arguments to the mkofboot/ybin program).\n## hd:X means /dev/sdaX.\n## \n## G5 users should uncomment this line!!\n\n#ofboot=hd:2\n\n## Users booting from firewire should use something like this line:\n# ofboot=fw/node/sbp-2/disk@0:\n\n## Users booting from USB should use something like this line:\n# ofboot=usb/disk@0:\n\n## hd: is shorthand for the first hard drive Open Firmware sees</comment>\ndevice=hd:\n\n<comment>## Firewire and USB users will need to specify the whole OF device name\n## This can be found using ofpath, which is included with yaboot.\n\n# device=fw/node@0001d200e00d0207/sbp-2@c000/disk@0:\n</comment>\n\ndelay=5\ndefaultos=macosx\ntimeout=30\ninstall=/usr/lib/yaboot/yaboot\nmagicboot=/usr/lib/yaboot/ofboot\n\n<comment>############################################################\n## This section can be duplicated if you have more than one\n## kernel or set of boot options - replace the image variable\n## with the exact filename of your kernel.\n###########################################################</comment>\nimage=/boot/<keyval id=\"kernel-name\"></keyval>\n  label=Linux\n  root=/dev/sda3\n  partition=3\n<comment>#  append=\"rootdelay=10\"  # Required for booting USB/Firewire</comment>\n  read-only\n<comment>##################\n\n## G5 users and some G4 users should set \n##   macos=hd:13/\n##   macosx=hd:12\n## instead of the example values.</comment>\nmacos=/dev/sda13\nmacosx=/dev/sda12\nenablecdboot\nenableofboot\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):254
msgid "Once <path>yaboot.conf</path> is configured, run <c>mkofboot -v</c> to format the Apple_bootstrap partition and install the settings. If you change yaboot.conf after the Apple_bootstrap partition has been created, you can update the settings by running <c>ybin -v</c>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):261
msgid "Setting up the bootstrap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):261
#, no-wrap
msgid "\n# <i>mkofboot -v</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):265
msgid "For more information on yaboot, take a look at the <uri link=\"http://penguinppc.org/bootloaders/yaboot\">yaboot project</uri>. For now, continue the installation with <uri link=\"#reboot\">Rebooting the System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):276
msgid "Alternative: BootX"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(impo):279
msgid "BootX can only be used on OldWorld Apple systems with MacOS 9 or earlier!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):283
msgid "Since BootX boots Linux from within MacOS, the kernel will need to be copied from the Linux Partition to the MacOS partition. First, mount the MacOS partition from outside of the chroot. Use <c>mac-fdisk -l</c> to find the MacOS partition number, sda6 is used as an example here. Once the partition is mounted, we'll copy the kernel to the system folder so BootX can find it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):291
msgid "Copying the kernel to the MacOS partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):291
#, no-wrap
msgid "\n# <i>exit</i>\ncdimage ~# <i>mkdir /mnt/mac</i>\ncdimage ~# <i>mount /dev/sda6 /mnt/mac -t hfs</i>\ncdimage ~# <i>cp /mnt/gentoo/usr/src/linux/vmlinux \"/mnt/mac/System Folder/Linux Kernels/<keyval id=\"kernel-name\"></keyval>\"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):298
msgid "If genkernel is used, both the kernel and initrd will need to be copied to the MacOS partition."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):303
msgid "Copying the Genkernel kernel and initrd to the MacOS partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):303
#, no-wrap
msgid "\n# <i>exit</i>\ncdimage ~# <i>mkdir /mnt/mac</i>\ncdimage ~# <i>mount /dev/sda6 /mnt/mac -t hfs</i>\ncdimage ~# <i>cp /mnt/gentoo/boot/<keyval id=\"genkernel-name\"></keyval> \"/mnt/mac/System Folder/Linux Kernels\"</i>\ncdimage ~# <i>cp /mnt/gentoo/boot/<keyval id=\"genkernel-initrd\"></keyval> \"/mnt/mac/System Folder\"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):311
msgid "Now that the kernel is copied over, we'll need to reboot to set up BootX."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):315
msgid "Unmounting all partitions and rebooting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):315
#, no-wrap
msgid "\ncdimage ~# <i>cd /</i>\ncdimage ~# <i>umount -l /mnt/gentoo/dev{/pts,/shm,}</i>\ncdimage ~# <i>umount -l /mnt/gentoo{/proc,/sys,}</i>\ncdimage ~# <i>umount -l /mnt/mac</i>\ncdimage ~# <i>reboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):323
msgid "Of course, don't forget to remove the bootable CD, otherwise the CD will be booted again instead of MacOS."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):328
msgid "Once the machine has booted into MacOS, open the BootX control panel. If you're not using genkernel, select <c>Options</c> and uncheck <c>Use specified RAM disk</c>. If you are using genkernel, ensure that the genkernel initrd is selected instead of the Installation CD initrd. If not using genkernel, there is now an option to specify the machine's Linux root disk and partition. Fill these in with the appropriate values. Depending upon the kernel configuration, additional boot arguments may need to be applied."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):338
msgid "BootX can be configured to start Linux upon boot. If you do this, you will first see your machine boot into MacOS then, during startup, BootX will load and start Linux. See the <uri link=\"http://penguinppc.org/bootloaders/bootx/\">BootX home page</uri> for more information."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(impo):345
msgid "Make sure that you have support for HFS and HFS+ filesystems in your kernel, otherwise you will not be able to upgrade or change the kernel on your MacOS partition."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):351
msgid "Now reboot again and boot into Linux, then continue with <uri link=\"?part=1&amp;chap=11\">Finalizing your Gentoo Installation</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):359
msgid "Alternative: quik"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):362
msgid "<c>quik</c> allows OldWorld Macs to boot without MacOS. However, it isn't well supported and has a number of quirks. If you have the option, it is recommended that you use BootX instead since it is much more reliable and easier to set up than quik."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):369
msgid "First, we'll need to install <c>quik</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):373
msgid "Emerge quik"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):373
#, no-wrap
msgid "\n# <i>emerge quik</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):377
msgid "Next, we'll need to set it up. Edit <path>/etc/quik.conf</path> and set your image to the kernel that we copied to your boot partition."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):382
msgid "Configuring quik.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):382
#, no-wrap
msgid "\n# Example of quik.conf\ninit-message = \"Gentoo Linux\\n\"\n<comment># This is the boot partition</comment>\npartition = 2\nroot = /dev/sda4\ntimeout = 30\ndefault = gentoo\n<comment># This is your kernel</comment>\nimage = /<keyval id=\"kernel-name\"></keyval>\n  label = gentoo\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):395
msgid "Your <path>quik.conf</path> file <e>must</e> be on the same disk as the quik boot images, however it can be on a different partition on the same disk, although it is recommended to move it to your boot partition."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):401
msgid "Moving quik.conf to /boot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):401
#, no-wrap
msgid "\n# <i>mv /etc/quik.conf /boot/quik.conf</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):405
msgid "We will now set your boot variables so that quik loads on boot. To do this, we'll use a program called <c>nvsetenv</c>. The variables that you want to set vary from machine to machine, it's best to find your machine's <uri link=\"http://penguinppc.org/bootloaders/quik/quirks.php\">quirks</uri> before attempting this."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):413
msgid "Setting the boot variables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):413
#, no-wrap
msgid "\n# <i>nvsetenv auto-boot true</i> <comment># Set to false if you want to boot into OF, not all models can display the OF output</comment>\n# <i>nvsetenv output-device video</i> <comment># Check the quirks page, there are many variations here</comment>\n# <i>nvsetenv input-device kbd</i>\n# <i>nvsetenv boot-device scsi/sd@1:0</i> <comment># For SCSI</comment>\n# <i>nvsetenv boot-device ata/ata-disk@0:0</i> <comment># For ATA</comment>\n# <i>nvsetenv boot-file /boot/<keyval id=\"kernel-name\"></keyval> root=/dev/sda4</i> <comment>First item is the path to the kernel, the second is the root partition.  You may append any kernel options to the end of this line.</comment>\n# <i>nvsetenv boot-command boot</i> <comment># Set this to bye for MacOS and boot for Linux</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(note):423
msgid "It is also possible to change your boot variables from MacOS. Depending upon the model, either <uri link=\"http://penguinppc.org/bootloaders/quik/BootVars.sit.hqx\">bootvars</uri> or <uri link=\"ftp://ftp.apple.com/developer/macosxserver/utilities/SystemDisk2.3.1.smi.bin\"> Apple System Disk</uri> should be used. Please see the quik quirks page above for more information."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):433
msgid "Now that we've set up our machine to boot, we'll need to make sure the boot images are installed correctly. Run <c>quik -v -C /boot/quik.conf</c>. It should tell you that it has installed the first stage QUIK boot block."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(note):439
msgid "If something has gone wrong, you can always reset your PRAM back to the default values by holding down <c>command + option + p + r</c> before powering on your machine. This will clear the values you set with nvsetenv and should allow you to boot either a MacOS bootdisk or a Linux bootdisk."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):446
msgid "Now, continue the installation with <uri link=\"#reboot\">Rebooting the System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):454
msgid "Alternative: BootCreator"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(impo):457
msgid "BootCreator will build a nice SmartFirmware bootmenu written in Forth for the Pegasos."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):462
msgid "First make sure you have <c>bootcreator</c> installed on your system:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):466
msgid "Installing bootcreator"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):466
#, no-wrap
msgid "\n# <i>emerge bootcreator</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):470
msgid "Now copy the file <path>/etc/bootmenu.example</path> into <path>/etc/bootmenu</path> and edit it to suit your needs:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):475
msgid "Edit the bootcreator config file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):475
#, no-wrap
msgid "\n# <i>cp /etc/bootmenu.example /etc/bootmenu</i>\n# <i>nano -w /etc/bootmenu</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):480
msgid "Below is a complete <path>/etc/bootmenu</path> config file. vmlinux and initrd should be replaced by your kernel and initrd image names."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):485
msgid "bootcreator config file"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):485
#, no-wrap
msgid "\n<comment>#\n# Example description file for bootcreator 1.1\n#</comment>\n\n[VERSION]\n1\n\n[TITLE]\nBoot Menu\n\n[SETTINGS]\nAbortOnKey = false\nTimeout    = 9\nDefault    = 1\n\n[SECTION]\nLocal HD -&gt; Morphos      (Normal)\nide:0 boot2.img ramdebug edebugflags=\"logkprintf\"\n\n[SECTION]\nLocal HD -&gt; Linux (Normal)\nide:0 <keyval id=\"kernel-name\"></keyval> video=radeonfb:1024x768@70 root=/dev/sda3\n\n[SECTION]\nLocal HD -&gt; Genkernel (Normal)\nide:0 <keyval id=\"genkernel-name\"></keyval> root=/dev/ram0\nreal_root=/dev/sda3 initrd=<keyval id=\"genkernel-initrd\"></keyval>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):515
msgid "Finally the <path>bootmenu</path> must be transferred into Forth and copied to your boot partition, so that the SmartFirmware can read it. Therefore you have to call <c>bootcreator</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):521
msgid "Install the bootmenu"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):521
#, no-wrap
msgid "\n# <i>bootcreator /etc/bootmenu /boot/menu</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(note):525
msgid "Be sure to have a look into the SmartFirmware's settings when you reboot, that <path>menu</path> is the file that will be loaded by default."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):530
msgid "For now, continue the installation with <uri link=\"#reboot\">Rebooting the System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(title):538
msgid "Rebooting the System"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):542
msgid "Exit the chrooted environment and unmount all mounted partitions. Then type in that one magical command you have been waiting for: <c>reboot</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre:caption):547
msgid "Exiting the chroot, unmounting all partitions and rebooting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(pre):547
#, no-wrap
msgid "\n# <i>exit</i>\nlivecd ~# <i>umount -l /mnt/gentoo/dev{/pts,/shm,}</i>\nlivecd ~# <i>umount -l /mnt/gentoo{/proc,/sys,}</i>\nlivecd ~# <i>reboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(p):554
msgid "Once rebooted in your Gentoo installation, finish up with <uri link=\"?part=1&amp;chap=11\">Finalizing your Gentoo Installation</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc-bootloader.xml(None):0
msgid "translator-credits"
msgstr ""

