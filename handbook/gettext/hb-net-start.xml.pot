msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(abstract):11
msgid "A guide to quickly get your network interface up and running in most common environments."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(version):16
msgid "9"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(date):17
msgid "2011-08-13"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(title):20
msgid "Getting started"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(note):23
msgid "This document assumes that you have correctly configured your kernel, its modules for your hardware and you know the interface name of your hardware. We also assume that you are configuring <c>eth0</c>, but it could also be <c>eth1</c>, <c>wlan0</c>, etc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(p):30
msgid "To get started configuring your network card, you need to tell the Gentoo RC system about it. This is done by creating a symbolic link from <path>net.lo</path> to <path>net.eth0</path> in <path>/etc/init.d</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(pre:caption):36
msgid "Symlinking net.eth0 to net.lo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(pre):36
#, no-wrap
msgid "\n# <i>cd /etc/init.d</i>\n# <i>ln -s net.lo net.eth0</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(p):41
msgid "Gentoo's RC system now knows about that interface. It also needs to know how to configure the new interface. All the network interfaces are configured in <path>/etc/conf.d/net</path>. Below is a sample configuration for DHCP and static addresses."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(pre:caption):48
msgid "Examples for /etc/conf.d/net"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(pre):48
#, no-wrap
msgid "\n<comment># For DHCP</comment>\nconfig_eth0=\"dhcp\"\n\n<comment># For static IP using CIDR notation</comment>\nconfig_eth0=\"192.168.0.7/24\"\nroutes_eth0=\"default via 192.168.0.1\"\n\n<comment># For static IP using netmask notation</comment>\nconfig_eth0=\"192.168.0.7 netmask 255.255.255.0\"\nroutes_eth0=\"default via 192.168.0.1\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(note):61
msgid "If you do not specify a configuration for your interface then DHCP is assumed."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(note):65
msgid "CIDR stands for Classless InterDomain Routing. Originally, IPv4 addresses were classified as A, B, or C. The early classification system did not envision the massive popularity of the Internet, and is in danger of running out of new unique addresses. CIDR is an addressing scheme that allows one IP address to designate many IP addresses. A CIDR IP address looks like a normal IP address except that it ends with a slash followed by a number; for example, 192.168.0.0/16. CIDR is described in <uri link=\"http://tools.ietf.org/html/rfc1519\">RFC 1519</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(p):76
msgid "Now that we have configured our interface, we can start and stop it using the following commands:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(pre:caption):81
msgid "Starting and stopping network scripts"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(pre):81
#, no-wrap
msgid "\n# <i>/etc/init.d/net.eth0 start</i>\n# <i>/etc/init.d/net.eth0 stop</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(impo):86
msgid "When troubleshooting networking, take a look at <path>/var/log/rc.log</path>. Unless you have <c>rc_logger=\"NO\"</c> set in <path>/etc/rc.conf</path>, you will find information on the boot activity stored in that log file."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(p):92
msgid "Now that you have successfully started and stopped your network interface, you may wish to get it to start when Gentoo boots. Here's how to do this. The last \"rc\" command instructs Gentoo to start any scripts in the current runlevel that have not yet been started."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(pre:caption):99
msgid "Configuring a network interface to load at boot time"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(pre):99
#, no-wrap
msgid "\n# <i>rc-update add net.eth0 default</i>\n# <i>rc</i>\n"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-net-start.xml(None):0
msgid "translator-credits"
msgstr ""

