msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(abstract):11
msgid "Gentoo installs work through a stage3 archive. In this chapter we describe how you extract the stage3 archive and configure Portage."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(version):16
msgid "11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(date):17
msgid "2011-08-03"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):20
msgid "Installing a Stage Tarball"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):22
msgid "Setting the Date/Time Right"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):25
msgid "Before you continue you need to check your date/time and update it. A misconfigured clock may lead to strange results in the future!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):30
msgid "To verify the current date/time, run <c>date</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):34
msgid "Verifying the date/time"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):34
#, no-wrap
msgid "\n# <i>date</i>\nFri Mar 29 16:21:18 UTC 2005\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):39
msgid "If the date/time displayed is wrong, update it using the <c>date MMDDhhmmYYYY</c> syntax (<b>M</b>onth, <b>D</b>ay, <b>h</b>our, <b>m</b>inute and <b>Y</b>ear). At this stage, you should use UTC time. You will be able to define your timezone later on. For instance, to set the date to March 29th, 16:21 in the year 2005:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):47
msgid "Setting the UTC date/time"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):47
#, no-wrap
msgid "\n# <i>date 032916212005</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):54
msgid "Making your Choice"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):57
msgid "The next step you need to perform is to install the <e>stage3</e> tarball onto your system. You have the option of downloading the required tarball from the Internet or, if you booted one of the Gentoo Universal CDs, copy it over from the disc itself. In most cases, the command <c>uname -m</c> can be used to help you decide which stage file to download."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):65
msgid "Minimal CDs and LiveDVDs do not contain any stage3 archive."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(uri:link):70
msgid "#doc_chap2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(uri):70 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):80
msgid "Default: Using a Stage from the Internet"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(uri:link):72
msgid "#doc_chap3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(uri):72 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):198
msgid "Alternative: Using a Stage from the Universal CD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):82
msgid "Downloading the Stage Tarball"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):85
msgid "Go to the Gentoo mountpoint at which you mounted your filesystems (most likely <path>/mnt/gentoo</path>):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):90 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):274
msgid "Going to the Gentoo mountpoint"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):90 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):227 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):274
#, no-wrap
msgid "\n# <i>cd /mnt/gentoo</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):94
msgid "Depending on your installation medium, you have a couple of tools available to download a stage. If you have <c>links</c> available, then you can immediately surf to <uri link=\"/main/en/mirrors.xml\">the Gentoo mirrorlist</uri> and choose a mirror close to you: type <c>links http://www.gentoo.org/main/en/mirrors.xml</c> and press enter."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):102
msgid "If you don't have <c>links</c> available you should have <c>lynx</c> at your disposal. If you need to go through a proxy, export the <c>http_proxy</c> and <c>ftp_proxy</c> variables:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):108
msgid "Setting proxy information for lynx"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):108
#, no-wrap
msgid "\n# <i>export http_proxy=\"http://proxy.server.com:port\"</i>\n# <i>export ftp_proxy=\"http://proxy.server.com:port\"</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):113
msgid "We will now assume that you have <c>links</c> at your disposal."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):117
msgid "Select a mirror closeby. Usually HTTP mirrors suffice, but other protocols are available as well. Move to the <path><keyval id=\"release-dir\"/></path> directory. There you should see all available stage files for your architecture (they might be stored within subdirectories named after the individual subarchitectures). Select one and press <c>D</c> to download. When you're finished, press <c>Q</c> to quit the browser."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):126
msgid "Most PC users should use the <b><keyval id=\"stage3\"/></b> stage3 archive. All modern PCs are considered i686. If you use an old machine, you can check the <uri link=\"http://en.wikipedia.org/wiki/I686\">list of i686-compatible processors</uri> on Wikipedia. Old processors such as the Pentium, K5, K6, or Via C3 and similar require the more generic <b>x86</b> stage3. Processors older than <b>i486</b> are not supported."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):135
msgid "Surfing to the mirror listing with links"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):135
#, no-wrap
msgid "\n# <i>links http://www.gentoo.org/main/en/mirrors.xml</i>\n\n<comment>(If you need proxy support with links:)</comment>\n# <i>links -http-proxy proxy.server.com:8080 http://www.gentoo.org/main/en/mirrors.xml</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):142
msgid "Make sure you download a <b>stage3</b> tarball - installations using a stage1 or stage2 tarball are not supported anymore."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):147
msgid "If you want to check the integrity of the downloaded stage tarball, use <c>md5sum</c> and compare the output with the MD5 checksum provided on the mirror."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):153
msgid "Checking integrity of a stage tarball"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):153
#, no-wrap
msgid "\n# <i>md5sum -c <keyval id=\"stage3\"></keyval>.DIGESTS</i>\n<keyval id=\"stage3\"></keyval>: OK\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):161
msgid "Unpacking the Stage Tarball"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):164
msgid "Now unpack your downloaded stage onto your system. We use <c>tar</c> to proceed as it is the easiest method:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):169
msgid "Unpacking the stage"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):169
#, no-wrap
msgid "\n# <i>tar xvjpf stage3-*.tar.bz2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):173
msgid "Make sure that you use the same options (<c>xvjpf</c>). The <c>x</c> stands for <e>Extract</e>, the <c>v</c> for <e>Verbose</e> to see what happens during the extraction process (optional), the <c>j</c> for <e>Decompress with bzip2</e>, the <c>p</c> for <e>Preserve permissions</e> and the <c>f</c> to denote that we want to extract a file, not standard input."
msgstr ""

#. MIPS uses its own hb-install-stage.xml file, any other arch?
#. <note>
#. Some architectures (e.g. MIPS) Installation CDs and boot images rely upon the
#. <c>tar</c> built into BusyBox which doesn't currently support the <c>v</c>
#. option. Use the <c>xjpf</c> options instead.
#. </note>
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):189 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):244
msgid "Now that the stage is installed, continue with <uri link=\"#installing_portage\">Installing Portage</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):200
msgid "Extracting the Stage Tarball"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):203
msgid "The stages on the CD reside in the <path>/mnt/cdrom/stages</path> directory. To see a listing of available stages, use <c>ls</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):208
msgid "List all available stages"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):208
#, no-wrap
msgid "\n# <i>ls /mnt/cdrom/stages</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):212
msgid "If the system replies with an error, you may need to mount the CD-ROM first:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):216
msgid "Mounting the CD-ROM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):216
#, no-wrap
msgid "\n# <i>ls /mnt/cdrom/stages</i>\nls: /mnt/cdrom/stages: No such file or directory\n# <i>mount /dev/cdroms/cdrom0 /mnt/cdrom</i>\n# <i>ls /mnt/cdrom/stages</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):223
msgid "Now go into your Gentoo mountpoint (usually <path>/mnt/gentoo</path>):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):227
msgid "Changing directory to /mnt/gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):231
msgid "We will now extract the stage tarball of your choice. We will do this with <c>tar</c>. Make sure you use the same options (<c>xvjpf</c>). The <c>v</c> argument is optional and not supported in some <c>tar</c> versions. In the next example, we extract the stage tarball <path>stage3-&lt;subarch&gt;-&lt;release&gt;.tar.bz2</path>. Be sure to substitute the tarball filename with your stage."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):240
msgid "Extracting the stage tarball"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):240
#, no-wrap
msgid "\n# <i>tar xvjpf /mnt/cdrom/stages/stage3-&lt;subarch&gt;-&lt;release&gt;.tar.bz2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):253
msgid "Installing Portage"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):255
msgid "Unpacking a Portage Snapshot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):258
msgid "You now have to install a Portage snapshot, a collection of files that inform Portage what software titles you can install, which profiles are available, etc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):266
msgid "Download and Install a Portage Snapshot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):269
msgid "Go to the mountpoint where you mounted your filesystem (most likely <path>/mnt/gentoo</path>):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):278
msgid "Fire up <c>links</c> (or <c>lynx</c>) and go to our <uri link=\"/main/en/mirrors.xml\">Gentoo mirror list</uri>. Pick a mirror close to you and open the <path>snapshots/</path> directory. There, download the latest Portage snapshot (<path>portage-latest.tar.bz2</path>) by selecting it and pressing <c>D</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):286
msgid "Browsing the Gentoo mirrorlist"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):286
#, no-wrap
msgid "\n# <i>links http://www.gentoo.org/main/en/mirrors.xml</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):290
msgid "Now exit your browser by pressing <c>Q</c>. You will now have a Portage snapshot stored in <path>/mnt/gentoo</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):295
msgid "If you want to check the integrity of the downloaded snapshot, use <c>md5sum</c> and compare the output with the MD5 checksum provided on the mirror."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):301
msgid "Checking integrity of a Portage snapshot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):301
#, no-wrap
msgid "\n# <i>md5sum -c portage-latest.tar.bz2.md5sum</i>\nportage-latest.tar.bz2: OK\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):306
msgid "In the next step, we extract the Portage snapshot onto your filesystem. Make sure that you use the exact command; the last option is a capital <c>C</c>, not <c>c</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):312
msgid "Extracting the Portage snapshot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):312
#, no-wrap
msgid "\n# <i>tar xvjf /mnt/gentoo/portage-latest.tar.bz2 -C /mnt/gentoo/usr</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):321
msgid "Configuring the Compile Options"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):323
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):326
msgid "To optimize Gentoo, you can set a couple of variables which impact Portage behaviour. All those variables can be set as environment variables (using <c>export</c>) but that isn't permanent. To keep your settings, Portage provides you with <path>/etc/make.conf</path>, a configuration file for Portage. It is this file we will edit now."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(note):334
msgid "A commented listing of all possible variables can be found in <path>/mnt/gentoo/usr/share/portage/config/make.conf.example</path>. For a successful Gentoo installation you'll only need to set the variables which are mentioned beneath."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):341
msgid "Fire up your favorite editor (in this guide we use <c>nano</c>) so we can alter the optimization variables we will discuss hereafter."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):346
msgid "Opening /etc/make.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):346
#, no-wrap
msgid "\n# <i>nano -w /mnt/gentoo/etc/make.conf</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):350
msgid "As you probably noticed, the <path>make.conf.example</path> file is structured in a generic way: commented lines start with \"#\", other lines define variables using the <c>VARIABLE=\"content\"</c> syntax. The <path>make.conf</path> file uses the same syntax. Several of those variables are discussed next."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):360
msgid "CFLAGS and CXXFLAGS"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):363
msgid "The <c>CFLAGS</c> and <c>CXXFLAGS</c> variables define the optimization flags for the <c>gcc</c> C and C++ compiler respectively. Although we define those generally here, you will only have maximum performance if you optimize these flags for each program separately. The reason for this is because every program is different."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):371
msgid "In <path>make.conf</path> you should define the optimization flags you think will make your system the most responsive <e>generally</e>. Don't place experimental settings in this variable; too much optimization can make programs behave bad (crash, or even worse, malfunction)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):378
msgid "We will not explain all possible optimization options. If you want to know them all, read the <uri link=\"http://gcc.gnu.org/onlinedocs/\">GNU Online Manual(s)</uri> or the <c>gcc</c> info page (<c>info gcc</c> -- only works on a working Linux system). The <path>make.conf.example</path> file itself also contains lots of examples and information; don't forget to read it too."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):387
msgid "A first setting is the <c>-march=</c> or <c>-mcpu=</c> flag, which specifies the name of the target architecture. Possible options are described in the <path>make.conf.example</path> file (as comments)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):393
msgid "A first setting is the <c>-march=</c> or <c>-mtune=</c> flag, which specifies the name of the target architecture. Possible options are described in the <path>make.conf.example</path> file (as comments)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):399
msgid "A second one is the <c>-O</c> flag (that is a capital O, not a zero), which specifies the <c>gcc</c> optimization class flag. Possible classes are <c>s</c> (for size-optimized), <c>0</c> (zero - for no optimizations), <c>1</c>, <c>2</c> or even <c>3</c> for more speed-optimization flags (every class has the same flags as the one before, plus some extras). <c>-O2</c> is the recommended default. <c>-O3</c> is known to cause problems when used system-wide, so we recommend that you stick to <c>-O2</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):410
msgid "Another popular optimization flag is <c>-pipe</c> (use pipes rather than temporary files for communication between the various stages of compilation). It has no impact on the generated code, but uses more memory. On systems with low memory, gcc might get killed. In that case, do not use this flag."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):417
msgid "Using <c>-fomit-frame-pointer</c> (which doesn't keep the frame pointer in a register for functions that don't need one) might have serious repercussions on the debugging of applications."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):423
msgid "When you define the <c>CFLAGS</c> and <c>CXXFLAGS</c>, you should combine several optimization flags. The default values contained in the stage3 archive you unpacked should be good enough. The following example is just an example:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):429 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):435 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):441
msgid "Defining the CFLAGS and CXXFLAGS variable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):429
#, no-wrap
msgid "\nCFLAGS=\"<keyval id=\"CFLAGS\"></keyval>\"\n<comment># Use the same settings for both variables</comment>\nCXXFLAGS=\"${CFLAGS}\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):435
#, no-wrap
msgid "\nCFLAGS=\"<keyval id=\"CFLAGS\"></keyval>\"   <comment># Intel EM64T users should use -march=core2</comment>\n<comment># Use the same settings for both variables</comment>\nCXXFLAGS=\"${CFLAGS}\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):441
#, no-wrap
msgid "\nCFLAGS=\"<keyval id=\"CFLAGS\"></keyval>   <comment># Be sure to change -march to match your CPU type</comment>\n<comment># Use the same settings for both variables</comment>\nCXXFLAGS=\"${CFLAGS}\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(note):447
msgid "You may also want to view the <uri link=\"/doc/en/gcc-optimization.xml\">Compilation Optimization Guide</uri> for more information on how the various compilation options can affect your system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):456
msgid "MAKEOPTS"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):459
msgid "With <c>MAKEOPTS</c> you define how many parallel compilations should occur when you install a package. A good choice is the number of CPUs (or CPU cores) in your system plus one, but this guideline isn't always perfect."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):465
msgid "MAKEOPTS for a regular, 1-CPU system"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):465
#, no-wrap
msgid "\nMAKEOPTS=\"-j2\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):472
msgid "Ready, Set, Go!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):475
msgid "Update your <path>/mnt/gentoo/etc/make.conf</path> to your own preference and save (<c>nano</c> users would hit <c>Ctrl-X</c>). You are now ready to continue with <uri link=\"?part=1&amp;chap=6\">Installing the Gentoo Base System</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(None):0
msgid "translator-credits"
msgstr ""

