msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(version):11
msgid "7"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(date):12
msgid "2011-09-04"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):15
msgid "Introduction to Block Devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):22
msgid "Partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):25
msgid "Although it is theoretically possible to use the entire disk to house your Linux system, this is almost never done in practice. Instead, full disk block devices are split up in smaller, more manageable block devices. These are known as <e>partitions</e> or <e>slices</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):32
msgid "The first partition on the first SCSI disk is <path>/dev/sda1</path>, the second <path>/dev/sda2</path> and so on."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):37
msgid "The third partition on Sun systems is set aside as a special \"whole disk\" slice. This partition must not contain a file system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):42
msgid "Users who are used to the DOS partitioning scheme should note that Sun disklabels do not have \"primary\" and \"extended\" partitions. Instead, up to eight partitions are available per drive, with the third of these being reserved."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):53
msgid "Designing a Partitioning Scheme"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):55
msgid "Default Partitioning Scheme"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):58
msgid "If you are not interested in drawing up a partitioning scheme, the table below suggests a suitable starting point for most systems."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):63
msgid "Note that a separate <path>/boot</path> partition is generally <e>not</e> recommended on SPARC, as it complicates the bootloader configuration."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):70 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):152
msgid "Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):71 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):476
msgid "Filesystem"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):72
msgid "Size"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):73
msgid "Mount Point"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):74 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):153
msgid "Description"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):77 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):156
msgid "/dev/sda1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):78 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):106 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):117 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):129 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):484
msgid "ext3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):79
msgid "&lt;2 GB"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):80 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):157
msgid "/"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):81
msgid "Root partition. For SPARC64 systems with older OBP versions, this <e>must</e> be less than 2 GB in size, and the first partition on the disk."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):88 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):160
msgid "/dev/sda2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):89 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):161
msgid "swap"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):90
msgid "512 MB"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):91 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):99 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):101
msgid "none"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):92
msgid "Swap partition. For bootstrap and certain larger compiles, at least 512 MB of RAM (including swap) is required."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):98 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):164
msgid "/dev/sda3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):100
msgid "Whole disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):102
msgid "Whole disk partition. This is required on SPARC systems."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):105 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):168
msgid "/dev/sda4"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):107
msgid "at least 2 GB"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):108 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):169
msgid "/usr"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):109
msgid "/usr partition. Applications are installed here. By default this partition is also used for Portage data (which takes around 500 MB excluding source code)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):116 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):172
msgid "/dev/sda5"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):118
msgid "at least 1 GB"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):119 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):173
msgid "/var"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):120
msgid "/var partition. Used for program-generated data. By default Portage uses this partition for temporary space whilst compiling. Certain larger applications such as Mozilla and OpenOffice.org can require over 1 GB of temporary space here when building."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):128 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):176
msgid "/dev/sda6"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):130
msgid "remaining space"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):131 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):177
msgid "/home"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):132
msgid "/home partition. Used for users' home directories."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):141
msgid "Using fdisk to Partition your Disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):145
msgid "The following parts explain how to create the example partition layout described previously, namely:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):165
msgid "whole disk slice"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):181
msgid "Change the partition layout as required. Remember to keep the root partition entirely within the first 2 GB of the disk for older systems. There is also a 15-partition limit for SCSI and SATA."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):190
msgid "Firing up fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):193
msgid "Start <c>fdisk</c> with your disk as argument:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):197
msgid "Starting fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):197
#, no-wrap
msgid "\n# <i>fdisk /dev/sda</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):201
msgid "You should be greeted with the fdisk prompt:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):205
msgid "The fdisk prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):205
#, no-wrap
msgid "\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):209
msgid "To view the available partitions, type in <c>p</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):213
msgid "Listing available partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):213 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):407
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda (Sun disk label): 64 heads, 32 sectors, 8635 cylinders\nUnits = cylinders of 2048 * 512 bytes\n\n   Device Flag    Start       End    Blocks   Id  System\n/dev/sda1             0       488    499712   83  Linux native\n/dev/sda2           488       976    499712   82  Linux swap\n/dev/sda3             0      8635   8842240    5  Whole disk\n/dev/sda4           976      1953   1000448   83  Linux native\n/dev/sda5          1953      2144    195584   83  Linux native\n/dev/sda6          2144      8635   6646784   83  Linux native\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):228
msgid "Note the <c>Sun disk label</c> in the output. If this is missing, the disk is using the DOS-partitioning, not the Sun partitioning. In this case, use <c>s</c> to ensure that the disk has a Sun partition table:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):234
msgid "Creating a Sun Disklabel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):234
#, no-wrap
msgid "\nCommand (m for help): <i>s</i>\nBuilding a new sun disklabel. Changes will remain in memory only,\nuntil you decide to write them. After that, of course, the previous\ncontent won't be recoverable.\n\nDrive type\n   ?   auto configure\n   0   custom (with hardware detected defaults)\n   a   Quantum ProDrive 80S\n   b   Quantum ProDrive 105S\n   c   CDC Wren IV 94171-344\n   d   IBM DPES-31080\n   e   IBM DORS-32160\n   f   IBM DNES-318350\n   g   SEAGATE ST34371\n   h   SUN0104\n   i   SUN0207\n   j   SUN0327\n   k   SUN0340\n   l   SUN0424\n   m   SUN0535\n   n   SUN0669\n   o   SUN1.0G\n   p   SUN1.05\n   q   SUN1.3G\n   r   SUN2.1G\n   s   IOMEGA Jaz\nSelect type (? for auto, 0 for custom): <i>0</i>\nHeads (1-1024, default 64): \nUsing default value 64\nSectors/track (1-1024, default 32): \nUsing default value 32\nCylinders (1-65535, default 8635): \nUsing default value 8635\nAlternate cylinders (0-65535, default 2): \nUsing default value 2\nPhysical cylinders (0-65535, default 8637): \nUsing default value 8637\nRotation speed (rpm) (1-100000, default 5400): <i>10000</i>\nInterleave factor (1-32, default 1): \nUsing default value 1\nExtra sectors per cylinder (0-32, default 0): \nUsing default value 0\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):280
msgid "You can find the correct values in your disk's documentation. The 'auto configure' option does not usually work."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):288
msgid "Deleting Existing Partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):291
msgid "It's time to delete any existing partitions. To do this, type <c>d</c> and hit Enter. You will then be prompted for the partition number you would like to delete. To delete a pre-existing <path>/dev/sda1</path>, you would type:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):297
msgid "Deleting a partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):297
#, no-wrap
msgid "\nCommand (m for help): <i>d</i>\nPartition number (1-4): <i>1</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):302
msgid "<e>You should not delete partition 3 (whole disk).</e> This is required. If this partition does not exist, follow the \"Creating a Sun Disklabel\" instructions above."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):308
msgid "After deleting all partitions except the Whole disk slice, you should have a partition layout similar to the following:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):313
msgid "View an empty partition scheme"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):313
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda (Sun disk label): 64 heads, 32 sectors, 8635 cylinders\nUnits = cylinders of 2048 * 512 bytes\n\n   Device Flag    Start       End    Blocks   Id  System\n/dev/sda3             0      8635   8842240    5  Whole disk\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):328
msgid "Creating the Root Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):331
msgid "We're ready to create the root partition. To do this, type <c>n</c> to create a new partition, then type <c>1</c> to create the partition. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, type <c>+512M</c> to create a partition <c>512 MB</c> in size. Make sure that the entire root partition fits within the first 2 GB of the disk. You can see output from these steps below:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):340
msgid "Creating a root partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):340
#, no-wrap
msgid "\nCommand (m for help): <i>n</i>\nPartition number (1-8): <i>1</i>\nFirst cylinder (0-8635): <i>(press Enter)</i>\nLast cylinder or +size or +sizeM or +sizeK (0-8635, default 8635): <i>+512M</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):347
msgid "Now, when you type <c>p</c>, you should see the following partition printout:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):351
msgid "Listing the partition layout"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):351
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda (Sun disk label): 64 heads, 32 sectors, 8635 cylinders\nUnits = cylinders of 2048 * 512 bytes\n\n   Device Flag    Start       End    Blocks   Id  System\n/dev/sda1             0       488    499712   83  Linux native\n/dev/sda3             0      8635   8842240    5  Whole disk\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):365
msgid "Creating a swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):368
msgid "Next, let's create the swap partition. To do this, type <c>n</c> to create a new partition, then <c>2</c> to create the second partition, <path>/dev/sda2</path> in our case. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, type <c>+512M</c> to create a partition 512 MB in size. After you've done this, type <c>t</c> to set the partition type, and then type in <c>82</c> to set the partition type to \"Linux Swap\". After completing these steps, typing <c>p</c> should display a partition table that looks similar to this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):379
msgid "Listing of available partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):379
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda (Sun disk label): 64 heads, 32 sectors, 8635 cylinders\nUnits = cylinders of 2048 * 512 bytes\n\n   Device Flag    Start       End    Blocks   Id  System\n/dev/sda1             0       488    499712   83  Linux native\n/dev/sda2           488       976    499712   82  Linux swap\n/dev/sda3             0      8635   8842240    5  Whole disk\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):394
msgid "Creating the /usr, /var and /home partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):397
msgid "Finally, let's create the /usr, /var and /home partitions. As before, type <c>n</c> to create a new partition, then type <c>4</c> to create the third partition, <path>/dev/sda4</path> in our case. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, enter <c>+2048M</c> to create a partition 2 GB in size. Repeat this process for <path>sda5</path> and <path>sda6</path>, using the desired sizes. Once you're done, you should see something like this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):407
msgid "Listing complete partition table"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):425
msgid "Save and Exit"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):428
msgid "To save your partition layout and exit <c>fdisk</c>, type <c>w</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):432
msgid "Save and exit fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):432
#, no-wrap
msgid "\nCommand (m for help): <i>w</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):436
msgid "Now that your partitions are created, you can continue with <uri link=\"#filesystems\">Creating Filesystems</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):445
msgid "Creating Filesystems"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):447
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):450
msgid "Now that your partitions are created, it is time to place a filesystem on them. If you don't care about what filesystem to choose and are happy with what is used as default in this handbook, continue with <uri link=\"#filesystems-apply\">Applying a Filesystem to a Partition</uri>. Otherwise, read on to learn about the available filesystems..."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):466
msgid "Applying a Filesystem to a Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):469
msgid "To create a filesystem on a partition or volume, tools specific to the chosen filesystem are available:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(th):477
msgid "Creation Command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):480
msgid "ext2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(ti):488
msgid "ext4"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):493
msgid "For instance, to create the root partition (<path>/dev/sda1</path> in our example) as ext2, and the <path>/usr</path>, <path>/var</path>, and <path>/home</path> partitions (<path>/dev/sda4</path>, <path>5</path> and <path>6</path> in our example, respectively) as ext3, you would use:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):500
msgid "Applying a filesystem on a partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):500
#, no-wrap
msgid "\n# <i>mkfs.ext2 /dev/sda1</i>\n# <i>mkfs.ext3 /dev/sda4</i>\n# <i>mkfs.ext3 /dev/sda5</i>\n# <i>mkfs.ext3 /dev/sda6</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):510
msgid "Activating the Swap Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):513
msgid "<c>mkswap</c> is the command used to initialize swap partitions:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):517
msgid "Creating a Swap signature"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):517
#, no-wrap
msgid "\n# <i>mkswap /dev/sda2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):521
msgid "To activate the swap partition, use <c>swapon</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):525
msgid "Activating the swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):525
#, no-wrap
msgid "\n# <i>swapon /dev/sda2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):529
msgid "Create and activate the swap with the commands mentioned above."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(title):537
msgid "Mounting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):540
msgid "Now that your partitions are initialized and are housing a filesystem, it is time to mount them using the <c>mount</c> command. Don't forget to first create the necessary mount directories for every partition you created. For example:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre:caption):547
msgid "Mounting partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(pre):547
#, no-wrap
msgid "\n# <i>mount /dev/sda1 /mnt/gentoo</i>\n# <i>mkdir /mnt/gentoo/usr</i>\n# <i>mount /dev/sda4 /mnt/gentoo/usr</i>\n# <i>mkdir /mnt/gentoo/var</i>\n# <i>mount /dev/sda5 /mnt/gentoo/var</i>\n# <i>mkdir /mnt/gentoo/home</i>\n# <i>mount /dev/sda6 /mnt/gentoo/home</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(note):557
msgid "If you want your <path>/tmp</path> to reside on a separate partition, be sure to change its permissions after mounting: <c>chmod 1777 /mnt/gentoo/tmp</c>. This also holds for <path>/var/tmp</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):563
msgid "We will also have to mount the proc filesystem (a virtual interface with the kernel) on <path>/proc</path>. But first we will need to place our files on the partitions."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(p):568
msgid "Continue with <uri link=\"?part=1&amp;chap=5\">Installing the Gentoo Installation Files</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-sparc-disk.xml(None):0
msgid "translator-credits"
msgstr ""

