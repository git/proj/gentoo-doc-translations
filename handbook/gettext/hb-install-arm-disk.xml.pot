msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-10-28 22:38+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(version):11
msgid "8"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(date):12
msgid "2011-10-17"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):17
msgid "Introduction to Block Devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):24
msgid "Partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):27
msgid "Although it is theoretically possible to use a full disk to house your Linux system, this is almost never done in practice. Instead, full disk block devices are split up in smaller, more manageable block devices. On <keyval id=\"arch\"/> systems, these are called <e>partitions</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):34
msgid "Partitions are divided in three types: <e>primary</e>, <e>extended</e> and <e>logical</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):39
msgid "A <e>primary</e> partition is a partition which has its information stored in the MBR (master boot record). As an MBR is very small (512 bytes) only four primary partitions can be defined (for instance, <path>/dev/sda1</path> to <path>/dev/sda4</path>)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):46
msgid "An <e>extended</e> partition is a special primary partition (meaning the extended partition must be one of the four possible primary partitions) which contains more partitions. Such a partition didn't exist originally, but as four partitions were too few, it was brought to life to extend the formatting scheme without losing backward compatibility."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):54
msgid "A <e>logical</e> partition is a partition inside the extended partition. Their definitions aren't placed inside the MBR, but are declared inside the extended partition."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):64
msgid "Designing a Partitioning Scheme"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):66
msgid "Default Partitioning Scheme"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(warn):69
msgid "The NetWinder firmware, NeTTrom, can only read ext2 partitions reliably so you must have a separate ext2 boot partition."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):74
msgid "If you are not interested in drawing up a partitioning scheme for your system, you can use the partitioning scheme we use throughout this book:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(th):81 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(th):214
msgid "Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(th):82 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(th):496
msgid "Filesystem"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(th):83
msgid "Size"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(th):84 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(th):215
msgid "Description"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):88 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):500
msgid "ext2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):89
msgid "32M"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):90 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):219
msgid "Boot partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):94
msgid "(swap)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):95
msgid "512M"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):96 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):223
msgid "Swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):100 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):504
msgid "ext3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):101
msgid "Rest of the disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):102 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):227
msgid "Root partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):106
msgid "If you are interested in knowing how big a partition should be, or even how many partitions you need, read on. Otherwise continue now with partitioning your disk by reading <uri link=\"#fdisk\">Using fdisk to Partition your Disk</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):116
msgid "How Many and How Big?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):119
msgid "The number of partitions is highly dependent on your environment. For instance, if you have lots of users, you will most likely want to have your <path>/home</path> separate as it increases security and makes backups easier. If you are installing Gentoo to perform as a mailserver, your <path>/var</path> should be separate as all mails are stored inside <path>/var</path>. A good choice of filesystem will then maximise your performance. Gameservers will have a separate <path>/opt</path> as most gaming servers are installed there. The reason is similar for <path>/home</path>: security and backups. You will definitely want to keep <path>/usr</path> big: not only will it contain the majority of applications, the Portage tree alone takes around 500 Mbyte excluding the various sources that are stored in it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):133
msgid "As you can see, it very much depends on what you want to achieve. Separate partitions or volumes have the following advantages:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(li):139
msgid "You can choose the best performing filesystem for each partition or volume"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(li):142
msgid "Your entire system cannot run out of free space if one defunct tool is continuously writing files to a partition or volume"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(li):146
msgid "If necessary, file system checks are reduced in time, as multiple checks can be done in parallel (although this advantage is more with multiple disks than it is with multiple partitions)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(li):151
msgid "Security can be enhanced by mounting some partitions or volumes read-only, nosuid (setuid bits are ignored), noexec (executable bits are ignored) etc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):157
msgid "However, multiple partitions have disadvantages as well. If not configured properly, you will have a system with lots of free space on one partition and none on another. Another nuisance is that separate partitions - especially for important mountpoints like <path>/usr</path> or <path>/var</path> - often require the administrator to boot with an initramfs to mount the partition before other boot scripts start. This isn't always the case though, so YMMV."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):166
msgid "There is also a 15-partition limit for SCSI and SATA."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):170
msgid "As an example partitioning, we show you one for a 20GB disk, used as a demonstration laptop (containing webserver, mailserver, gnome, ...):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):175
msgid "Filesystem usage example"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):175
#, no-wrap
msgid "\n$ <i>df -h</i>\nFilesystem    Type    Size  Used Avail Use% Mounted on\n/dev/sda5     ext3    509M  132M  351M  28% /\n/dev/sda2     ext3    5.0G  3.0G  1.8G  63% /home\n/dev/sda7     ext3    7.9G  6.2G  1.3G  83% /usr\n/dev/sda8     ext3   1011M  483M  477M  51% /opt\n/dev/sda9     ext3    2.0G  607M  1.3G  32% /var\n/dev/sda1     ext2     51M   17M   31M  36% /boot\n/dev/sda6     swap    516M   12M  504M   2% &lt;not mounted&gt;\n<comment>(Unpartitioned space for future usage: 2 GB)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):188
msgid "<path>/usr</path> is rather full (83% used) here, but once all software is installed, <path>/usr</path> doesn't tend to grow that much. Although allocating a few gigabytes of disk space for <path>/var</path> may seem excessive, remember that Portage uses this partition by default for compiling packages. If you want to keep <path>/var</path> at a more reasonable size, such as 1GB, you will need to alter your <c>PORTAGE_TMPDIR</c> variable in <path>/etc/make.conf</path> to point to the partition with enough free space for compiling extremely large packages such as OpenOffice."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):203
msgid "Using fdisk to Partition your Disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):207
msgid "The following parts explain how to create the example partition layout described previously, namely:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):231
msgid "Change your partition layout according to your own preference."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):238
msgid "Viewing the Current Partition Layout"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):241
msgid "<c>fdisk</c> is a popular and powerful tool to split your disk into partitions. Fire up <c>fdisk</c> on your disk (in our example, we use <path>/dev/sda</path>):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):247
msgid "Starting fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):247
#, no-wrap
msgid "\n# <i>fdisk /dev/sda</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):251
msgid "Once in <c>fdisk</c>, you'll be greeted with a prompt that looks like this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):255
msgid "fdisk prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):255
#, no-wrap
msgid "\nCommand (m for help): \n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):259
msgid "Type <c>p</c> to display your disk's current partition configuration:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):263
msgid "An example partition configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):263
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 240 heads, 63 sectors, 2184 cylinders\nUnits = cylinders of 15120 * 512 bytes\n\nDevice Boot    Start       End    Blocks   Id  System\n/dev/sda1             1        14    105808+  83  Linux\n/dev/sda2            15        49    264600   82  Linux swap\n/dev/sda3            50        70    158760   83  Linux\n/dev/sda4            71      2184  15981840    5  Extended\n/dev/sda5            71       209   1050808+  83  Linux\n/dev/sda6           210       348   1050808+  83  Linux\n/dev/sda7           349       626   2101648+  83  Linux\n/dev/sda8           627       904   2101648+  83  Linux\n/dev/sda9           905      2184   9676768+  83  Linux\n\nCommand (m for help): \n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):283
msgid "This particular disk is configured to house seven Linux filesystems (each with a corresponding partition listed as \"Linux\") as well as a swap partition (listed as \"Linux swap\")."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):292
msgid "Removing all Partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):295
msgid "We will first remove all existing partitions from the disk. Type <c>d</c> to delete a partition. For instance, to delete an existing <path>/dev/sda1</path>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):300
msgid "Deleting a partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):300
#, no-wrap
msgid "\nCommand (m for help): <i>d</i>\nPartition number (1-4): <i>1</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):305
msgid "The partition has been scheduled for deletion. It will no longer show up if you type <c>p</c>, but it will not be erased until your changes have been saved. If you made a mistake and want to abort without saving your changes, type <c>q</c> immediately and hit enter and your partition will not be deleted."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):312
msgid "Now, assuming that you do indeed want to wipe out all the partitions on your system, repeatedly type <c>p</c> to print out a partition listing and then type <c>d</c> and the number of the partition to delete it. Eventually, you'll end up with a partition table with nothing in it:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):319
msgid "An empty partition table"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):319
#, no-wrap
msgid "\nDisk /dev/sda: 30.0 GB, 30005821440 bytes\n240 heads, 63 sectors/track, 3876 cylinders\nUnits = cylinders of 15120 * 512 = 7741440 bytes\n\nDevice Boot    Start       End    Blocks   Id  System\n\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):329
msgid "Now that the in-memory partition table is empty, we're ready to create the partitions. We will use a default partitioning scheme as discussed previously. Of course, don't follow these instructions to the letter if you don't want the same partitioning scheme!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):339
msgid "Creating the Boot Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):342
msgid "We first create a small boot partition. Type <c>n</c> to create a new partition, then <c>p</c> to select a primary partition, followed by <c>1</c> to select the first primary partition. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, type <c>+32M</c> to create a partition 32 Mbyte in size:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):350
msgid "Creating the boot partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):350
#, no-wrap
msgid "\nCommand (m for help): <i>n</i>\nCommand action\n  e   extended\n  p   primary partition (1-4)\n<i>p</i>\nPartition number (1-4): <i>1</i>\nFirst cylinder (1-3876, default 1): <comment>(Hit Enter)</comment>\nUsing default value 1\nLast cylinder or +size or +sizeM or +sizeK (1-3876, default 3876): <i>+32M</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):362
msgid "Now, when you type <c>p</c>, you should see the following partition printout:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):366
msgid "Created boot partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):366
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 30.0 GB, 30005821440 bytes\n240 heads, 63 sectors/track, 3876 cylinders\nUnits = cylinders of 15120 * 512 = 7741440 bytes\n\nDevice Boot    Start       End    Blocks   Id  System\n/dev/sda1          1        14    105808+  83  Linux\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):377
msgid "We need to make this partition bootable. Type <c>a</c> to toggle the bootable flag on a partition and select <c>1</c>. If you press <c>p</c> again, you will notice that an <path>*</path> is placed in the \"Boot\" column."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):386
msgid "Creating the Swap Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):389
msgid "Let's now create the swap partition. To do this, type <c>n</c> to create a new partition, then <c>p</c> to tell fdisk that you want a primary partition. Then type <c>2</c> to create the second primary partition, <path>/dev/sda2</path> in our case. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, type <c>+512M</c> to create a partition 512MB in size. After you've done this, type <c>t</c> to set the partition type, <c>2</c> to select the partition you just created and then type in <c>82</c> to set the partition type to \"Linux Swap\". After completing these steps, typing <c>p</c> should display a partition table that looks similar to this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):401
msgid "Partition listing after creating a swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):401
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 30.0 GB, 30005821440 bytes\n240 heads, 63 sectors/track, 3876 cylinders\nUnits = cylinders of 15120 * 512 = 7741440 bytes\n\nDevice Boot    Start       End    Blocks   Id  System\n/dev/sda1 *        1        14    105808+  83  Linux\n/dev/sda2         15        81    506520   82  Linux swap\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):416
msgid "Creating the Root Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):419
msgid "Finally, let's create the root partition. To do this, type <c>n</c> to create a new partition, then <c>p</c> to tell fdisk that you want a primary partition. Then type <c>3</c> to create the third primary partition, <path>/dev/sda3</path> in our case. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, hit enter to create a partition that takes up the rest of the remaining space on your disk. After completing these steps, typing <c>p</c> should display a partition table that looks similar to this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):429
msgid "Partition listing after creating the root partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):429
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 30.0 GB, 30005821440 bytes\n240 heads, 63 sectors/track, 3876 cylinders\nUnits = cylinders of 15120 * 512 = 7741440 bytes\n\nDevice Boot    Start       End    Blocks   Id  System\n/dev/sda1 *        1        14    105808+  83  Linux\n/dev/sda2         15        81    506520   82  Linux swap\n/dev/sda3         82      3876  28690200   83  Linux\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):445
msgid "Saving the Partition Layout"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):448
msgid "To save the partition layout and exit <c>fdisk</c>, type <c>w</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):452
msgid "Save and exit fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):452
#, no-wrap
msgid "\nCommand (m for help): <i>w</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):456
msgid "Now that your partitions are created, you can continue with <uri link=\"#filesystems\">Creating Filesystems</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):465
msgid "Creating Filesystems"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):467
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):470
msgid "Now that your partitions are created, it is time to place a filesystem on them. If you don't care about what filesystem to choose and are happy with what we use as default in this handbook, continue with <uri link=\"#filesystems-apply\">Applying a Filesystem to a Partition</uri>. Otherwise read on to learn about the available filesystems..."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):486
msgid "Applying a Filesystem to a Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):489
msgid "To create a filesystem on a partition or volume, there are tools available for each possible filesystem:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(th):497
msgid "Creation Command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):508
msgid "ext4"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):512
msgid "reiserfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):516
msgid "xfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(ti):520
msgid "jfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):525
msgid "For instance, to have the boot partition (<path>/dev/sda1</path> in our example) in ext2 and the root partition (<path>/dev/sda3</path> in our example) in ext3 (as in our example), you would use:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):531
msgid "Applying a filesystem on a partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):531
#, no-wrap
msgid "\n# <i>mkfs.ext2 /dev/sda1</i>\n# <i>mkfs.ext3 /dev/sda3</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):536
msgid "Now create the filesystems on your newly created partitions (or logical volumes)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):544
msgid "Activating the Swap Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):547
msgid "<c>mkswap</c> is the command that is used to initialize swap partitions:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):551
msgid "Creating a Swap signature"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):551
#, no-wrap
msgid "\n# <i>mkswap /dev/sda2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):555
msgid "To activate the swap partition, use <c>swapon</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):559
msgid "Activating the swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):559
#, no-wrap
msgid "\n# <i>swapon /dev/sda2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):563
msgid "Create and activate the swap with the commands mentioned above."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(title):571
msgid "Mounting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):574
msgid "Now that your partitions are initialized and are housing a filesystem, it is time to mount those partitions. Use the <c>mount</c> command. Don't forget to create the necessary mount directories for every partition you created. As an example we mount the root and boot partition:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre:caption):581
msgid "Mounting partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(pre):581
#, no-wrap
msgid "\n# <i>mount /dev/sda3 /mnt/gentoo</i>\n# <i>mkdir /mnt/gentoo/boot</i>\n# <i>mount /dev/sda1 /mnt/gentoo/boot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(note):587
msgid "If you want your <path>/tmp</path> to reside on a separate partition, be sure to change its permissions after mounting: <c>chmod 1777 /mnt/gentoo/tmp</c>. This also holds for <path>/var/tmp</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):593
msgid "We will also have to mount the proc filesystem (a virtual interface with the kernel) on <path>/proc</path>. But first we will need to place our files on the partitions."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(p):598
msgid "Continue with <uri link=\"?part=1&amp;chap=5\">Installing the Gentoo Installation Files</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-disk.xml(None):0
msgid "translator-credits"
msgstr ""

