msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-05-18 19:19+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(version):11
msgid "11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(date):12
msgid "2011-05-09"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(title):15
msgid "Making your Choice"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(title):17
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):20
msgid "Now that your kernel is configured and compiled and the necessary system configuration files are filled in correctly, it is time to install a program that will fire up your kernel when you start the system. Such a program is called a <e>bootloader</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):27
msgid "Several bootloaders exist for Linux/Alpha. You must choose one of the supported bootloaders, not all. You have the choice between <uri link=\"#aboot\">aBoot</uri> and <uri link=\"#milo\">MILO</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(title):37
msgid "Default: Using aboot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(note):40
msgid "<c>aboot</c> only supports booting from <b>ext2</b> and <b>ext3</b> partitions."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):45
msgid "We first install aboot on our system. Of course we use <c>emerge</c> to do so:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre:caption):50
msgid "Installing aboot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre):50
#, no-wrap
msgid "\n# <i>emerge aboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):54
msgid "The next step is to make our bootdisk bootable. This will start <c>aboot</c> when you boot your system. We make our bootdisk bootable by writing the <c>aboot</c> bootloader to the start of the disk."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre:caption):60
msgid "Making your bootdisk bootable"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre):60
#, no-wrap
msgid "\n# <i>swriteboot -f3 /dev/sda /boot/bootlx</i> \n# <i>abootconf /dev/sda 2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(note):65
msgid "If you use a different partitioning scheme than the one we use throughout this chapter, you have to change the commands accordingly. Please read the appropriate manual pages (<c>man 8 swriteboot</c> and <c>man 8 abootconf</c>). Also, if your root filesystem is ran using the JFS filesystem, make sure it gets mounted read-only at first by adding <c>ro</c> as a kernel option."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):74
msgid "Additionally, you can make Gentoo boot automatically by setting up the aboot configuration file and some SRM variables. You can try setting these variables from Linux, but it may be easier to do so from the SRM console itself."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre:caption):81
msgid "Automatically booting Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre):81
#, no-wrap
msgid "\n# <i>echo '0:2/boot/vmlinux.gz root=/dev/sda2' &gt; /etc/aboot.conf</i>\n# <i>cd /proc/srm_environment/named_variables</i>\n# <i>echo -n 0 &gt; boot_osflags</i>\n# <i>echo -n '' &gt; boot_file</i>\n# <i>echo -n 'BOOT' &gt; auto_action</i>\n# <i>echo -n 'dkc100' &gt; bootdef_dev</i>\n<comment>(Substitute dkc100 with whatever your boot device is)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):91
msgid "If you need to get into the SRM console again in the future (to recover your Gentoo install, play with some variables, or whatever), just hit CTRL+C to abort the automatic loading process."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):97
msgid "If you're installing using a serial console, don't forget to include the serial console boot flag in <path>aboot.conf</path>. See <path>/etc/aboot.conf.example</path> for some further information."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):103
msgid "Aboot is now configured and ready to use. Continue with <uri link=\"#reboot\">Rebooting the System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(title):111
msgid "Alternative: Using MILO"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):114
msgid "Before continuing, you should decide on how to use MILO. In this section, we will assume that you want to make a MILO boot floppy. If you are going to boot from an MS-DOS partition on your hard disk, you should amend the commands appropriately."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):121
msgid "To install MILO, we use <c>emerge</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre:caption):125
msgid "Installing MILO"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre):125
#, no-wrap
msgid "\n# <i>emerge milo</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):129
msgid "After MILO has been installed, the MILO images should be in <path>/opt/milo</path>. The commands below make a bootfloppy for use with MILO. Remember to use the correct image for your Alpha-system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre:caption):135
msgid "Installing MILO on a floppy"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre):135
#, no-wrap
msgid "\n<comment>(First insert a blank floppy)</comment>\n# <i>fdformat /dev/fd0</i>\n# <i>mformat a:</i>\n# <i>mcopy /opt/milo/milo-2.4-18-gentoo-ruffian a:\\milo</i>\n# <i>mcopy /opt/milo/linload.exe a:\\linload.exe</i>\n<comment>(Only if you have a Ruffian:</comment>     \n  # <i>mcopy /opt/milo/ldmilo.exe a:\\ldmilo.exe</i>\n<comment>)</comment>\n# <i>echo -ne '\\125\\252' | dd of=/dev/fd0 bs=1 seek=510 count=2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):147
msgid "Your MILO boot floppy is now ready to boot Gentoo Linux. You may need to set environment variables in your ARCS Firmware to get MILO to start; this is all explained in the <uri link=\"http://tldp.org/HOWTO/MILO-HOWTO/\">MILO-HOWTO</uri> with some examples on common systems, and examples of the commands to use in interactive mode."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):155
msgid "Not reading the <uri link=\"http://tldp.org/HOWTO/MILO-HOWTO/\">MILO-HOWTO</uri> is a <e>bad</e> idea."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):160
msgid "Now continue with <uri link=\"#reboot\">Rebooting the System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(title):167
msgid "Rebooting the System"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):171
msgid "Exit the chrooted environment and unmount all mounted partitions. Then type in that one magical command you have been waiting for: <c>reboot</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre:caption):176
msgid "Exiting the chroot, unmounting all partitions and rebooting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(pre):176
#, no-wrap
msgid "\n# <i>exit</i>\ncdimage ~# <i>cd</i>\ncdimage ~# <i>umount -l /mnt/gentoo/dev{/shm,/pts,}</i>\ncdimage ~# <i>umount -l /mnt/gentoo{/boot,/proc,}</i>\ncdimage ~# <i>reboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):184
msgid "Of course, don't forget to remove the bootable CD, otherwise the CD will be booted again instead of your new Gentoo system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(p):189
msgid "Once rebooted in your Gentoo installation, finish up with <uri link=\"?part=1&amp;chap=11\">Finalizing your Gentoo Installation</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-alpha-bootloader.xml(None):0
msgid "translator-credits"
msgstr ""

