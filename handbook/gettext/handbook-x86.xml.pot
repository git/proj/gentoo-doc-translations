msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:40+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):7
msgid "Gentoo Linux x86 Handbook"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):10
msgid "x86"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):11
msgid "i386"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):12
msgid "/dev/sda1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):13
msgid "2.6.34-r1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):14
msgid "kernel-2.6.34-gentoo-r1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):15
msgid "kernel-genkernel-x86-2.6.34-gentoo-r1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):16
msgid "initramfs-genkernel-x86-2.6.34-gentoo-r1"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):17
msgid "install-x86-minimal-&lt;release&gt;.iso"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):18
msgid "104"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):19
msgid "releases/x86/autobuilds/"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):20
msgid "stage3-i686-&lt;release&gt;.tar.bz2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):21
msgid "default/linux/x86/10.0"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):22
msgid "handbook-x86.xml"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(key):23
msgid "-O2 -march=i686 -pipe"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):26 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):29 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):32 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):35 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):38 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):41
msgid "Author"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):27
msgid "swift@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):27
msgid "Sven Vermeulen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):30
msgid "g2boojum@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):30
msgid "Grant Goodyear"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):33
msgid "uberlord@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):33
msgid "Roy Marples"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):36
msgid "drobbins@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):36
msgid "Daniel Robbins"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):39
msgid "chouser@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):39
msgid "Chris Houser"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):42
msgid "jerry@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):42
msgid "Jerry Alexandratos"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):44
msgid "Gentoo x86 Developer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):45
msgid "seemant@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):45
msgid "Seemant Kulleen"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):47
msgid "Gentoo Alpha Developer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):48
msgid "taviso@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):48
msgid "Tavis Ormandy"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):54
msgid "Gentoo AMD64 Developer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):55
msgid "jhuebel@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):55
msgid "Jason Huebel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):57
msgid "Gentoo HPPA developer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):58
msgid "gmsoft@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):58
msgid "Guy Martin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):60
msgid "Gentoo PPC developer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):61
msgid "pvdabeel@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):61
msgid "Pieter Van den Abeele"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):63
msgid "Gentoo SPARC developer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):64
msgid "blademan@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):64
msgid "Joe Kallar"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):66 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):69 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):70 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):73 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):76 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):79 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):82 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):85 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):88 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):91 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):94 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):97 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):100 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):103 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):106 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):109 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):112 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):115 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):118
msgid "Editor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):67
msgid "zhen@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):67
msgid "John P. Davis"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author):69
msgid "Pierre-Henri Jondot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):71
msgid "stocke2@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):71
msgid "Eric Stockbridge"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):74
msgid "rajiv@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):74
msgid "Rajiv Manglani"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):77
msgid "seo@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):77
msgid "Jungmin Seo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):80
msgid "zhware@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):80
msgid "Stoyan Zhekov"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):83
msgid "jhhudso@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):83
msgid "Jared Hudson"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):86
msgid "peitolm@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):86
msgid "Colin Morey"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):89
msgid "peesh@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):89
msgid "Jorge Paulo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):92
msgid "carl@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):92
msgid "Carl Anderson"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):95
msgid "avenj@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):95
msgid "Jon Portnoy"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):98
msgid "klasikahl@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):98
msgid "Zack Gilburd"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):101
msgid "jmorgan@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):101
msgid "Jack Morgan"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):104
msgid "bennyc@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):104
msgid "Benny Chuang"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):107
msgid "erwin@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):107
msgid "Erwin"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):110
msgid "kumba@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):110
msgid "Joshua Kinard"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):113
msgid "dertobi123@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):113
msgid "Tobias Scherbaum"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):116
msgid "neysx@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):116
msgid "Xavier Neys"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):119
msgid "nightmorph@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):119
msgid "Joshua Saddler"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):121 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):124 ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):127
msgid "Reviewer"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):122
msgid "gerrynjr@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):122
msgid "Gerald J. Normandin Jr."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):125
msgid "dberkholz@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):125
msgid "Donnie Berkholz"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):128
msgid "antifa@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):128
msgid "Ken Nowack"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(author:title):130
msgid "Contributor"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail:link):131
msgid "pylon@gentoo.org"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(mail):131
msgid "Lars Weiler"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(abstract):134
msgid "This is the Gentoo Handbook, an effort to centralize Gentoo/Linux information. This handbook contains the installation instructions for an Internet-based installation on x86 systems and parts about working with Gentoo and Portage."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(version):144
msgid "10.2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(date):145
msgid "2010-07-19"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):148
msgid "Installing Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(abstract):149
msgid "In this part you learn how to install Gentoo on your system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):154
msgid "About the Gentoo Linux Installation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):159
msgid "Choosing the Right Installation Medium"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):164
msgid "Configuring your Network"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):169
msgid "Preparing the Disks"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):174
msgid "Installing the Gentoo Installation Files"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):179
msgid "Installing the Gentoo Base System"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):184
msgid "Configuring the Kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):189
msgid "Configuring your System"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):194
msgid "Installing Necessary System Tools"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):199
msgid "Configuring the Bootloader"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(abstract):200
msgid "Several bootloaders exist for the x86 architecture. Each one of them has its own way of configuration. We step you through the process of configuring a bootloader to your needs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):209
msgid "Finalizing your Gentoo Installation"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):214
msgid "Where to go from here?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):220
msgid "Working with Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(abstract):221
msgid "Learn how to work with Gentoo: installing software, altering variables, changing Portage behaviour etc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):227
msgid "A Portage Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):232
msgid "USE flags"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):237
msgid "Portage Features"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):242
msgid "Initscripts"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):247
msgid "Environment Variables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):253
msgid "Working with Portage"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(abstract):254
msgid "\"Working with Portage\" provides an in-depth coverage of Portage, Gentoo's Software Management Tool."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):260
msgid "Files and Directories"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):265
msgid "Configuring through Variables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):270
msgid "Mixing Software Branches"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):275
msgid "Additional Portage Tools"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):280
msgid "Diverting from the Official Tree"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):286
msgid "Gentoo Network Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(abstract):287
msgid "A comprehensive guide to Networking in Gentoo."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):290
msgid "Getting Started"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):295
msgid "Advanced Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):300
msgid "Modular Networking"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):305
msgid "Wireless Networking"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):310
msgid "Adding Functionality"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(title):315
msgid "Network Management"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//handbook-x86.xml(None):0
msgid "translator-credits"
msgstr ""

