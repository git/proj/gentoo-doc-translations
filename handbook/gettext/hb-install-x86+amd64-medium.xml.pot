msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-10-28 22:38+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(abstract):11
msgid "You can install Gentoo in many ways. This chapter explains how to install Gentoo using the minimal Installation CD."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(version):16
msgid "9"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(date):17
msgid "2011-10-09"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):20 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):33
msgid "Hardware Requirements"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):22 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):107
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):25
msgid "Before we start, we first list what hardware requirements you need to successfully install Gentoo on your box."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):39 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):68
msgid "Minimal CD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):40 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):69
msgid "LiveCD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):43 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):72
msgid "CPU"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):44
msgid "i486 or later"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):45
msgid "<b>i686</b> or later"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):48 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):80
msgid "Memory"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):49 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):81
msgid "64 MB"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):50 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):82
msgid "256 MB"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):53 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):85
msgid "Diskspace"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):54 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):86
msgid "1.5 GB (excluding swap space)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):57 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):89
msgid "Swap space"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):58 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):90
msgid "At least 256 MB"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):73
msgid "Any AMD64 CPU or <uri link=\"http://en.wikipedia.org/wiki/EMT64#Intel_64\">EM64T CPU</uri> (Core 2 Duo &amp; Quad processors are EM64T)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):94
msgid "You should check the <uri link=\"/proj/en/base/amd64/\">Gentoo AMD64 Project Page</uri> before proceeding."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):105
msgid "The Gentoo Installation CDs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):110
msgid "The <e>Gentoo Installation CDs</e> are bootable CDs which contain a self-sustained Gentoo environment. They allow you to boot Linux from the CD. During the boot process your hardware is detected and the appropriate drivers are loaded. They are maintained by Gentoo developers."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):117
msgid "All Installation CDs allow you to boot, set up networking, initialize your partitions and start installing Gentoo from the Internet."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):157
msgid "Gentoo Minimal Installation CD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):160
msgid "The Minimal Installation CD is called <c><keyval id=\"min-cd-name\"/></c> and takes up only <keyval id=\"min-cd-size\"/> MB of diskspace. You can use this Installation CD to install Gentoo, but <e>only</e> with a working Internet connection."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):221
msgid "The Stage3 Tarball"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):224
msgid "A stage3 tarball is an archive containing a minimal Gentoo environment, suitable to continue the Gentoo installation using the instructions in this manual. Previously, the Gentoo Handbook described the installation using one of three stage tarballs. While Gentoo still offers stage1 and stage2 tarballs, the official installation method uses the stage3 tarball. If you are interested in performing a Gentoo installation using a stage1 or stage2 tarball, please read the Gentoo FAQ on <uri link=\"/doc/en/faq.xml#stage12\">How do I Install Gentoo Using a Stage1 or Stage2 Tarball?</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):235
msgid "Stage3 tarballs can be downloaded from <path><keyval id=\"release-dir\"/>current-stage3/</path> on any of the <uri link=\"/main/en/mirrors.xml\">Official Gentoo Mirrors</uri> and are not provided on the LiveCD."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):247
msgid "Download, Burn and Boot a Gentoo Installation CD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):249
msgid "Downloading and Burning the Installation CDs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):252
msgid "You have chosen to use a Gentoo Installation CD. We'll first start by downloading and burning the chosen Installation CD. We previously discussed the several available Installation CDs, but where can you find them?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):258
msgid "You can download any of the Installation CDs from one of our <uri link=\"/main/en/mirrors.xml\">mirrors</uri>. The Installation CDs are located in the <path><keyval id=\"release-dir\"/>current-iso/</path> directory."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):264
msgid "Inside that directory you'll find ISO files. Those are full CD images which you can write on a CD-R."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):269
msgid "In case you wonder if your downloaded file is corrupted or not, you can check its MD5 checksum and compare it with the MD5 checksum we provide (such as <path><keyval id=\"min-cd-name\"/>.DIGESTS</path>). You can check the MD5 checksum with the <c>md5sum</c> tool under Linux/Unix or <uri link=\"http://www.etree.org/md5com.html\">md5sum</uri> for Windows."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):277
msgid "Another way to check the validity of the downloaded file is to use GnuPG to verify the cryptographic signature that we provide (the file ending with <path>.asc</path>). Download the signature file and obtain the public keys:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(pre:caption):283
msgid "Obtaining the public key"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(pre):283
#, no-wrap
msgid "\n$ <i>gpg --keyserver subkeys.pgp.net --recv-keys 2D182910 17072058</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):287
msgid "Now verify the signature:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(pre:caption):291
msgid "Verify the files"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(pre):291
#, no-wrap
msgid "\n<comment>(Verify the cryptographic signature)</comment>\n$ <i>gpg --verify &lt;downloaded iso.DIGESTS.asc&gt;</i>\n<comment>(Verify the checksum)</comment>\n$ <i>sha1sum -c &lt;downloaded iso.DIGESTS.asc&gt;</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):298
msgid "To burn the downloaded ISO(s), you have to select raw-burning. How you do this is highly program-dependent. We will discuss <c>cdrecord</c> and <c>K3B</c> here; more information can be found in our <uri link=\"/doc/en/faq.xml#isoburning\">Gentoo FAQ</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(li):306
msgid "With cdrecord, you simply type <c>cdrecord dev=/dev/hdc &lt;downloaded iso file&gt;</c> (replace <path>/dev/hdc</path> with your CD-RW drive's device path)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(li):311
msgid "With K3B, select <c>Tools</c> &gt; <c>Burn CD Image</c>. Then you can locate your ISO file within the 'Image to Burn' area. Finally click <c>Start</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(title):320
msgid "Booting the Installation CD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):323
msgid "Once you have burnt your installation CD, it is time to boot it. Remove all CDs from your CD drives, reboot your system and enter the BIOS. This is usually done by hitting DEL, F1 or ESC, depending on your BIOS. Inside the BIOS, change the boot order so that the CD-ROM is tried before the hard disk. This is often found under \"CMOS Setup\". If you don't do this, your system will just reboot from the hard disk, ignoring the CD-ROM."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):332
msgid "Now place the installation CD in the CD-ROM drive and reboot. You should see a boot prompt. At this screen, you can hit Enter to begin the boot process with the default boot options, or boot the Installation CD with custom boot options by specifying a kernel followed by boot options and then hitting Enter."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):339
msgid "When the boot prompt is shown, you get the option of displaying the available kernels (<c>F1</c>) and boot options (<c>F2</c>). If you make no selection within 20 seconds (either displaying information or using a kernel) then the LiveCD will fall back to booting from disk. This allows installations to reboot and try out their installed environment without the need to remove the CD from the tray (something well appreciated for remote installations)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):348
msgid "Now we mentioned specifying a kernel. On our Installation CDs, we provide several kernels. The default one is <c>gentoo</c>. Other kernels are for specific hardware needs and the <c>-nofb</c> variants which disable framebuffer."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):355
msgid "Below you'll find a short overview on the available kernels:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):361
msgid "Kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(th):362
msgid "Description"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):365 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):369
msgid "gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):366
msgid "Default 2.6 kernel with support for multiple CPUs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):370
msgid "Default kernel with support for K8 CPUS (including NUMA support) and EM64T CPUs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):376
msgid "gentoo-nofb"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):377
msgid "Same as <c>gentoo</c> but without framebuffer support"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):380
msgid "memtest86"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(ti):381
msgid "Test your local RAM for errors"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):385
msgid "You can also provide kernel options. They represent optional settings you can (de)activate at will."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(note):398
msgid "The CD will check for \"no*\" options before \"do*\" options, so that you can override any option in the exact order you specify."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):403
msgid "Now boot your CD, select a kernel (if you are not happy with the default <c>gentoo</c> kernel) and boot options. As an example, we show you how to boot the <c>gentoo</c> kernel, with <c>dopcmcia</c> as kernel parameters:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(pre:caption):410
msgid "Booting an Installation CD"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(pre):410
#, no-wrap
msgid "\nboot: <i>gentoo dopcmcia</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):414
msgid "You will then be greeted with a boot screen and progress bar. If you are installing Gentoo on a system with a non-US keyboard, make sure you immediately press Alt-F1 to switch to verbose mode and follow the prompt. If no selection is made in 10 seconds the default (US keyboard) will be accepted and the boot process will continue. Once the boot process completes, you will be automatically logged in to the \"Live\" Gentoo Linux as \"root\", the super user. You should have a root (\"#\") prompt on the current console and can also switch to other consoles by pressing Alt-F2, Alt-F3 and Alt-F4. Get back to the one you started on by pressing Alt-F1."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(p):426
msgid "Now continue with <uri link=\"#hardware\">Extra Hardware Configuration</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-x86+amd64-medium.xml(None):0
msgid "translator-credits"
msgstr ""

