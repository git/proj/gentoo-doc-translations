msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-05-18 19:19+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(abstract):11
msgid "Several bootloaders exist. Each one of them has its own way of configuration. In this chapter we'll describe all possibilities for you and step you through the process of configuring a bootloader to your needs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(version):18
msgid "11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(date):19
msgid "2011-05-09"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(title):22
msgid "Making your Choice"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(title):24 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(title):45
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):27
msgid "Now that your kernel is configured and compiled and the necessary system configuration files are filled in correctly, it is time to install a program that will fire up your kernel when you start the system. Such a program is called a <e>bootloader</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):34
msgid "On Linux/PPC64 we have only yaBoot as a bootloader until grub2 is finished."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(title):43
msgid "Using yaBoot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(impo):48
msgid "If you are using a 64-bit userland, you must use the <c>yaboot-static</c> package because yaboot cannot be compiled as a 64-bit application. The 32-bit userlands should use the regular <c>yaboot</c> package."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):54
msgid "There are two ways to configure yaBoot for your system. You can use the new and improved <c>yabootconfig</c> included with <path>yaboot-1.3.8-r1</path> and later to automatically set up yaboot. If for some reason you do not want to run <c>yabootconfig</c> to automatically set up <path>/etc/yaboot.conf</path> or you are installing Gentoo on a G5 (on which <c>yabootconfig</c> does not always work), you can just edit the sample file already installed on your system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(impo):63
msgid "yabootconfig/ybin won't work on IBM. You have to install yaboot another way: <uri link=\"#yaboot-ibm\">Using yaboot on IBM hardware</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(note):68
msgid "If your root filesystem uses the JFS filesystem, be sure to add <c>ro</c> as a kernel parameter. JFS must be able to replay its log in read-only mode before it gets mounted read-write."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(uri:link):75
msgid "#yabootconfig"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(uri):75 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(title):84
msgid "Default: Using yabootconfig"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(uri:link):77
msgid "#manual_yaboot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(uri):77 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(title):133
msgid "Alternative: Manual yaBoot Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):87
msgid "<c>yabootconfig</c> will auto-detect the partitions on your machine and will set up dual and triple boot combinations with Linux, Mac OS, and Mac OS X."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):93
msgid "To use <c>yabootconfig</c>, your drive must have a bootstrap partition, and <path>/etc/fstab</path> must be configured with your Linux partitions. Both of these should have been done already in the steps above. To start, ensure that you have the latest version of yaboot installed by running <c>emerge --update yaboot-static</c>. This is necessary as the latest version will be available via Portage, but it may not have made it into the stage files."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre:caption):102
msgid "Installing yaboot-static"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre):102 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre):141
#, no-wrap
msgid "\n<comment>(For 64-bit users)</comment>\n# <i>emerge --update yaboot-static</i>\n\n<comment>(For 32-bit users)</comment>\n# <i>emerge yaboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):110
msgid "Now run <c>yabootconfig</c>. The program will run and it will confirm the location of the bootstrap partition. Type <c>Y</c> if it is correct. If not, double check <path>/etc/fstab</path>. yabootconfig will then scan your system setup, create <path>/etc/yaboot.conf</path> and run <c>mkofboot</c> for you. <c>mkofboot</c> is used to format the bootstrap partition, and install the yaboot configuration file into it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):119
msgid "You might want to verify the contents of <path>/etc/yaboot.conf</path>. If you make changes to <path>/etc/yaboot.conf</path> (like setting the default/boot OS), make sure to rerun <c>ybin -v</c> to apply changes to the bootstrap partition."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):126
msgid "Now continue with <uri link=\"#reboot\">Rebooting your System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):136
msgid "First make sure you have the latest <c>yaboot-static</c> installed on your system:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre:caption):141
msgid "Installing yaboot"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):149
msgid "Below you find a completed <path>yaboot.conf</path> file. Alter it at will."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre:caption):154
msgid "/etc/yaboot.conf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre):154
#, no-wrap
msgid "\n<comment>## /etc/yaboot.conf\n##\n## run: \"man yaboot.conf\" for details. Do not make changes until you have!!\n## see also: /usr/share/doc/yaboot/examples for example configurations.\n##\n## For a dual-boot menu, add one or more of:\n## bsd=/dev/sdaX, macos=/dev/sdaY, macosx=/dev/sdaZ\n\n## our bootstrap partition:</comment>\n\nboot=/dev/sda2\n\n<comment>## ofboot is the Open Firmware way to specify the bootstrap partition.\n## If this isn't defined, yaboot fails on the G5 and some G4s (unless \n## you pass the necessary arguments to the mkofboot/ybin program).\n## hd:X means /dev/sdaX.</comment>\n\nofboot=hd:2\n\n<comment>## hd: is Open Firmware speak for sda</comment>\ndevice=hd:\n\ndelay=5\ndefaultos=macosx\ntimeout=30\ninstall=/usr/lib/yaboot/yaboot\nmagicboot=/usr/lib/yaboot/ofboot\n\n<comment>#################\n## This section can be duplicated if you have more than one kernel or set of\n## boot options - replace <keyval id=\"kernel-name\"></keyval> with your kernel\n#################</comment>\nimage=/boot/<keyval id=\"kernel-name\"></keyval>\n  label=Linux\n  root=/dev/sda3\n  partition=3\n  read-only\n\nmacos=hd:13\nmacosx=hd:12\nenablecdboot\nenableofboot\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):199
msgid "Once <path>yaboot.conf</path> is set up the way you want it, you run <c>mkofboot -v</c> to install the settings in the bootstrap partition. <e>Don't forget this!</e> Confirm when <c>mkofboot</c> asks you to create a new filesystem."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):205
msgid "If all goes well, and you have the same options as the sample above, your next reboot will give you a simple, five-entry boot menu. If you update your yaboot config later on, you'll just need to run <c>ybin -v</c> to update the bootstrap partition - <c>mkofboot</c> is for initial setup only."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):212
msgid "For more information on yaboot, take a look at the <uri link=\"http://penguinppc.org/projects/yaboot\">yaboot project</uri>. For now, continue the installation with <uri link=\"#reboot\">Rebooting your System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(title):222
msgid "Using yaboot on IBM hardware"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):225
msgid "On IBM hardware you cannot run <c>yabootconfig</c> or <c>ybin</c>. You must proceed with the following steps:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(li):231
msgid "Install yaboot-static"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(li):232
msgid "Run <c>dd if=/usr/lib/yaboot/yaboot.chrp of=/dev/sdXX</c> (fill in XX with your disk and partition for the PReP partition; this was in our example <path>/dev/sda1</path>)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(li):237
msgid "Next construct your own <path>yaboot.conf</path> file and place into <path>/etc</path>. (Take a look at the config above, look into the man page of yaboot.conf or look at the below <path>yaboot.conf</path> example.)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(li):242
msgid "Assuming your boot device in OF is pointing to the harddrive you prep boot partition is on then it'll just work, otherwise at IPL time, go into the multiboot menu and set the boot device to the one with your prep boot partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(li):248
msgid "That's it!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre:caption):251
msgid "yaboot.conf for IBM hardware"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre):251
#, no-wrap
msgid "\ndevice=disk:\npartition=2\nroot=/dev/sda2\ndefault=linux\ntimeout=50\n\nimage=/boot/<keyval id=\"kernel-name\"></keyval>\n    label=linux\n    append=\"console=ttyS0,9600\"\n    read-only\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):264
msgid "For POWER4, POWER5, and blade-based hardware where the PReP disk partition and the disk partition that contains your kernel are on the same physical disk, you can use a simplified <path>yaboot.conf</path>. The following should be sufficient:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre:caption):271
msgid "yaboot.conf for PReP hardware"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre):271
#, no-wrap
msgid "\ndefault = linux\ntimeout = 100\nimage=/boot/<keyval id=\"kernel-name\"></keyval>\n        label=linux\n        read-only\n        root = /dev/sda2\n        append=\"root=/dev/sda2\"\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):281
msgid "To verify that yaboot has been copied to the PReP partition:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre:caption):285
msgid "Verifying the yaboot install on PReP"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre):285
#, no-wrap
msgid "\n# <i>dd if=/dev/sda1 count=10 | grep ELF</i>\nBinary file (standard input) matches\n10+0 records in\n10+0 records out\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):292
msgid "A match signifies that yaboot was installed correctly."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(title):299
msgid "Rebooting the System"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):303
msgid "Exit the chrooted environment and unmount all mounted partitions. Then type in that one magical command you have been waiting for: <c>reboot</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre:caption):308
msgid "Exiting the chroot, unmounting all partitions and rebooting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(pre):308
#, no-wrap
msgid "\n# <i>exit</i>\n~# <i>cd</i>\n~# <i>umount -l /mnt/gentoo/dev{/shm,/pts,}</i>\n~# <i>umount -l /mnt/gentoo{/boot,/proc,}</i>\n~# <i>reboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):316
msgid "Of course, don't forget to remove the bootable CD, otherwise the CD will be booted again instead of your new Gentoo system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(p):321
msgid "Once rebooted in your Gentoo installation, finish up with <uri link=\"?part=1&amp;chap=11\">Finalizing your Gentoo Installation</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-bootloader.xml(None):0
msgid "translator-credits"
msgstr ""

