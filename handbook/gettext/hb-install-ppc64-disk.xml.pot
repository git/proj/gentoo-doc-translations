msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-10-28 22:38+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(abstract):11
msgid "To be able to install Gentoo, you must create the necessary partitions. This chapter describes how to partition a disk for future usage."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(version):16
msgid "11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(date):17
msgid "2011-10-17"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):20
msgid "Introduction to Block Devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):27
msgid "Partitions and Slices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):30
msgid "Although it is theoretically possible to use a full disk to house your Linux system, this is almost never done in practice. Instead, full disk block devices are split up in smaller, more manageable block devices. On most systems, these are called <e>partitions</e>. Other architectures use a similar technique, called <e>slices</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):42
msgid "Designing a Partitioning Scheme"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):44
msgid "Default Partitioning Scheme"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):47
msgid "If you are not interested in drawing up a partitioning scheme for your system, you can use the partitioning scheme we use throughout this book:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(th):54 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(th):254
msgid "Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(th):55 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(th):600
msgid "Filesystem"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(th):56
msgid "Size"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(th):57 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(th):255
msgid "Description"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):61 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):63
msgid "Partition map"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):62
msgid "31.5k"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):67
msgid "(bootstrap)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):68
msgid "800k"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):69
msgid "Apple_Bootstrap"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):73
msgid "(swap)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):74
msgid "512M"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):75 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):263
msgid "Swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):79 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):608
msgid "ext3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):80
msgid "Rest of the disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):81 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):267
msgid "Root partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(note):85
msgid "There are some partitions named like this: <path>Apple_Driver43</path>, <path>Apple_Driver_ATA</path>, <path>Apple_FWDriver</path>, <path>Apple_Driver_IOKit</path>, and <path>Apple_Patches</path>. If you are not planning to use MacOS 9 you can delete them, because MacOS X and Linux don't need them. You might have to use parted in order to delete them, as mac-fdisk can't delete them yet."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):94
msgid "If you are interested in knowing how big a partition should be, or even how many partitions you need, read on. Otherwise continue now with <uri link=\"#mac-fdisk\">Apple G5: Using mac-fdisk to Partition your Disk</uri> or <uri link=\"#fdisk\">IBM pSeries: using fdisk to Partition your Disk</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):105
msgid "How Many and How Big?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):108
msgid "The number of partitions is highly dependent on your environment. For instance, if you have lots of users, you will most likely want to have your <path>/home</path> separate as it increases security and makes backups easier. If you are installing Gentoo to perform as a mailserver, your <path>/var</path> should be separate as all mails are stored inside <path>/var</path>. A good choice of filesystem will then maximise your performance. Gameservers will have a separate <path>/opt</path> as most gaming servers are installed there. The reason is similar for <path>/home</path>: security and backups. You will definitely want to keep <path>/usr</path> big: not only will it contain the majority of applications, the Portage tree alone takes around 500 Mbyte excluding the various sources that are stored in it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):122
msgid "As you can see, it very much depends on what you want to achieve. Separate partitions or volumes have the following advantages:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(li):128
msgid "You can choose the best performing filesystem for each partition or volume"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(li):131
msgid "Your entire system cannot run out of free space if one defunct tool is continuously writing files to a partition or volume"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(li):135
msgid "If necessary, file system checks are reduced in time, as multiple checks can be done in parallel (although this advantage is more with multiple disks than it is with multiple partitions)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(li):140
msgid "Security can be enhanced by mounting some partitions or volumes read-only, nosuid (setuid bits are ignored), noexec (executable bits are ignored) etc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):146
msgid "However, multiple partitions have disadvantages as well. If not configured properly, you will have a system with lots of free space on one partition and none on another. Another nuisance is that separate partitions - especially for important mountpoints like <path>/usr</path> or <path>/var</path> - often require the administrator to boot with an initramfs to mount the partition before other boot scripts start. This isn't always the case though, so YMMV."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):155
msgid "There is also a 15-partition limit for SCSI and SATA."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):163
msgid "Default: Using mac-fdisk (Apple G5) to Partition your Disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):166
msgid "At this point, create your partitions using <c>mac-fdisk</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):170
msgid "Starting mac-fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):170
#, no-wrap
msgid "\n# <i>mac-fdisk /dev/sda</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):174
msgid "First delete the partitions you have cleared previously to make room for your Linux partitions. Use <c>d</c> in <c>mac-fdisk</c> to delete those partition(s). It will ask for the partition number to delete."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):180
msgid "Second, create an <e>Apple_Bootstrap</e> partition by using <c>b</c>. It will ask for what block you want to start. Enter the number of your first free partition, followed by a <c>p</c>. For instance this is <c>2p</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(note):186
msgid "This partition is <e>not</e> a \"boot\" partition. It is not used by Linux at all; you don't have to place any filesystem on it and you should never mount it. PPC users don't need an extra partition for <path>/boot</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):192
msgid "Now create a swap partition by pressing <c>c</c>. Again <c>mac-fdisk</c> will ask for what block you want to start this partition from. As we used <c>2</c> before to create the Apple_Bootstrap partition, you now have to enter <c>3p</c>. When you're asked for the size, enter <c>512M</c> (or whatever size you want). When asked for a name, enter <c>swap</c> (mandatory)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):200
msgid "To create the root partition, enter <c>c</c>, followed by <c>4p</c> to select from what block the root partition should start. When asked for the size, enter <c>4p</c> again. <c>mac-fdisk</c> will interpret this as \"Use all available space\". When asked for the name, enter <c>root</c> (mandatory)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):207
msgid "To finish up, write the partition to the disk using <c>w</c> and <c>q</c> to quit <c>mac-fdisk</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(note):212
msgid "To make sure everything is ok, you should run mac-fdisk once more and check whether all the partitions are there. If you don't see any of the partitions you created, or the changes you made, you should reinitialize your partitions by pressing <c>i</c> in mac-fdisk. Note that this will recreate the partition map and thus remove all your partitions."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):220 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):560
msgid "Now that your partitions are created, you can continue with <uri link=\"#filesystems\">Creating Filesystems</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):228
msgid "IBM pSeries, iSeries and OpenPower: using fdisk to Partition your Disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(note):232
msgid "If you are planning to use a RAID disk array for your Gentoo installation and you are using POWER5-based hardware, you should now run <c>iprconfig</c> to format the disks to Advanced Function format and create the disk array. You should emerge <c>iprutils</c> after your install is complete."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):239
msgid "If you have an ipr-based SCSI adapter, you should start the ipr utilities now."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):243
msgid "Starting ipr utilities"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):243
#, no-wrap
msgid "\n# <i>/etc/init.d/iprinit start</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):247
msgid "The following parts explain how to create the example partition layout described previously, namely:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):259
msgid "PPC PReP Boot partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):271
msgid "Change your partition layout according to your own preference."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):278
msgid "Viewing the Current Partition Layout"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):281
msgid "<c>fdisk</c> is a popular and powerful tool to split your disk into partitions. Fire up <c>fdisk</c> on your disk (in our example, we use <path>/dev/sda</path>):"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):287
msgid "Starting fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):287
#, no-wrap
msgid "\n# <i>fdisk /dev/sda</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):291
msgid "Once in <c>fdisk</c>, you'll be greeted with a prompt that looks like this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):296
msgid "fdisk prompt"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):296
#, no-wrap
msgid "\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):300
msgid "If you still have an AIX partition layout on your system, you will get the following error message:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):305
msgid "Error message from fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):305
#, no-wrap
msgid "\n  There is a valid AIX label on this disk.\n  Unfortunately Linux cannot handle these\n  disks at the moment.  Nevertheless some\n  advice:\n  1. fdisk will destroy its contents on write.\n  2. Be sure that this disk is NOT a still vital\n     part of a volume group. (Otherwise you may\n     erase the other disks as well, if unmirrored.)\n  3. Before deleting this physical volume be sure\n     to remove the disk logically from your AIX\n     machine.  (Otherwise you become an AIXpert).\n\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):321
msgid "Don't worry, you can create a new empty DOS partition table by pressing <c>o</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(warn):326
msgid "This will destroy any installed AIX version!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):330
msgid "Type <c>p</c> to display your disk current partition configuration:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):334
msgid "An example partition configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):334
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 30.7 GB, 30750031872 bytes\n141 heads, 63 sectors/track, 6761 cylinders\nUnits = cylinders of 8883 * 512 = 4548096 bytes\n\n   Device Boot      Start         End      Blocks   Id  System\n/dev/sda1               1          12       53266+  83  Linux\n/dev/sda2              13         233      981571+  82  Linux swap\n/dev/sda3             234         674     1958701+  83  Linux\n/dev/sda4             675        6761    27035410+   5  Extended\n/dev/sda5             675        2874     9771268+  83  Linux\n/dev/sda6            2875        2919      199836   83  Linux\n/dev/sda7            2920        3008      395262   83  Linux\n/dev/sda8            3009        6761    16668918   83  Linux\n\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):354
msgid "This particular disk is configured to house six Linux filesystems (each with a corresponding partition listed as \"Linux\") as well as a swap partition (listed as \"Linux swap\")."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):363
msgid "Removing all Partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):366
msgid "We will first remove all existing partitions from the disk. Type <c>d</c> to delete a partition. For instance, to delete an existing <path>/dev/sda1</path>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(note):372
msgid "If you don't want to delete all partitions just delete those you want to delete. At this point you should create a backup of your data to avoid losing it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):377
msgid "Deleting a partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):377
#, no-wrap
msgid "\nCommand (m for help): <i>d</i>\nPartition number (1-4): <i>1</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):382
msgid "The partition has been scheduled for deletion. It will no longer show up if you type <c>p</c>, but it will not be erased until your changes have been saved. If you made a mistake and want to abort without saving your changes, type <c>q</c> immediately and hit Enter and your partition will not be deleted."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):390
msgid "Now, assuming that you do indeed want to wipe out all the partitions on your system, repeatedly type <c>p</c> to print out a partition listing and then type <c>d</c> and the number of the partition to delete it. Eventually, you'll end up with a partition table with nothing in it:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):397
msgid "An empty partition table"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):397
#, no-wrap
msgid "\nDisk /dev/sda: 30.7 GB, 30750031872 bytes\n141 heads, 63 sectors/track, 6761 cylinders\nUnits = cylinders of 8883 * 512 = 4548096 bytes\n\nDevice Boot    Start       End    Blocks   Id  System\n\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):407
msgid "Now that the in-memory partition table is empty, we're ready to create the partitions. We will use a default partitioning scheme as discussed previously. Of course, don't follow these instructions to the letter if you don't want the same partitioning scheme!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):417
msgid "Creating the PPC PReP boot partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):420
msgid "We first create a small PReP boot partition. Type <c>n</c> to create a new partition, then <c>p</c> to select a primary partition, followed by <c>1</c> to select the first primary partition. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, type <c>+7M</c> to create a partition 7 MB in size. After you've done this, type <c>t</c> to set the partition type, <c>1</c> to select the partition you just created and then type in <c>41</c> to set the partition type to \"PPC PReP Boot\". Finally, you'll need to mark the PReP partition as bootable."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(note):432
msgid "The PReP partition has to be smaller than 8 MB!"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):436
msgid "Creating the PReP boot partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):436
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 30.7 GB, 30750031872 bytes\n141 heads, 63 sectors/track, 6761 cylinders\nUnits = cylinders of 8883 * 512 = 4548096 bytes\n\n   Device Boot      Start         End      Blocks   Id  System\n\nCommand (m for help): <i>n</i>\nCommand action\n      e   extended\n      p   primary partition (1-4)\n<i>p</i>\nPartition number (1-4): <i>1</i>\nFirst cylinder (1-6761, default 1): \nUsing default value 1\nLast cylinder or +size or +sizeM or +sizeK (1-6761, default\n6761): <i>+8M</i>\n\nCommand (m for help): <i>t</i>\nSelected partition 1\nHex code (type L to list codes): <i>41</i>\nChanged system type of partition 1 to 41 (PPC PReP Boot)\n\nCommand (m for help): <i>a</i>\nPartition number (1-4): <i>1</i>\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):466
msgid "Now, when you type <c>p</c>, you should see the following partition information:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):470
msgid "Created boot partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):470
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 30.7 GB, 30750031872 bytes\n141 heads, 63 sectors/track, 6761 cylinders\nUnits = cylinders of 8883 * 512 = 4548096 bytes\n\n   Device Boot      Start         End      Blocks   Id  System\n/dev/sda1  *            1           3       13293   41  PPC PReP Boot\n\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):485
msgid "Creating the Swap Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):488
msgid "Let's now create the swap partition. To do this, type <c>n</c> to create a new partition, then <c>p</c> to tell fdisk that you want a primary partition. Then type <c>2</c> to create the second primary partition, <path>/dev/sda2</path> in our case. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, type <c>+512M</c> to create a partition 512MB in size. After you've done this, type <c>t</c> to set the partition type, <c>2</c> to select the partition you just created and then type in <c>82</c> to set the partition type to \"Linux Swap\". After completing these steps, typing <c>p</c> should display a partition table that looks similar to this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):501
msgid "Partition listing after creating a swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):501
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 30.7 GB, 30750031872 bytes\n141 heads, 63 sectors/track, 6761 cylinders\nUnits = cylinders of 8883 * 512 = 4548096 bytes\n\n   Device Boot      Start         End      Blocks   Id  System\n/dev/sda1               1           3       13293   41  PPC PReP Boot\n/dev/sda2               4         117      506331   82  Linux swap\n\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):518
msgid "Creating the Root Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):521
msgid "Finally, let's create the root partition. To do this, type <c>n</c> to create a new partition, then <c>p</c> to tell fdisk that you want a primary partition. Then type <c>3</c> to create the third primary partition, <path>/dev/sda3</path> in our case. When prompted for the first cylinder, hit enter. When prompted for the last cylinder, hit enter to create a partition that takes up the rest of the remaining space on your disk. After completing these steps, typing <c>p</c> should display a partition table that looks similar to this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):532
msgid "Partition listing after creating the root partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):532
#, no-wrap
msgid "\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 30.7 GB, 30750031872 bytes\n141 heads, 63 sectors/track, 6761 cylinders\nUnits = cylinders of 8883 * 512 = 4548096 bytes\n\n   Device Boot      Start         End      Blocks   Id  System\n/dev/sda1               1           3       13293   41  PPC PReP Boot\n/dev/sda2               4         117      506331   82  Linux swap\n/dev/sda3             118        6761    29509326   83  Linux\n\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):549
msgid "Saving the Partition Layout"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):552
msgid "To save the partition layout and exit <c>fdisk</c>, type <c>w</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):556
msgid "Save and exit fdisk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):556
#, no-wrap
msgid "\nCommand (m for help): <i>w</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):569
msgid "Creating Filesystems"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):571
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):574
msgid "Now that your partitions are created, it is time to place a filesystem on them. If you don't care about what filesystem to choose and are happy with what we use as default in this handbook, continue with <uri link=\"#filesystems-apply\">Applying a Filesystem to a Partition</uri>. Otherwise read on to learn about the available filesystems..."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):590
msgid "Applying a Filesystem to a Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):593
msgid "To create a filesystem on a partition or volume, there are tools available for each possible filesystem:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(th):601
msgid "Creation Command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):604
msgid "ext2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):612
msgid "reiserfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):616
msgid "xfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(ti):620
msgid "jfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):625
msgid "For instance, to have the root partition (<path>/dev/sda4</path> in our example) in ext3 (as in our example), you would use:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):630
msgid "Applying a filesystem on a partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):630
#, no-wrap
msgid "\n# <i>mke2fs -j /dev/sda4</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):634
msgid "Now create the filesystems on your newly created partitions (or logical volumes)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(impo):639
msgid "If you choose to use ReiserFS for <path>/</path>, do not change its default block size if you will also be using <c>yaboot</c> as your bootloader, as explained in <uri link=\"?part=1&amp;chap=10\">Configuring the Bootloader</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):648
msgid "Activating the Swap Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):651
msgid "<c>mkswap</c> is the command that is used to initialize swap partitions:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):655
msgid "Creating a Swap signature"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):655
#, no-wrap
msgid "\n# <i>mkswap /dev/sda3</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):659
msgid "To activate the swap partition, use <c>swapon</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):663
msgid "Activating the swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):663
#, no-wrap
msgid "\n# <i>swapon /dev/sda3</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):667
msgid "Create and activate the swap with the commands mentioned above."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(title):675
msgid "Mounting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):678
msgid "Now that your partitions are initialized and are housing a filesystem, it is time to mount those partitions. Use the <c>mount</c> command. Don't forget to create the necessary mount directories for every partition you created. As an example we create a mount point and mount the root partition:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre:caption):685
msgid "Mounting partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(pre):685
#, no-wrap
msgid "\n# <i>mkdir /mnt/gentoo</i>\n# <i>mount /dev/sda4 /mnt/gentoo</i> \n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(note):690
msgid "If you want your <path>/tmp</path> to reside on a separate partition, be sure to change its permissions after mounting: <c>chmod 1777 /mnt/gentoo/tmp</c>. This also holds for <path>/var/tmp</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(p):696
msgid "Continue with <uri link=\"?part=1&amp;chap=5\">Installing the Gentoo Installation Files</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-ppc64-disk.xml(None):0
msgid "translator-credits"
msgstr ""

