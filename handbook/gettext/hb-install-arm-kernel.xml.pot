msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:40+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(version):11
msgid "9.0"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(date):12
msgid "2008-04-01"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):15
msgid "Timezone"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):18
msgid "You first need to select your timezone so that your system knows where it is located. Look for your timezone in <path>/usr/share/zoneinfo</path>, then copy it to <path>/etc/localtime</path>. Please avoid the <path>/usr/share/zoneinfo/Etc/GMT*</path> timezones as their names do not indicate the expected zones. For instance, <path>GMT-8</path> is in fact GMT+8."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre:caption):27
msgid "Setting the timezone information"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre):27
#, no-wrap
msgid "\n# <i>ls /usr/share/zoneinfo</i>\n<comment>(Suppose you want to use GMT)</comment>\n# <i>cp /usr/share/zoneinfo/GMT /etc/localtime</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):36
msgid "Installing the Sources"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):38
msgid "Choosing a Kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):41
msgid "The core around which all distributions are built is the Linux kernel. It is the layer between the user programs and your system hardware. Gentoo provides its users several possible kernel sources. A full listing with description is available at the <uri link=\"/doc/en/gentoo-kernel.xml\">Gentoo Kernel Guide</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):49
msgid "For ARM systems, we will use <c>gentoo-sources</c> (contains additional patches for extra features)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):54
msgid "Now install it using <c>emerge</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre:caption):58
msgid "Installing a kernel source"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre):58
#, no-wrap
msgid "\n# <i>emerge gentoo-sources</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):62
msgid "When you take a look in <path>/usr/src</path> you should see a symlink called <path>linux</path> pointing to your kernel source. In this case, the installed kernel source points to <c>gentoo-sources-<keyval id=\"kernel-version\"/></c>. Your version may be different, so keep this in mind."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre:caption):69
msgid "Viewing the kernel source symlink"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre):69
#, no-wrap
msgid "\n# <i>ls -l /usr/src/linux</i>\nlrwxrwxrwx    1 root   root    12 Oct 13 11:04 /usr/src/linux -&gt; linux-<keyval id=\"kernel-version\"></keyval>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):78
msgid "Default: Manual Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):80
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):83
msgid "Manually configuring a kernel is often seen as the most difficult procedure a Linux user ever has to perform. Nothing is less true -- after configuring a couple of kernels you don't even remember that it was difficult ;)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):89
msgid "However, one thing <e>is</e> true: you must know your system when you start configuring a kernel manually. Most information can be gathered by emerging pciutils (<c>emerge pciutils</c>) which contains <c>lspci</c>. You will now be able to use <c>lspci</c> within the chrooted environment. You may safely ignore any <e>pcilib</e> warnings (like pcilib: cannot open /sys/bus/pci/devices) that <c>lspci</c> throws out. Alternatively, you can run <c>lspci</c> from a <e>non-chrooted</e> environment. The results are the same. You can also run <c>lsmod</c> to see what kernel modules the Installation CD uses (it might provide you with a nice hint on what to enable)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):101
msgid "Now go to your kernel source directory and execute <c>make menuconfig</c>. This will fire up an ncurses-based configuration menu."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre:caption):106
msgid "Invoking menuconfig"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre):106
#, no-wrap
msgid "\n# <i>cd /usr/src/linux</i>\n# <i>make menuconfig</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):111
msgid "You will be greeted with several configuration sections. We'll first list some options you must activate (otherwise Gentoo will not function, or not function properly without additional tweaks)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):120
msgid "Activating Required Options"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):123
msgid "Due to the highly specific nature of the embedded, we'll cover known configurations for boards here. If your machine is not listed, then you should visit the respective community website to figure out how to properly configure your kernel."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):130
msgid "Please select your machine from the list below to jump to the configuration section."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(uri:link):136
msgid "#netwinder"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(uri):136
msgid "NetWinder"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):143 ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre:caption):153
msgid "NetWinder configuration options"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):146
msgid "Remember that EXT2 support is required for the boot partition as that is the only filesystem that the bootloader can read reliably. Otherwise, the only filesystem that has been tested is EXT3 but your welcome to try your luck with the others ;)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre):153
#, no-wrap
msgid "\n<comment>First generate a default config</comment>\n# <i>make netwinder_defconfig</i>\n\n<comment>Required options</comment>\nSystem Type ---&gt;\n  ARM system type (FootBridge) ---&gt;\n    (X) FootBridge\n  Footbridge Implementations ---&gt;\n    [*] NetWinder\n\nFloating point emulation ---&gt;\n  [*] NWFPE math emulation\n\nFile systems ---&gt;\n  [*] Second extended fs support\n  Pseudo Filesystems ---&gt;\n    [*] /proc file system support\n    [*] Virtual memory file system support (former shm fs)\n\nDevice Drivers ---&gt;\n  ATA/ATAPI/MFM/RLL support ---&gt;\n    [*] ATA/ATAPI/MFM/RLL support\n    [*]   Enhanced IDE/MFM/RLL disk/cdrom/tape/floppy support\n    [*]     Include IDE/ATA-2 DISK support\n    ---     IDE chipset support/bugfixes\n    [*]     PCI IDE chipset support\n    [*]       Winbond SL82c105 support\n    [*]       Generic PCI bus-master DMA support\n\n  Network device support ---&gt;\n    [*] Network device support\n    Ethernet (10 or 100Mbit) ---&gt;\n      [*] Ethernet (10 or 100Mbit)\n      Tulip family network device support ---&gt;\n        [*] \"Tulip\" family network device support\n        [*]   DECchip Tulip (dc2114x) PCI support\n        [*]     Use PCI shared mem for NIC registers\n        [*]     Use NAPI RX polling\n      [*] EISA, VLB, PCI and on board controllers\n      [*]   PCI NE2000 and clones support\n\n  Character devices ---&gt;\n    Serial drivers ---&gt;\n      [*] 8250/16550 and compatible serial support\n      [*]   Console on 8250/16550 and compatible serial port\n      --- Non-8250 serial port support\n      [*] DC21285 serial port support\n      [*]   Console on DC21285 serial port\n    Watchdog Cards ---&gt;\n      [*] Watchdog Timer Support\n      [*]   NetWinder WB83C977 watchdog\n    [*] NetWinder thermometer support\n    [*] NetWinder Button\n    [*]   Reboot Using Button\n\n<comment>Recommended options</comment>\nKernel Features ---&gt;\n  [*] Preemptible Kernel\n  [*] Timer and CPU usage LEDs\n  [*]   CPU usage LED\n\nFile systems ---&gt;\n  [*] Ext3 journalling file system support\n\nDevice Drivers ---&gt;\n  Input device support ---&gt;\n    [*] Keyboards ---&gt;\n      [*] AT keyboard\n    [*] Mouse ---&gt;\n      [*] PS/2 mouse\n\n  Graphics support ---&gt;\n    [*] Support for frame buffer devices\n    [*]   Enable firmware EDID\n    [*]   CyberPro 2000/2010/5000 support\n    Logo configuration ---&gt;\n      [*] Bootup logo\n      [*]   Standard 224-color Linux logo\n\n  Sound ---&gt;\n    [*] Sound card support\n    Open Sound System ---&gt;\n      [*] Open Sound System\n      [*]   OSS sound modules\n      [*]     Yamaha FM synthesizer (YM3812/OPL-3) support\n      [*]     Netwinder WaveArtist\n\n<comment>You should only enable this to upgrade your flash</comment>\nDevice Drivers ---&gt;\n  Character devices ---&gt;\n    [*] NetWinder flash support\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):247
msgid "When you've finished configuring the kernel, continue with <uri link=\"#compiling\">Compiling and Installing</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):255
msgid "Compiling and Installing"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):258
msgid "Now that your kernel is configured, it is time to compile and install it. Exit the configuration and start the compilation process:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre:caption):263
msgid "Compiling the kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre):263
#, no-wrap
msgid "\n# <i>make &amp;&amp; make modules_install</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):268
msgid "When the kernel has finished compiling, copy the kernel image to <path>/boot</path>. Use whatever name you feel is appropriate for your kernel choice and remember it as you will need it later on when you configure your bootloader. Remember to replace <c><keyval id=\"kernel-name\"/></c> with the name and version of your kernel."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre:caption):276
msgid "Installing the kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(pre):276
#, no-wrap
msgid "\n# <i>cp vmlinux.gz /boot/<keyval id=\"kernel-name\"></keyval></i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(p):280
msgid "Now continue with <uri link=\"#kernel_modules\">Kernel Modules</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(title):287
msgid "Kernel Modules"
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-arm-kernel.xml(None):0
msgid "translator-credits"
msgstr ""

