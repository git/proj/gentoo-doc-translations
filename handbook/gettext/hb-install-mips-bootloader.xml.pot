msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-05-18 19:19+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(abstract):11
msgid "On both Silicon Graphics machines, and on Cobalt servers, both require the use of a bootloader to load the kernel. This section covers setting up arcboot/arcload (for SGI machines) and colo for Cobalt servers."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(version):17
msgid "11"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(date):18
msgid "2011-05-09"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):21
msgid "Silicon Graphics Machines -- Setting Up arcload"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):23
msgid "Which one?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):26
msgid "On SGI machines, we use the <c>arcload</c> boot loader. In previous releases, we also provided <c>arcboot</c>, however it has been officially declared obsolete, in favour of <c>arcload</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):32
msgid "The SGI volume header filenames are limited to 8 characters, and there may be no more than 16 files contained in a single volume header."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):41
msgid "Installing arcload"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):44
msgid "<c>arcload</c> was written for machines that require 64-bit kernels, and therefore can't use <c>arcboot</c> (which can't easily be compiled as a 64-bit binary). It also works around peculiarities that arise when loading kernels directly from the volume header. So, now you know what this is about, we can proceed with the installation:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):52
msgid "Merging arcload and dvhtool"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):52
#, no-wrap
msgid "\n# <i>emerge arcload dvhtool</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):56
msgid "Once this has finished, you should find the <c>arcload</c> binary in <path>/usr/lib/arcload</path>. Now, two files exist:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(li):62
msgid "<c>sashARCS</c>: The 32-bit binary for Indy, Indigo2 (R4k), Challenge S and O2 systems"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(li):66
msgid "<c>sash64</c>: The 64-bit binary for Octane/Octane2, Origin 200/2000 and Indigo2 Impact systems"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):72
msgid "Use <c>dvhtool</c> to install the appropriate binary for your system into the volume header:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):77
msgid "Placing arcload in the volume header"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):77
#, no-wrap
msgid "\n<comment>(Indy/Indigo2/Challenge S/O2 users)</comment>\n# <i>dvhtool --unix-to-vh /usr/lib/arcload/sashARCS sashARCS</i>\n\n<comment>(Indigo2 Impact/Octane/Octane2/Origin 200/Origin 2000 users)</comment>\n# <i>dvhtool --unix-to-vh /usr/lib/arcload/sash64 sash64</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):85
msgid "You don't have to use the name <c>sashARCS</c> or <c>sash64</c>, unless you are installing to the volume header of a bootable CD. For normal boot from hard-disk, you may name them something else if you wish."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):91
msgid "Now just use <c>dvhtool</c> to verify they are in the volume header."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):95
msgid "Checking arcload is present in the volume header"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):95
#, no-wrap
msgid "\n# <i>dvhtool --print-volume-directory</i>\n----- directory entries -----\nEntry #0, name \"sash64\", start 4, bytes 55859\n#\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):102
msgid "Now, the <c>arc.cf</c> file has a C-like syntax. For the full detail on how one configures it, see the <uri link=\"http://www.linux-mips.org/wiki/Arcload\">arcload page on the Linux/MIPS wiki</uri>. In short, you define a number of options, which you enable and disable at boot time using the <c>OSLoadFilename</c> variable."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):110
msgid "An example arc.cf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):110
#, no-wrap
msgid "\n<comment># ARCLoad Configuration</comment>\n\n<comment># Some default settings...</comment>\nappend  \"root=/dev/sda3\";\nappend  \"ro\";\nappend  \"console=ttyS0,9600\";\n\n<comment># Our main definition. ip28 may be changed if you wish.</comment>\nip28 {\n        <comment># Definition for a \"working\" kernel</comment>\n        <comment># Select this by setting OSLoadFilename=\"ip28(working)\"</comment>\n        working {\n                description     \"SGI Indigo2 Impact R10000\\n\\r\";\n                image system    \"/working\";\n        }\n\n        <comment># Definition for a \"new\" kernel</comment>\n        <comment># Select this by setting OSLoadFilename=\"ip28(new)\"</comment>\n        new {\n                description     \"SGI Indigo2 Impact R10000 - Testing Kernel\\n\\r\";\n                image system    \"/new\";\n        }\n\n        <comment># For debugging a kernel</comment>\n        <comment># Select this by setting OSLoadFilename=\"ip28(working,debug)\"</comment>\n        <comment># or OSLoadFilename=\"ip28(new,debug)\"</comment>\n        debug {\n                description     \"Debug console\";\n                append          \"init=/bin/bash\";\n        }\n}\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):144
msgid "Starting with <c>arcload-0.5</c>, <path>arc.cf</path> and kernels may reside either in the volume header, or on a partition. If you wish to utilise this newer feature, you may instead place the files in your <path>/boot</path> partition (or <path>/</path> if your boot partition is not separate). <c>arcload</c> uses the filesystem driver code from the popular <c>grub</c> bootloader, and thus supports the same range of filesystems."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):153
msgid "Placing arc.cf and kernel in the volume header"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):153
#, no-wrap
msgid "\n# <i>dvhtool --unix-to-vh arc.cf arc.cf</i>\n# <i>dvhtool --unix-to-vh /usr/src/linux/vmlinux new</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):158
msgid "With this done, now all that's left is to set some options in the PROM. See the section on <uri link=\"#reboot\">Rebooting the System</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):169
msgid "Cobalt MicroServers -- Setting Up CoLo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):171
msgid "Installing CoLo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):174
msgid "On Cobalt servers, these machines have a much less capable firmware installed on chip. The Cobalt BOOTROM is primitive, by comparison to the SGI PROM, and has a number of serious limitations."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(li):181
msgid "There's a 675kB (approximate) limit on kernels. The current size of Linux 2.4 makes it nearly impossible to make a kernel this size. Linux 2.6 is totally out of the question."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(li):186
msgid "64-bit kernels are not supported by the stock firmware (although these are highly experimental on Cobalt machines at this time)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(li):190
msgid "The shell is basic at best"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):195
msgid "To overcome these limitations, an alternative firmware, called <uri link=\"http://www.colonel-panic.org/cobalt-mips/\">CoLo</uri> (Cobalt Loader) was developed. This is a BOOTROM image that can either be flashed into the chip inside the Cobalt server, or loaded from the existing firmware."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):202
msgid "This guide will take you through setting up CoLo so that it is loaded by the stock firmware. This is the only truly safe, and recommended way to set up CoLo."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(warn):208
msgid "You may, if you wish, flash it into the server, and totally replace the original firmware -- however, you are entirely on your own in that endeavour. Should anything go wrong, you will need to physically remove the BOOTROM and reprogram it yourself with the stock firmware. If you are not sure how to do this -- then <e>DO NOT</e> flash your machine. We take no responsibility for whatever happens if you ignore this advice."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):217
msgid "Okay, with the warnings over now, we'll get on with installing CoLo. First, start by emerging the package."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):222
msgid "Emerging colo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):222
#, no-wrap
msgid "\n# <i>emerge colo</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):226
msgid "With that installed (I hope you read those messages ;-) you should be able to look inside the <path>/usr/lib/colo</path> directory to find two files, <path>colo-chain.elf</path>: the \"kernel\" for the stock firmware to load, and <path>colo-rom-image.bin</path>: a ROM image for flashing into the BOOTROM. We start by mounting /boot and dumping a compressed copy of <path>colo-chain.elf</path> in <path>/boot</path> where the system expects it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):235
msgid "Putting CoLo in its place"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):235
#, no-wrap
msgid "\n# <i>gzip -9vc /usr/lib/colo/colo-chain.elf &gt; /boot/vmlinux.gz</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):243
msgid "Configuring CoLo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):247
msgid "Now, when the system first boots up, it'll load CoLo which will spit up a menu on the back LCD. The first option (and default that is assumed after roughly 5 seconds) is to boot to the hard disk. The system would then attempt to mount the first Linux partition it finds, and run the script <path>default.colo</path>. The syntax is fully documented in the CoLo documentation (have a peek at <path>/usr/share/doc/colo-X.YY/README.shell.gz</path> -- where X.YY is the version installed), and is very simple."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):258
msgid "Just a tip: when installing kernels, I usually create two kernel images, <path>kernel.gz.working</path> -- a known working kernel, and <path>kernel.gz.new</path> -- a kernel that's just been compiled. You can either use symlinks to point to the curent \"new\" and \"working\" kernels, or just rename the kernel images."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):266
msgid "A basic default.colo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):266
#, no-wrap
msgid "\n<comment>#:CoLo:#</comment>\nmount hda1\nload /kernel.gz.working\nexecute root=/dev/sda3 ro console=ttyS0,115200\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):273
msgid "CoLo will refuse to load a script that does not begin with the <c>#:CoLo:#</c> line. Think of it as the equivalent of saying <c>#!/bin/sh</c> in shell scripts."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):279
msgid "It is also possible to ask a question, such as which kernel &amp; configuration you'd like to boot, with a default timeout. This configuration does exactly this, asks the user which kernel they wish to use, and executes the chosen image. <path>vmlinux.gz.new</path> and <path>vmlinux.gz.working</path> may be actual kernel images, or just symlinks pointing to the kernel images on that disk. The <c>50</c> argument to <c>select</c> specifies that it should proceed with the first option (\"Working\") after 50/10 seconds."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):289
msgid "Menu-based configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):289
#, no-wrap
msgid "\n<comment>#:CoLo:#</comment>\n\nlcd \"Mounting hda1\"\nmount hda1\nselect \"Which Kernel?\" 50 Working New\n\ngoto {menu-option}\nvar image-name vmlinux.gz.working\ngoto 3f\n@var image-name vmlinux.gz.working\ngoto 2f\n@var image-name vmlinux.gz.new\n\n@lcd \"Loading Linux\" {image-name}\nload /{image-name}\nlcd \"Booting...\"\nexecute root=/dev/sda5 ro console=ttyS0,115200\nboot\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):310
msgid "See the documentation in <path>/usr/share/doc/colo-VERSION</path> for more details."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):321
msgid "Setting up for Serial Console"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):326
msgid "Okay, the Linux installation as it stands now, would boot fine, but assumes you're going to be logged in at a physical terminal. On Cobalt machines, this is particularly bad -- there's no such thing as a physical terminal."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):332
msgid "Those who do have the luxury of a supported video chipset may skip this section if they wish."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):337
msgid "First, pull up an editor and hack away at <path>/etc/inittab</path>. Further down in the file, you'll see something like this:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):342
msgid "inittab Configuration"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):342
#, no-wrap
msgid "\n<comment># SERIAL CONSOLE</comment>\n<comment>#c0:12345:respawn:/sbin/agetty 9600 ttyS0 vt102</comment>\n\n<comment># TERMINALS</comment>\nc1:12345:respawn:/sbin/agetty 38400 tty1 linux\nc2:12345:respawn:/sbin/agetty 38400 tty2 linux\nc3:12345:respawn:/sbin/agetty 38400 tty3 linux\nc4:12345:respawn:/sbin/agetty 38400 tty4 linux\nc5:12345:respawn:/sbin/agetty 38400 tty5 linux\nc6:12345:respawn:/sbin/agetty 38400 tty6 linux\n\n<comment># What to do at the \"Three Finger Salute\".</comment>\nca:12345:ctrlaltdel:/sbin/shutdown -r now\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):358
msgid "First, uncomment the <c>c0</c> line. By default, it's set to use a terminal baud rate of 9600 bps. On Cobalt servers, you may want to change this to 115200 to match the baud rate decided by the BOOT ROM. This is how that section looks on my machine. On a headless machine (e.g. Cobalt servers), I'll also recommend commenting out the local terminal lines (<c>c1</c> through to <c>c6</c>) as these have a habit of misbehaving when they can't open <path>/dev/ttyX</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):367
msgid "Example snippet from inittab"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):367
#, no-wrap
msgid "\n<comment># SERIAL CONSOLE</comment>\nc0:12345:respawn:/sbin/agetty 115200 ttyS0 vt102\n\n<comment># TERMINALS -- These are useless on a headless qube</comment>\n<comment>#c1:12345:respawn:/sbin/agetty 38400 tty1 linux</comment>\n<comment>#c2:12345:respawn:/sbin/agetty 38400 tty2 linux</comment>\n<comment>#c3:12345:respawn:/sbin/agetty 38400 tty3 linux</comment>\n<comment>#c4:12345:respawn:/sbin/agetty 38400 tty4 linux</comment>\n<comment>#c5:12345:respawn:/sbin/agetty 38400 tty5 linux</comment>\n<comment>#c6:12345:respawn:/sbin/agetty 38400 tty6 linux</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):380
msgid "Now, lastly... we have to tell the system, that the local serial port can be trusted as a secure terminal. The file we need to poke at is <path>/etc/securetty</path>. It contains a list of terminals that the system trusts. We simply stick in two more lines, permitting the serial line to be used for <c>root</c> logins."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):388
msgid "Enabling root logins on serial console"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):388
#, no-wrap
msgid "\n<comment>(/dev/ttyS0 -- the traditional name for the first serial port)</comment>\n# <i>echo 'ttyS0' &gt;&gt; /etc/securetty</i>\n\n<comment>(Lately, Linux also calls this /dev/tts/0 -- so we add this\ntoo)</comment>\n# <i>echo 'tts/0' &gt;&gt; /etc/securetty</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):402
msgid "Rebooting the System"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):406
msgid "Exit the chrooted environment and unmount all mounted partitions. Then type in that one magical command you have been waiting for: <c>reboot</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):411
msgid "Exiting the chroot, unmounting all partitions and rebooting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):411
#, no-wrap
msgid "\n# <i>exit</i>\ncdimage ~# <i>cd</i>\ncdimage ~# <i>umount -l /mnt/gentoo/dev{/shm,/pts,}</i>\ncdimage ~# <i>umount -l /mnt/gentoo{/boot,/proc,}</i>\ncdimage ~# <i>reboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):419
msgid "<e>Cobalt Users:</e> The rest of this section covers the setting up of the SGI PROM so that it boots <c>arcload</c> off disk and loads Linux. This is not applicable to the setup of Cobalt servers. In fact, all your work is done -- there is no configuration needed for the first boot up, you can skip to the next section: <uri link=\"?part=1&amp;chap=11\">Finalising your Gentoo Installation</uri>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):433
msgid "Tweaking the SGI PROM"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):435
msgid "Setting generic PROM settings"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):438
msgid "Now that you've installed the bootloader, you're ready to reboot the machine."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):442
msgid "Rebooting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):442
#, no-wrap
msgid "\n<comment>(Exit the chroot environment)</comment>\n# <i>exit</i>\n\n<comment>(Unmount the drives)</comment>\ncdimage ~# <i>umount -l /mnt/gentoo/dev{/shm,/pts,}</i>\ncdimage ~# <i>umount -l /mnt/gentoo{/boot,/proc,}</i>\n\n<comment>(Reboot)</comment>\n# <i>reboot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):454
msgid "When you are rebooted, go to the <e>System Maintenance Menu</e> and select <e>Enter Command Monitor</e> (<c>5</c>) like you did when you netbooted the machine."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):460
msgid "Configuring the PROM to Boot Gentoo"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):460
#, no-wrap
msgid "\n1) Start System\n2) Install System Software\n3) Run Diagnostics\n4) Recover System\n5) Enter Command Monitor\n\nOption? <i>5</i>\nCommand Monitor. Type \"exit\" to return to the menu.\n\n<comment>(Set some options for arcload)</comment>\n\n<comment>(Provide the location of the Volume Header)</comment>\n&gt;&gt; <i>setenv SystemPartition scsi(0)disk(1)rdisk(0)partition(8)</i>\n\n<comment>(Automatically boot Gentoo)</comment>\n&gt;&gt; <i>setenv AutoLoad Yes</i>\n\n<comment>(Set the timezone)</comment>\n&gt;&gt; <i>setenv TimeZone EST5EDT</i>\n\n<comment>(Use the serial console - graphic adapter users should have \"g\" instead of \"d1\" (one))</comment>\n&gt;&gt; <i>setenv console d1</i>\n\n<comment>(Setting the serial console baud rate. This is optional, 9600 is the          )\n(default setting, although one may use rates up to 38400 if that is desired.  )</comment>\n&gt;&gt; <i>setenv dbaud 9600</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):489
msgid "Now, the next settings depend on how you are booting the system."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):497
msgid "Settings for direct volume-header booting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):500
msgid "This is covered here for completeness. It's recommended that users look into installing <c>arcload</c> instead."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):505
msgid "This only works on the Indy, Indigo2 (R4k) and Challenge S."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):509
msgid "PROM settings for booting off the volume header"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):509
#, no-wrap
msgid "\n<comment>(&lt;root device&gt; = Gentoo's root partition, e.g. /dev/sda3)</comment>\n&gt;&gt; <i>setenv OSLoadPartition &lt;root device&gt;</i>\n\n<comment>(To list the available kernels, type \"ls\")</comment>\n&gt;&gt; <i>setenv OSLoader &lt;kernel name&gt;</i>\n&gt;&gt; <i>setenv OSLoadFilename &lt;kernel name&gt;</i>\n\n<comment>(Declare the kernel parameters you want to pass)</comment>\n&gt;&gt; <i>setenv OSLoadOptions &lt;kernel parameters&gt;</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):521
msgid "If you wish to try a kernel without messing with kernel parameters, you may do so using the <c>boot -f</c> PROM command:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):526
msgid "Booting without changing environment variables"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):526
#, no-wrap
msgid "\n<comment>(Booting a kernel, \"new\", with additional options)</comment>\n# <i>boot -f new root=/dev/sda3 ro</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):535
msgid "Settings for arcload"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):538
msgid "<c>arcload</c> uses the <c>OSLoadFilename</c> option to specify which options to set from <path>arc.cf</path>. The configuration file is essentially a script, with the top-level blocks defining boot images for different systems, and inside that, optional settings. Thus, setting <c>OSLoadFilename=mysys(serial)</c> pulls in the settings for the <c>mysys</c> block, then sets further options overridden in <c>serial</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):547
msgid "In the example file above, we have one system block defined, <c>ip28</c> with <c>working</c>, <c>new</c> and <c>debug</c> options available. We define our PROM variables as so:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):553
msgid "PROM settings for using arcload"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):553
#, no-wrap
msgid "\n<comment>(Select arcload as the bootloader:- sash64 or sashARCS)</comment>\n&gt;&gt; <i>setenv OSLoader sash64</i>\n\n<comment>(Use the \"working\" kernel image, defined in \"ip28\" section of arc.cf)</comment>\n&gt;&gt; <i>setenv OSLoadFilename ip28(working)</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):561
msgid "Starting with <c>arcload-0.5</c>, files no longer need to be placed in the volume header -- they may be placed in a partition instead. To tell <c>arcload</c> where to look for its configuration file and kernels, one must set the <c>OSLoadPartition</c> PROM variable. The exact value here will depend on where your disk resides on the SCSI bus. Use the <c>SystemPartition</c> PROM variable as a guide -- only the partition number should need to change."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(note):570
msgid "Partitions are numbered starting at 0, not 1 as is the case in Linux."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre:caption):574
msgid "Telling arcload where to find arc.cf"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(pre):574
#, no-wrap
msgid "\n<comment>(If you wish to load from the volume header -- use partition 8)</comment>\n&gt;&gt; <i>setenv OSLoadPartition scsi(0)disk(1)rdisk(0)partition(8)</i>\n\n<comment>(Otherwise, specify the partition and filesystem type)</comment>\n&gt;&gt; <i>setenv OSLoadPartition scsi(0)disk(1)rdisk(0)partition(0)[ext2]</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(title):586
msgid "All Done"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(p):589
msgid "Now you're ready to enjoy Gentoo! Boot up your Gentoo installation and finish up with <uri link=\"?part=1&amp;chap=11\">Finalizing your Gentoo Installation</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-bootloader.xml(None):0
msgid "translator-credits"
msgstr ""

