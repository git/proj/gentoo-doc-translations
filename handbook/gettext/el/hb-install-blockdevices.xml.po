#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:40+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-blockdevices.xml(version):7
msgid "2"
msgstr "2"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-blockdevices.xml(date):8
msgid "2008-05-02"
msgstr "2008-05-02"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-blockdevices.xml(title):11
msgid "Block Devices"
msgstr "Συσκευές Αποθήκευσης"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-blockdevices.xml(p):14
msgid ""
"We'll take a good look at disk-oriented aspects of Gentoo Linux and Linux in "
"general, including Linux filesystems, partitions and block devices. Then, "
"once you're familiar with the ins and outs of disks and filesystems, you'll "
"be guided through the process of setting up partitions and filesystems for "
"your Gentoo Linux installation."
msgstr ""
"Ρίχνουμε πρώτα μια προσεκτική ματιά στις προσανατολισμένες στο δίσκο πτυχές "
"του Gentoo Linux και του Linux γενικότερα, συμπεριλαμβανομένων των "
"συστημάτων αρχείων του Linux, τις κατατμήσεις και τις συσκευές αποθήκευσης. "
"Κατόπιν, από τη στιγμή που εξοικιωθείτε με τα χαρακτηριστικά των δίσκων και "
"των συστημάτων αρχείων, θα καθοδηγηθείτε μέσα απ' τη διαδικασία της "
"δημιουργίας των κατατμήσεων και των συστημάτων αρχείων για την εγκατάσταση "
"του δικού σας Gentoo Linux."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-blockdevices.xml(p):22
msgid ""
"To begin, we'll introduce <e>block devices</e>. The most famous block device "
"is probably the one that represents the first drive in a Linux system, "
"namely <path>/dev/sda</path>. SCSI and Serial ATA drives are both labeled "
"<path>/dev/sd*</path>; even IDE drives are labeled <path>/dev/sd*</path> "
"with the new libata framework in the kernel. If you're using the old device "
"framework, then your first IDE drive is <path>/dev/hda</path>."
msgstr ""
"Ξεκινώντας, θα σας παρουσιάσουμε τις <e>συσκευές αποθήκευσης</e>. Η πιο "
"γνωστή συσκευή αποθήκευσης είναι προφανώς αυτή που αντιπροσωπεύει το πρώτο "
"drive σ' ένα σύστημα Linux και ονομάζεται <path>/dev/sda</path>. Οι οδηγοί "
"SCSI και Serial ATA ονομάζονται <path>/dev/sd*</path>, επίσης οι IDE drives "
"ονομάζονται <path>/dev/sd*</path> με το νέο πλαίσιο (framework) libata στον "
"πυρήνα. Εάν χρησιμοποιήτε την παλαιά συσκευή framework, τότε ο πρώτος σας "
"οδηγός IDE είναι ο <path>/dev/hda</path>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-blockdevices.xml(p):31
msgid ""
"The block devices above represent an abstract interface to the disk. User "
"programs can use these block devices to interact with your disk without "
"worrying about whether your drives are IDE, SCSI or something else. The "
"program can simply address the storage on the disk as a bunch of contiguous, "
"randomly-accessible 512-byte blocks."
msgstr ""
"Οι παραπάνω συσκευές αποθήκευσης αντιπροσωπεύουν μια αφηρημένη διεπαφή στον "
"δίσκο. Τα προγράμματα χρήσης μπορούν να χρησιμοποιήσουν αυτές τις συσκευές "
"αποθήκευσης για να αλληλεπιδράσουν με τον δίσκο σας χωρίς ν' ανησυχείτε αν "
"οι οδηγοί σας είναι IDE, SCSI ή κάτι άλλο. Το πρόγραμμα μπορεί απλά να "
"χειριστεί την αποθήκευση στο δίσκο ως μια δέσμη συναφών, τυχαίως προσιτών "
"512-byte blocks."

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-blockdevices.xml(None):0
msgid "translator-credits"
msgstr ""
