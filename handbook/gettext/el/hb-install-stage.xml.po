# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-09-05 14:12+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(abstract):11
msgid "Gentoo installs work through a stage3 archive. In this chapter we describe how you extract the stage3 archive and configure Portage."
msgstr "Οι εγκαταστάσεις του Gentoo δουλεύουν με ένα αρχείο stage3. Σε αυτό το κεφάλαιο περιγράφουμε πως αποσυμπιέζετε ένα αρχείο stage3 και πως να ρυθμίσετε το Portage."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(version):16
msgid "11"
msgstr "11"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(date):17
msgid "2011-08-03"
msgstr "2011-08-03"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):20
msgid "Installing a Stage Tarball"
msgstr "Εγκαθιστώντας ένα Stage Tarball"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):22
msgid "Setting the Date/Time Right"
msgstr "Ρυθμίζοντας την Ημερομηνία/Ώρα Σωστά"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):25
msgid "Before you continue you need to check your date/time and update it. A misconfigured clock may lead to strange results in the future!"
msgstr "Πριν συνεχίσετε πρέπει να ελέγξετε την ημερομηνία/ώρα και να την ενημερώσετε. Ένα ρολόι που δεν έχει ρυθμιστεί σωστά μπορεί να οδηγήσει σε περίεργα αποτελέσματα στο μέλλον!"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):30
msgid "To verify the current date/time, run <c>date</c>:"
msgstr "Για να επιβεβαιώσετε την τρέχουσα ημερομηνία/ώρα, εκτελέστε <c>date</c>:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):34
msgid "Verifying the date/time"
msgstr "Επιβεβαίωση ημερομηνίας/ώρας"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):34
#, no-wrap
msgid ""
"\n"
"# <i>date</i>\n"
"Fri Mar 29 16:21:18 UTC 2005\n"
msgstr ""
"\n"
"# <i>date</i>\n"
"Fri Mar 29 16:21:18 UTC 2005\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):39
msgid "If the date/time displayed is wrong, update it using the <c>date MMDDhhmmYYYY</c> syntax (<b>M</b>onth, <b>D</b>ay, <b>h</b>our, <b>m</b>inute and <b>Y</b>ear). At this stage, you should use UTC time. You will be able to define your timezone later on. For instance, to set the date to March 29th, 16:21 in the year 2005:"
msgstr "Αν η ημερομηνία/ώρα που εμφανίζεται είναι λάθος, ενημερώστε τα χρησιμοποιώντας την <c>date ΜΜΗΗωωλλΕΕΕΕ</c> σύνταξη (<b>Μ</b>ήνας, <b>Η</b>μέρα, <b>ώ</b>ρα, <b>λ</b>επτά και <b>Έ</b>τος). Σε αυτό το σημείο, θα πρέπει να χρησιμοποιείτε ώρα UTC. Θα μπορείτε να ορίσετε την ώρα ζώνης σας αργότερα. Για παράδειγμα, για να ορίσουμε την ημερομηνία σε 29 Μαρτίου, 16:21 του έτους 2005:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):47
msgid "Setting the UTC date/time"
msgstr "Ορίζοντας την ημερομηνία/ώρα σε UTC"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):47
#, no-wrap
msgid ""
"\n"
"# <i>date 032916212005</i>\n"
msgstr ""
"\n"
"# <i>date 032916212005</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):54
msgid "Making your Choice"
msgstr "Κάνοντας Επιλογές"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):57
msgid "The next step you need to perform is to install the <e>stage3</e> tarball onto your system. You have the option of downloading the required tarball from the Internet or, if you booted one of the Gentoo Universal CDs, copy it over from the disc itself. In most cases, the command <c>uname -m</c> can be used to help you decide which stage file to download."
msgstr "Στο επόμενο βήμα θα χρειαστεί να εγκαταστήσετε το <e>stage3</e> tarball στο σύστημά σας. Έχετε την επιλογή να κάνετε λήψη του απαιτούμενου tarball από το Διαδίκτυο ή, αν έχετε κάνει εκκίνηση από ένα από τα Universal CD του Gentoo, να το αντιγράψτε από το δίσκο. Στις περισσότερες περιπτώσεις, η εντολή <c>uname -m</c> μπορεί να χρησιμοποιηθεί για να σας βοηθήσει να αποφασίσετε ποιο αρχείο stage να κάνετε λήψη."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):65
msgid "Minimal CDs and LiveDVDs do not contain any stage3 archive."
msgstr "Τα CD Ελάχιστης Εγκατάστασης και τα LiveDVD δεν περιέχουν αρχεία stage3."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(uri:link):70
msgid "#doc_chap2"
msgstr "#doc_chap2"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(uri):70
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):80
msgid "Default: Using a Stage from the Internet"
msgstr "Προκαθορισμένο: Χρησιμοποιώντας ένα Stage από το Διαδίκτυο"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(uri:link):72
msgid "#doc_chap3"
msgstr "#doc_chap3"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(uri):72
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):198
msgid "Alternative: Using a Stage from the Universal CD"
msgstr "Εναλλακτικά: Χρησιμοποιώντας ένα Stage από το Universal CD"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):82
msgid "Downloading the Stage Tarball"
msgstr "Λαμβάνοντας το Stage Tarball"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):85
msgid "Go to the Gentoo mountpoint at which you mounted your filesystems (most likely <path>/mnt/gentoo</path>):"
msgstr "Πηγαίνετε στο σημείο προσάρτησης του Gentoo στο οποίο προσαρτήσατε τα συστήματα αρχείων σας (το πιο πιθανό στο <path>/mnt/gentoo</path>):"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):90
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):274
msgid "Going to the Gentoo mountpoint"
msgstr "Πηγαίνοντας στο σημείο προσάρτησης του Gentoo"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):90
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):227
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):274
#, no-wrap
msgid ""
"\n"
"# <i>cd /mnt/gentoo</i>\n"
msgstr ""
"\n"
"# <i>cd /mnt/gentoo</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):94
msgid "Depending on your installation medium, you have a couple of tools available to download a stage. If you have <c>links</c> available, then you can immediately surf to <uri link=\"/main/en/mirrors.xml\">the Gentoo mirrorlist</uri> and choose a mirror close to you: type <c>links http://www.gentoo.org/main/en/mirrors.xml</c> and press enter."
msgstr "Ανάλογα με το μέσο εγκατάστασής σας, έχετε ορισμένα εργαλεία διαθέσιμα για να λάβετε ένα stage. Αν έχετε το <c>links</c> διαθέσιμο, τότε μπορείτε να πλοηγηθείτε άμεσα στη <uri link=\"/main/en/mirrors.xml\">λίστα mirror του Gentoo</uri> και να επιλέξετε ένα mirror κοντά σε εσάς: πληκτρολογήστε <c>links http://www.gentoo.org/main/en/mirrors.xml</c> και πατήστε enter."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):102
msgid "If you don't have <c>links</c> available you should have <c>lynx</c> at your disposal. If you need to go through a proxy, export the <c>http_proxy</c> and <c>ftp_proxy</c> variables:"
msgstr "Αν δεν έχετε διαθέσιμο το <c>links</c> θα πρέπει να έχετε το <c>lynx</c> στη διάθεσή σας. Αν πρέπει να περάσετε από κάποιον διαμεσολαβητή, εξάγετε τις μεταβλητές <c>http_proxy</c> και <c>ftp_proxy</c>:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):108
msgid "Setting proxy information for lynx"
msgstr "Ρυθμίζοντας τις πληροφορίες του διαμεσολαβητή για το lynx"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):108
#, no-wrap
msgid ""
"\n"
"# <i>export http_proxy=\"http://proxy.server.com:port\"</i>\n"
"# <i>export ftp_proxy=\"http://proxy.server.com:port\"</i>\n"
msgstr ""
"\n"
"# <i>export http_proxy=\"http://proxy.server.com:port\"</i>\n"
"# <i>export ftp_proxy=\"http://proxy.server.com:port\"</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):113
msgid "We will now assume that you have <c>links</c> at your disposal."
msgstr "Θα θεωρήσουμε για τώρα ότι έχετε το <c>links</c> στη διάθεσή σας."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):117
msgid "Select a mirror closeby. Usually HTTP mirrors suffice, but other protocols are available as well. Move to the <path><keyval id=\"release-dir\"/></path> directory. There you should see all available stage files for your architecture (they might be stored within subdirectories named after the individual subarchitectures). Select one and press <c>D</c> to download. When you're finished, press <c>Q</c> to quit the browser."
msgstr "Επιλέξτε ένα κοντινό mirror. Συνήθως τα HTTP mirrors αρκούν, αλλά υπάρχουν διαθέσιμα και για άλλα πρωτόκολλα. Μετακινηθείτε στον κατάλογο <path><keyval id=\"release-dir\"/></path>. Εκεί θα πρέπει να δείτε όλα τα διαθέσιμα αρχεία stage για την αρχιτεκτονική σας (μπορεί να είναι αποθηκευμένα σε υποκαταλόγους με ονόματα ανάλογα με την κάθε αρχιτεκτονική). Επιλέξτε ένα και πατήστε <c>D</c> για να το λάβετε. Όταν ολοκληρωθεί η λήψη, πατήστε <c>Q</c> για να βγείτε από τον πλοηγό."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):126
msgid "Most PC users should use the <b><keyval id=\"stage3\"/></b> stage3 archive. All modern PCs are considered i686. If you use an old machine, you can check the <uri link=\"http://en.wikipedia.org/wiki/I686\">list of i686-compatible processors</uri> on Wikipedia. Old processors such as the Pentium, K5, K6, or Via C3 and similar require the more generic <b>x86</b> stage3. Processors older than <b>i486</b> are not supported."
msgstr "Οι περισσότεροι χρήστες PC θα πρέπει να χρησιμοποιήσουν το αρχείο <b><keyval id=\"stage3\"/></b> stage3. Όλα τα σύγχρονα PC θεωρούνται i686. Αν χρησιμοποιείτε ένα παλιό μηχάνημα, μπορείτε να ελέγξετε την <uri link=\"http://en.wikipedia.org/wiki/I686\">λίστα των συμβατών με i686 επεξεργαστών</uri> στη Wikipedia. Παλιοί επεξεργαστές όπως οι Pentium, οι K5, οι K6, ή οι Via C3 και οι παρόμοιοι απαιτούν το πιο γενικό <b>x86</b> stage3. Επεξεργαστές παλιότεροι από τους <b>i486</b> δεν υποστηρίζονται."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):135
msgid "Surfing to the mirror listing with links"
msgstr "Πλοήγηση στη λίστα των mirror με το links"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):135
#, no-wrap
msgid ""
"\n"
"# <i>links http://www.gentoo.org/main/en/mirrors.xml</i>\n"
"\n"
"<comment>(If you need proxy support with links:)</comment>\n"
"# <i>links -http-proxy proxy.server.com:8080 http://www.gentoo.org/main/en/mirrors.xml</i>\n"
msgstr ""
"\n"
"# <i>links http://www.gentoo.org/main/en/mirrors.xml</i>\n"
"\n"
"<comment>(Αν χρειάζεστε υποστήριξη για διαμεσολαβητή με το links:)</comment>\n"
"# <i>links -http-proxy proxy.server.com:8080 http://www.gentoo.org/main/en/mirrors.xml</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):142
msgid "Make sure you download a <b>stage3</b> tarball - installations using a stage1 or stage2 tarball are not supported anymore."
msgstr "Σιγουρευτείτε ότι λάβατε ένα <b>stage3</b> tarball - εγκαταστάσεις χρησιμοποιώντας ένα stage1 ή stage2 tarball δεν υποστηρίζονται πλέον."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):147
msgid "If you want to check the integrity of the downloaded stage tarball, use <c>md5sum</c> and compare the output with the MD5 checksum provided on the mirror."
msgstr "Αν θέλετε να ελέγξετε την ακεραιότητα του ληφθέντος stage tarball, χρησιμοποιήστε το <c>md5sum</c> και συγκρίνετε το αποτέλεσμα με το MD5 checksum που παρέχεται στο mirror."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):153
msgid "Checking integrity of a stage tarball"
msgstr "Ελέγχοντας την ακεραιότητα ενός stage tarball"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):153
#, no-wrap
msgid ""
"\n"
"# <i>md5sum -c <keyval id=\"stage3\"></keyval>.DIGESTS</i>\n"
"<keyval id=\"stage3\"></keyval>: OK\n"
msgstr ""
"\n"
"# <i>md5sum -c <keyval id=\"stage3\"></keyval>.DIGESTS</i>\n"
"<keyval id=\"stage3\"></keyval>: OK\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):161
msgid "Unpacking the Stage Tarball"
msgstr "Αποσυμπιέζοντας το Stage Tarball"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):164
msgid "Now unpack your downloaded stage onto your system. We use <c>tar</c> to proceed as it is the easiest method:"
msgstr "Τώρα αποσυμπιέστε το ληφθέν stage στο σύστημά σας. Χρησιμοποιούμε το <c>tar</c> για να συνεχίσουμε καθώς είναι η πιο εύκολη μέθοδος:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):169
msgid "Unpacking the stage"
msgstr "Αποσυμπιέζοντας το stage"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):169
#, no-wrap
msgid ""
"\n"
"# <i>tar xvjpf stage3-*.tar.bz2</i>\n"
msgstr ""
"\n"
"# <i>tar xvjpf stage3-*.tar.bz2</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):173
msgid "Make sure that you use the same options (<c>xvjpf</c>). The <c>x</c> stands for <e>Extract</e>, the <c>v</c> for <e>Verbose</e> to see what happens during the extraction process (optional), the <c>j</c> for <e>Decompress with bzip2</e>, the <c>p</c> for <e>Preserve permissions</e> and the <c>f</c> to denote that we want to extract a file, not standard input."
msgstr "Σιγουρευτείτε ότι χρησιμοποιήσατε τις ίδιες επιλογές (<c>xvjpf</c>). Το <c>x</c> συμβολίζει την <e>Εξαγωγή</e>, το <c>v</c> για <e>Βερμπαλιστικό</e> για να βλέπετε τι συμβαίνει κατά την διάδικασία της εξαγωγής (προαιρετικά), το <c>j</c> για <e>Αποσυμπίεση με το bzip2</e>, το <c>p</c> για <e>Διατήρηση αδειών</e> και το <c>f</c> υποδηλώνει ότι θέλουμε να κάνουμε εξαγωγή από αρχείο, όχι από την προκαθορισμένη είσοδο."

#. MIPS uses its own hb-install-stage.xml file, any other arch?
#. <note>
#. Some architectures (e.g. MIPS) Installation CDs and boot images rely upon
#. the
#. <c>tar</c> built into BusyBox which doesn't currently support the <c>v</c>
#. option. Use the <c>xjpf</c> options instead.
#. </note>
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):189
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):244
msgid "Now that the stage is installed, continue with <uri link=\"#installing_portage\">Installing Portage</uri>."
msgstr "Τώρα που το stage έχει εγκατασταθεί, συνεχίστε με το <uri link=\"#installing_portage\">Εγκαθιστώντας το Portage</uri>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):200
msgid "Extracting the Stage Tarball"
msgstr "Αποσυμπιέζοντας το Stage Tarball"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):203
msgid "The stages on the CD reside in the <path>/mnt/cdrom/stages</path> directory. To see a listing of available stages, use <c>ls</c>:"
msgstr "Τα stages στο CD βρίσκονται στον κατάλογο <path>/mnt/cdrom/stages</path>. Για να δείτε μια λίστα από τα διαθέσιμα stage, χρησιμοποιήστε το <c>ls</c>:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):208
msgid "List all available stages"
msgstr "Λίστα όλων των διαθέσιμων stage"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):208
#, no-wrap
msgid ""
"\n"
"# <i>ls /mnt/cdrom/stages</i>\n"
msgstr ""
"\n"
"# <i>ls /mnt/cdrom/stages</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):212
msgid "If the system replies with an error, you may need to mount the CD-ROM first:"
msgstr "Αν το σύστημα απαντήσει με ένα σφάλμα, μπορεί να χρειάζεται πρώτα να προσαρτήσετε το CD-ROM:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):216
msgid "Mounting the CD-ROM"
msgstr "Προσαρτώντας το CD-ROM"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):216
#, no-wrap
msgid ""
"\n"
"# <i>ls /mnt/cdrom/stages</i>\n"
"ls: /mnt/cdrom/stages: No such file or directory\n"
"# <i>mount /dev/cdroms/cdrom0 /mnt/cdrom</i>\n"
"# <i>ls /mnt/cdrom/stages</i>\n"
msgstr ""
"\n"
"# <i>ls /mnt/cdrom/stages</i>\n"
"ls: /mnt/cdrom/stages: No such file or directory\n"
"# <i>mount /dev/cdroms/cdrom0 /mnt/cdrom</i>\n"
"# <i>ls /mnt/cdrom/stages</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):223
msgid "Now go into your Gentoo mountpoint (usually <path>/mnt/gentoo</path>):"
msgstr "Τώρα πηγαίνετε στο δικό σας σημείο προσάρτησης του Gentoo (συνήθως <path>/mnt/gentoo</path>):"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):227
msgid "Changing directory to /mnt/gentoo"
msgstr "Αλλάζοντας κατάλογο σε /mnt/gentoo"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):231
msgid "We will now extract the stage tarball of your choice. We will do this with <c>tar</c>. Make sure you use the same options (<c>xvjpf</c>). The <c>v</c> argument is optional and not supported in some <c>tar</c> versions. In the next example, we extract the stage tarball <path>stage3-&lt;subarch&gt;-&lt;release&gt;.tar.bz2</path>. Be sure to substitute the tarball filename with your stage."
msgstr "Θα αποσυμπιέσουμε τώρα το stage tarball της επιλογής σας. Θα το κάνουμε αυτό με το <c>tar</c>. Σιγουρευτείτε ότι χρησιμοποιείτε τις ίδιες επιλογές (<c>xvjpf</c>). Το όρισμα <c>v</c> είναι προαιρετικό και δεν υποστηρίζεται σε κάποιες εκδόσεις του <c>tar</c>. Στο επόμενο παράδειγμα, αποσυμπιέζουμε το stage tarball <path>stage3-&lt;subarch&gt;-&lt;release&gt;.tar.bz2</path>. Σιγουρευτείτε ότι αντικαταστήσατε το όνομα αρχείου του tarball με αυτό του δικού σας stage."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):240
msgid "Extracting the stage tarball"
msgstr "Αποσυμπίεση του stage tarball"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):240
#, no-wrap
msgid ""
"\n"
"# <i>tar xvjpf /mnt/cdrom/stages/stage3-&lt;subarch&gt;-&lt;release&gt;.tar.bz2</i>\n"
msgstr ""
"\n"
"# <i>tar xvjpf /mnt/cdrom/stages/stage3-&lt;subarch&gt;-&lt;release&gt;.tar.bz2</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):253
msgid "Installing Portage"
msgstr "Εγκαθιστώντας το Portage"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):255
msgid "Unpacking a Portage Snapshot"
msgstr "Αποσυμπιέζοντας ένα Στιγμιότυπο του Portage Snapshot"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):258
msgid "You now have to install a Portage snapshot, a collection of files that inform Portage what software titles you can install, which profiles are available, etc."
msgstr "Τώρα πρέπει να εγκαταστήσετε ένα στιγμιότυπο του Portage, μια συλλογή αρχείων που πληροφορούν το Portage τι τίτλους λογισμικού μπορείτε να εγκαταστήσετε, ποια προφίλ είναι διαθέσιμα, κα."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):266
msgid "Download and Install a Portage Snapshot"
msgstr "Λήψη και Εγκατάσταση ενός Στιγμιοτύπου του Portage"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):269
msgid "Go to the mountpoint where you mounted your filesystem (most likely <path>/mnt/gentoo</path>):"
msgstr "Πηγαίνετε στο σημείο προσάρτησης όπου προσαρτήσατε το σύστημα αρχείων σας (το πιο πιθανό στο <path>/mnt/gentoo</path>):"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):278
msgid "Fire up <c>links</c> (or <c>lynx</c>) and go to our <uri link=\"/main/en/mirrors.xml\">Gentoo mirror list</uri>. Pick a mirror close to you and open the <path>snapshots/</path> directory. There, download the latest Portage snapshot (<path>portage-latest.tar.bz2</path>) by selecting it and pressing <c>D</c>."
msgstr "Σηκώστε το <c>links</c> (ή το <c>lynx</c>) και πηγαίνετε στη <uri link=\"/main/en/mirrors.xml\">λίστα των Gentoo mirror</uri>. Διαλέξτε ένα mirror κοντά σε εσάς και ανοίξτε τον κατάλογο <path>snapshots/</path>. Από εκεί, κάντε λήψη του τελευταίου στιγμιότυπου του Portage (<path>portage-latest.tar.bz2</path>) επιλέγοντάς το και πατώντας <c>D</c>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):286
msgid "Browsing the Gentoo mirrorlist"
msgstr "Κοιτώντας τη λίστα των mirror του Gentoo"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):286
#, no-wrap
msgid ""
"\n"
"# <i>links http://www.gentoo.org/main/en/mirrors.xml</i>\n"
msgstr ""
"\n"
"# <i>links http://www.gentoo.org/main/en/mirrors.xml</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):290
msgid "Now exit your browser by pressing <c>Q</c>. You will now have a Portage snapshot stored in <path>/mnt/gentoo</path>."
msgstr "Τώρα βγείτε από τον περιηγητή ιστού σας πατώντας <c>Q</c>. Θα έχετε πλέον ένα στιγμιότυπο του Portage αποθηκευμένο στο <path>/mnt/gentoo</path>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):295
msgid "If you want to check the integrity of the downloaded snapshot, use <c>md5sum</c> and compare the output with the MD5 checksum provided on the mirror."
msgstr "Αν θέλετε να ελέγξετε την ακεραιότητα του ληφθέντος στιγμιότυπου, χρησιμοποιήστε το <c>md5sum</c> και συγκρίνετε το αποτέλεσμα με το MD5 checksum που παρέχεται στο mirror."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):301
msgid "Checking integrity of a Portage snapshot"
msgstr "Ελέγχοντας την ακεραιότητα ενός στιγμιότυπου του Portage"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):301
#, no-wrap
msgid ""
"\n"
"# <i>md5sum -c portage-latest.tar.bz2.md5sum</i>\n"
"portage-latest.tar.bz2: OK\n"
msgstr ""
"\n"
"# <i>md5sum -c portage-latest.tar.bz2.md5sum</i>\n"
"portage-latest.tar.bz2: OK\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):306
msgid "In the next step, we extract the Portage snapshot onto your filesystem. Make sure that you use the exact command; the last option is a capital <c>C</c>, not <c>c</c>."
msgstr "Στο επόμενο βήμα, αποσυμπιέζουμε το στιγμιότυπο του Portage στο σύστημα αρχείων σας. Σιγουρευτείτε ότι χρησιμοποιήσατε την ίδια ακριβώς εντολή· η τελευταία επιλογή είναι ένα κεφαλαίο <c>C</c>, όχι ένα <c>c</c>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):312
msgid "Extracting the Portage snapshot"
msgstr "Αποσυμπιέζοντας το στιγμιότυπο του Portage"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):312
#, no-wrap
msgid ""
"\n"
"# <i>tar xvjf /mnt/gentoo/portage-latest.tar.bz2 -C /mnt/gentoo/usr</i>\n"
msgstr ""
"\n"
"# <i>tar xvjf /mnt/gentoo/portage-latest.tar.bz2 -C /mnt/gentoo/usr</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):321
msgid "Configuring the Compile Options"
msgstr "Ρυθμίζοντας τις Επιλογές Μεταγλώττισης"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):323
msgid "Introduction"
msgstr "Εισαγωγή"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):326
msgid "To optimize Gentoo, you can set a couple of variables which impact Portage behaviour. All those variables can be set as environment variables (using <c>export</c>) but that isn't permanent. To keep your settings, Portage provides you with <path>/etc/make.conf</path>, a configuration file for Portage. It is this file we will edit now."
msgstr "Για να βελτιστοποιήσετε το Gentoo, μπορείτε να ορίσετε κάποιες μεταβλητές οι οποίες αλλάζουν την συμπεριφορά του Portage. Όλες αυτές οι μεταβλητές μπορούν να οριστούν ως μεταβλητές περιβάλλοντος (χρησιμοποιώντας την <c>export</c>) αλλά αυτό δεν είναι μόνιμο. Για να διατηρήσετε τις ρυθμίσεις σας, το Portage σας παρέχει το <path>/etc/make.conf</path>, ένα αρχείο ρυθμίσεων για το Portage. Είναι αυτό το αρχείο το οποίο θα επεξεργαστούμε τώρα."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(note):334
msgid "A commented listing of all possible variables can be found in <path>/mnt/gentoo/usr/share/portage/config/make.conf.example</path>. For a successful Gentoo installation you'll only need to set the variables which are mentioned beneath."
msgstr "Μια λίστα με όλες τις πιθανές μεταβλητές σαν σχόλιο μπορεί να βρεθεί στο <path>/mnt/gentoo/usr/share/portage/config/make.conf.example</path>. Για μια επιτυχή εγκατάσταση Gentoo θα χρειαστεί μόνο να ρυθμίσετε τις μεταβλητές που αναφέρονται παρακάτω."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):341
msgid "Fire up your favorite editor (in this guide we use <c>nano</c>) so we can alter the optimization variables we will discuss hereafter."
msgstr "Ανοίξτε τον αγαπημένο σας επεξεργαστή κειμένου (σε αυτόν τον οδηγό χρησιμοποιούμε το <c>nano</c>) ώστε να μπορείτε να τροποποιήσετε τις μεταβλητές βελτιστοποίησης που θα συζητήσουμε από εδώ και πέρα."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):346
msgid "Opening /etc/make.conf"
msgstr "Ανοίγοντας το /etc/make.conf"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):346
#, no-wrap
msgid ""
"\n"
"# <i>nano -w /mnt/gentoo/etc/make.conf</i>\n"
msgstr ""
"\n"
"# <i>nano -w /mnt/gentoo/etc/make.conf</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):350
msgid "As you probably noticed, the <path>make.conf.example</path> file is structured in a generic way: commented lines start with \"#\", other lines define variables using the <c>VARIABLE=\"content\"</c> syntax. The <path>make.conf</path> file uses the same syntax. Several of those variables are discussed next."
msgstr "Όπως πιθανώς παρατηρήσατε, το αρχείο <path>make.conf.example</path> έχει δομηθεί με ένα γενικό τρόπο: οι γραμμές σχολίων ξεκινούν με \"#\", οι άλλες γραμμές ορίζουν μεταβλητές χρησιμοποιώντας τη σύνταξη <c>ΜΕΤΑΒΛΗΤΗ=\"περιεχόμενο\"</c>. Το αρχείο <path>make.conf</path> χρησιμοποιεί την ίδια σύνταξη. Αρκετές από αυτές τις μεταβλητές συζητούνται παρακάτω."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):360
msgid "CFLAGS and CXXFLAGS"
msgstr "CFLAGS και CXXFLAGS"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):363
msgid "The <c>CFLAGS</c> and <c>CXXFLAGS</c> variables define the optimization flags for the <c>gcc</c> C and C++ compiler respectively. Although we define those generally here, you will only have maximum performance if you optimize these flags for each program separately. The reason for this is because every program is different."
msgstr "Οι μεταβλητές <c>CFLAGS</c> και <c>CXXFLAGS</c> ορίζουν τις σημαίες βελτιστοποίησης για τον <c>gcc</c> μεταγλωττιστή C και C++ αντίστοιχα. Παρόλο που ορίζουμε τις συγκεκριμένες γενικά εδώ, θα πάρετε μέγιστη απόδοση αν βελτιστοποιήσετε τις σημαίες αυτές για κάθε πρόγραμμα ξεχωριστά. Ο λόγος είναι ότι κάθε πρόγραμμα είναι διαφορετικό."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):371
msgid "In <path>make.conf</path> you should define the optimization flags you think will make your system the most responsive <e>generally</e>. Don't place experimental settings in this variable; too much optimization can make programs behave bad (crash, or even worse, malfunction)."
msgstr "Στο <path>make.conf</path> θα πρέπει να ορίσετε τις σημαίες βελτιστοποίησης που νομίζετε ότι θα κάνουν το σύστημά σας να αποκρίνεται όσο γίνεται καλύτερα <e>γενικά</e>. Μην βάζετε πειραματικές ρυθμίσεις σε αυτή τη μεταβλητή· η πάρα πολύ βελτιστοποίηση μπορεί να κάνει τα προγράμματα να συμπεριφέρονται άσχημα (να καταρρέουν, ή ακόμη χειρότερα, να δυσλειτουργούν)."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):378
msgid "We will not explain all possible optimization options. If you want to know them all, read the <uri link=\"http://gcc.gnu.org/onlinedocs/\">GNU Online Manual(s)</uri> or the <c>gcc</c> info page (<c>info gcc</c> -- only works on a working Linux system). The <path>make.conf.example</path> file itself also contains lots of examples and information; don't forget to read it too."
msgstr "Δεν θα εξηγήσουμε όλες τις πιθανές επιλογές βελτιστοποίησης. Αν θέλετε να τις μάθετε όλες, διαβάστε το <uri link=\"http://gcc.gnu.org/onlinedocs/\">GNU Online Manual(s)</uri> ή τη σελίδα πληροφοριών του <c>gcc</c> (<c>info gcc</c> -- δουλεύει μόνο σε ένα λειτουργικό σύστημα Linux). Το αρχείο <path>make.conf.example</path> από μόνο του περιέχει πολλά παραδείγματα και πληροφορίες· μην ξεχάσετε να το διαβάσετε και αυτό επίσης."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):387
msgid "A first setting is the <c>-march=</c> or <c>-mcpu=</c> flag, which specifies the name of the target architecture. Possible options are described in the <path>make.conf.example</path> file (as comments)."
msgstr "Μια πρώτη ρύθμιση είναι η σημαία <c>-march=</c> ή η <c>-mcpu=</c>, η οποία καθορίζει το όνομα της αρχιτεκτονικής που στοχεύουμε. Οι πιθανές επιλογές περιγράφονται στο αρχείο <path>make.conf.example</path> (ως σχόλια)."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):393
msgid "A first setting is the <c>-march=</c> or <c>-mtune=</c> flag, which specifies the name of the target architecture. Possible options are described in the <path>make.conf.example</path> file (as comments)."
msgstr "Μια πρώτη ρύθμιση είναι η σημαία <c>-march=</c> ή η <c>-mtune=</c>, η οποία καθορίζει το όνομα της αρχιτεκτονικής που στοχεύουμε. Οι πιθανές επιλογές περιγράφονται στο αρχείο <path>make.conf.example</path> (ως σχόλια)."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):399
msgid "A second one is the <c>-O</c> flag (that is a capital O, not a zero), which specifies the <c>gcc</c> optimization class flag. Possible classes are <c>s</c> (for size-optimized), <c>0</c> (zero - for no optimizations), <c>1</c>, <c>2</c> or even <c>3</c> for more speed-optimization flags (every class has the same flags as the one before, plus some extras). <c>-O2</c> is the recommended default. <c>-O3</c> is known to cause problems when used system-wide, so we recommend that you stick to <c>-O2</c>."
msgstr "Μια δεύτερη είναι η σημαία <c>-O</c> (αυτό είναι ένα κεφαλαίο O, όχι μηδέν), η οποία καθορίζει την κλάση βελτιστοποίησης του <c>gcc</c>. Οι πιθανές κλάσεις είναι οι <c>s</c> (για βελτιστοποίηση σε σχέση με το μέγεθος), <c>0</c> (μηδέν - για μη βελτιστοποίηση), <c>1</c>, <c>2</c> ή ακόμη και <c>3</c> για περισσότερες σημαίες βελτιστοποίησης ταχύτητας (κάθε κλάση έχει τις ίδιες σημαίες με την προηγούμενη, συν μερικές επιπλέον). Η <c>-O2</c> είναι η προτεινόμενη εξ ορισμού. Η <c>-O3</c> είναι γνωστό ότι προκαλεί προβλήματα όταν χρησιμοποιείται σε ολόκληρο το σύστημα, οπότε προτείνουμε να παραμείνετε στο <c>-O2</c>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):410
msgid "Another popular optimization flag is <c>-pipe</c> (use pipes rather than temporary files for communication between the various stages of compilation). It has no impact on the generated code, but uses more memory. On systems with low memory, gcc might get killed. In that case, do not use this flag."
msgstr "Μια άλλη δημοφιλής σημαία βελτιστοποίησης είναι η <c>-pipe</c> (χρησιμοποιεί διασωληνώσεις αντί για προσωρινά αρχεία για επικοινωνία μεταξύ των διαφόρων σταδίων μεταγλώττισης). Δεν έχει καμιά επίπτωση στον παραγόμενο κώδικα, αλλά χρησιμοποιεί περισσότερη μνήμη. Σε συστήματα με λίγη μνήμη, το gcc μπορεί να σκοτωθεί. Σε αυτή την περίπτωση, μην χρησιμοποιήσετε αυτή τη σημαία."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):417
msgid "Using <c>-fomit-frame-pointer</c> (which doesn't keep the frame pointer in a register for functions that don't need one) might have serious repercussions on the debugging of applications."
msgstr "Χρησιμοποιώντας την <c>-fomit-frame-pointer</c> (η οποία δεν κρατάει το δείκτη πλαισίου σε έναν καταχωρητή για συναρτήσεις που δεν χρειάζονται έναν) μπορεί να έχει σοβαρές επιπτώσεις στην αποσφαλμάτωση των εφαρμογών."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):423
msgid "When you define the <c>CFLAGS</c> and <c>CXXFLAGS</c>, you should combine several optimization flags. The default values contained in the stage3 archive you unpacked should be good enough. The following example is just an example:"
msgstr "Όταν ορίζετε τις <c>CFLAGS</c> και <c>CXXFLAGS</c>, θα πρέπει να συνδυάσετε αρκετές σημαίες βελτιστοποίησης. Οι προκαθορισμένες τιμές που περιέχονται στο αρχείο stage3 που αποσυμπιέσατε θα πρέπει να είναι αρκετά καλές. Το παρακάτω παράδειγμα είναι απλά ένα παράδειγμα:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):429
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):435
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):441
msgid "Defining the CFLAGS and CXXFLAGS variable"
msgstr "Ορίζοντας τις μεταβλητές CFLAGS και CXXFLAGS"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):429
#, no-wrap
msgid ""
"\n"
"CFLAGS=\"<keyval id=\"CFLAGS\"></keyval>\"\n"
"<comment># Use the same settings for both variables</comment>\n"
"CXXFLAGS=\"${CFLAGS}\"\n"
msgstr ""
"\n"
"CFLAGS=\"<keyval id=\"CFLAGS\"></keyval>\"\n"
"<comment># Χρησιμοποίησε τις ίδιες ρυθμίσεις και για τις δύο μεταβλητές</comment>\n"
"CXXFLAGS=\"${CFLAGS}\"\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):435
#, no-wrap
msgid ""
"\n"
"CFLAGS=\"<keyval id=\"CFLAGS\"></keyval>\"   <comment># Intel EM64T users should use -march=core2</comment>\n"
"<comment># Use the same settings for both variables</comment>\n"
"CXXFLAGS=\"${CFLAGS}\"\n"
msgstr ""
"\n"
"CFLAGS=\"<keyval id=\"CFLAGS\"></keyval>\"   <comment># Οι χρήστες Intel EM64T θα πρέπει να χρησιμοποιήσετε τη -march=core2</comment>\n"
"<comment># Χρησιμοποίησε τις ίδιες ρυθμίσεις και για τις δύο μεταβλητές</comment>\n"
"CXXFLAGS=\"${CFLAGS}\"\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):441
#, no-wrap
msgid ""
"\n"
"CFLAGS=\"<keyval id=\"CFLAGS\"></keyval>   <comment># Be sure to change -march to match your CPU type</comment>\n"
"<comment># Use the same settings for both variables</comment>\n"
"CXXFLAGS=\"${CFLAGS}\"\n"
msgstr ""
"\n"
"CFLAGS=\"<keyval id=\"CFLAGS\"></keyval>   <comment># Σιγουρευτείτε ότι αλλάξατε τη -march να ταιριάζει με τον τύπο του επεξεργαστή σας</comment>\n"
"<comment># Χρησιμοποίησε τις ίδιες ρυθμίσεις και για τις δύο μεταβλητές</comment>\n"
"CXXFLAGS=\"${CFLAGS}\"\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(note):447
msgid "You may also want to view the <uri link=\"/doc/en/gcc-optimization.xml\">Compilation Optimization Guide</uri> for more information on how the various compilation options can affect your system."
msgstr "Μπορεί να θέλετε επίσης να δείτε τον <uri link=\"/doc/en/gcc-optimization.xml\"> Οδηγό Βελτιστοποίησης Μεταγλώττισης</uri> για περισσότερες πληροφορίες για το πως οι διάφορες επιλογές μεταγλώττισης μπορούν να επηρεάσουν το σύστημά σας."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):456
msgid "MAKEOPTS"
msgstr "MAKEOPTS"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):459
msgid "With <c>MAKEOPTS</c> you define how many parallel compilations should occur when you install a package. A good choice is the number of CPUs (or CPU cores) in your system plus one, but this guideline isn't always perfect."
msgstr "Με τη <c>MAKEOPTS</c> ορίζετε πόσες παράλληλες μεταγλωττίσεις θα πρέπει να εμφανίζονται όταν εγκαθιστάτε ένα πακέτο. Μια καλή επιλογή είναι ο αριθμός των ΚΜΕ (ή των πυρήνων της ΚΜΕ) στο σύστημά σας συν ένα, αλλά αυτή η κατευθυντήρια γραμμή δεν λειτουργεί πάντα τέλεια."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre:caption):465
msgid "MAKEOPTS for a regular, 1-CPU system"
msgstr "Η MAKEOPTS για ένα κανονικό, με 1 ΚΜΕ σύστημα"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(pre):465
#, no-wrap
msgid ""
"\n"
"MAKEOPTS=\"-j2\"\n"
msgstr ""
"\n"
"MAKEOPTS=\"-j2\"\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(title):472
msgid "Ready, Set, Go!"
msgstr "Έτοιμοι, Θέσατε, Πάμε!"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(p):475
msgid "Update your <path>/mnt/gentoo/etc/make.conf</path> to your own preference and save (<c>nano</c> users would hit <c>Ctrl-X</c>). You are now ready to continue with <uri link=\"?part=1&amp;chap=6\">Installing the Gentoo Base System</uri>."
msgstr "Ενημερώστε το δικό σας <path>/mnt/gentoo/etc/make.conf</path> με τις δικές προτιμήσεις και αποθηκεύστε (οι χρήστες του <c>nano</c> θα πρέπει να πατήσουν <c>Ctrl-X</c>). Είστε πλέον έτοιμοι να συνεχίσετε με την <uri link=\"?part=1&amp;chap=6\">Εγκατάσταση του Βασικού Συστήματος του Gentoo</uri>."

#. Place here names of translator, one per line. Format should be NAME; ROLE;
#. E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-stage.xml(None):0
msgid "translator-credits"
msgstr ""
