#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2010-10-22 00:40+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(abstract):11
msgid ""
"Portage comes with a few extra tools that might make your Gentoo experience "
"even better. Read on to discover how to use dispatch-conf and other tools."
msgstr ""
"Το portage έρχεται με κάποια επιπλέον εργαλεία που μπορεί να κάνουν την "
"εμπειρία με το Gentoo ακόμη καλύτερη. Διαβάστε παρακάτω για να ανακαλύψετε "
"πως θα χρησιμοποιήσετε το dispatch-conf και άλλα εργαλεία."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(version):16
msgid "2"
msgstr "2"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(date):17
msgid "2010-10-04"
msgstr "2010-10-04"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(title):20
msgid "dispatch-conf"
msgstr "dispatch-conf"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):23
msgid ""
"<c>dispatch-conf</c> is a tool that aids in merging the <path>._cfg0000_&lt;"
"name&gt;</path> files. <path>._cfg0000_&lt;name&gt;</path> files are "
"generated by Portage when it wants to overwrite a file in a directory "
"protected by the CONFIG_PROTECT variable."
msgstr ""
"Το <c>dispatch-conf</c> είναι ένα εργαλείο που βοηθάει στην συγχώνευση των "
"αρχείων <path>._cfg0000_&lt;name&gt;</path>. Τα <path>._cfg0000_&lt;name&gt;"
"</path> αρχεία δημιουργούνται από το portage όταν θέλει να γράψει πάνω από "
"ένα αρχείο ή φάκελο που προστατεύονται από την μεταβλητή CONFIG_PROTECT."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):30
msgid ""
"With <c>dispatch-conf</c>, you are able to merge updates to your "
"configuration files while keeping track of all changes. <c>dispatch-conf</c> "
"stores the differences between the configuration files as patches or by "
"using the RCS revision system. This means that if you make a mistake when "
"updating a config file, you can revert to the previous version of your "
"config file at any time."
msgstr ""
"Χρησιμοποιώντας το <c>dispatch-conf</c>, μπορείτε να συγχωνεύσετε τις "
"αλλαγές στα αρχεία ρυθμίσεων παρακολουθώντας ταυτόχρονα όλες τις αλλαγές. Το "
"<c>dispatch-conf</c> αποθηκεύει τις διαφορές μεταξύ των αρχείων ρυθμίσεων ως "
"διορθώσεις (patches) ή χρησιμοποιώντας το σύστημα αναθεωρήσεων RCS. Αυτό "
"σημαίνει ότι αν κάνετε κάποιο λάθος όταν ενημερώνετε ένα αρχείο ρυθμίσεων, "
"μπορείτε να επαναφέρετε μια προηγούμενη έκδοση του αρχείου ρυθμίσεων "
"οποιαδήποτε στιγμή."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):38
msgid ""
"When using <c>dispatch-conf</c>, you can ask to keep the configuration file "
"as-is, use the new configuration file, edit the current one or merge the "
"changes interactively. <c>dispatch-conf</c> also has some nice additional "
"features:"
msgstr ""
"Όταν χρησιμοποιείτε το <c>dispatch-conf</c>, μπορείτε να ζητήσετε να "
"διατηρηθεί το αρχείο ρυθμίσεων ως έχει, να επεξεργαστείτε το τρέχον ή να "
"συγχωνεύσετε τις αλλαγές διαδραστικά. Το <c>dispatch-conf</c> έχει μερικά "
"επιπλέον χρήσιμα χαρακτηριστικά:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(li):45
msgid ""
"Automatically merge configuration file updates that only contain updates to "
"comments"
msgstr ""
"Αυτόματη συγχώνευση των αλλαγών στα αρχεία ρυθμίσεων που περιέχουν "
"ενημερώσεις μόνο των σχολίων"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(li):49
msgid ""
"Automatically merge configuration files which only differ in the amount of "
"whitespace"
msgstr ""
"Αυτόματη συγχώνευση αρχείων ρυθμίσεων που διαφέρουν μόνο στο πλήθος των "
"λευκών χαρακτήρων (whitespace)"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):55
msgid ""
"Make certain you edit <path>/etc/dispatch-conf.conf</path> first and create "
"the directory referenced by the archive-dir variable."
msgstr ""
"Σιγουρευτείτε πρώτα ότι επεξεργαστήκατε το αρχείο <path>/etc/dispatch-conf."
"conf</path> και δημιουργήσατε τον κατάλογο στον οποίο αναφέρεται η μεταβλητή "
"περιβάλλοντος archive-dir."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre:caption):60
msgid "Running dispatch-conf"
msgstr "Εκτελώντας το dispatch-conf"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre):60
#, no-wrap
msgid ""
"\n"
"# <i>dispatch-conf</i>\n"
msgstr ""
"\n"
"# <i>dispatch-conf</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):64
msgid ""
"When running <c>dispatch-conf</c>, you'll be taken through each changed "
"config file, one at a time. Press <c>u</c> to update (replace) the current "
"config file with the new one and continue to the next file. Press <c>z</c> "
"to zap (delete) the new config file and continue to the next file. Once all "
"config files have been taken care of, <c>dispatch-conf</c> will exit. You "
"can also press <c>q</c> to exit any time."
msgstr ""
"Όταν εκτελείτε το <c>dispatch-conf</c>, θα περνάτε από κάθε ενημερωμένο "
"αρχείο ρυθμίσεων, ένα την φορά. Πατήστε <c>u</c> για να ενημερώσετε "
"(αντικαταστήσετε) το τρέχον αρχείο ρυθμίσεων με το νέο και να συνεχίσετε στο "
"επόμενο αρχείο. Πατήστε <c>z</c> για να διαγράψετε το νέο αρχείο ρυθμίσεων "
"και να συνεχίσετε στο επόμενο αρχείο. Όταν όλα τα αρχεία ρυθμίσεων έχουν "
"τακτοποιηθεί, το <c>dispatch-conf</c> θα τερματίσει. Μπορείτε επίσης να "
"πατήσετε <c>q</c> για να το τερματίσετε οποιαδήποτε στιγμή."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):73
msgid ""
"For more information, check out the <c>dispatch-conf</c> man page. It tells "
"you how to interactively merge current and new config files, edit new config "
"files, examine differences between files, and more."
msgstr ""
"Για περισσότερες πληροφορίες, δείτε τη σελίδα man του <c>dispatch-conf</c>. "
"Σας πληροφορεί για το πως μπορείτε να συγχωνεύσετε τα τωρινά και τα "
"καινούρια αρχεία ρυθμίσεων διαδραστικά, να επεξεργαστείτε τα νέα αρχεία "
"ρυθμίσεων, να εξετάσετε τις διαφορές μεταξύ των αρχείων, και ακόμη "
"περισσότερα."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre:caption):79
msgid "Reading the dispatch-conf man page"
msgstr "Ανάγνωση της σελίδας man του dispatch-conf"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre):79
#, no-wrap
msgid ""
"\n"
"$ <i>man dispatch-conf</i>\n"
msgstr ""
"\n"
"$ <i>man dispatch-conf</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(title):86
msgid "etc-update"
msgstr "etc-update"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):89
msgid ""
"You can also use <c>etc-update</c> to merge config files. It's not as simple "
"to use as <c>dispatch-conf</c>, nor as featureful, but it does provide an "
"interactive merging setup and can also auto-merge trivial changes."
msgstr ""
"Μπορείτε επίσης να χρησιμοποιήσετε το <c>etc-update</c> για να συγχωνεύσετε "
"τα αρχεία ρυθμίσεων. Δεν είναι το ίδιο απλό να το χρησιμοποιήσετε όσο το "
"<c>dispatch-conf</c>, ούτε το ίδιο πλήρες στα χαρακτηριστικά του, αλλά "
"παρέχει μια διαδραστική εγκατάσταση συγχώνευσης και μπορεί να συγχωνεύει "
"αυτόματα τις ασήμαντες αλλαγές."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):95
msgid ""
"However, unlike <c>dispatch-conf</c>, <c>etc-update</c> does <e>not</e> "
"preserve the old versions of your config files. Once you update the file, "
"the old version is gone forever! So be very careful, as using <c>etc-update</"
"c> is <e>significantly</e> less safe than using <c>dispatch-conf</c>."
msgstr ""
"Ωστόσο, αντίθετα με το <c>dispatch-conf</c>, το <c>etc-update</c><e>δεν</e> "
"διατηρεί τις παλιές εκδόσεις των αρχείων ρυθμίσεων. Μόλις ενημερώσετε ένα "
"αρχείο, η παλιά έκδοση εξαφανίζεται μια και καλή! Οπότε να είστε πολύ "
"προσεκτικοί, καθώς η χρήση του <c>etc-update</c> είναι <e>σημαντικά</e> "
"λιγότερο ασφαλές από ότι η χρήση του <c>dispatch-conf</c>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre:caption):102
msgid "Running etc-update"
msgstr "Εκτέλεση του etc-update"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre):102
#, no-wrap
msgid ""
"\n"
"# <i>etc-update</i>\n"
msgstr ""
"\n"
"# <i>etc-update</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):106
msgid ""
"After merging the straightforward changes, you will be prompted with a list "
"of protected files that have an update waiting. At the bottom you are "
"greeted by the possible options:"
msgstr ""
"Μετά τη συγχώνευση-εγκατάσταση των ξεκάθαρων αλλαγών, θα εμφανιστεί μια "
"λίστα με τα προστατευμένα αρχεία που είναι σε κατάσταση αναμονής για "
"ενημέρωση. Στο κάτω μέρος θα εμφανίζονται οι πιθανές επιλογές:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre:caption):112
msgid "etc-update options"
msgstr "Οι επιλογές του etc-update"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre):112
#, no-wrap
msgid ""
"\n"
"Please select a file to edit by entering the corresponding number.\n"
"              (-1 to exit) (-3 to auto merge all remaining files)\n"
"                           (-5 to auto-merge AND not use 'mv -i'):\n"
msgstr ""
"\n"
"Please select a file to edit by entering the corresponding number.\n"
"              (-1 to exit) (-3 to auto merge all remaining files)\n"
"                           (-5 to auto-merge AND not use 'mv -i'):\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):118
msgid ""
"If you enter <c>-1</c>, <c>etc-update</c> will exit and discontinue any "
"further changes. If you enter <c>-3</c> or <c>-5</c>, <e>all</e> listed "
"configuration files will be overwritten with the newer versions. It is "
"therefore very important to first select the configuration files that should "
"not be automatically updated. This is simply a matter of entering the number "
"listed to the left of that configuration file."
msgstr ""
"Αν εισάγετε <c>-1</c>, το <c>etc-update</c> θα τερματίσει και θα διακόψει "
"οποιεσδήποτε αλλαγές. Αν εισάγετε <c>-3</c> ή <c>-5</c>, <e>όλα</e> τα "
"αρχεία ρυθμίσεων που εμφανίζονται στη λίστα θα αντικατασταθούν με τις "
"νεότερες εκδόσεις. Είναι λοιπόν ιδιαίτερα σημαντικό να επιλέξετε πρώτα τα "
"αρχεία ρυθμίσεων που δεν πρέπει να ενημερωθούν αυτόματα. Αυτό γίνεται απλά "
"με την εισαγωγή του αριθμού που εμφανίζεται στα αριστερά του αρχείου "
"ρυθμίσεων."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):127
msgid ""
"As an example, we select the configuration file <path>/etc/pear.conf</path>:"
msgstr ""
"Για παράδειγμα, επιλέγουμε το αρχείο ρυθμίσεων <path>/etc/pear.conf</path>:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre:caption):131
msgid "Updating a specific configuration file"
msgstr "Ενημέρωση ενός συγκεκριμένου αρχείου ρυθμίσεων"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre):131
#, no-wrap
msgid ""
"\n"
"Beginning of differences between /etc/pear.conf and /etc/._cfg0000_pear.conf\n"
"<comment>[...]</comment>\n"
"End of differences between /etc/pear.conf and /etc/._cfg0000_pear.conf\n"
"1) Replace original with update\n"
"2) Delete update, keeping original as is\n"
"3) Interactively merge original with update\n"
"4) Show differences again\n"
msgstr ""
"\n"
"Beginning of differences between /etc/pear.conf and /etc/._cfg0000_pear.conf\n"
"<comment>[...]</comment>\n"
"End of differences between /etc/pear.conf and /etc/._cfg0000_pear.conf\n"
"1) Replace original with update\n"
"2) Delete update, keeping original as is\n"
"3) Interactively merge original with update\n"
"4) Show differences again\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):141
msgid ""
"You can now see the differences between the two files. If you believe that "
"the updated configuration file can be used without problems, enter <c>1</c>. "
"If you believe that the updated configuration file isn't necessary, or "
"doesn't provide any new or useful information, enter <c>2</c>. If you want "
"to interactively update your current configuration file, enter <c>3</c>."
msgstr ""
"Μπορείτε τώρα να δείτε τις διαφορές μεταξύ των δύο αρχείων. Αν νομίζετε ότι "
"το ενημερωμένο αρχείο ρυθμίσεων μπορεί να χρησιμοποιηθεί χωρίς πρόβλημα, "
"εισάγετε <c>1</c>. Αν νομίζετε ότι το ενημερωμένο αρχείο δεν είναι "
"απαραίτητο, ή δεν προσφέρει νέα ή χρήσιμη πληροφορία, εισάγετε <c>2</c>. Αν "
"θέλετε να ενημερώσετε το τρέχων αρχείο ρυθμίσεων με διαδραστικό τρόπο, "
"εισάγετε <c>3</c>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):149
msgid ""
"There is no point in further elaborating the interactive merging here. For "
"completeness sake, we will list the possible commands you can use while you "
"are interactively merging the two files. You are greeted with two lines (the "
"original one, and the proposed new one) and a prompt at which you can enter "
"one of the following commands:"
msgstr ""
"Δεν έχει νόημα σε αυτό το σημείο να δούμε με περισσότερες λεπτομέρειες τη "
"διαδραστική συγχώνευση-εγκατάσταση. Για λόγους πληρότητας, θα δούμε μια "
"λίστα με τις πιθανές εντολές που μπορείτε να χρησιμοποιήσετε κατά τη "
"διάρκεια της διαδραστικής διαδικασίας συγχώνευσης-εγκατάστασης των δύο "
"αρχείων. Βλέπετε δύο γραμμές (την αρχική και τη νέα προτεινόμενη) και ένα "
"σύμβολο προτροπής (prompt) στο οποίο εισάγετε μία από τις ακόλουθες εντολές:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre:caption):157
msgid "Commands available for the interactive merging"
msgstr "Διαθέσιμες εντολές για τη διαδραστική συγχώνευση-εγκατάσταση"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre):157
#, no-wrap
msgid ""
"\n"
"ed:     Edit then use both versions, each decorated with a header.\n"
"eb:     Edit then use both versions.\n"
"el:     Edit then use the left version.\n"
"er:     Edit then use the right version.\n"
"e:      Edit a new version.\n"
"l:      Use the left version.\n"
"r:      Use the right version.\n"
"s:      Silently include common lines.\n"
"v:      Verbosely include common lines.\n"
"q:      Quit.\n"
msgstr ""
"\n"
"ed:     Edit then use both versions, each decorated with a header.\n"
"eb:     Edit then use both versions.\n"
"el:     Edit then use the left version.\n"
"er:     Edit then use the right version.\n"
"e:      Edit a new version.\n"
"l:      Use the left version.\n"
"r:      Use the right version.\n"
"s:      Silently include common lines.\n"
"v:      Verbosely include common lines.\n"
"q:      Quit.\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):170
msgid ""
"When you have finished updating the important configuration files, you can "
"now automatically update all the other configuration files. <c>etc-update</"
"c> will exit if it doesn't find any more updateable configuration files."
msgstr ""
"Όταν τελειώσετε με την ενημέρωση των σημαντικότερων αρχείων ρυθμίσεων, "
"μπορείτε να προχωρήσετε στην αυτόματη ενημέρωση των υπολοίπων. Το <c>etc-"
"update</c> θα τερματίσει όταν δεν βρει περισσότερα αρχεία ρυθμίσεων προς "
"ενημέρωση."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(title):179
msgid "quickpkg"
msgstr "quickpkg"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):182
msgid ""
"With <c>quickpkg</c> you can create archives of the packages that are "
"already merged on your system. These archives can be used as prebuilt "
"packages. Running <c>quickpkg</c> is straightforward: just add the names of "
"the packages you want to archive."
msgstr ""
"Με το <c>quickpkg</c> μπορείτε να δημιουργήσετε αρχεία (archives) των "
"πακέτων που είναι ήδη εγκατεστημένα στο σύστημά σας. Τα αρχεία αυτά μπορούν "
"να χρησιμοποιηθούν ως πακέτα προ-κτισίματος (prebuilt packages). Η εκτέλεση "
"του <c>quickpkg</c> είναι απλή διαδικασία: απλά προσθέστε τα ονόματα των "
"πακέτων για τα οποία θέλετε να δημιουργήσετε τα αρχεία."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):189
msgid "For instance, to archive <c>curl</c>, <c>orage</c>, and <c>procps</c>:"
msgstr ""
"Για παράδειγμα, για να δημιουργήσετε αρχεία των <c>curl</c>, <c>orange</c>, "
"και <c>procps</c>:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre:caption):193
msgid "Example quickpkg usage"
msgstr "Παράδειγμα χρήσης του quickpkg"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(pre):193
#, no-wrap
msgid ""
"\n"
"# <i>quickpkg curl orage procps</i>\n"
msgstr ""
"\n"
"# <i>quickpkg curl orange procps</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(p):197
msgid ""
"The prebuilt packages will be stored in <path>$PKGDIR</path> (<path>/usr/"
"portage/packages/</path> by default). These packages are placed in <path>"
"$PKGDIR/&lt;category&gt;</path>."
msgstr ""
"Τα πακέτα προ-κτισίματος αποθηκεύονται στον κατάλογο <path>$PKGDIR</path> "
"(εξ' ορισμού <path>/usr/portage/packages/</path>). Αυτά τα πακέτα "
"τοποθετούνται στον κατάλογο <path>$PKGDIR/&lt;κατηγορία&gt;</path>."

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-portage-tools.xml(None):0
msgid "translator-credits"
msgstr ""
