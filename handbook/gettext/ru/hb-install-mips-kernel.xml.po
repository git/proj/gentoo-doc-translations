#
# Azamat H. Hackimov <azamat.hackimov@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: Gentoo Documentation\n"
"POT-Creation-Date: 2010-10-22 00:40+0600\n"
"PO-Revision-Date: 2011-01-21 02:51+0500\n"
"Last-Translator: Azamat H. Hackimov <azamat.hackimov@gmail.com>\n"
"Language-Team: Russian <gentoo-doc-ru@gentoo.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)\n"
"X-Generator: Lokalize 1.0\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(abstract):10
msgid ""
"The Linux kernel is the core of every distribution. This chapter explains "
"how to configure your kernel."
msgstr ""
"Ядро Linux — это сердце любого дистрибутива. В этой главе описывается, как "
"его настроить."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(version):15
msgid "10.0"
msgstr "10.0"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(date):16
msgid "2010-07-27"
msgstr "2010-07-27"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):19
msgid "Timezone"
msgstr "Часовой пояс"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):22
msgid ""
"You first need to select your timezone so that your system knows where it is "
"located. Look for your timezone in <path>/usr/share/zoneinfo</path>, then "
"copy it to <path>/etc/localtime</path>. Please avoid the <path>/usr/share/"
"zoneinfo/Etc/GMT*</path> timezones as their names do not indicate the "
"expected zones. For instance, <path>GMT-8</path> is in fact GMT+8."
msgstr ""
"Сначала вы должны выбрать часовой пояс, чтобы система знала, где вы "
"находитесь. Найдите нужный пояс в <path>/usr/share/zoneinfo</path>, затем "
"скопируйте его в <path>/etc/localtime</path>. Старайтесь избегать "
"использования часовых поясов <path>/usr/share/zoneinfo/Etc/GMT*</path>, так "
"как их названия не соответствуют ожидаемым поясам. Например, <path>GMT-8</"
"path> на самом деле является поясом GMT+8."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):31
msgid "Setting the timezone information"
msgstr "Установка часового пояса"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):31
#, no-wrap
msgid ""
"\n"
"# <i>ls /usr/share/zoneinfo</i>\n"
"<comment>(Suppose you want to use GMT)</comment>\n"
"# <i>cp /usr/share/zoneinfo/GMT /etc/localtime</i>\n"
msgstr ""
"\n"
"# <i>ls /usr/share/zoneinfo</i>\n"
"<comment>(Допустим, вы хотите использовать GMT)</comment>\n"
"# <i>cp /usr/share/zoneinfo/GMT /etc/localtime</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):41
msgid "Installing the Sources"
msgstr "Установка исходных кодов ядра"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):43
msgid "Choosing a Kernel"
msgstr "Выбор ядра"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):46
msgid ""
"The core around which all distributions are built is the Linux kernel. It is "
"the layer between the user programs and your system hardware. Gentoo "
"provides its users several possible kernel sources. A full listing with "
"description is available at the <uri link=\"/doc/en/gentoo-kernel.xml"
"\">Gentoo Kernel Guide</uri>."
msgstr ""
"Ядро Linux — это то, вокруг чего формируются все дистрибутивы. Это слой "
"между пользовательскими программами и вашими аппаратными средствами. "
"Пользователям Gentoo доступны несколько типов ядра. Полный их список с "
"описанием можно найти в <uri link=\"/doc/en/gentoo-kernel.xml\">руководстве "
"по выбору ядра Gentoo</uri>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):54
msgid ""
"MIPS-based systems have just the one kernel tree to choose from, <c>mips-"
"sources</c>. This patchset differs from the ones available for other "
"architectures, in that it has lots of patches specific to the MIPS "
"architecture."
msgstr ""
"Для систем на базе MIPS есть только одно ядро для выбора — <c>mips-sources</"
"c>. Набор заплаток отличается от других архитектур, в них содержатся "
"множество заплаток, специфичных для архитектуры MIPS."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):61
msgid "Merging kernel sources..."
msgstr "Установка исходных кодов ядра..."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):61
#, no-wrap
msgid ""
"\n"
"# <i>emerge mips-sources</i>\n"
msgstr ""
"\n"
"# <i>emerge mips-sources</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(impo):65
msgid ""
"On the Origin 200/2000, Indigo2 Impact (R10000), Octane/Octane2 and O2, a 64-"
"bit kernel is required to boot these systems. For these machines, you should "
"<c>emerge kgcc64</c> to create a cross-compiler for building 64-bit kernels."
msgstr ""
"Для загрузки Origin 200/2000, Indigo2 Impact (R10000), Octane/Octane2 и O2 "
"требуется 64-битное ядро. Для этих компьютеров вы должны запустить <c>emerge "
"kgcc64</c> для создания кросс-компилятора, способного собирать 64-битные "
"ядра."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):71
msgid "Installing kgcc64"
msgstr "Установка kgcc64"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):71
#, no-wrap
msgid ""
"\n"
"# <i>emerge kgcc64</i>\n"
msgstr ""
"\n"
"# <i>emerge kgcc64</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):75
msgid ""
"When you take a look in <path>/usr/src</path> you should see a symlink "
"called <path>linux</path> pointing to your kernel source. In this case, the "
"installed kernel source points to <c>mips-sources-<keyval id=\"kernel-version"
"\"/></c>. Your version may be different, so keep this in mind."
msgstr ""
"Если вы просмотрите содержимое каталога <path>/usr/src</path>, то увидите "
"символическую ссылку <path>linux</path>, которая указывает на каталог с "
"исходными кодами ядра. В данном случае установленный исходный код ядра "
"указывает на <c>mips-sources-<keyval id=\"kernel-version\"/></c>. Ваша "
"версия может отличаться, имейте это в виду."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):82
msgid "Viewing the kernel source symlink"
msgstr "Просмотр каталога с исходными кодами ядра"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):82
#, no-wrap
msgid ""
"\n"
"# <i>ls -l /usr/src/linux</i>\n"
"lrwxrwxrwx    1 root     root           12 Oct 13 11:04 /usr/src/linux -&gt; linux-<keyval id=\"kernel-version\"></keyval>\n"
msgstr ""
"\n"
"# <i>ls -l /usr/src/linux</i>\n"
"lrwxrwxrwx    1 root     root           12 Oct 13 11:04 /usr/src/linux -&gt; linux-<keyval id=\"kernel-version\"></keyval>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):87
msgid "Now it is time to configure and compile your kernel source."
msgstr "Теперь настала пора настроить и скомпилировать исходники ядра."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):95
msgid "Kernel Compilation &amp; Installation"
msgstr "Компиляция и установка ядра"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):97
msgid "Introduction"
msgstr "Введение"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):100
msgid ""
"Previously, we went through the manual configuration of how to set up the "
"kernel sources. This has become impractical with the number of systems we "
"now support. This section details various sources for sample kernel "
"configurations."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):109
msgid "Using sample configurations in the kernel source"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):112
msgid ""
"Many of the systems supported have sample .configs hiding in amongst the "
"kernel source. Not all systems have configs distributed in this way. Those "
"that do, can be configured using the commands mentioned in the table below."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(th):120
msgid "System"
msgstr "Система"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(th):121
msgid "Configure command"
msgstr "Команда конфигурации"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(ti):124
msgid "Cobalt Servers"
msgstr "Серверы Cobalt"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(ti):128
msgid "Indy, Indigo2 (R4k), Challenge S"
msgstr "Indy, Indigo2 (R4k), Challenge S"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(ti):132
msgid "Origin 200/2000"
msgstr "Origin 200/2000"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(ti):136
msgid "Indigo2 Impact (R10k)"
msgstr "Indigo2 Impact (R10k)"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(ti):142
msgid "O2"
msgstr "O2"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):151
msgid "Using the running kernel config from the installation media"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):154
msgid ""
"All of the Gentoo installation images provide a kernel config option as part "
"of the image itself, accessible as <path>/proc/config.gz</path>. This may be "
"used in many cases. It is best though if your kernel source matches closely, "
"the kernel that is currently running. To extract it, simply run it through "
"<c>zcat</c> as shown below."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):162
msgid "Extracting .config from /proc/config.gz"
msgstr "Распаковка .config из /proc/config.gz"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):162
#, no-wrap
msgid ""
"\n"
"# <i>zcat /proc/config.gz &gt; .config</i>\n"
msgstr ""
"\n"
"# <i>zcat /proc/config.gz &gt; .config</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(impo):166
msgid ""
"This kernel config is set up for a netboot image. That is, it will expect to "
"find a root filesystem image somewhere nearby, either as a directory for "
"initramfs, or a loopback device for initrd. When you run <c>make menuconfig</"
"c> below, don't forget to go into General Setup and disable the options for "
"initramfs."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):178
msgid "The Hardware Compatability Database"
msgstr "База данных совместимости оборудования"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):181
msgid ""
"As an aid to users in finding working settings, a hardware compatability "
"database was set up. This database lists the support for various MIPS "
"devices, and allows users to contribute kernel configurations that are known "
"to work. The address for this site is <uri>http://stuartl.longlandclan.hopto."
"org/gentoo/mips</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):189
msgid ""
"If you find this service useful, you're welcome to contribute your notes "
"and .config files so that others may benefit from your experience. It should "
"be noted however that there is no guarantee that any of the configuration "
"files downloaded from this site will work."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):200
msgid "Customising the configuration for your needs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):203
msgid ""
"Once you have found a configuration, download it into your kernel source "
"directory, and rename it to <path>.config</path>. From there, you can run "
"<c>make oldconfig</c> to bring everything up to date, and allow you to "
"customise the configuration before compiling."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):210
msgid "Configuring the kernel"
msgstr "Конфигурация ядра"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):210
#, no-wrap
msgid ""
"\n"
"# <i>cd /usr/src/linux</i>\n"
"# <i>cp /path/to/example-config .config</i>\n"
"# <i>make oldconfig</i>\n"
"<comment>(Just press ENTER at each prompt to accept the defaults... we'll customise later)</comment>\n"
"# <i>make menuconfig</i>\n"
msgstr ""
"\n"
"# <i>cd /usr/src/linux</i>\n"
"# <i>cp /path/to/example-config .config</i>\n"
"# <i>make oldconfig</i>\n"
"<comment>(Просто нажимайте Ввод при каждом запросе... изменять мы будем позднее)</comment>\n"
"# <i>make menuconfig</i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(impo):218
msgid ""
"In the Kernel Hacking section, there is an option named \"Are You Using A "
"Cross Compiler?\". This tells the kernel Makefiles to prepend \"<c>mips-"
"linux-</c>\" (or <c>mipsel-linux</c> ... etc) to <c>gcc</c> and <c>as</c> "
"commands when compiling the kernel. This should be turned off, even if cross-"
"compiling. Instead, if you do need to call a cross-compiler, specify the "
"prefix using the <c>CROSS_COMPILE</c> variable as shown in the next section."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(impo):227
msgid ""
"There is a known issue with JFS and ALSA on Octane systems where the ALSA "
"fails to work. Given the experimental nature of JFS on MIPS, it is "
"recommended that people avoid using JFS for the time being."
msgstr ""
"Существует известная проблема JFS и ALSA на системах Octane, при которой "
"ALSA перестает работать. Из-за того, что поддержка JFS в MIPS носит "
"экспериментальный характер, рекомендуется не допускать использование JFS."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):236
msgid "Compiling and Installing"
msgstr "Компиляция и установка"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):239
msgid ""
"Now that your kernel is configured, it is time to compile and install it. "
"Exit the configuration and start the compilation process:"
msgstr ""
"Теперь, когда ваше ядро настроено, настало время его скомпилировать и "
"установить. Выйдете из конфигурационного меню и запустите процесс компиляции:"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(note):244
msgid ""
"On 64-bit machines, you need to specify <c>CROSS_COMPILE=mips64-unknown-"
"linux-gnu-</c> (or <c>mips64el-...</c> if on a little-endian system) to use "
"the 64-bit compiler."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):250
msgid "Compiling the kernel"
msgstr "Компиляция ядра"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):250
#, no-wrap
msgid ""
"\n"
"<comment>(Compiling natively)</comment>\n"
"# <i>make vmlinux modules modules_install</i>\n"
"\n"
"<comment>(Cross-compiling on target machine)</comment>\n"
"<comment>(Adjust the mips64-unknown-linux-gnu- accordingly)</comment>\n"
"# <i>make vmlinux modules modules_install CROSS_COMPILE=mips64-unknown-linux-gnu-</i>\n"
"\n"
"<comment>(When compiling on another machine, such as an x86 box, use the)</comment>\n"
"<comment>(following commands to compile the kernel &amp; install modules into)</comment>\n"
"<comment>(a specific directory to be transferred to the target machine.)</comment>\n"
"# <i>make vmlinux modules CROSS_COMPILE=mips64-unknown-linux-gnu-</i>\n"
"# <i>make modules_install INSTALL_MOD_PATH=/somewhere</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(impo):265
msgid ""
"When compiling a 64-bit kernel for the Indy, Indigo2 (R4k), Challenge S and "
"O2, use the <c>vmlinux.32</c> target instead of <c>vmlinux</c>. Otherwise, "
"your machine will not be able to boot. This is to work around the PROM not "
"understanding the ELF64 format."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):272
msgid "Using the vmlinux.32 target"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):272
#, no-wrap
msgid ""
"\n"
"# <i>make vmlinux.32</i>\n"
"<comment>(This will create vmlinux.32 -- which is your final kernel)</comment>\n"
msgstr ""
"\n"
"# <i>make vmlinux.32</i>\n"
"<comment>(Будет создан vmlinux.32 — финальное ядро)</comment>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(p):277
msgid ""
"When the kernel has finished compiling, copy the kernel image to <path>/"
"boot</path>."
msgstr ""
"Когда компиляция ядра закончится, скопируйте образ ядра в <path>/boot</path>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(note):282
msgid ""
"On Cobalt servers, the bootloader will expect to see a compressed kernel "
"image. Remember to <c>gzip -9</c> the file once it is in <path>/boot</path>."
msgstr ""
"Начальный загрузчик серверов Cobalt ожидает сжатый образ ядра. Не забудьте "
"выполнить <c>gzip -9</c> для файла, когда тот окажется в <path>/boot</path>."

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre:caption):287
msgid "Installing the kernel"
msgstr "Установка ядра"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(pre):287
#, no-wrap
msgid ""
"\n"
"# <i>cp vmlinux /boot/kernel-<keyval id=\"kernel-version\"></keyval></i>\n"
"\n"
"<comment>(Cobalt Servers -- Compressing the kernel image)</comment>\n"
"# <i>gzip -9v /boot/kernel-<keyval id=\"kernel-version\"></keyval></i>\n"
msgstr ""
"\n"
"# <i>cp vmlinux /boot/kernel-<keyval id=\"kernel-version\"></keyval></i>\n"
"\n"
"<comment>(Серверы Cobalt — сжатие образа ядра)</comment>\n"
"# <i>gzip -9v /boot/kernel-<keyval id=\"kernel-version\"></keyval></i>\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(title):298
msgid "Kernel Modules"
msgstr "Модули ядра"

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-kernel.xml(None):0
msgid "translator-credits"
msgstr ""
"Азамат Хакимов; переводчик, редактор перевода; azamat.hackimov@gmail.com"
