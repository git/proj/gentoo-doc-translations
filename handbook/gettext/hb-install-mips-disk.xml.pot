msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2011-10-28 22:38+0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(abstract):11
msgid "To be able to install Gentoo, you must create the necessary partitions. This chapter describes how to partition a disk for future usage."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(version):16
msgid "7"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(date):17
msgid "2011-10-17"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):20
msgid "Introduction to Block Devices"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):27
msgid "Partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):30
msgid "Although it is theoretically possible to use a full disk to house your Linux system, this is almost never done in practice. Instead, full disk block devices are split up in smaller, more manageable block devices. These are called <e>partitions</e>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):41
msgid "Designing a Partitioning Scheme"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):43
msgid "How Many and How Big?"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):46
msgid "The number of partitions is highly dependent on your environment. For instance, if you have lots of users, you will most likely want to have your <path>/home</path> separate as it increases security and makes backups easier. If you are installing Gentoo to perform as a mailserver, your <path>/var</path> should be separate as all mails are stored inside <path>/var</path>. A good choice of filesystem will then maximise your performance. Gameservers will have a separate <path>/opt</path> as most gaming servers are installed there. The reason is similar for <path>/home</path>: security and backups. You will definitely want to keep <path>/usr</path> big: not only will it contain the majority of applications, the Portage tree alone takes around 500 Mbyte excluding the various sources that are stored in it."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):60
msgid "As you can see, it very much depends on what you want to achieve. Separate partitions or volumes have the following advantages:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):66
msgid "You can choose the best performing filesystem for each partition or volume"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):69
msgid "Your entire system cannot run out of free space if one defunct tool is continuously writing files to a partition or volume"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):73
msgid "If necessary, file system checks are reduced in time, as multiple checks can be done in parallel (although this advantage is more with multiple disks than it is with multiple partitions)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):78
msgid "Security can be enhanced by mounting some partitions or volumes read-only, nosuid (setuid bits are ignored), noexec (executable bits are ignored) etc."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):84
msgid "However, multiple partitions have disadvantages as well. If not configured properly, you will have a system with lots of free space on one partition and none on another. Another nuisance is that separate partitions - especially for important mountpoints like <path>/usr</path> or <path>/var</path> - often require the administrator to boot with an initramfs to mount the partition before other boot scripts start. This isn't always the case though, so YMMV."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):93
msgid "There is also a 15-partition limit for SCSI and SATA."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):101
msgid "Using fdisk on MIPS to Partition your Disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):103
msgid "SGI Machines: Creating an SGI Disk Label"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):106
msgid "All disks in an SGI System require an <e>SGI Disk Label</e>, which serves a similar function as Sun &amp; MS-DOS disklabels -- It stores information about the disk partitions. Creating a new SGI Disk Label will create two special partitions on the disk:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):114
msgid "<e>SGI Volume Header</e> (9th partition): This partition is important. It is where the bootloader will reside, and in some cases, it will also contain the kernel images."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):119
msgid "<e>SGI Volume</e> (11th partition): This partition is similar in purpose to the Sun Disklabel's third partition of \"Whole Disk\". This partition spans the entire disk, and should be left untouched. It serves no special purpose other than to assist the PROM in some undocumented fashion (or it is used by IRIX in some way)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(warn):128
msgid "The SGI Volume Header <e>must</e> begin at cylinder 0. Failure to do so means you won't be able to boot from the disk."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):133
msgid "The following is an example excerpt from an <c>fdisk</c> session. Read and tailor it to your needs..."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre:caption):138
msgid "Creating an SGI Disklabel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre):138
#, no-wrap
msgid "\n# <i>fdisk /dev/sda</i>\n\nCommand (m for help): <i>x</i>\n\nExpert command (m for help): <i>m</i>\nCommand action\n   b   move beginning of data in a partition\n   c   change number of cylinders\n   d   print the raw data in the partition table\n   e   list extended partitions\n   f   fix partition order\n   g   create an IRIX (SGI) partition table\n   h   change number of heads\n   m   print this menu\n   p   print the partition table\n   q   quit without saving changes\n   r   return to main menu\n   s   change number of sectors/track\n   v   verify the partition table\n   w   write table to disk and exit\n\nExpert command (m for help): <i>g</i>\nBuilding a new SGI disklabel. Changes will remain in memory only,\nuntil you decide to write them. After that, of course, the previous\ncontent will be irrecoverably lost.\n\nExpert command (m for help): <i>r</i>\n\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda (SGI disk label): 64 heads, 32 sectors, 17482 cylinders\nUnits = cylinders of 2048 * 512 bytes\n\n----- partitions -----\nPt#     Device  Info     Start       End   Sectors  Id  System\n 9:  /dev/sda1               0         4     10240   0  SGI volhdr\n11:  /dev/sda2               0     17481  35803136   6  SGI volume\n----- Bootinfo -----\nBootfile: /unix\n----- Directory Entries -----\n\nCommand (m for help):\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(note):183
msgid "If your disk already has an existing SGI Disklabel, then fdisk will not allow the creation of a new label. There are two ways around this. One is to create a Sun or MS-DOS disklabel, write the changes to disk, and restart fdisk. The second is to overwrite the partition table with null data via the following command: <c>dd if=/dev/zero of=/dev/sda bs=512 count=1</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):194
msgid "Getting the SGI Volume Header to just the right size"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(impo):197
msgid "This step is often needed, due to a bug in <c>fdisk</c>. For some reason, the volume header isn't created correctly, the end result being it starts and ends on cylinder 0. This prevents multiple partitions from being created. To get around this issue... read on."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):204
msgid "Now that an SGI Disklabel is created, partitions may now be defined. In the above example, there are already two partitions defined for you. These are the special partitions mentioned above and should not normally be altered. However, for installing Gentoo, we'll need to load a bootloader, and possibly multiple kernel images (depending on system type) directly into the volume header. The volume header itself can hold up to <e>eight</e> images of any size, with each image allowed eight-character names."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):214
msgid "The process of making the volume header larger isn't exactly straight-forward; there's a bit of a trick to it. One cannot simply delete and re-add the volume header due to odd fdisk behavior. In the example provided below, we'll create a 50MB Volume header in conjunction with a 50MB /boot partition. The actual layout of your disk may vary, but this is for illustrative purposes only."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre:caption):222
msgid "Resizing the SGI Volume Header correctly"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre):222
#, no-wrap
msgid "\nCommand (m for help): <i>n</i>\nPartition number (1-16): <i>1</i>\nFirst cylinder (5-8682, default 5): <i>51</i>\n Last cylinder (51-8682, default 8682): <i>101</i>\n\n<comment>(Notice how fdisk only allows Partition #1 to be re-created starting at a     )\n(minimum of cylinder 5?  Had you attempted to delete &amp; re-create the SGI      )\n(Volume Header this way, this is the same issue you would have encountered.   )\n(In our example, we want /boot to be 50MB, so we start it at cylinder 51 (the )\n(Volume Header needs to start at cylinder 0, remember?), and set its ending   )\n(cylinder to 101, which will roughly be 50MB (+/- 1-5MB).                     )</comment>\n\nCommand (m for help): <i>d</i>\nPartition number (1-16): <i>9</i>\n\n<comment>(Delete Partition #9 (SGI Volume Header))</comment>\n\nCommand (m for help): <i>n</i>\nPartition number (1-16): <i>9</i>\nFirst cylinder (0-50, default 0): <i>0</i>\n Last cylinder (0-50, default 50): <i>50</i>\n\n<comment>(Re-Create Partition #9, ending just before Partition #1)</comment>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):248
msgid "If you're unsure how to use <c>fdisk</c> have a look down further at the instructions for partitioning on Cobalts. The concepts are exactly the same -- just remember to leave the volume header and whole disk partitions alone."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):254
msgid "Once this is done, you are safe to create the rest of your partitions as you see fit. After all your partitions are laid out, make sure you set the partition ID of your swap partition to <c>82</c>, which is Linux Swap. By default, it will be <c>83</c>, Linux Native."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):261
msgid "Now that your partitions are created, you can continue with <uri link=\"#filesystems\">Creating Filesystems</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):270
msgid "Cobalt Machines: Partitioning your drive"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):273
msgid "On Cobalt machines, the BOOTROM expects to see a MS-DOS MBR, so partitioning the drive is relatively straightforward -- in fact, it's done the same way as you'd do for an Intel x86 machine. <e>However</e> there are some things you need to bear in mind."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):281
msgid "Cobalt firmware will expect <path>/dev/sda1</path> to be a Linux partition formatted <e>EXT2 Revision 0</e>. <e>EXT2 Revision 1 partitions will NOT WORK!</e> (The Cobalt BOOTROM only understands EXT2r0)"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):286
msgid "The above said partition must contain a gzipped ELF image, <path>vmlinux.gz</path> in the root of that partition, which it loads as the kernel"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):293
msgid "For that reason, I recommend creating a ~20MB <path>/boot</path> partition formatted EXT2r0 upon which you can install CoLo &amp; your kernels. This allows you to run a modern filesystem (EXT3 or ReiserFS) for your root filesystem."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):300
msgid "I will assume you have created <path>/dev/sda1</path> to mount later as a <path>/boot</path> partition. If you wish to make this <path>/</path>, you'll need to keep the PROM's expectations in mind."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):306
msgid "So, continuing on... To create the partitions you type <c>fdisk /dev/sda</c> at the prompt. The main commands you need to know are these:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):312
msgid "<c>o</c>: Wipe out old partition table, starting with an empty MS-DOS partition table"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):316
msgid "<c>n</c>: New Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):322
msgid "Use type <c>82</c> for Linux Swap, <c>83</c> for Linux FS"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):319
msgid "<c>t</c>: Change Partition Type <placeholder-1/>"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):325
msgid "<c>d</c>: Delete a partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):328
msgid "<c>p</c>: Display (print) Partition Table"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):331
msgid "<c>q</c>: Quit -- leaving old partition table as is."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(li):334
msgid "<c>w</c>: Quit -- writing partition table in the process."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre:caption):339
msgid "Partitioning the disk"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre):339
#, no-wrap
msgid "\n# <i>fdisk /dev/sda</i>\n\nThe number of cylinders for this disk is set to 19870.\nThere is nothing wrong with that, but this is larger than 1024,\nand could in certain setups cause problems with:\n1) software that runs at boot time (e.g., old versions of LILO)\n2) booting and partitioning software from other OSs\n   (e.g., DOS FDISK, OS/2 FDISK)\n\n<comment>(Start by clearing out any existing partitions)</comment>\nCommand (m for help): <i>o</i>\nBuilding a new DOS disklabel. Changes will remain in memory only,\nuntil you decide to write them. After that, of course, the previous\ncontent won't be recoverable.\n\n\nThe number of cylinders for this disk is set to 19870.\nThere is nothing wrong with that, but this is larger than 1024,\nand could in certain setups cause problems with:\n1) software that runs at boot time (e.g., old versions of LILO)\n2) booting and partitioning software from other OSs\n   (e.g., DOS FDISK, OS/2 FDISK)\nWarning: invalid flag 0x0000 of partition table 4 will be corrected by w(rite)\n\n<comment>(You can now verify the partition table is empty using the 'p' command)</comment>\n\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 10.2 GB, 10254827520 bytes\n16 heads, 63 sectors/track, 19870 cylinders\nUnits = cylinders of 1008 * 512 = 516096 bytes\n\n   Device Boot      Start         End      Blocks   Id  System\n\n<comment>(Create the /boot partition)</comment>\n\nCommand (m for help): <i>n</i>\nCommand action\n   e   extended\n   p   primary partition (1-4)\n<i>p</i>\nPartition number (1-4): <i>1</i>\n\n<comment>(Just press ENTER here to accept the default)</comment>\n\nFirst cylinder (1-19870, default 1):\nLast cylinder or +size or +sizeM or +sizeK (1-19870, default 19870): <i>+20M</i>\n\n<comment>(and now if we type 'p' again, we should see the new partition)</comment>\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 10.2 GB, 10254827520 bytes\n16 heads, 63 sectors/track, 19870 cylinders\nUnits = cylinders of 1008 * 512 = 516096 bytes\n\n   Device Boot      Start         End      Blocks   Id  System\n/dev/sda1               1          40       20128+  83  Linux\n\n<comment>(The rest, I prefer to put in an extended partition, so I'll create that)</comment>\n\nCommand (m for help): <i>n</i>\nCommand action\n   e   extended\n   p   primary partition (1-4)\n<i>e</i>\nPartition number (1-4): <i>2</i>\n\n<comment>(Again, the default is fine, just press ENTER.)</comment>\n\nFirst cylinder (41-19870, default 41):\nUsing default value 41\n\n<comment>(We want to use the whole disk here, so just press ENTER again)</comment>\nLast cylinder or +size or +sizeM or +sizeK (41-19870, default 19870):\nUsing default value 19870\n\n<comment>(Now, the / partition -- I use separate partitions for /usr, /var,\netc... so / can be small. Adjust as per your preference.)</comment>\n\nCommand (m for help): <i>n</i>\nCommand action\n   l   logical (5 or over)\n   p   primary partition (1-4)\n<i>l</i>\nFirst cylinder (41-19870, default 41):<i>&lt;Press ENTER&gt;</i>\nUsing default value 41\nLast cylinder or +size or +sizeM or +sizeK (41-19870, default 19870): <i>+500M</i>\n\n<comment>(... and similar for any other partitions ...)</comment>\n\n<comment>(Last but not least, the swap space. I recommend at least 250MB swap,\npreferrably 1GB)</comment>\n\nCommand (m for help): <i>n</i>\nCommand action\n   l   logical (5 or over)\n   p   primary partition (1-4)\n<i>l</i>\nFirst cylinder (17294-19870, default 17294): <i>&lt;Press ENTER&gt;</i>\nUsing default value 17294\nLast cylinder or +size or +sizeM or +sizeK (1011-19870, default 19870): <i>&lt;Press ENTER&gt;</i>\nUsing default value 19870\n\n<comment>(Now, if we check our partition table, everything should mostly be ship\nshape except for one thing...)</comment>\n\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 10.2 GB, 10254827520 bytes\n16 heads, 63 sectors/track, 19870 cylinders\nUnits = cylinders of 1008 * 512 = 516096 bytes\n\nDevice Boot      Start         End      Blocks      ID  System\n/dev/sda1               1          21       10552+  83  Linux\n/dev/sda2              22       19870    10003896    5  Extended\n/dev/sda5              22        1037      512032+  83  Linux\n/dev/sda6            1038        5101     2048224+  83  Linux\n/dev/sda7            5102        9165     2048224+  83  Linux\n/dev/sda8            9166       13229     2048224+  83  Linux\n/dev/sda9           13230       17293     2048224+  83  Linux\n/dev/sda10          17294       19870     1298776+  83  Linux\n\n<comment>(Notice how #10, our swap partition is still type 83?)</comment>\n\nCommand (m for help): <i>t</i>\nPartition number (1-10): <i>10</i>\nHex code (type L to list codes): <i>82</i>\nChanged system type of partition 10 to 82 (Linux swap)\n\n<comment>(That should fix it... just to verify...)</comment>\n\nCommand (m for help): <i>p</i>\n\nDisk /dev/sda: 10.2 GB, 10254827520 bytes\n16 heads, 63 sectors/track, 19870 cylinders\nUnits = cylinders of 1008 * 512 = 516096 bytes\n\nDevice Boot      Start         End      Blocks      ID  System\n/dev/sda1               1          21       10552+  83  Linux\n/dev/sda2              22       19870    10003896    5  Extended\n/dev/sda5              22        1037      512032+  83  Linux\n/dev/sda6            1038        5101     2048224+  83  Linux\n/dev/sda7            5102        9165     2048224+  83  Linux\n/dev/sda8            9166       13229     2048224+  83  Linux\n/dev/sda9           13230       17293     2048224+  83  Linux\n/dev/sda10          17294       19870     1298776+  82  Linux Swap\n\n<comment>(Now, we write out the new partition table.)</comment>\n\nCommand (m for help): <i>w</i>\nThe partition table has been altered!\n\nCalling ioctl() to re-read partition table.\nSyncing disks.\n\n#\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):498
msgid "And that's all there is to it. You should now be right to proceed onto the next stage: <uri link=\"#filesystems\">Creating Filesystems</uri>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):508
msgid "Creating Filesystems"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):510
msgid "Introduction"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):513
msgid "Now that your partitions are created, it is time to place a filesystem on them. If you don't care about what filesystem to choose and are happy with what we use as default in this handbook, continue with <uri link=\"#filesystems-apply\">Applying a Filesystem to a Partition</uri>. Otherwise read on to learn about the available filesystems..."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):529
msgid "Applying a Filesystem to a Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):532
msgid "To create a filesystem on a partition or volume, there are tools available for each possible filesystem:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(th):539
msgid "Filesystem"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(th):540
msgid "Creation Command"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(ti):543
msgid "ext2"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(ti):547
msgid "ext3"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(ti):551
msgid "ext4"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(ti):555
msgid "reiserfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(ti):559
msgid "xfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(ti):563
msgid "jfs"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):568
msgid "For instance, to have the boot partition (<path>/dev/sda1</path> in our example) in ext2 and the root partition (<path>/dev/sda3</path> in our example) in ext3, you would use:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre:caption):574
msgid "Applying a filesystem on a partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre):574
#, no-wrap
msgid "\n# <i>mkfs.ext2 /dev/sda1</i>\n# <i>mkfs.ext3 /dev/sda3</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):579
msgid "Now create the filesystems on your newly created partitions (or logical volumes)."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(warn):584
msgid "If you're installing on a Cobalt server, remember <path>/dev/sda1</path> MUST be of type <e>EXT2 revision 0</e>; Anything else (e.g. EXT2 revision 1, EXT3, ReiserFS, XFS, JFS and others) <e>WILL NOT WORK!</e> You can format the partition using the command: <c>mkfs.ext2 -r 0 /dev/sda1</c>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):594
msgid "Activating the Swap Partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):597
msgid "<c>mkswap</c> is the command that is used to create and initialize swap partitions:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre:caption):602
msgid "Creating a Swap signature"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre):602
#, no-wrap
msgid "\n# <i>mkswap /dev/sda2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):606
msgid "To activate the swap partition, use <c>swapon</c>:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre:caption):610
msgid "Activating the swap partition"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre):610
#, no-wrap
msgid "\n# <i>swapon /dev/sda2</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):614
msgid "Create and activate the swap with the commands mentioned above."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(title):622
msgid "Mounting"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):625
msgid "Now that your partitions are initialized and are housing a filesystem, it is time to mount those partitions. Use the <c>mount</c> command. Don't forget to create the necessary mount directories for every partition you created. As an example we mount the root and boot partition:"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre:caption):632
msgid "Mounting partitions"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(pre):632
#, no-wrap
msgid "\n# <i>mount /dev/sda3 /mnt/gentoo</i>\n# <i>mkdir /mnt/gentoo/boot</i>\n# <i>mount /dev/sda1 /mnt/gentoo/boot</i>\n"
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(note):638
msgid "If you want your <path>/tmp</path> to reside on a separate partition, be sure to change its permissions after mounting: <c>chmod 1777 /mnt/gentoo/tmp</c>. This also holds for <path>/var/tmp</path>."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):644
msgid "We will also have to mount the proc filesystem (a virtual interface with the kernel) on <path>/proc</path>. But first we will need to place our files on the partitions."
msgstr ""

#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(p):650
msgid "Continue with <uri link=\"?part=1&amp;chap=5\">Installing the Gentoo Installation Files</uri>."
msgstr ""

#. Place here names of translator, one per line. Format should be NAME; ROLE; E-MAIL
#: ../../gentoo/xml/htdocs/doc/en/handbook//hb-install-mips-disk.xml(None):0
msgid "translator-credits"
msgstr ""

